#!/usr/bin/env python3

# This python script ssh's into hay and executes some simple bash scripts
# that query the database for a count of new members by quarter. It tranlates
# these numbers into charts saves as png files.

from matplotlib import pyplot as plt
from matplotlib import style
from matplotlib import dates as mpl_dates
from datetime import datetime, timedelta
import pandas as pd
import tempfile
import subprocess

from numpy import genfromtxt

def save_chart(member_filter = None):
    script = None
    output_file = None

    if member_filter == None:
        script = 'bin/plot-new-members-count-by-month'
        output_file = 'all-members.png'
        legend = 'All members'
    elif member_filter == 'individuals':
        script = 'bin/plot-new-members-count-by-month-individuals'
        output_file = 'individual-members.png'
        legend = 'All individual members'
    elif member_filter == 'organizations':
        script = 'bin/plot-new-members-count-by-month-organizations'
        output_file = 'organization-members.png'
        legend = 'All organization members'
    elif member_filter == 'usd':
        script = 'bin/plot-new-members-count-by-month-usd'
        output_file = 'usd-members.png'
        legend = 'All USD members'
    elif member_filter == 'mxn':
        script = 'bin/plot-new-members-count-by-month-mxn'
        output_file = 'mxn-members.png'
        legend = 'All MXN members'
    else:
        raise RuntimeError("Unknown member_filter")

    import_file = tempfile.NamedTemporaryFile(mode="w+t", encoding="utf-8")
    args = [ '/usr/bin/ssh', "mayfirst@hay.mayfirst.org", script ]
    subprocess.run(args,stdout = import_file, text=True)

    data = pd.read_csv(import_file.name, sep = "\t")

    data['date'] = pd.to_datetime(data['date'])
    data.sort_values('date', inplace=True)

    #data = genfromtxt('data.csv',delimiter="\t")

    start_date = data['date']
    member_count = data['count']
    plt.plot_date(start_date, member_count, linestyle='solid')
    plt.legend([legend])

    plt.gcf().autofmt_xdate()
    date_format = mpl_dates.DateFormatter('%d-%m-%Y')
    plt.gca().xaxis.set_major_formatter(date_format)

    plt.title('New Members By Quarter')
    plt.ylabel('Count of New Members per quarter')
    plt.xlabel('Date')

    #plt.show()

f = plt.figure()
f.set_figwidth(18)
f.set_figheight(8)

save_chart()
save_chart('individuals')
save_chart('organizations')
plt.legend(["All members", "Individual Members", "Organizational Members"])
plt.savefig("members-type.png")

plt.clf()

save_chart()
save_chart('usd')
save_chart('mxn')
plt.legend(["All members", "USD Members", "MXN Members"])
plt.savefig("members-currency.png")
