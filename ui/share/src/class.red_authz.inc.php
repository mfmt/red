<?php

/**
 * red authorization class. Determines if a given user is an admin
 * and determines which items the user should have access to
 **/

class red_authz {
  var $_user_name;
  var $_errors = array();
  var $_is_admin = null;
  var $_is_disabled = null;

  // Whether the user can use the API.
  var $_is_api_allowed = null;

  function get_user_name() {
    return $this->_user_name;
  }

  function set_user_name($user_name) {
    $this->_user_name = $user_name;
  }

  function set_is_admin($val = null) {
    if(!is_null($val)) {
      $this->_is_admin = $val;
      return true;
    }
    $user_name = $this->get_user_name();
    if(empty($user_name)) {
      $this->_set_error(red_t("Please set username before setting is_admin."));
      return false;
    }

    $sql = "SELECT sitewide_admin_id FROM red_sitewide_admin WHERE ".
      "user_name = @user_name";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    if(red_sql_num_rows($result) == 1) {
      $this->_is_admin = true;
      return true;
    }
    $this->_is_admin = false;
    return true;
  }

  /**
   * Get membership level for id and entity
   *
   * Given an id (e.g. item_id, hosting_order_id, or member_id) and
   * an entity (e.g. item, hosting_order or member), return the
   * the membership level (basic or standard).
   */
  static public function get_membership_level_for_id($id, $entity) {
    $sql = "SELECT member_benefits_level FROM red_member ";
    $joins = [];
    $where = '';
    if ($entity == 'member') {
      $where = " WHERE member_id = #id";
    }
    elseif ($entity == 'hosting_order') {
      $joins[] = "JOIN red_hosting_order USING (member_id)";
      $where = " WHERE hosting_order_id = #id";
    }
    elseif ($entity == 'item') {
      $joins[] = "JOIN red_hosting_order USING (member_id)";
      $joins[] = "JOIN red_item USING (item_id)";
      $where = " WHERE item_id = #id";
    }
    else {
      throw new \Red_Exception("Unknown entity when getting membership level: $entity.");
    }
    $params = ['#id' => $id];
    $sql .= implode(' ', $joins) . $where;
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  // Given a username, get the user's membership level.
  function get_membership_level_for_user($user_name = NULL) {
    if(is_null($user_name)) {
      $user_name = $this->get_user_name();
    }

    if(empty($user_name)) return NULL;

    $sql = "SELECT member_benefits_level FROM red_member
      JOIN red_hosting_order USING(member_id)
      JOIN red_item USING(hosting_order_id)
      JOIN red_item_user_account USING(item_id)
      WHERE user_account_login = @user_name AND
      item_status = 'active' AND hosting_order_status = 'active'
      AND member_status = 'active'";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    if(red_sql_num_rows($result) == 0) {
      return NULL;
    }

    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  // API access is only allowed if your membership is standard
  // or extra (not basic).
  function set_is_api_allowed($val = null) {
    if(!is_null($val)) {
      $this->_is_api_allowed = $val;
      return true;
    }
    $user_name = $this->get_user_name();
    if(empty($user_name)) {
      $this->_set_error(red_t("Please set username before setting is_api_allowed."));
      return false;
    }

    $level = $this->get_membership_level_for_user($user_name);
    if($level == 'standard' || $level == 'extra') {
      $this->_is_api_allowed = TRUE;
      return true;
    }
    $this->_is_api_allowed = FALSE;
    return true;
  }

  function set_is_disabled($val = null) {
    if(!is_null($val)) {
      $this->_is_disabled = $val;
      return true;
    }
    $user_name = $this->get_user_name();
    if(empty($user_name)) {
      $this->_set_error(red_t("Please set username before setting is_disabled."));
      return false;
    }

    $sql = "SELECT item_status FROM red_item JOIN red_item_user_account ".
      "USING(item_id) WHERE user_account_login = @user_name";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    if(red_sql_num_rows($result) == 0) {
      // this shouldn't happen. The login isn't technically disabled, but since
      // it doesn't exist, set to disabled to be safe
      $this->_is_disabled = true;
      return true;
    }
    $row = red_sql_fetch_row($result);
    if($row[0] == 'disabled' || $row[0] == 'pending-disable') {
      $this->_is_disabled = true;
      return true;
    }
    $this->_is_disabled = false;
    return true;
  }

  function get_user_name_item_id() {
    $user_name = $this->get_user_name();
    $sql = "SELECT red_item.item_id FROM red_item JOIN red_item_user_account 
      USING(item_id) WHERE item_status = 'active' AND user_account_login = 
      @user_name";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    $row = red_sql_fetch_row($result);
    if(isset($row[0])) {
      return $row[0];
    }
  }

  function _set_error($v) {
    $this->_errors[] = $v;
  }

  function get_errors() {
    return $this->_errors;
  }

  function is_admin() {
    if(is_null($this->_is_admin)) {
      if(!$this->set_is_admin()) return false;
    }
    return $this->_is_admin;
  }

  function is_disabled() {
    if(is_null($this->_is_disabled)) {
      // if we fail to call set_is_disabled(), then default
      // to closed
      if(!$this->set_is_disabled()) return true;
    }
    return $this->_is_disabled;
  }

  function is_api_allowed() {
    if(is_null($this->_is_api_allowed)) {
      // if we fail to call set_is_disabled(), then default
      // to closed
      if(!$this->set_is_api_allowed()) return FALSE;
    }
    return $this->_is_api_allowed;
  }

  // not yet implemented...
  function check_access($object, $id = null, $parent_id = null ) {
    if($this->is_admin()) return true;
    return false;
  }
    
  function get_member_ids() {
    $user_name = $this->get_user_name();
    $sql = "SELECT member_id FROM red_map_user_member WHERE login = @user_name AND status = 'active'";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  function get_hosting_order_ids() {
    // Add access via hosting orders.
		$user_name = $this->get_user_name();
    $sql = "SELECT hosting_order_id FROM red_item_hosting_order_access ".
      "JOIN red_item USING(item_id) WHERE hosting_order_access_login = 
      @user_name AND item_status = 'active' ";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    // Add access via memberships.
    $sql = "SELECT hosting_order_id FROM red_map_user_member JOIN " .
      "red_member USING(member_id) JOIN red_hosting_order USING(member_id) " . 
      "WHERE status = 'active' && login = @user_name AND 
      member_status = 'active' AND hosting_order_status = 'active'";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  function rewrite_sql($query, $object) {
    if($this->is_admin()) return '';

    $joins = array();
    $extra_wheres = array();

    // Check for member level access
    $member_ids = $this->get_member_ids(); 
    if (count($member_ids) > 0) {
      if ($object == 'item') {
        $joins[] = " JOIN red_hosting_order USING(hosting_order_id) ";
      }
      $joins[] = " JOIN red_member ON red_member.member_id = red_hosting_order.member_id ";
      foreach ($member_ids as $member_id) {
        $extra_wheres[] = "red_member.member_id = " . intval($member_id); 
      }
    }
    if ($object == 'item') {
      $hosting_order_ids = $this->get_hosting_order_ids(); 
      if (count($hosting_order_ids) > 0) {
        $joins[] = " JOIN red_hosting_order USING(hosting_order_id) ";
        foreach ($hosting_order_ids as $hosting_order_id) {
          $extra_wheres[] = "red_item.hosting_order_id = " . intval($hosting_order_id); 
        }
      }
    }
    if(count($extra_wheres) == 0) return $sql;

    preg_match('/^(SELECT .*)(FROM .*)(WHERE .*)/', $query, $matches);
    $select = $matches[1];
    $from = $matches[2];
    $where = $matches[3];

    $joins = array_unique($joins);
    return $select . $from . implode(' ', $joins) . $where . ' AND (' . implode(' OR ', $extra_wheres) . ')';
  }

  /**
   * Check insert perms
   *
   * Check if this user is allowed to insert this kind of record.
   * Parent id here refers to the record above the given record. If it's
   * an item, it means the hosting order, if it's a hosting order it means
   * the member_id, etc.
   */
  function check_insert_perms($service_name, $parent_id, $id = NULL) {
    // If parent id is not specified, lock them out.
    if (empty($parent_id)) {
      return FALSE;
    }

    // Check perms for this service_name.
    $sql = "SELECT service_id, service_basic_access, service_standard_access, service_item
      FROM red_service WHERE service_name = @service_name";
    $result = red_sql_query($sql, ['@service_name' => $service_name]);
    $row = red_sql_fetch_row($result);
    if (!$row) {
      throw new RedException("Failed to find service: {$service_name} in red_service table.");
    }
    $service_id = $row[0];
    $basic_access = $row[1];
    $standard_access = $row[2];
    $service_item = $row[3];

    // Check level of the membership in question, not the logged in user.
    if ($service_item == 0) {
      // A member level item.
      $level = self::get_membership_level_for_id($parent_id, 'member');
    }
    else {
      // A hosting order level item.
      $level = self::get_membership_level_for_id($parent_id, 'hosting_order');
    }

    // Only allow certain service_names for basic level members..
    if ($level == 'basic') {
      $check_access = $basic_access;
    }
    else {
      $check_access = $standard_access;
    }
    if ($check_access == 'none' or $check_access == 'view-only') {
      return FALSE;
    }

    // This is a member level service, not a hosting order service.
    if ($service_item == 0) {
      $member_ids = $this->get_member_ids();
      if (in_array($parent_id, $member_ids) || $this->is_admin()) {
        return TRUE;
      }
    }

    if ($service_item == 1) {
      $hosting_order_ids = $this->get_hosting_order_ids();
      if (in_array($parent_id, $hosting_order_ids) || $this->is_admin()) {
        if ($check_access == 'single') {
          $sql = "SELECT COUNT(*) FROM red_item
            WHERE
              hosting_order_id = #hosting_order_id AND
              service_id = #service_id AND
              item_status != 'deleted'
          ";
          $params = [
            '#hosting_order_id' => $parent_id,
            '#service_id' => $service_id,
          ];
          if ($id) {
            $sql .= ' AND item_id != #item_id';
            $params['#item_id'] = $id;
          }
          $result = red_sql_query($sql, $params);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            // Only allowed one item, already have one item.
            return FALSE;
          }
        }
        return TRUE;
      }
    }
    return FALSE;
  }
}

?>
