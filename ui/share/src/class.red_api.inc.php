<?php

class red_api {

  var $_values = array();
  var $_errors = array();
  var $_output = array();
  var $_construction_options = array();
  var $_log_level = 0;
  var $_params = array(
    'where' => array(),
    'set' => array(),
    'sub' => array(),
  );

  // limit number of records returned on select
  // to avoid memory problems
  var $_max_records = 50;

  // text or json
  var $_output_format;

  // track if we are accessing via cli, in which case
  // we skip auth checks.
  var $cli = false;

  var $_authz = null;

  // Some connections provide their own username and password
  // so we initialize and use an alternate sql_resource.
  private $node_sql_resource = NULL;

  public function set_cli() {
    $this->cli = TRUE;
  }
  function set_error($errors) {
    $this->_errors[] = $errors;
    if (!is_array($errors)) {
      $errors = [ $errors ];
    }
    foreach($errors as $error) {
      red_log($error);
    }
  }
  function set_values($value) {
    $this->_values[] = $value;
  }
  
  function set_log_level($value) {
    $this->_log_level = $value;
  }

  function log($message, $level) {
    if($level <= $this->_log_level) {
      print "$message\n";
    }
  }

  function init() {
    $this->_construction_options = array(
      'mode' => 'ui',
    );
    return true;
  }

  function get_output() {
    return $this->_output;
  }

  function set_output_format($format) {
    $this->_output_format = $format;
  }

  function authenticate() {
    if (array_key_exists('action', $this->_params) && $this->_params['action'] == 'mx_verify') {
      // mx verification doesn't require authentication. We only act if the key is correct
      // or the domain exists.
      if (!array_key_exists('key', $this->_params['where']) && !array_key_exists('domain', $this->_params['where'])) {
        return FALSE;
      }
      return TRUE;
    }   

    // set auth flag - this will indicate that we 
    // should both authenticate and authorize
    if (!array_key_exists('user_name', $this->_params)) {
      $this->set_error(red_t("Username not provided."));
      return false;
    }
    if (!array_key_exists('user_pass', $this->_params)) {
      $this->set_error(red_t("Password not provided."));
      return false;
    }

    // Remote actions are sent in via our nodes, which don't have a
    // normal username and password. Instead, they have a database user
    // and database password. So, instead of using the traditional method
    // for authentication, we will instead re-create our sql resource
    // with the credentials they provide and fail if the login fails.
    if (array_key_exists('action', $this->_params) && $this->_params['action'] == 'remote_action') {
      global $globals;
      // Make a copy.
      $config = $globals['config'];
      // Overwrite with passed in info.
      $config['db_user'] = $this->_params['user_name'];
      $config['db_pass'] = $this->_params['user_pass'];
      $config['mysql_cert_file'] = '/etc/mysql/server-cert.pem';
      $config['db_host'] = '127.0.0.1';
      if(!$this->node_sql_resource = red_db::init_db($config)) {
        $this->set_error("Failed authentication from remote action user: {$config['db_user']}.");
        return false;
      }
      return TRUE;
    }
    $auth = new red_auth();
    $user_name = $this->_params['user_name'];
    $user_pass = $this->_params['user_pass'];
    if(!$auth->valid_user($user_name, $user_pass)) {
      $this->set_error("Failed authentication from {$user_name}.");
      return false;
    }
    
    $this->_authz = new red_authz();
    $this->_authz->set_user_name($user_name);
    // API usage is not permitted from disabled user accounts
    if($this->_authz->is_disabled()) {
      $this->set_error("Your user account is disabled, please contact support.");
      return false;
    }
    // API usage is not permitted from basic memberships 
    if(!$this->_authz->is_api_allowed()) {
      $this->set_error("Your user account is not allowed to use the API.");
      return false;
    }
    // Grant site access so they can ssh/sftp for the next 24 hours.
    $auth->grant_site_access($user_name);
    return true;
  }

  function web_dns_to_alias() {
    global $globals;
    $active_web_proxy = $globals['config']['active_web_proxy'];
    $web_conf_item_id = $this->_params['where']['web_conf_item_id'] ?? NULL;
    $ip = $this->_params['where']['ip'] ?? NULL;
    if (!$web_conf_item_id) {
      $this->set_error("Failed to find web_conf_item_id in where clause.");
      return FALSE;
    }
    if (!$ip) {
      $this->set_error("Failed to find ip in where clause.");
      return FALSE;
    }
    $domains = [];
    $sql = "SELECT web_conf_domain_names FROM red_item_web_conf WHERE item_id
      = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $web_conf_item_id]);
    $num_results = red_sql_num_rows($result);
    if ($num_results != 1) {
      $this->set_error("Failed to find web conf record using item id {$web_conf_item_id}.");
      return FALSE;
    }
    $row = red_sql_fetch_row($result);
    $raw_domains_str = $row[0] ?? '';
    $raw_domains = explode(' ', $raw_domains_str);
    foreach ($raw_domains as $d) {
      $domains[] = trim($d);
    }
    if (count($domains) == 0) {
      $this->set_error("No domains names configured for item {$web_conf_item_id}.");
      return FALSE;
    }
    foreach ($domains as $d) {
      // Ensure there is an A record pointing to the old IP address.
      $sql = "SELECT * FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_status IN ('active', 'disabled') AND dns_type = 'a' AND dns_fqdn = @d AND
        dns_ip = @ip";
      $d_result = red_sql_query($sql, ['@d' => $d, '@ip' => $ip]);
      $num_results = red_sql_num_rows($d_result);
      if ($num_results > 1) {
        $this->set_error("Received unexpected number of results ($num_results) for {$d}.");
        return FALSE;
      }
      else if ($num_results == 0) {
        // This is fine. Maybe we already have an ALIAS for it. Or maybe the DNS record is
        // pointing some where else.
        red_log("No results: $sql for $d and $ip\n");
        continue;
      }
      $d_row = red_sql_fetch_assoc($d_result);
      $item_id = $d_row['item_id'] ?? NULL;
      if ($item_id) {
        $co = ['mode' => 'ui'];
        $co['rs'] = $d_row;
        $item = red_item::get_red_object($co);
        $item->set_dns_type('alias');
        $item->set_dns_ip('');
        $item->set_dns_server_name("{$active_web_proxy}.webproxy.mayfirst.org");
        if ($d_row['item_status'] == 'disabled') {
          $item->set_disable_flag();
        }
        if (!$item->validate()) {
          $this->set_error("Failed to validate: " . $item->get_errors_as_string());
          return FALSE;
        }
        if (!$item->commit_to_db()) {
          $this->set_error("Failed to commit to db: " . $item->get_errors_as_string());
          return FALSE;
        }
      }
    }
    return TRUE;
  }
  function unset_email_host() {
    $result = FALSE;
    $hosting_order_id = $this->_params['where']['hosting_order_id'] ?? NULL;
    if (!$hosting_order_id) {
      $this->set_error("Failed to find hosting_order_id");
      return FALSE;
    }
    $sql = "UPDATE red_item SET item_host = '' WHERE item_status != 'deleted' AND
      hosting_order_id = #hosting_order_id AND service_id = 2";
    red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
    return TRUE;
  }

  function mx_verify() {
    $mx_verify = new red_mx_verify();

    $result = FALSE;
    if (array_key_exists('key', $this->_params['where'])) {
      $result = $mx_verify->process_key($this->_params['where']['key']);
    }
    else if (array_key_exists('domain', $this->_params['where'])) {
      if ($mx_verify->set_item_id_for_domain($this->_params['where']['domain'])) {
        $result = $mx_verify->send();
        if ($result) {
          return TRUE;
        }
      }
    }
    if (is_null($result)) {
      // No error, but now key was processed.
      return TRUE;
    }
    elseif (!$result) {
      $this->set_error($mx_verify->get_errors());
      return FALSE;
    }
    // We have successfully verified the domain, now trigger the rebuild
    // of all the databases.
    global $globals;
    $servers = [
      'mx_servers' => $globals['config']['mx_servers'],
      'cf_servers' => $globals['config']['cf_servers'],
      'filter_servers' => $globals['config']['filter_servers'],
    ];
    red_item_mailbox::rebuild_email_databases();
    red_item_mailbox::rebuild_unverified_domains_database();
    return TRUE;
  }

  // Return the mysql db users that are using this database. This is used
  // by the mf-transfer script to ensure mysql users are moved along with
  // their database.
  function fetch_mysql_users() {
    $mysql_db_name = $this->_params['where']['mysql_db_name'] ?? NULL;
    if (!$mysql_db_name) {
      $this->set_error("Missing mysql_db_name");
      return FALSE;
    }

    $sql = "SELECT red_item.item_id, item_status, mysql_user_priv, mysql_user_name
      FROM red_item JOIN red_item_mysql_user USING(item_id)
      WHERE mysql_user_db REGEXP @mysql_db_name  AND item_status != 'deleted'";
    $params['@mysql_db_name'] = '(:|^)' . $mysql_db_name . '(:|$)';
    $result = red_sql_query($sql, $params, $this->node_sql_resource);
    while ($row = red_sql_fetch_row($result)) {
      $this->set_values([
        'item_id' => $row[0], 
        'item_status' => $row[1], 
        'mysql_user_priv' => $row[2],
        'mysql_user_name' => $row[3],
      ]);
    }
    return TRUE;
  }

  function remote_action() {
    $item_id = $this->_params['where']['item_id'] ?? NULL;
    $mailbox_login = $this->_params['where']['mailbox_login'] ?? NULL;
    $mailbox_abandoned = $this->_params['set']['mailbox_abandoned'] ?? NULL;
    $nextcloud_login = $this->_params['where']['nextcloud_login'] ?? NULL;
    $nextcloud_abandoned = $this->_params['set']['nextcloud_abandoned'] ?? NULL;

    $mysql_db_name = $this->_params['where']['mysql_db_name'] ?? NULL;

    if (!$item_id && !$mailbox_login && !$mysql_db_name &&!$nextcloud_login) {
      $this->set_error("Missing item_id and mailbox login and nextcloud_login and mysql_db_name");
      return FALSE;
    }

    if ($mysql_db_name) {
      // Deprecated. Instead of calling "remote_action" we should be calling
      // "mysql_users" as the API action.
      return $this->fetch_mysql_users(); 
    }
    
    if (!$item_id) {
      // This means we have a mailbox/nextcloud_login but not an item_id. This
      // happens when we are processing abandoned resources. We have to take
      // two steps:
      //
      // 1. Update the red_item_mailbox/nextcloud table and set the correct
      // abandoned field values.
      // 2. If it's a mailbox We have to run remote_tasks against the email
      // address record to rebuild our database tables.
      if ($mailbox_login) {

        // Update the mailbox.
        $sql = "UPDATE red_item_mailbox JOIN red_item USING(item_id) 
          SET mailbox_abandoned = #mailbox_abandoned
          WHERE mailbox_login = @mailbox_login AND item_status != 'deleted'";
        $result = red_sql_query($sql, ['#mailbox_abandoned' => $mailbox_abandoned, '@mailbox_login' => $mailbox_login]);

        // Now fetch the item_id so it can get passed down below for remote
        // actions.
        $sql = "SELECT red_item.item_id FROM red_item JOIN red_item_mailbox USING(item_id)
          WHERE mailbox_login = @mailbox_login AND item_status != 'deleted'";
        $result = red_sql_query($sql, ['@mailbox_login' => $mailbox_login]);
        $row = red_sql_fetch_row($result);
        $item_id = $row[0] ?? NULL;
      }
      else {
        // Update the nextcloud record.
        $sql = "UPDATE red_item_nextcloud JOIN red_item USING(item_id) 
          SET nextcloud_abandoned = #nextcloud_abandoned
          WHERE nextcloud_login = @nextcloud_login AND item_status != 'deleted'";
        $result = red_sql_query($sql, ['#nextcloud_abandoned' => $nextcloud_abandoned, '@nextcloud_login' => $nextcloud_login]);

        // All done.
        return TRUE;
      }
    }
    if (!$item_id) {
      $this->set_error("Failed to lookup item_id using mailbox login: $mailbox_login");
      return FALSE;
    }

    // We need the service id.
    $sql = "SELECT service_id FROM red_item WHERE item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $item_id]);
    $row = red_sql_fetch_row($result);
    $service_id = intval($row[0]);

    if (!$service_id) {
      $this->set_error("Failed to get service id for item $item_id");
      return FALSE;
    }

    $co =  $this->_construction_options;
    $co['id'] = $item_id;
    $co['service_id'] = $service_id;
    $obj = red_item::get_red_object($co);

    if (!$obj) {
      $this->set_error("Failed to create object");
      return FALSE;
    }
    return TRUE;
  }

  function run() {
    $action = $this->_params['action'] ?? NULL;
    if ($action == 'remote_action') {
      return $this->remote_action();
    }
    elseif ($action == 'mx_verify') {
      return $this->mx_verify();
    } 
    elseif($action == 'grant') {
      // This action is for granting two factor auth for access to the shell server.
      // That happened already in the authenticate function so we can simply return.
      $this->set_values("Access granted.");
      return TRUE;
    }
    elseif($action == 'unset_email_host') {
      // This action is for transitioning from MOSH to new infrastructure. After
      // a hosting order is moved, all email addresses should have the host unset.
      return $this->unset_email_host();
    }
    elseif($action == 'web_dns_to_alias') {
      // This action is for transitioning from MOSH to new infrastructure. After
      // a web conf is moved, convert DNS A records to alias records.
      return $this->web_dns_to_alias();
    }
    elseif ($action == 'mysql_users') {
      // This action returns all mysql users of a given database. It's used when
      // moving a database from one server to another to ensure all users are
      // moved as well.
      return $this->fetch_mysql_users();
    }
    $this->param_substitute();
    if (!$this->sanity_check()) {
      $this->set_error(red_t("Failed sanity check"));
      return false;
    }

    $object = $this->convert_to_object();

    // return on error
    if(false === $object) return false;

    // $object should be true if action is select (which
    // populates $this->_values, nothing left to do
    if($object === true) return true;

    if ($this->cli || $this->_authz->is_admin()) { 
      if (property_exists($object, 'is_admin')) {
        $object->is_admin = TRUE;
      }
    }
    $params = $this->_params;
    // set delete flag if necessary
    if($params['action'] == 'delete') $object->set_delete_flag();

    // set to pending-disabled if necessary
    if($params['action'] == 'disable') $object->set_disable_flag();

    // set object properties based on supplied user input
    // and validate
    if(!$this->process_user_input($object)) {
      $this->set_error(red_t("Failed validation"));
      return false;
    } elseif ($params['action'] == 'validate') {
      return true;
    }

    if ($params['object'] == 'hosting_order') {
      // Normally when updating an object we are 're-enabling' it so we re-enable
      // all related objects. Don't do this via the API. Only via the UI.
      $object->reenable_items_on_update = FALSE;
    }
    // save changes
    if(!$object->commit_to_db()) {
      $this->set_error(red_t("Errors were found when committing to db!"));
      $errors = $object->get_errors('system');
      foreach ($errors as $v)  {
        $this->set_error($v['error']);
      }
      return false;
    } else {
      // success. now, populate the values array that will
      // be returned as output 
      $this->populate_values_from_items(array($object));
      return true;
    }
  }

  /**
   * Given a set of parameters, attempt to create one or more objects
   * based on the given paramaters. On select, create multiple objects
   * matching the criteria in params, on update or delete, try to create
   * the object matching the params that should be update/deleted, on 
   * insert create an empty object to be populated.
   **/
  function convert_to_object() {
    $result = null;
    $params = $this->_params;
    if($params['action'] != 'insert') {
      // get a sql result set based on the params
      // this result set will be restricted to records they
      // should have access to
      $result = $this->get_result_set();
    } 
    else {
      if (!$this->cli && !$this->_authz->is_admin()) {
        // ensure they have permission to create an object of this type
        $object = $params['object'];
        if ($object == 'member') {
          // Only admins can create members.
          return FALSE;
        }
        $parent_id = null;
        if ($object == 'item') {
          $parent_id = $params['set']['hosting_order_id'];
          $service_name = red_get_service_name_for_service_id($params['set']['service_id']);
        } 
        else {
          $parent_id = $params['set']['member_id'];
          $service_name = 'object';
        }
        $item_id = $this->_params['where']['item_id'] ?? NULL;
        if (!$this->_authz->check_insert_perms($service_name, $parent_id, $item_id)) {
          $this->set_error(red_t("You are not authorized to create this record."));
          return false;
        }
      }
    }

    // handle errors
    if(false === $result) return false;

    // First, handle select, which could have multiple items
    if ($params['action'] == 'select') {
      // $result is null if we can't find any matching
      // items with the parameters given.
      if (is_null($result)) return true;

      // otherwise, populate the output with the values found
      $items = array();
      while ($row = red_sql_fetch_assoc($result)) {
        $this->_construction_options['rs'] = $row;
        $items[] = $this->get_object();
      }
      $this->populate_values_from_items($items);

      // nothing left to do, since we're only selecting
      return true;
    } 
    // if we have a record, prepopulate the item with it's
    // values
    if (!is_null($result)) {
      $row = red_sql_fetch_assoc($result);
      $this->_construction_options['rs'] = $row;
    } 
    else {
      // otherwise, the result set is null.
      // if the action was to delete, then we return true 
      // (trying to delete a record that doesn't exist is considered
      // a success)
      if ($params['action'] == 'delete') return true;

      // otherwise, we need to create an empty record
      // we need a few more values
      if (array_key_exists('hosting_order_id',$params['set'])) {
        $this->_construction_options['hosting_order_id'] = $params['set']['hosting_order_id'];
      } 
      elseif(array_key_exists('hosting_order_id', $params['where'])) {
        $this->_construction_options['hosting_order_id'] = $params['where']['hosting_order_id'];
      }
      if (array_key_exists('service_id',$params['set'])) {
        $this->_construction_options['service_id'] = $params['set']['service_id'];
      } 
      elseif(array_key_exists('service_id',$params['where'])) {
        $this->_construction_options['service_id'] = $params['where']['service_id'];
      }
    }
    return $this->get_object();
  }

  // return an object givin the passed params
  function get_object() {
    $params = $this->_params;
    if($params['object'] == 'item') {
      $obj = red_item::get_red_object($this->_construction_options);
    } else {
      $object = $params['object'];
      global $globals;
      $object_path = $globals['config']['src_path'] . '/modules/class.red.' . $object . '.inc.php';
      $object_name = 'red_' . $object;
      $object_key_field_function = 'get_' . $object . '_id';
      if (file_exists($object_path)) {
        $obj = new $object_name($this->_construction_options);
      }
    }
    return $obj;
  }

  function populate_values_from_items($items) {
    reset($items);
    foreach($items as $item) {
      $fields = array_keys($item->_datafields);
      $value = array();
      foreach ($fields as $field)  {
        $func = 'get_' . $field;
        $value[$field] = $item->$func();
      }
      if (get_class($item) == 'red_member') {
        // Add a few extra values for members.
        $member_id = $value['member_id'];
        $value['member_allocated_quota'] = red_get_allocated_quota_for_membership($member_id);
        $value['member_disk_usage'] = red_get_disk_usage_for_membership($member_id);
        $value['member_virtual_private_servers'] = red_get_virtual_private_servers_for_membership($member_id);
        $value['member_cpu'] = red_get_cpu_for_membership($member_id);
        $value['member_ram'] = red_get_ram_for_membership($member_id);
        $value['member_ssd'] = red_get_ssd_for_membership($member_id);
        $value['member_servers'] = implode(',', red_get_servers_for_membership($member_id));
      }
      $this->set_values($value);
    }
  }
  /**
   * Get a result set matching the passed parameters 
   * in $params
   **/
  function get_result_set() {
    $params = $this->_params;
    $join = [];
    $where = [];
    $fields = [];
    $sql_params = [];
    $action = $params['action'];
    if ($params['object'] == 'item') {
      $service_id = $params['where']['service_id'] ?? NULL;
      if (is_null($service_id)) {
        $service_id = $params['set']['service_id'] ?? NULL;
      }
      if (is_null($service_id)) {
        // Maybe we have an item_id.
        $item_id = $params['where']['item_id'] ?? NULL;
        if ($item_id) {
          $service_id = red_get_service_id_for_item_id($item_id);
        }
      }
      if (!$service_id) {
        $this->set_error(red_t("service_id is required."));
        return false;
      }
      $service_table = red_get_service_table_for_service_id($service_id);
      $join[] = "JOIN !service_table USING(item_id)";
      $fields[] = "!service_table.*";
      $sql_params['!service_table'] = $service_table;
    } 
    $object = $params['object'];
    $table = "red_{$object}";
    // If the status field is not specified, add a default.
    $status_field = "{$object}_status";
    if (!array_key_exists($status_field, $params['where'])) {
      $where[] = "(!status_field = 'active' OR !status_field = 'disabled' OR !status_field = 'soft-error')";
      $sql_params['!status_field'] = $status_field;
    }
    $fields[] = "!table.*";
    $sql_params['!table'] = $table;
    reset($params);
    foreach($params['where'] as $k => $v) { 
      $where[] = "$k = @{$k}";
      $sql_params["@{$k}"] = $v;
    }
    $sql = "SELECT " . implode(',',$fields) . " FROM !table " . implode(' ', $join) . " WHERE " . implode(' AND ', $where);
    if(!$this->cli && !$this->_authz->is_admin()) {
      $sql = $this->_authz->rewrite_sql($sql, $params['object']);  
    }
    $result = red_sql_query($sql, $sql_params);
    $num = red_sql_num_rows($result);
    if ($this->_max_records != 0 && $num > $this->_max_records) {
      $this->set_error(red_t("Max records exceeded ($num, $sql). Please limit your select to criteria that will return less than %num recordes.", array('%num' => $this->_max_records)));
      return false;
    }


    if($num == 0) {
      if ($action == 'update' || $action == 'disable' ) {
        $this->set_error(red_t("Failed to find existing record via api when updating or disabling."));
        return false;
      }
    }
    if($num == 0) return null;
    if($num == 1) return $result;
    if($num > 1) {
      if ($action != 'select') {
        $this->set_error(red_t("Found more than one matching record. Aborting."));
        return false;
      } else {
        return $result;
      }
    }
  }

  /**
   * Certain fields can be substituted for other fields
   * (e.g. you can pass the hosting_order_identifier which
   * will be substituted for the hosting_order_id). This
   * function performs that substitution.
   **/
  function param_substitute() {
    $params =& $this->_params;
    if(!array_key_exists('sub',$params)) return;

    foreach ($params['sub'] as $k => $v) {
      if($k == 'hosting_order_identifier' && !array_key_exists('hosting_order_id',$params['where'])) {
        $id = $this->get_hosting_order_id_for_identifier($v);
        if ($id) {
          $params['set']['hosting_order_id'] = $id;
        }
      } 
      if($k == 'member_friendly_name' && !array_key_exists('member_id',$params['where'])) {
        $id = $this->get_member_id_for_friendly_name($v);
        if ($id) {
          $params['set']['member_id'] = $id;
        }
      }
      if($k == 'mysql_db_name' && !array_key_exists('web_app_mysql_db_id',$params['set'])) {
        $id = $this->get_item_id_for_name('mysql_db','mysql_db_name',$params['where']['hosting_order_id'],$v);
        if ($id) {
          $params['set']['web_app_mysql_db_id'] = $id;
        }
      }
      if($k == 'mysql_user_name' && !array_key_exists('web_app_mysql_user_id',$params['set'])) {
        $id = $this->get_item_id_for_name('mysql_user','mysql_user_name',$params['where']['hosting_order_id'],$v);
        if ($id) {
          $params['set']['web_app_mysql_user_id'] = $id;
        }
      }
      if($k == 'cron_cmd' && !array_key_exists('web_app_cron_id',$params['set'])) {
        $id = $this->get_item_id_for_name('cron','cron_cmd',$v,$params['where']['hosting_order_id']);
        if ($id) {
          $params['set']['web_app_cron_id'] = $id;
        }
      }
      if($k == 'unique_unix_group_name' && !array_key_exists('unique_unix_group_id',$params['set'])) {
        $id = $this->get_unique_unix_group_id_for_name($v);
        if ($id) {
          $params['set']['unique_unix_group_id'] = $id;
        }
      }
    }
  }

  function get_item_id_for_name($table,$name_field,$name,$hosting_order_id) {
    $sql = "SELECT red_item.item_id FROM !table JOIN red_item ".
      "USING(item_id) WHERE !name_field = @name " .
      "AND (item_status = 'active' OR item_status = 'disabled') AND hosting_order_id = #hosting_order_id ";
    $result = red_sql_query($sql, ['@name' => $name, '#hosting_order_id' => $hosting_order_id, '!name_field' => $name_field, '!table' => $table]);
    $row = red_sql_fetch_row($result);
    $id = intval($row[0]);
    if(!empty($id)) {
      return $id;
    }
    return null;
  }

  function get_unique_unix_group_id_for_name($name) {
    $sql = "SELECT unique_unix_group_id FROM red_unique_unix_group WHERE ".
      "unique_unix_group_name = @name " .
      "AND unique_unix_group_status = 'active' ";
    $result = red_sql_query($sql, ['@name' => $name]);
    $row = red_sql_fetch_row($result);
    $unique_unix_group_id = intval($row[0]);
    if(!empty($unique_unix_group_id)) {
      return $unique_unix_group_id;
    }
    return null;
  }

  function get_hosting_order_id_for_identifier($identifier) {
    $sql = "SELECT hosting_order_id FROM red_hosting_order WHERE ".
      "hosting_order_identifier = @hosting_order_identifier " .
      "AND (hosting_order_status = 'active' OR hosting_order_status = 'disabled' ) ";
    $result = red_sql_query($sql, ['@hosting_order_identifier' => $hosting_order_identifier]);
    $row = red_sql_fetch_row($result);
    $hosting_order_id = intval($row[0]);
    if(!empty($hosting_order_id)) {
      return $hosting_order_id;
    }
    return null;
  }
  function get_member_id_for_friendly_name($member_friendly_name) {
    $sql = "SELECT member_id FROM red_member WHERE ".
      "member_friendly_name = @member_friendly_name " .
      "AND member_status = 'active' ";
    $result = red_sql_query($sql, ['@member_friendly_name' => $member_friendly_name]);
    $row = red_sql_fetch_row($result);
    $member_id = intval($row[0]);
    if(!empty($member_id)) {
      return $member_id;
    }
    return null;


  }
  /**
   * Parse cli passed arguments into an array
   **/
  function parse_cli_arguments($args) {
    foreach ($args as $k => $v) {
      if (preg_match('/^--(.*)$/',$v,$matches)) {
        $foo = $matches[1];
        // check for --arg=value syntax
        if (preg_match('/^(.*)=(.*)$/',$foo,$matches)) {
          $field = $matches[1];
          $value = $matches[2];
        } else {
          // otherwise assume --arg value syntax
          $field = $foo;
          $value_index = $k+1;
          if (array_key_exists($value_index,$args) && !preg_match('/^--/',$args[$value_index])) {
            $value = $args[$value_index];
            unset($args[$value_index]);
          } else {
            $this->set_error(red_t("Warning: %field missing value.", array('%field' => $foo)));
            return FALSE;
          }
        }
        $this->parse_argument($field, $value);
      }
    }
    return TRUE;
  }

  // parse web/REST provided arguments
  function parse_request_arguments($args) {
    foreach ($args as $field => $value) {
      $this->parse_argument($field,$value);
    }
  }

  /**
   * parse arguments into an api consistent 
   * array syntax
   **/
  function parse_argument($field, $value) {
    $field = str_replace('-','_',$field);
    // set/sub/where indexes are sanitized here to ensure they
    // only contain letters, numbers and underscore
    if(preg_match('/^set:([a-z0-1_]+)/', $field, $matches)) {
      $field = $matches[1];
      $this->_params['set'][$field] = $value;
    } elseif(preg_match('/^sub:([a-z0-1_]+)/', $field, $matches)) {
      $field = $matches[1];
      $this->_params['sub'][$field] = $value;
    } elseif(preg_match('/^where:([a-z0-1_]+)/', $field, $matches)) {
      $field = $matches[1];
      $this->_params['where'][$field] = $value;
    } elseif ($field == 'action') {
      $this->_params['action'] = $value;
    } elseif ($field == 'object') {
      $this->_params['object'] = $value;
    } elseif ($field == 'log_level') {
      $this->set_log_level(intval($value));
    } elseif ($field == 'user_name') {
      $this->_params['user_name'] = $value;
    } elseif ($field == 'user_pass') {
      $this->_params['user_pass'] = $value;
    } elseif ($field == 'max_records') {
      $this->_max_records = intval($value);
    }
  }

  function sanity_check() {
    $params = $this->_params;
    $ret = true;

    // ensure known params are integers 
    $validate = array(
      'service_id' => '/^[0-9]+$/',
      'hosting_order_id' => '/^[0-9]+$/',
      'member_id' => '/^[0-9]+$/',
    );

    if (!array_key_exists('action', $params)) {
      $this->set_error(red_t("Missing action parameter."));
      $ret = false; 
      // set to emtpy to avoid missing index errors below
      $params['action'] = '';
    } 
    elseif (!preg_match('/^(delete|insert|validate|update|replace|select|disable)$/', $params['action'])) {
      $this->set_error(red_t("Unknown action"));
      $ret = false;
    }
    if (!array_key_exists('object', $params)) {
      $this->set_error(red_t("Missing object parameter."));
      $ret = false; 
    } 
    elseif (!preg_match('/^(member|item|hosting_order|contact|unique_unix_group|vps)$/', $params['object'])) {
      $this->set_error(red_t("Unknown object."));
      $ret = false;
    }

    $set_actions = array('insert', 'validate', 'update', 'replace');
    if (!in_array($params['action'], $set_actions) && count($params['set']) > 0) {
      $this->set_error(red_t("Set fields are meaningless with this action."));
      $ret = false;
    }
    // validate fields that should be integers
    foreach ($validate as $field => $regexp) {
      if (!array_key_exists($field, $params['where'])) continue;
      if (!preg_match($regexp, $params['where'][$field])) {
        $this->set_error(red_t("Failed regexp for $field"));
        $ret = false;
      }
    }
    if (!$ret) {
      return false;
    }

    // make sure service id is valid
    if(array_key_exists('service_id', $params['where'])) {
      $check = red_get_service_table_for_service_id($params['where']['service_id']);
      if (empty($check)) {
        $this->set_error(red_t("Service id is invalid."));
        $ret = false;
      }
    }
    if (array_key_exists('hosting_order_id',$params['where'])) {
      $hoid = $params['where']['hosting_order_id'];
      if (!$this->valid_hosting_order_id($hoid)) {
        $this->set_error(red_t("Hosting Order Id doesn't exist or is inactive."));
        $ret = false;
      }
    }
    if (array_key_exists('member_id',$params['where'])) {
      $mid = $params['where']['member_id'];
      if (!$this->valid_member_id($mid)) {
        $this->set_error(red_t("Member Id doesn't exist or is inactive."));
        $ret = false;
      }
    }

    // specific sanity checks
    if ($params['object'] == 'item') {
      if ($params['action'] == 'insert') {
        if (!array_key_exists('hosting_order_id', $params['set'])) {
          $this->set_error(red_t("You must specify a hosting_order_id."));
          $ret = false;
        }
        if (!array_key_exists('service_id', $params['set'])) {
          $this->set_error(red_t("You must specify a service_id."));
          $ret = false;
        }
      }
    }
    return $ret;
  }

  function valid_member_id($id) {
    $sql = "SELECT member_id FROM red_member WHERE member_id ".
      "= #id AND member_status = 'active'";
    $result = red_sql_query($sql, ['#id' => $id]);
    if(red_sql_num_rows($result) == 0) {
      return false;
    }
    return true;
  }

  function valid_hosting_order_id($id) {
    $sql = "SELECT hosting_order_id FROM red_hosting_order WHERE hosting_order_id ".
      "= #id AND (hosting_order_status = 'active' OR hosting_order_status = 'disabled') ";
    $result = red_sql_query($sql, ['#id' => $id]);
    if(red_sql_num_rows($result) == 0) {
      return false;
    }
    return true;
  }

  function process_user_input(&$item) {
    $params = $this->_params;
    $datafields = $item->_datafields;
    reset($datafields);
    foreach ($datafields as $field => $properties)  {
      if($field != 'item_id') {
        $get_function = "get_".$field;
        $set_function = "set_".$field;
        $value = $item->$get_function();
        if(array_key_exists($field,$params['set'])) {
          $value = $params['set'][$field];
        }
        $item->$set_function($value);
      }
    }
    // Special case for setting quota.
    $field = 'item_quota_to_percent_over_usage';
    if(array_key_exists($field, $params['set'])) {
      $method = 'set_' . $field;
      if (method_exists($item, $method)) {
        $value = $params['set'][$field];
        $item->$method($value);
      }
    }
    // Special case for creating memberships. It is possible to set
    // extra fields that will trigger the creation of related records.
    if (get_class($item) == 'red_member') {
      $member_extra_fields = array(
        'contact_email',
        'contact_first_name',
        'contact_lang',
        'contact_last_name',
      );
      foreach($member_extra_fields as $extra_field) {
        $get_function = "get_".$extra_field;
        $set_function = "set_".$extra_field;
        $value = $item->$get_function();
        if(array_key_exists($extra_field, $params['set'])) {
          $value = $params['set'][$extra_field];
        }
        $item->$set_function($value);
      }
    }
    // Special case for creating hosting orders. We also need to set the unique unix group
    // name.
    if (get_class($item) == 'red_hosting_order') {
      $hosting_order_extra_fields = array(
        'unique_unix_group_name',
        'notification_email',
        'notification_lang'
      );
      foreach($hosting_order_extra_fields as $extra_field) {
        $get_function = "get_".$extra_field;
        $set_function = "set_".$extra_field;
        $value = $item->$get_function();
        if(array_key_exists($extra_field, $params['set'])) {
          $value = $params['set'][$extra_field];
        }
        $item->$set_function($value);
      }

      // Lastly, if hosting_order_host is empty, we fill in the default.
      $hosting_order_host = $item->get_hosting_order_host();
      if (empty($hosting_order_host)) {
        // Pop off the first of the available options.
        $options = $item->get_host_options();
        $host = array_shift($options);
        $item->set_hosting_order_host($host);
      }
    }
    if($item->validate()) return true;
    $this->set_error(red_t("Validation errors."));
    $errors = $item->get_errors('validation');
    foreach ($errors as $error) {
      $this->set_error($error['error']);
    }
    return false;
  }

  function print_output_text() {
    if(array_key_exists('values',$this->_output) &&
      count($this->_output['values']) == 0) {
      unset($this->_output['values']);
    }
    print_r($this->_output);
  }

  function print_output_json() {
    if(array_key_exists('values',$this->_output) &&
      count($this->_output['values']) == 0) {
      unset($this->_output['values']);
    }
    print json_encode($this->_output);
  }
  function print_output() {
    $this->build_output_array();
    if($this->_output_format == 'text') {
      return $this->print_output_text();
    }
    if($this->_output_format == 'json') {
      return $this->print_output_json();
    }
  }

  function build_output_array() {
    $this->_output = array();
    if(count($this->_errors) > 0) {
      $this->_output['is_error'] = 1;
      $this->_output['error_message'] = $this->_errors;
    } else {
      $this->_output['is_error'] = 0;
      $this->_output['values'] = $this->_values;
    }
  }
}


?>
