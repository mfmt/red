<?php


class html_generator {
  function get_form_element($name, $value, $type, $attributes = []) {
    if($type == 'textarea') {
      $attributes['name'] = $name;
      return $this->get_tag('textarea',$value,$attributes);
    } 
    elseif ($type == 'checkbox') {
      $options = $attributes['options'];
      unset($attributes['options']);
      return $this->get_checkbox($name, $options, $value, $attributes);
    }
    elseif ($type == 'select') {
      $options = $attributes['options'];
      unset($attributes['options']);
      $multiple = FALSE;
      return $this->get_select($name, $options, $value, $multiple, $attributes);
    }
    return $this->get_input($name, $value, $type, $attributes);
  }

  function get_input($name, $value = '', $type = 'text', $attributes = []) {
    // Oof. FIXME - we should not be passing NULL I guess?
    if (is_null($value)) {
      $value = '';
    }
    $value = htmlentities($value, ENT_COMPAT, 'UTF-8');
    $html = '<input type="' . $type . '" name="'.$name.'" value="'.$value.'"';
    $html .= $this->_get_imploded_attributes($attributes);
    $html .= " />\n";
    return $html;
  }

  /**
   * Return a select html tag
   *
   * @name - name of the tag (name=)
   * @options - if array, it's a key-value pair that should be converted into a set of option tags
   *            if a string, it should be inserted as is
   **/
  function get_select($name, $options, $selected = array(), $multiple = FALSE, $attributes = array(), $prepend_choose_one_option = true) {
    // if there's only one option, emit hidden field and display only option
    // available
    if(is_array($options) && count($options) == 1) {
      foreach($options as $key => $value);
      $attr = array('type' => 'hidden', 'name' => $name, 'value' => $key);
      $ret = $this->get_tag('input','',$attr);
      $ret .= $value;
      $span_attr = array('class' => 'red-explanation');
      return "$ret " . $this->get_tag('span','(only available option)',$span_attr);
    }
    if(is_array($options) && $prepend_choose_one_option && !array_key_exists('', $options)) { 
      $options = array('' => '--Choose one--') + $options;
    }
    $html = '<select ';
    if ( (!$multiple) && is_array($selected) && (count($selected) > 1))  {
      trigger_error(red_t('You should not attempt to emit a select html element which is not multiple, but has more than one item selected.'), E_USER_ERROR);
    }

    if ($multiple)  {
      $html .= ' multiple';
    }
    if(is_array($options)) {
      $options_html = $this->get_options($options, $selected);
    }
    else {
      $options_html = $options;
    }
    if(!array_key_exists('id', $attributes)) {
      $attributes['id'] = str_replace('sf_', '', $name);
    }

    $html .= $this->_get_imploded_attributes($attributes) .
      ' name="'.$name .'" >' . "\n" .
      $options_html .
      "\n</select>";
    return $html;

  }
    
  function get_checkbox($name, $option, $selected, $attributes = array()) {
    $k = key($option);
    $v = array_pop($option);
    $ret = '<input type="checkbox"';
    $ret .= ' ' . $this->_get_imploded_attributes($attributes);
    $ret .= ' name="' . $name . '['.$k.']" value="' . $k . '"';
    $checked_attr = ' ';
    $checked = false;
    if(is_array($selected) && in_array($k,$selected)) {
      $checked = true;
    }
    if(!is_array($selected) && $k == $selected) {
      $checked = true;
    }   
    if($checked) $ret .= ' checked="checked" ';
    $ret .= ' id="' . $k . '"> ';
    $ret .= $v;
    $ret .= '</input>';
    return $ret; 
  }

  function get_checkboxes($name, $options, $selected, $attributes = array()) {
    if (!is_array($selected)) $selected = array($selected);
    $parts = array();
    foreach($options as $k => $option) {
      $parts[] = $this->get_checkbox($name, array($k => $option), $selected, $attributes);
    }
    return implode("\n", $parts);
  }

  function get_radio($name,$options,$selected, $attributes = array()) {
    $begin = '<div class="red-radio-option"><input type="radio"';
    $begin .= $this->_get_imploded_attributes($attributes);
    $begin.= ' name="'.$name .'" value=';
    $end = '/>';
    $parts = array();
    foreach($options as $k => $v) {
      $checked_attr = ' ';
      if($selected == $k) $checked_attr = ' checked="checked"';
      $id_attr = ' id="' . $k . '"';
      $parts[] = $begin . '"' . $k . '"' . $id_attr . $checked_attr . $end . '<label for="' . $k . '">' . $v . '</label></div>';
    }
    return implode("\n", $parts);
  }

  function get_options($options, $selected = NULL, $attributes = array()) {
    // Thanks Lightningbug!

    // If the selected item is not an array but a single value, then make 
    // it into an array with just one value pair. This way, we can match 
    // both single values and arrays (we need to match arrays in case 
    // we are dealing with a select multiple tag).
    // NULL for $selected should always mean no item is selected
    if(!is_array($selected) && !is_null($selected)) {
      $selected_array[] = $selected;
      $selected = $selected_array;
    }

    if(is_array($options)) {
      $html_options = '';
      reset($options);
      foreach($options as $key => $value) {
        $html_options .= "<option"; 
        $html_options .= $this->_get_imploded_attributes($attributes);
        // If $selected = '', then the selected array will come out as 
        // having zero members 
        if (!is_null($selected)) {
          if(in_array($key,$selected) || $key == '' && count($selected) == 0) {
            $html_options .= ' selected="selected"';
          }
        }
        $html_options .= " value=\"$key\">$value</option>\n";
      }
    } 
    else {
      // If they are not passing options, create an empty option to help 
      // trouble shoot
      $html_options = '<option>[empty]</option>';
    }
    return $html_options;
  }

  function get_table($data, $attributes = array()) {
    $ret = "<table";
    $ret .= $this->_get_imploded_attributes($attributes);
    $ret .= ">\n";
    if(is_array($data)) {
      // $data is a multi-dimensional array
      foreach($data as $k => $row) {
        $row_attr = array();
        if(!empty($k)) {
          $row_attr = array('id' => $k);
        }
        $ret .= $this->get_table_row($row, $row_attr);
      }
    }
    else {
      $ret .= $data;
    }
    return $ret . "</table>\n";
  }

  function get_table_row($row,$attr = array()) {
    $ret = "\t<tr";
    $ret .= $this->_get_imploded_attributes($attr);
    $ret .= ">\n";
    if(is_array($row)) {
      $ret .= $this->get_table_cells($row);  
    }
    else {
      $ret .= $row;
    }
    return $ret .= "\t</tr>\n";
  }

  function get_table_cells($row,$attr = array(), $tag = 'td') {
    if(is_array($row)) {
      $ret = '';
      foreach($row as $cell) {
        $ret .= "\t\t<{$tag}";
        $value = $cell['value'];
        if(isset($cell['attributes'])) {
          $ret .= $this->_get_imploded_attributes($cell['attributes']);
        }
        $ret .= ">\n" . $value . "\n\t\t</{$tag}>\n";
      }
    }
    else {
      $ret = "<{$tag}" . $this->_get_imploded_attributes($attr) . ">$row</{$tag}>";
    }
    return $ret;
  }

  function _get_imploded_attributes($attributes) {
    if(!is_array($attributes)) return 'attributes not an array:' . $attributes;
    $ret = '';
    foreach($attributes as $k => $v) {
      // If there is no text key, assume it's a solo attribute, like
      // disabled.
      if(is_numeric($k))  {
        $ret .= " $v";
      } else {
        $ret .= " $k=\"$v\"";
      }
    }
    return $ret;
  }

  function get_form_submit($name,$value = '',$attributes = array()) {
    $ret = "<input type=\"submit\" name=\"$name\" value=\"$value\"";
    $ret .= $this->_get_imploded_attributes($attributes);
    return $ret . " />";
  }

  function get_tag($tag,$content,$attributes = array()) {
    $ret = "<$tag";
    $ret .= $this->_get_imploded_attributes($attributes);
    $ret .= '>' . $content . "</$tag>";
    return $ret;
  }
}


?>
