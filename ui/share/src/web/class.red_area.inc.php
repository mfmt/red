<?php

class red_area {
  // general variables
  var $order_by_fields = [];
  var $html_generator;
  var $template;
  var $area;
  var $filter_keywords;
  var $show_filter_form = true;

  // active ids of objects
  var $service_id = null;
  var $service_name = NULL;
  var $service_basic_access = NULL;
  var $service_standard_access = NULL;
  var $unit_id = null;
  var $id = null;

  // authorization related
  public $authz = NULL;
  var $available_units = null;
  var $is_admin = false;
  var $user_name = null;

  // error handling
  var $errors = array();

  // override in children
  var $default_service_id;
  var $unit_friendly_name;
  var $unit_key_field;
  var $unit_friendly_field;
  var $unit_table;
  var $service_key_field;

  // auto generated
  var $service_table;
  var $unit_active_friendly_name;

  // used to hold service objects that don't pass validation
  // so the values can be used to populate the form after
  // the error 
  var $invalid_service_object;

  // factory method
  static function get_red_object($area, $service_id) {
    global $globals;

    $obj = '';
    $src_path = $globals['config']['src_path'];

    if($area == 'hosting_order') {
      $service_table = red_get_service_table_for_service_id($service_id);
      $object_name = 'red_area_hosting_order';
    } 
    else {
      $object_name = 'red_area_' . $area;
    }
    $obj = new $object_name($area, $service_id);
    return $obj;
  }

  function __construct($area, $service_id = NULL) {
    $this->area = $area;
    if (empty($service_id)) {
      $this->service_id = $this->default_service_id;
    }
    else {
      $this->service_id = $service_id;
    }
    $this->set_service_fields();
  }

  function set_service_fields() {
    $sql = "SELECT service_table,
        service_name,
        service_basic_access,
        service_standard_access,
        service_item,
        service_order_by
      FROM red_service 
      WHERE service_id = #service_id";
    $result = red_sql_query($sql, ['#service_id' => $this->service_id]);
    $row = red_sql_fetch_row($result);
    $this->service_table = $row[0];
    $this->service_name = $row[1];
    $this->service_basic_access = $row[2];
    $this->service_standard_access = $row[3];
    $service_item = $row[4];
    $service_order_by = $row[5];
    if ($service_item == 1) {
      $this->service_key_field = 'item_id';
    } else {
      // all others use the table name minus the initial red_item plus _id
      $this->service_key_field = substr($this->service_table,4) . '_id';
    }
    if ($service_order_by) {
      $this->order_by_fields = explode(',', $service_order_by);
    }
  }

  function get_errors_as_string() {
    return implode(' ',$this->errors);
  }
  function list_children($start, $limit) {
    // override
    return false;

  }

  function set_available_units() {
    // should be overridden in child
    $this->available_units = array();
  }

  function set_unit_id($unit_id = null) {
    if(!empty($unit_id)) {
      $this->unit_id = $unit_id;
    } else {
      $this->auto_generate_unit_id();
      // if it's still empty... pop off the first available
      // unit_id
      if(empty($this->unit_id)) {
        if(!empty($this->available_units)) {
          $keys = array_keys($this->available_units);
          $this->unit_id = array_shift($keys);
        }
      }
    }
  }

  function auto_generate_unit_id() {
    // if the unit_id (member_id or hosting_order_id) is empty, and id 
    // is passed, auto generate the {member,hosting_order}_id 
    if(!empty($this->id)) {
      $service_field = $this->service_key_field;
      $service_table = $this->service_table;
      $unit_field = $this->unit_key_field;
      $unit_table = $this->unit_table;

      if(empty($service_field) || empty($service_table) || empty($unit_field) ||
        empty($unit_table)) return;

      // exception for red_items... we want to lookup in the red_item table,
      // not the red_item_user_account, etc. table
      if(preg_match('/^red_item/',$service_table)) {
        $service_table = 'red_item';
      }

      $sql = "SELECT !unit_field FROM !unit_table JOIN !service_table ".
        "USING(!unit_field) WHERE !service_field = #id";
      $params = [
        '#id' => $this->id,
        '!unit_field' => $unit_field,
        '!unit_table' => $unit_table,
        '!service_table' => $service_table,
        '!service_field' => $service_field,
      ];
      $result = red_sql_query($sql, $params);
      $row = red_sql_fetch_row($result);
      $this->unit_id = $row[0];
    }
  }

  function check_access($id = null, $action = null) {
    //override in child
    return false;
  }

  function get_single_object($co) {
    // override
    return false;
  }

  function get_human_readable_tags($object_name = 'red_item') {
    $sql = "SELECT service_label, service_description, service_help_link FROM red_service WHERE service_id = #service_id";
    $result = red_sql_query($sql, ['#service_id' => $this->service_id]);
    $row = red_sql_fetch_row($result);
    $name = $row[0];
    $desc = $row[1];
    $help_link = $row[2];
    return array('description' => $desc, 'name' => $name, 'help_link' => $help_link);
  }

  function add_new_item_link() {
    $service_name = red_get_service_name_for_service_id($this->service_id);
    if(!$this->authz->check_insert_perms($service_name, $this->get_unit_id())) {
      // Disable this button.
      $this->template->set_add_new_item_link('none');
    }
    else {
      $this->template->set_add_new_item_link();
    }
  }
      
  function prepare_list() {
    $this->template->set_var('lang_benefits_level_is', red_t("Your benefits level is:"));
    $benefits_level = $this->get_member_benefits_level();
    if($benefits_level != 'basic') {
      $this->template->set_var('display_if_basic_membership', 'display:none;');
    }
    $this->template->set_var('benefits_level',$benefits_level);

    $this->template->set_var('lang_member_quota_is',red_t("Membership disk quota:"));
    $this->template->set_var('lang_member_allocated_quota_is',red_t("Disk quota allocated (membership-wide):"));
    $this->template->set_var('lang_member_disk_usage_is',red_t("Disk usage (membership-wide):"));
    if (red_member_has_vps($this->get_member_id())) {
      $this->template->set_var('lang_member_allocation_based_on_vps',red_t("Note: Allocation based on VPS"));
    }
    $member_quota = $this->get_member_quota();
    $member_disk_usage = $this->get_member_disk_usage();
    $member_allocated_quota = $this->get_member_allocated_quota();
    $this->template->set_var('member_quota', $member_quota);
    $this->template->set_var('member_allocated_quota', $member_allocated_quota);
    $this->template->set_var('member_disk_usage', $member_disk_usage);

    $human_readable = $this->get_human_readable_tags();
    $this->template->set_file('children_file','children.ihtml');
    $this->add_new_item_link();
    $this->template->set_var('lang_add_new_item',red_t("Add a new item"));
    $this->template->set_var('lang_refresh',red_t("Refresh"));
    if($this->area == 'hosting_order') {
      $this->template->set_var('service_id',$this->service_id);
      $this->template->set_var('unit_key_field',$this->unit_key_field);
      $this->template->set_var('unit_key_value',$this->unit_id);
      if($this->show_filter_form) {
        $this->template->set_file('filter_file','filter.ihtml');
        $this->template->set_var('lang_filter',red_t("Filter"));
        $this->template->parse('filter_block','filter_file');
      }
    }
    $this->template->set_var('item_description',$human_readable['description']);
    $this->template->set_var('item_name',$human_readable['name']);
    if(!empty($human_readable['help_link'])) {
      $i_attr = array('class' => 'fa fa-question');
      $icon = $this->html_generator->get_tag('i', '', $i_attr);
      $attr = array('href' => $human_readable['help_link'], 'class' => 'red-help-link');
      $this->template->set_var('help_link', $this->html_generator->get_tag('a', $icon, $attr));
    }
    $this->set_area_navigation();
  }

  function get_list_block($start, $limit) {
    return $this->get_header() . $this->get_content($start,$limit) .
      $this->get_footer();
  }
  
  function get_header() {


  }

  function get_footer() {

  }

  function get_unit_id() {
    return $this->unit_id;
  }

  function set_unit_active_friendly_name() {
    if(is_null($this->unit_key_field) || 
      is_null($this->unit_friendly_field) || 
      empty($this->unit_id) || is_null($this->unit_table)) {
      $this->unit_active_friendly_name = $this->unit_friendly_name;
      return;
    }
    $sql = 'SELECT !unit_friendly_field FROM !unit_table WHERE !unit_key_field = #unit_id';
    $params = [
      '#unit_id' => $this->unit_id,
      '!unit_friendly_field' => $this->unit_friendly_field,
      '!unit_table' => $this->unit_table,
      '!unit_key_field' => $this->unit_key_field,
    ];
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_row($result);
    if(empty($row[0])) {
      $this->unit_active_friendly_name = $this->unit_friendly_name;
    } else {
      $this->unit_active_friendly_name = $row[0];
    }
  }

  function set_area_navigation() {
    $this->set_unit_active_friendly_name();
    $this->template->set_file('area_navigation_file','area_navigation.ihtml');
    $this->template->set_var('lang_go',red_t('Go'));
    $this->template->set_var('area',$this->area);

    // fill units drop down
    $units = $this->available_units;
    if(count($units) == 0) {
      // no units to display, delete both blocks
      $this->template->set_block('area_navigation_file','multiple_unit_display','delete_me');
      $this->template->set_block('area_navigation_file','single_unit_display','delete_me');
    } elseif(count($units) == 1) {
      // one unit, display single unit
      $k = key($units);
      $v = $units[$k];
      $v = red_truncate_from_middle($v,60);
      $this->template->set_block('area_navigation_file','multiple_unit_display','delete_me');
      $this->template->set_var('unit_id_option',$k);
      $this->template->set_var('unit_name',$v);
    } else {
      // multiple units display drop down
      $this->template->set_block('area_navigation_file','single_unit_display','delete_me');
      $this->template->set_block('area_navigation_file','area_unit_row','area_unit_rows');
      foreach($units as $k => $v) {
        $v = red_truncate_from_middle($v,60);
        $this->template->set_var('unit_id_option',$k);
        $this->template->set_var('unit_name',red_htmlentities($v));
        if($k == $this->get_unit_id()) {
          $this->template->set_var('unit_selected','selected="selected"');
        } else {
          $this->template->set_var('unit_selected','');
        }
        $this->template->parse('area_unit_rows','area_unit_row',true);
      }
    }

    $this->template->set_var('unit_friendly_name',$this->unit_friendly_name);
    $this->template->set_var('unit_key_field',$this->unit_key_field);
    // fill services drop down
    $services = $this->get_available_services();
    $this->template->set_file('area_services','area_services.ihtml');
    $this->template->set_block('area_services','area_service_row','area_service_rows');
    foreach($services as $k => $v) {
      $this->template->set_var('service_id_option',$k);
      $this->template->set_var('service_label',str_replace(' ','&nbsp;',red_htmlentities($v)));
      $this->template->set_var('unit_id',$this->get_unit_id());
      if($k == $this->service_id) {
        $this->template->set_var('service_selected','active');
      } else {
        $this->template->set_var('service_selected','');
      }
      $this->template->parse('area_service_rows','area_service_row',true);
    }
    // Determine access levels
    $top = false;
    $member = false;
    $hosting_order = false;

    $top = $this->get_top_area_access();
    // if you access to the top level, you automatically have member level access
    if($top) {
      $member = true;
    } else {
      $member = $this->get_member_area_access(); 
    }
    if($member) {
      $hosting_order = true;
    } else {
      $hosting_order = $this->get_hosting_order_area_access();
    }
    $active = $this->area;

    $area_nav_items = $this->get_area_navigation_items($top,$member,$hosting_order);
    $member_id = null;
    if ($this->area != 'hosting_order') {
      $this->template->set_block('area_navigation_file', 'primary_host_block', 'delete_me');
    }

    if($this->area == 'member') {
      $member_id = $this->get_unit_id();
    }
    if($this->area == 'hosting_order') {
      $member_id = $this->get_member_id();
    }
    $item_errors = red_get_error_items();
    if ($this->is_admin) {
      if (count($item_errors) > 0 ) {
        $this->template->set_item_errors_message();
      }
      if ($member_id) {
        if (red_member::active_hosting_orders($member_id) == 0) {
          if (red_member::available_items($member_id) == 0) {
            $currentDate = new DateTime();
            $currentDate->modify('-3 months');
            $threeMonthsAgo = $currentDate->format('Y-m-d');
            $lastDisabledItem = red_member::date_of_last_disabled_item($member_id);
            if ($lastDisabledItem) {
              if ($lastDisabledItem < $threeMonthsAgo) {
                $msg = red_t("Member is ready to be deleted. Last item disabled on @disabled_date.", 
                  ['@disabled_date' => $lastDisabledItem]);
                red_set_message($msg);
              }
              else {
                $msg = red_t("Member NOT ready to be deleted. Last item disabled on @disabled_date.", 
                  ['@disabled_date' => $lastDisabledItem]);
                red_set_message($msg);
              }
            }
          }
          else {
            $msg = red_t("Warning: not all items in disabled hosting orders are disabled.");
            red_set_message($msg, 'error');
          }
        }
      }
    }
    if($member_id) {
      $member_id;
      $sql = "SELECT member_benefits_level FROM red_member WHERE member_id = #member_id";
      $result = red_sql_query($sql, ['#member_id' => $member_id]);
      $row = red_sql_fetch_row($result);
      $this->template->set_var('benefits_level', $row[0]);
      if($this->is_admin) {
        $this->template->set_var('user_auth', 'admin');
      }
      else {
        $this->template->set_var('user_auth', 'user');
      }
    }

    $this->template->set_var('delete_me', '');
    $this->template->set_area_navigation_list($area_nav_items,$active);
    $this->template->parse('area_navigation_block','area_navigation_file');
    $this->template->parse('area_services_block','area_services');

  }

  function get_area_navigation_links($top,$member,$hosting_order,$active) {
    // override in child
    return array();
  }

  function get_member_id() {
    //override in child class 
    return null;
  }

  function get_top_area_access() {
    return $this->is_admin;
  }

  function get_member_area_access() {
    if(!$this->is_admin) {
      $sql = "SELECT member_id FROM red_map_user_member ".
        "WHERE login = @user_name AND status = 'active'";
      $result = red_sql_query($sql, ['@user_name' => $this->user_name]);
      if(red_sql_num_rows($result) == 0) return false;
    }
    return true;
  }

  function get_hosting_order_area_access() {
    if($this->is_admin) return true;
    $sql = "SELECT hosting_order_id FROM red_item_hosting_order_access ".
      "JOIN red_item USING(item_id) WHERE hosting_order_access_login = ".
      "@user_name AND item_status = 'active'";
    $result = red_sql_query($sql, ['@user_name' => $this->user_name]);
    if(red_sql_num_rows($result) == 0) return false;
    return true;
  }

  function get_table_row_for_single_object(&$object, $row_number) {
    $table_row = array();

    if(!$object) {
      red_set_message(red_t("Failed to create single object when listing!"),'error');
      return false;
    }

    $object->set_html_generator($this->html_generator);
    $key_field_name = $object->get_key_field();
    $func = 'get_' . $key_field_name;
    $id = $object->$func();
    if ($row_number == 0)  {
      $row = $object->get_enumerate_header_block().
        $this->html_generator->get_table_cells("&nbsp;");
      $table_row[] = $this->html_generator->get_table_row($row);
    }
    $disabled_by_admin = 0;
    $disabled_by_hosting_order = 0;
    $service_name = red_get_service_name_for_service_id($this->service_id);
    if ($this->area == 'hosting_order') {
      $status = $object->get_item_status();
      $disabled_by_hosting_order = $object->get_item_disabled_by_hosting_order();
    } 
    elseif ($this->area == 'member' && $service_name == 'hosting_order') {
      $status = $object->get_hosting_order_status();
      $disabled_by_admin = $object->get_hosting_order_disabled_by_admin();
    } 
    else {
      $status = NULL;
    }
      
    $links = $this->get_list_links($key_field_name,$id, $status);  
    $row = $object->get_enumerate_data_block() . 
      $this->html_generator->get_table_cells($links, array('class' => 'red-table-links'));
    $attr = array('class' => 'red-row-even');
    if(!is_int($row_number/2)) {
      $attr = array('class' => 'red-row-odd');
    }
    if(!is_null($status)) {
      $attr['class'] .= ' red-status-' . $status;
    }
    if (method_exists($object, 'is_abandoned')) {
      if ($object->is_abandoned()) {
        $attr['class'] .= ' red-status-abandoned';
      }
    }
    if ($disabled_by_admin) {
      $attr['class'] .= ' red-status-disabled-by-admin';
    }
    if ($disabled_by_hosting_order) {
      $attr['class'] .= ' red-status-disabled-by-hosting-order';
    }
    $table_row[] = $this->html_generator->get_table_row($row, $attr);
    return $table_row;
  }

  function get_list_links($key_field_name, $id, $status = null) {
    $links = '';
    if (preg_match('/^pending-/', $status) || $status == 'hard-error') {
      // No actions are possible on pending or hard-erroritems.
      return '';
    }
    if($status == 'disabled') {
      $links .= $this->get_list_enable_link($key_field_name,$id);  
      $links .= $this->get_list_edit_link($key_field_name,$id);
    }
    elseif($status == 'active') {
      $links .= $this->get_list_disable_link($key_field_name,$id);  
      $class_name = $this->service_table;
      $links .= $this->get_list_edit_link($key_field_name,$id);
      if (class_exists($class_name) && $class_name::$track_disk_usage) {
        $links .= $this->get_list_recalculate_disk_usage_link($key_field_name, $id);
      }
    }
    elseif($status == 'soft-error') {
      $links .= $this->get_list_disable_link($key_field_name,$id);  
      $links .= $this->get_list_edit_link($key_field_name,$id);
    }

    $links .=  $this->get_list_delete_link($key_field_name,$id);
    return $links;
  }

  function get_list_edit_link($key_field_name,$id) {
    $edit_url = $this->template->constructed_self_url . 
      "&amp;action=edit&amp;$key_field_name=".$id;
    $edit_attr = array('href' => $edit_url, 'class' => 'btn btn-xs btn-info');
    return $this->html_generator->get_tag('a', red_t('edit'),$edit_attr);
  }

  function get_list_delete_link($key_field_name,$id) {
    $del_url = $this->template->constructed_self_url . 
      "&amp;action=delete&amp;$key_field_name=".$id;
    $del_attr = array('href' => $del_url, 'class' => 'btn btn-xs btn-danger');
    return $this->html_generator->get_tag('a',red_t('delete'),$del_attr);
  }
  function get_list_disable_link($key_field_name,$id) {
    $dis_url = $this->template->constructed_self_url . 
      "&amp;action=disable&amp;$key_field_name=".$id;
    $dis_attr = array('href' => $dis_url, 'class' => 'btn btn-xs btn-warning');
    return $this->html_generator->get_tag('a', red_t('disable'),$dis_attr);
  }
  function get_list_enable_link($key_field_name,$id) {
    $en_url = $this->template->constructed_self_url . 
      "&amp;action=enable&amp;$key_field_name=".$id;
    $en_attr = array('href' => $en_url, 'class' => 'btn btn-xs btn-success');
    return $this->html_generator->get_tag('a', red_t('enable'),$en_attr);
  }
  function get_list_recalculate_disk_usage_link($key_field_name,$id) {
    $en_url = $this->template->constructed_self_url . 
      "&amp;action=recalculate&amp;$key_field_name=".$id;
    $en_attr = array('href' => $en_url, 
      'class' => 'btn btn-xs btn-success',
      'title' => red_t("Recalculate disk usage")
    );
    return $this->html_generator->get_tag('a', red_t('recalculate'),$en_attr);
  }
  function get_list_view_link($area,$key_field_name,$id) {
    $url_parts = $this->template->self_url_parts;
    $url_parts['area'] = $area;
    unset($url_parts['service_id']);
    $url_parts[$key_field_name] = $id;
    $url = $this->template->get_constructed_self_url($url_parts);
    $class = 'btn btn-xs btn-info';
    $attr = array('href' => $url, 'class' => $class);
    return $this->html_generator->get_tag('a','view',$attr);
  }

  function get_session_id() {
    return session_id();
  }

  function get_available_services() {
    $sql = "SELECT
        red_service.service_id, service_label
      FROM red_service
      WHERE service_area = @area AND
        service_status = 'active'";
    if ($this->get_member_benefits_level() == 'basic') {
      $sql .= " AND service_basic_access != 'none'";
    }
    else {
      $sql .= " AND service_standard_access != 'none'";
    }
    $sql .= " ORDER BY service_display_order";
    $result = red_sql_query($sql, ['@area' => $this->area]);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $id = $row[0];
      $name = $row[1];
      $ret[$id] = $name;
    }
    return $ret;
  }

  function get_filter_where($service_table,$keywords) {
    $ret = array();
    // create dummy object to get search fields
    $dummy_co = ['mode' => 'ui'];
    $dummy_co['service_id'] = $this->service_id;
    $dummy_co[$this->unit_key_field] = $this->unit_id;
    $dummy_item = $this->get_single_object($dummy_co);
    $is_numeric = false;
    if(is_numeric($keywords)) {
      $is_numeric = true;
      $keywords = intval($keywords);
    }
    $keywords = addslashes($keywords);
    foreach($dummy_item->_datafields as $k => $v) {
      if(array_key_exists('filter',$v) && $v['filter'] === true) {
        if($is_numeric && $v['type'] == 'int') {
          $ret[] = "$k = $keywords";
        } elseif($is_numeric && $v['type'] == 'varchar') {
          $ret[] = "$k = '$keywords'";
        } elseif(!$is_numeric && $v['type'] == 'varchar') {
          $ret[] = "$k LIKE '%$keywords%'";
        }
      }
    }
    return implode(' OR ',$ret);
  }
  function get_list_sql($start = 0, $limit = 50) {
    $service_table = red_get_service_table_for_service_id($this->service_id);
    $hosting_order_id = intval($this->unit_id);
    $order_by = '';
    if ($this->order_by_fields) {
      foreach ($this->order_by_fields as $field) {
        // This will trigger an error if any illegal characters are present.
        red_escape_field_or_table_name($field, 'order by red area');
      }
      $order_by = "ORDER BY " . implode(',', $this->order_by_fields);
    }
    $service_id = intval($this->service_id);
    $filter = '';
    if (!empty($this->filter_keywords)) {
      $filter_clause = trim($this->get_filter_where($service_table, $this->filter_keywords));
      if ($filter_clause) {
        $filter = " AND (" . $filter_clause . ")";
      }
    }
    $sql = "SELECT * FROM red_item INNER JOIN $service_table ".
      "ON red_item.item_id = $service_table.item_id ".
      "WHERE hosting_order_id = $hosting_order_id AND ".
      "red_item.service_id = $service_id ".
      "AND red_item.item_status != 'deleted' $filter $order_by LIMIT $start, $limit";

    return $sql;
  }

  function check_unit_access() {
    // first make sure they have access to the unit (unit_id)
    if(count($this->available_units) == 0) {
      $this->errors[4] = "You do not have access to any units.";
      return false;
    }
    if(!array_key_exists($this->unit_id,$this->available_units)) {
      $this->errors[1] = red_t("You do not have access to the unit with id @unit_id", array('@unit_id' => $this->unit_id));
      return false;
    }
    return true;
  }

  function get_count_sql($sql) {
    // thanks drupal
    if(preg_match('/ UNION /',$sql)) {
      $count_sql ="SELECT COUNT(*) FROM (" . preg_replace('/LIMIT .*$/','', $sql) . ') AS count';
    } else {
      // thanks drupal
      $count_sql = preg_replace(array('/SELECT.*?FROM /', '/ORDER BY [^)]*/','/LIMIT [^)]*/'), array('SELECT COUNT(*) FROM ', '',''), $sql);
    }
    //echo "$count_sql";
    return $count_sql;
  }

  function get_content($start = 0, $limit = 50) {
    // Now display the existing items
    $sql = $this->get_list_sql($start, $limit);
    if(empty($sql)) return '';
    $result = red_sql_query($sql);
    $list_co = ['mode' => 'ui']; 
    $row_number = 0;
    $table_row = array();
    while($row = red_sql_fetch_assoc($result)) {
      $list_co['rs'] =& $row;
      $list_item = $this->get_single_object($list_co);
      $ret = $this->get_table_row_for_single_object($list_item, $row_number);
      if(!$ret) return $ret;
      $table_row = array_merge($table_row,$ret);
      $row_number++;
    }
    $table_data = implode("\n",$table_row);
    $count_sql = $this->get_count_sql($sql);
    $result = red_sql_query($count_sql);
    $row = red_sql_fetch_row($result);
    $count = $row[0] ?? 0;
    $search_keywords = '';
    if(property_exists($this,'search_keywords')) $search_keywords = $this->search_keywords;
    $this->template->set_pager_block($count,$start,$limit,$search_keywords);
    return $this->html_generator->get_table($table_data,array('id' => 'red-items-table','class' => 'table table-striped red-table')); 
  }

  function get_item_error_row($item_id) {
    $sql = "SELECT error_log_message,error_log_datetime FROM red_error_log ".
      "WHERE item_id = #item_id ORDER BY error_log_id DESC LIMIT 25 ";
    $result = red_sql_query($sql, ['#item_id' => $item_id]);
    $table_rows = array();
    $error_data = '';
    while($error_row = red_sql_fetch_row($result)) {
      $span_attr = array('class' => 'red_error');
      $data = $this->html_generator->get_tag('span','Error!',$span_attr);
      $data .= ' ' . $error_row[1] . ' ' . 
        $error_row[0];
      $error_data .= $this->html_generator->get_tag('li',$data);
    }
    $ul_attr = array('class' => 'red_error_list');
    $error_data = $this->html_generator->get_tag('ul',$error_data,$ul_attr);
    $cell_attr = array('colspan' => '10');
    $cell = $this->html_generator->get_table_cells($error_data,$cell_attr);
    return $this->html_generator->get_table_row($cell);
  }

  function edit_child($id = false) {
    // When showing the form to edit a child, hide the links to create a new
    // child to avoid confusing the user.
    $this->template->set_var('hide_or_show_items_nav_links', 'hide');

    // $this->invalid_service_object will be populated if
    // we submitted a change that had a validation error.
    // this ensures that the user doesn't lose the values
    // they entered in the form.
    if(!empty($this->invalid_service_object)) {
      $edit =& $this->invalid_service_object;
    } else {
      // otherwise built it from the db values.
      $edit_co = ['mode' => 'ui'];
      $edit_co['id'] = $id;
      $edit_co['service_id'] = $this->service_id;
      $edit = $this->get_single_object($edit_co);
    }
      
    if(!$edit) {
      $message = red_t("Failed to create object for editing. Please check the ".
                 "red_error_log table for details.");
      red_set_message($message,'error');
    }
    else {
      $js_includes = $edit->get_javascript_includes();
      $this->template->add_js($js_includes);
      $edit->set_html_generator($this->html_generator);
      $this->template->set_file('edit_file','edit.ihtml');
      $this->template->set_var('form_name', "{$this->service_name}-form");
      $this->template->set_var('edit_block',$edit->get_edit_block());
      $this->template->set_var('session_id',$this->get_session_id());
      $this->template->parse('edit_block','edit_file');
    }
    // Don't show filter option on edit - it clutters the ui
    $this->show_filter_form = false;

  }

  function print_child($id = false) {
    if(!$id) {
      $message = red_t("Failed to create object for printing. No id passed.");
      red_set_message($message,'error');
      return;
    }
    $co = ['mode' => 'ui'];
    $co['id'] = $id;
    $co['service_id'] = $this->service_id;
    $obj =& $this->get_single_object($co);
      
    if(!$obj) {
      $message = red_t("Failed to create object for printing. Please check the ".
                 "red_error_log table for details.");
      red_set_message($message,'error');
    }
    else {
      $obj->set_html_generator($this->html_generator);
      echo $obj->printme();
      exit;
    }
  }
  function write_child($passed_session_id) {
    $standard_construction_options = ['mode' => 'ui'];
    $write_co = $standard_construction_options;
    $write_co['service_id'] = $this->service_id;

    $write_co[$this->unit_key_field] = $this->unit_id;

    $write_item = $this->get_single_object($write_co);
    $write_item->set_html_generator($this->html_generator);

    if(!$write_item) {
      $message = red_t("Failed to create a red object when trying to write: @error_message",
                 array('@error_message' => $write_item->get_errors_as_string()));
      red_set_message($message,'error');
      return false;
    }

    $post = $_POST;
    // check for valid session
    if(empty($passed_session_id) || $passed_session_id != $this->get_session_id()) {
      red_set_message(red_t("Missing or invalid session id. You are either experiencing a bug or someting sneaky is going on. If you really want to edit/insert this record, please try again. Otherwise, please contact your system administrator."),'error');
      $this->invalid_service_object = $write_item;
      return false;
    }
      
    $write_item->set_user_input($post);

    $this->post_user_input_modifications($write_item);

    // If the user makes a input error - return this item as the 
    // edit_item. with these values filled in
    if(!$write_item->validate()) {
      red_set_message($write_item->get_errors('validation'),'error');
      $this->invalid_service_object = $write_item;
      return false;
    }
    elseif(!$write_item->commit_to_db())  {
      red_set_message($write_item->get_errors('system'),'error');
      $this->invalid_service_object = $write_item;
      return false;
    }
    red_set_message(red_t('Record saved.'),'success');

    // Surprise, it all worked out
    // redirect to a non-write URL so if the user hits reload
    // on their browser it won't repeat the write action
    $redirect = $this->template->get_constructed_self_url(null,'raw');
    header("Location: $redirect");
    exit;
  }

  function post_user_input_modifications(&$obj) {
    // overrid

  }

  function new_child() {
    // When showing the form to create a new child, hide the links to create a new
    // child to avoid confusing the user.
    $this->template->set_var('hide_or_show_items_nav_links', 'hide');
    $standard_construction_options = ['mode' => 'ui'];
    $new_co = $standard_construction_options;
    $new_co['service_id'] = $this->service_id;
    $unit_key_field = $this->unit_key_field;
    $new_co[$unit_key_field] = $this->unit_id;
    $new_item = $this->get_single_object($new_co);
    $js_includes = $new_item->get_javascript_includes();
    $this->template->add_js($js_includes);

    if(!$new_item) {
      $message = red_t("Failed to create object for editing. Please check the ".
                 "red_error_log table for details.");
      red_set_message($message,'error');
      return false;
    }
    else {
      $new_item->set_html_generator($this->html_generator);
      $this->template->set_file('edit_file','edit.ihtml');
      $this->template->set_var('form_name', "{$this->service_name}-form");
      $this->template->set_var('edit_block',$new_item->get_edit_block());
      $this->template->set_var('session_id',$this->get_session_id());
      $this->template->parse('edit_block','edit_file');
    }
    // Don't show filter option on new - it clutters the ui
    $this->show_filter_form = false;
    return true;
  }

  function delete_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable_or_recalculate($id, $session_id, $confirm, $cancel, 'delete');
  }
  function disable_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable_or_recalculate($id, $session_id, $confirm, $cancel, 'disable');
  }
  function enable_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable_or_recalculate($id, $session_id, $confirm, $cancel, 'enable');
  }
  function recalculate_child($id,$session_id,$confirm, $cancel) {
    return $this->delete_or_disable_or_enable_or_recalculate($id, $session_id, $confirm, $cancel, 'recalculate');
  }
  function delete_or_disable_or_enable_or_recalculate($id, $session_id, $confirm, $cancel, $action ) {
    $co = ['mode' => 'ui'];

    if(empty($id)) {
      throw new RedException(red_t("You can't do this action without passing an id."),E_USER_ERROR);
    }
    $co['id'] = $id;
    $co['service_id'] = $this->service_id;
    $item = $this->get_single_object($co);

    if(!$item) {
      $message = red_t("Failed to create a red object. ".
                 "Please check the red_error_log table for details.");
      red_set_message($message,'error');
      return false;
    } 
    else {
      // make sure we're not deleting or disabling a resource providing our login
      if($action == 'disable' || $action == 'delete') {
        // hosting order
        $borking_yourself = false;
        $service_name = red_get_service_name_for_service_id($this->service_id);
        if ($service_name == 'hosting_order') {
          $sql = "SELECT hosting_order_id FROM red_item JOIN red_item_user_account " .
            "WHERE item_status = 'active' AND ".
            "user_account_login = @user_name"; 
          $result = red_sql_query($sql, ['@user_name' => $this->authz->get_user_name()]);
          $row = red_sql_fetch_row($result);
          if($row[0] == $item->get_hosting_order_id()) {
            $borking_yourself = true;
          }
        } elseif ($service_name == 'user_account') {
          // user account
          if($item->get_user_account_login() == $this->user_name) {
            $borking_yourself = true;
          }
        }
        if($borking_yourself) {
          red_set_message(red_t("You cannot @action a record providing your current login.", array('@action' => $action)));
          return false;
        }
      }
      $item->set_html_generator($this->html_generator);
      if($action == 'delete') {
        $confirm_message = $item->get_delete_confirmation_message();
      } elseif($action == 'disable') {
        $confirm_message = red_t("Are you sure you want to disable this record?");
      } elseif($action == 'enable') {
        $confirm_message = red_t("Are you sure you want to enable this record?");
      } elseif($action == 'recalculate') {
        $confirm_message = red_t("Are you sure you want to immediately recalculate the disk usage for this record? (Disk usage is automatically calculated every 15 minutes.)");
      }

      if(!$cancel && !$confirm && !is_null($confirm_message)) {
        $this->template->display_confirm_message($id,$this->service_key_field,$confirm_message,$this->get_session_id(), $action);
      } elseif($session_id != $this->get_session_id()) {
        red_set_message(red_t("Missing or invalid session id. You are either experiencing a bug or someting sneaky is going on. If you really want to edit this record, please try again. Otherwise, please contact your system administrator."),'error');

      }
      elseif($cancel) {
        red_set_message(red_t('The record was not affected.'),'info');
      } else {
        if($action == 'delete') {
          $item->set_delete_flag();
          $msg = red_t("Record deleted.");
        } elseif ($action == 'disable') {
          $item->set_disable_flag();
          $msg = red_t("Record disabled.");
        } elseif ($action == 'enable') {
          $item->unset_disable_flag();
          $msg = red_t("Record enabled.");
        } elseif ($action == 'recalculate') {
          // No action is taken to recalculate - just resave.
          $msg = red_t("Record recalculating.");
        }
        if(!$item->validate()) {
          red_set_message($item->get_errors('validation'),'error');
          return false;
        } elseif(!$item->commit_to_db()) {
          red_set_message($item->get_errors('system'),'error');
          return false;
        }

        red_set_message($msg,'success');

        // redirect to a non-write URL so if the user hits reload
        // on their browser it won't repeat the delete action
        $redirect = $this->template->get_constructed_self_url(null,'raw');
        header("Location: $redirect");
        exit;
      }
    }
  }

  function get_member_disk_usage() {
    $member_id = intval($this->get_member_id());
    $raw_usage = red_get_disk_usage_for_membership($member_id);
    return red_human_readable_bytes($raw_usage); 
  }
  function get_member_allocated_quota() {
    $member_id = intval($this->get_member_id());
    $raw_quota = red_get_allocated_quota_for_membership($member_id);
    return red_human_readable_bytes($raw_quota); 
  }
  function get_member_quota() {
    $member_id = intval($this->get_member_id());
    $raw_quota = red_get_member_quota($member_id);
    if (intval($raw_quota) == 0) {
      return red_t("Not set");
    }
    return red_human_readable_bytes($raw_quota); 
  }

  function get_member_benefits_level() {
    if (empty($this->unit_id)) {
      return '';
    }
    $sql = "SELECT member_benefits_level FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = #unit_id";
    $result = red_sql_query($sql, ['#unit_id' => $this->unit_id]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? '';
  }

  function get_access_level() {
    if ($this->get_member_benefits_level() == 'basic') {
      return $this->service_basic_access;
    }
    else {
      return $this->service_standard_access;
    }
  }
}
?>
