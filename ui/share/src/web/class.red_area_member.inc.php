<?php
  
class red_area_member extends red_area {
  var $default_service_id = 12; // hosting_order 
  var $unit_friendly_name;
  var $unit_key_field = 'member_id';
  var $unit_friendly_field = 'member_friendly_name';
  var $unit_table = 'red_member';
  var $service_key_field = 'hosting_order_id';

  function __construct($area, $service_id) {
    parent::__construct($area, $service_id);
    $this->unit_friendly_name = red_t('Members');
  }

  function &get_single_object($co) {
    global $globals;

    // See if we are dealing with an "item" (which is lives in red_item)
    // or an object independent of the red_item table.
    $item = NULL;
    $service_id = intval($co['service_id'] ?? 0);
    if ($service_id) {
      $sql = "SELECT service_item FROM red_service WHERE service_id = #service_id";
      $result = red_sql_query($sql, ['#service_id' => $service_id]);
      $row = red_sql_fetch_row($result);
      $item = $row[0] == 1 ? TRUE: FALSE;
    }
    if (!$item) {
      $file_name = 'class.' . str_replace('red_','red.',$this->service_table) . '.inc.php';
      $class_name = $this->service_table;
      $ret = new $class_name($co);
    }
    else {
      $ret =& red_item::get_red_object($co);
      if (!$ret) {
        red_log("Error retrieving object: " . print_r($co, TRUE));
        trigger_error("Sorry, we hit an error!");
      }
      $ret->set_hosting_order_id(0);
    }
      
    // vps need to know if the user is an admin or not
    if(property_exists($ret, 'is_admin')) {
      $ret->is_admin = $this->is_admin;
    }

    return $ret;
  }

  function get_tags() {
    $member_id = $this->get_unit_id();
    $sql = "SELECT tag_id, tag FROM red_tag WHERE member_id = #member_id AND tag_status = 'active'";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $tag_id = $row[0];
      $tag = $row[1];
      $ret[$tag_id] = $tag;
    }
    return $ret;
  }

  function get_area_navigation_items($top,$member,$hosting_order) {
    $ret = array();
      
    $items = array();
    if($top) {
      $items['top'] = array(
        'friendly' => 'Top',
      );
    }
    if($member) {
      $edit = null;
      if($this->is_admin) {
        $attr = array('href' => '?area=top&action=edit&member_id=' .  
                $this->get_unit_id(), 'class' => 'button tiny round');
        $content = red_t("edit");
        $edit = $this->html_generator->get_tag('a',$content,$attr);
        $edit = ' ' . $this->html_generator->get_tag('sup',$edit);
      }
      $items['member'] = array(
        'friendly' => $this->unit_active_friendly_name . $edit, 
        'field' => 'member_id',
        'id' => $this->get_unit_id(),
      );
    }
    if($hosting_order) {
      $items['hosting_order'] = array(
        'friendly' => $this->get_first_hosting_order_friendly_name(),
        'field' => 'hosting_order_id',
        'id' => $this->get_first_hosting_order_id(),
      );
    }

    return $items;
  }

  function get_first_hosting_order_friendly_name() {
    $sql = "SELECT hosting_order_name FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE member_id = #member_id AND " .
      "(hosting_order_status = 'active' OR hosting_order_status = 'disabled') ORDER BY hosting_order_identifier LIMIT 1";
    $result = red_sql_query($sql, ['#member_id' => $this->get_member_id()]);
    $row = red_sql_fetch_row($result);
    if(empty($row[0])) return 'Hosting Orders';
    return $row[0];
  }

  function get_first_hosting_order_id() {
    $sql = "SELECT red_hosting_order.hosting_order_id FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE member_id = #member_id AND " .
      "(hosting_order_status = 'active' OR hosting_order_status = 'disabled') ORDER BY hosting_order_identifier LIMIT 1";
    $result = red_sql_query($sql, ['#member_id' => $this->get_member_id()]);
    $row = red_sql_fetch_row($result);
    if(empty($row[0])) return null;
    return $row[0];
  }

  function add_new_item_link() {
    if (in_array($this->get_access_level(), ['none', 'read-only'])) {
      $this->template->set_add_new_item_link('none');
      return;
    }
    parent::add_new_item_link();
  }

  function list_children($start = 0,$limit = 0) {
    $this->prepare_list();
    $this->template->set_file('child_items_file','child_items.ihtml');
    $this->template->set_var('list_block',$this->get_list_block($start,$limit));
    $this->template->parse('children','children_file');
    $this->template->parse('body_block','child_items_file');
  }

  function get_member_id(){
    return $this->unit_id;
  }

  function get_list_sql($start = false, $limit = false) {
    $member_id = $this->get_member_id();
    $order_by = '';
    if ($this->order_by_fields) {
      foreach ($this->order_by_fields as $field) {
        // This will trigger an error if any illegal characters are present.
        red_escape_field_or_table_name($field, 'order by red area member');
      }
      $order_by = "ORDER BY " . implode(',', $this->order_by_fields);
    }
    $status_field = str_replace('_id','_status',$this->service_key_field);
    $status_clause = "AND $status_field = 'active' ";

    $join_table = null;
    $select = $this->service_table . ".*";
    // a few anoying exceptions...
    if($this->service_key_field == 'correspondence_id') {
      $status_clause = '';
    } elseif($this->service_key_field == 'map_user_member_id') {
      $status_clause = "AND status = 'active'";
    } elseif($this->service_key_field == 'hosting_order_id') {
      $status_clause = "AND ($status_field = 'active' OR $status_field = 'disabled') ";
    } elseif($this->service_key_field == 'item_id') {
      $join_table = " JOIN red_item USING(item_id)";
      $select = '*';
      $status_clause = "AND $status_field != 'deleted' ";
    }
    
    $sql = "SELECT " . $select . " FROM " . $this->service_table . $join_table . " " .
      "WHERE member_id = $member_id $status_clause $order_by ".
      "LIMIT $start, $limit";
    return $sql;
  }

  function post_user_input_modifications(&$obj) {
    if(empty($obj->member_id)) {
      $obj->set_member_id($this->unit_id);
    }
  }

  function get_table_row_for_single_object(&$object,$row_number) {
    $table_row = array();

    // call parent to build the row
    $ret = parent::get_table_row_for_single_object($object,$row_number);
    if(!$ret) return $ret;
    return array_merge($ret,$table_row);
  }

  function get_list_links($key_field_name,$id, $status = null) {
    $ret = '';
    if($key_field_name == 'hosting_order_id')
      $ret .= $this->get_list_view_link('hosting_order',$key_field_name,$id);
    $ret .= $this->get_list_edit_link($key_field_name,$id);
    if($status == 'active') {
      $ret .= $this->get_list_disable_link($key_field_name,$id);  
    } elseif($status == 'disabled') {
      $ret .= $this->get_list_enable_link($key_field_name,$id);  
    }
    $ret .= $this->get_list_delete_link($key_field_name,$id);  
    return $ret;
  }

  function get_list_print_link($key_field_name, $id) {
    $edit_url = $this->template->self_url . 
      "?area=reports&amp;$key_field_name=$id";
    $edit_attr = array('href' => $edit_url, 'target' => '_blank', 'class' => 'btn btn-xs btn-info');
    return $this->html_generator->get_tag('a',red_t('print'),$edit_attr);
  }

  function get_list_edit_link($key_field_name,$id) {
    if (in_array($this->get_access_level(), ['none', 'read-only'])) {
      return '';
    }
    if($key_field_name == 'correspondence_id') {
      // convert to a "resend" link  
      $edit_url = $this->template->constructed_self_url . 
        "&amp;action=edit&amp;$key_field_name=".$id;
      $edit_attr = array('href' => $edit_url, 'class' => 'btn btn-xs btn-info');
      return $this->html_generator->get_tag('a',red_t('view/resend'),$edit_attr);
    }

    return parent::get_list_edit_link($key_field_name,$id);
  }

  function get_list_delete_link($key_field_name,$id) {
    if (in_array($this->get_access_level(), ['none', 'read-only'])) {
      return '';
    }
    // nobody can delete correspondence
    if($key_field_name == 'correspondence_id') return ''; 
    return parent::get_list_delete_link($key_field_name,$id);
  }

  function set_available_units() {
    if(!is_null($this->available_units)) return;
    $ret = array();
    $sql = $this->get_available_units_sql();
    $result = red_sql_query($sql);
    if(red_sql_num_rows($result) > 0) {
      $ret = $this->_get_members_for_sql_result($result);    
    }
    $this->available_units = $ret;
  }

  function get_available_units_sql() {
    $is_admin = $this->is_admin;
    $user_name = addslashes($this->user_name);
    $sql_limit = '';
    $extra = '';

    // common sql statement
    $sql_start = "SELECT red_member.member_id,member_friendly_name, ".
      "member_status FROM red_member ";
    $sql_where = "WHERE member_status = 'active' ";

    $sql_access_where = '';
    if (!$is_admin) {
      $sql_limit = "JOIN red_map_user_member USING (member_id) ";
      $sql_access_where = " AND login = '$user_name' ";
    }

    $extra = "ORDER BY member_friendly_name";
    return "$sql_start $sql_limit $sql_where $sql_access_where $extra";

  }

  function _get_members_for_sql_result($result) {
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $id = $row[0];  
      $member_friendly_name = $row[1];  
      $status = $row[2];  
      $ret[$id] = "$member_friendly_name ($status)";
    }
    return $ret;
  }

  function check_access($id = null, $action = null) {
    if (!$this->check_unit_access()) return false;

    // Local DNS, Tags and Notes can only be accessed by admins
    $admin_only_service_ids = array(33);
    if(!$this->is_admin && ($this->service_table == 'red_tag' || $this->service_table == 'red_note' || in_array($this->service_id, $admin_only_service_ids))) {
      red_set_message("You cannot access tags, notes, or local DNS.");
      return false;
    }

    if(is_null($id)) $id = $this->id;

    global $globals;
    $service_name = red_get_service_name_for_service_id($this->service_id);
    if (!empty($action)) {
      // They are trying to write an action. Make sure they have write access.
      if(!$this->authz->check_insert_perms($service_name, $this->unit_id)) {
        return FALSE;
      }
    }
    
    // If they're not checking an id, and they have unit access from above, then
    // let them in. They are just getting a listing. 
    if(empty($id)) {
      return TRUE;
    }

    // Make sure they have read-access to this id.
    // See if this is a service item (e.g. it has a record in the red_item table).
    $service_id = $this->service_id;
    $sql = "SELECT service_item FROM red_service WHERE service_id = #service_id";
    $result = red_sql_query($sql, ['#service_id' => $service_id]);
    $row = red_sql_fetch_row($result);
    $service_item = $row[0];

    $sql = "SELECT member_id FROM " . $this->service_table . " WHERE " . $this->service_key_field . " = #id";
    if ($service_item == 1) {
      $sql = "SELECT member_id FROM " . $this->service_table . " JOIN red_item USING(item_id) WHERE " . $this->service_key_field . " = #id";
    }

    $result = red_sql_query($sql, ['#id' => $id]);
    $row = red_sql_fetch_row($result);
    if($this->unit_id != $row[0]) {
      $this->errors[2] = "There is an error. The " . $this->service_key_field . " ($id) does not seem to match the member id " . $this->unit_id;
      return false;
    }

    return TRUE; 
  }

  function get_member_benefits_level() {
    if (empty($this->unit_id)) {
      return '';
    }
    $sql = "SELECT member_benefits_level FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = #unit_id";
    $result = red_sql_query($sql, ['#unit_id' => $this->unit_id]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? NULL;
  }
}


?>
