<?php
  
class red_area_hosting_order extends red_area {
  var $default_service_id = 1; // user accounts
  var $unit_friendly_name;
  var $unit_key_field = 'hosting_order_id';
  var $unit_friendly_field = 'hosting_order_name';
  var $unit_table = 'red_hosting_order';
  var $service_key_field = 'item_id';

  function __construct($area, $service_id) {
    parent::__construct($area, $service_id);
    $this->unit_friendly_name = red_t('Hosting Orders');
  }

  function get_footer() {
    $footer = '';
    $class_name = $this->service_table;
    if (class_exists($class_name) && $class_name::$track_disk_usage) {
      $msg = '<sup>&dagger;</sup>' . red_t("Disk usage is updated every 15 minutes. Click the recalculate button to update the disk usage immediately.");
      if ($class_name == 'red_item_user_account') {
        // Check to see if this hosting order is hosted on a vps. If so we add a note to let them know
        // their disk allocations won't count against their membeship totals - that's because their
        // vps total allocation is what really counts, not their individual allocation.
        if (red_hosting_order_on_vps($this->unit_id)) {
          $msg .= red_t(" Note: this hosting order is on a virtual private server, so quota allocations do not contribute to your membership-wide quota.");
        }
      }
      $attr = array('class' => 'red-item-description');
      $footer .= $this->html_generator->get_tag('div',$msg, $attr);
    }
    if ($class_name == 'red_item_nextcloud' || $class_name == 'red_item_mailbox') {
      $msg = red_t("Items in grey indicate that the item has not been logged into for over 6 months.");
      $attr = array('class' => 'red-item-description red-status-abandoned');
      $footer .= $this->html_generator->get_tag('div',$msg, $attr);
    }
    return $footer;
  }

  function get_primary_host() {
    $sql = "SELECT hosting_order_host FROM red_hosting_order WHERE ".
      "hosting_order_id = #unit_id";
    $result = red_sql_query($sql, ['#unit_id' => $this->unit_id]);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  function get_tags() {
    $member_id = intval($this->get_member_id());
    $sql = "SELECT tag_id, tag FROM red_tag WHERE member_id = #member_id AND tag_status = 'active'";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $tag_id = $row[0];
      $tag = $row[1];
      $ret[$tag_id] = $tag;
    }
    return $ret;
  }

  function get_member_friendly_name() {
    $sql = "SELECT member_friendly_name FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = #unit_id AND " .
      "member_status = 'active'";
    $result = red_sql_query($sql, ['#unit_id' => $this->unit_id]);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  function get_member_id() {
    $sql = "SELECT member_id FROM red_member JOIN red_hosting_order " .
      "USING(member_id) WHERE hosting_order_id = #unit_id AND " .
      "member_status = 'active'";
    $result = red_sql_query($sql, ['#unit_id' => $this->unit_id]);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  
  function get_area_navigation_items($top,$member,$hosting_order) {
    $items = array();
    if($top) {
      $items['top'] = array(
        'friendly' => 'Top',
      );
    }
    if($member) {
      $items['member'] = array(
        'friendly' => $this->get_member_friendly_name(), 
        'field' => 'member_id',
        'id' => $this->get_member_id(),
      );
    }
    if($hosting_order) {
      $items['hosting_order'] = array(
        'friendly' => $this->unit_active_friendly_name,
        'field' => 'hosting_order_id',
        'id' => $this->get_unit_id(),
      );
    }

    return $items;
  }

  function list_children($start = 0, $limit = 0) {
    $this->prepare_list();
    $this->template->set_file('child_items_file','child_items.ihtml');
    $this->template->set_file('area_navigation_file','area_navigation.ihtml');
    $this->template->set_var('lang_primary_host_is',red_t("Hosting order host is:"));
    $this->template->set_var('lang_primary_email_address_is',red_t("Your primary email address is:"));
    $this->template->set_var('primary_user_account_login',$this->get_primary_user_account_login());
    $primary_host = $this->get_primary_host();
    $this->template->set_var('primary_host', $primary_host);
    $this->template->set_var('lang_allocated_this_hosting_order', red_t("Disk quota allocated (this hosting order):"));
    $this->template->set_var('lang_hosting_order_disk_usage_is', red_t("Disk usage (this hosting order):"));
    $this->template->set_var('hosting_order_allocated_quota', $this->get_hosting_order_allocated_quota());
    $this->template->set_var('hosting_order_disk_usage', $this->get_hosting_order_disk_usage());
    
    $this->template->set_var('list_block', $this->get_list_block($start,$limit));
    $this->template->parse('children', 'children_file');
    $this->template->parse('body_block', 'child_items_file');
  }

  function get_hosting_order_disk_usage() {
    $raw = red_get_disk_usage_for_membership($this->get_member_id(), $this->unit_id);
    return red_human_readable_bytes($raw);
  }
  function get_hosting_order_allocated_quota() {
    $raw = red_get_allocated_quota_for_membership($this->get_member_id(), $this->unit_id);
    return red_human_readable_bytes($raw);
  }

  // For basic memberships, we should have only one user account login.
  // This function fetches it.
  function get_primary_user_account_login() {
    $hosting_order_id = intval($this->unit_id);
    $sql = "SELECT user_account_login FROM red_item JOIN red_item_user_account USING(item_id) ".
      "WHERE item_status = 'active' AND hosting_order_id = #hosting_order_id LIMIT 1";
    $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? NULL;
  }

  function get_table_row_for_single_object(&$object,$row_number) {
    $table_row = array();

    $status = $object->get_item_status();
    $hosting_order_id = intval($object->get_hosting_order_id());
    $sql = "SELECT hosting_order_disabled_by_admin FROM red_hosting_order WHERE hosting_order_id = #hosting_order_id";
    $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
    $admin_disabled_row = red_sql_fetch_row($result);

    // If an item is disabled by a user, we don't count the full allocated quota against them.
    if ($status == 'disabled' && $admin_disabled_row[0] == 0) {
      $object->set_item_quota($object->get_item_disk_usage());
    }
    $ret = parent::get_table_row_for_single_object($object,$row_number);
    if(!$ret) return $ret;

    $key_field = '_' . $object->_key_field;
    if(preg_match('/.*error$/',$status) > 0) {
      $table_row[] = $this->get_item_error_row($object->$key_field);
    }

    return array_merge($ret,$table_row);
  }

  function get_single_object($co) {
    $ret = red_item::get_red_object($co);
    if(property_exists($ret, 'is_admin')) {
      $ret->is_admin = $this->is_admin;
    }
    return $ret;
  }

  function set_available_units() {
    if (!is_null($this->available_units)) return;
    $is_admin = $this->is_admin;

    $ret = array();
    $user_name = $this->user_name;
    $params = [];

    // get list of all hosting orders

    // admin sql statement
    $sql_select ="SELECT member_friendly_name,".
      "red_hosting_order.hosting_order_id,hosting_order_identifier,".
      "hosting_order_status ";

    $sql_from = "FROM red_hosting_order JOIN red_member USING ".
      "(member_id) ";

    $sql_member_union = '';

    $sql_where = "WHERE (hosting_order_status = 'active' OR hosting_order_status = 'disabled') ";

    $sql_join = '';
    $sql_extra = '';
    $sql_access_where = '';
    if(!$is_admin)  {
      $params['@user_name'] = $user_name;
      $sql_join = "JOIN red_item USING(hosting_order_id) JOIN ".
        "red_item_hosting_order_access USING(item_id) ";
      $sql_access_where = "AND hosting_order_access_login = @user_name AND ".
        "red_item.item_status != 'deleted' AND red_item.item_status != 'disabled'";

      // build additional sql statement to pull in via member access - if you have access to a 
      // member, you should have access to all hosting orders coded to that member regardless of
      // whether you have explicit hosting order access
      $sql_join_member = "JOIN red_map_user_member USING(member_id) ";
      $sql_access_where_member = "$sql_where AND login = @user_name AND status = 'active'";
      $sql_member_union = "UNION ($sql_select $sql_from $sql_join_member $sql_access_where_member) ";
    }

    $sql_extra = "ORDER BY member_friendly_name,hosting_order_identifier ";

    $sql = '(' . $sql_select . $sql_from . $sql_join . $sql_where . 
      $sql_access_where .  ') ' . $sql_member_union . $sql_extra;
    $result = red_sql_query($sql, $params);
    if(red_sql_num_rows($result) > 0) {
      $ret = $this->_get_hosting_orders_for_sql_result($result);    
    }
    $this->available_units = $ret;
  }

  function _get_hosting_orders_for_sql_result($result) {
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $member_friendly_name = $row[0];  
      $id = $row[1];  
      $identifier = $row[2];  
      $status = $row[3];  
      $ret[$id] = "$member_friendly_name: $identifier ($status)";
    }
    return $ret;
  }

  function check_access($id = null, $action = null) {
    if (!$this->check_unit_access()) return false;

    if (is_null($id)) {
      $id = $this->id;
    }

    $service_name =  red_get_service_name_for_service_id($this->service_id);
    if (!empty($action)) {
      // They are trying to write an action. Make sure they have write access.
      if (!$this->authz->check_insert_perms($service_name, $this->unit_id, $id)) {
        // The only exception is if they are editing their own user account record.
        if ($service_name == 'user_account' && $id == $this->authz->get_user_name_item_id()) {
          return TRUE;
        } 
        return FALSE;
      }
    }

    // if they're not checking an id, return true
    if (empty($id)) {
      return TRUE;
    }

    // make sure this id reallly belongs to the hosting order
    $sql = "SELECT hosting_order_id FROM red_item WHERE item_id = #id";
    $result = red_sql_query($sql, ['#id' => $id]);
    $row = red_sql_fetch_row($result);
    if($this->unit_id != $row[0]) {
      $msg = red_t("There is an error. The item id @id does not seem to match the " .
             "hosting order id @unit_id",array('@id' => $id, '@unit_id' => $this->unit_id));
      $this->errors[2] = $msg;
      return FALSE;
    }
    return TRUE;
  }
    
  function get_list_edit_link($key_field_name,$id) {
    // rename edit link for password reset
    $service_name = red_get_service_name_for_service_id($this->service_id);
    if ($service_name == 'password_reset') {
      $edit_url = $this->template->constructed_self_url . 
        "&amp;action=edit&amp;$key_field_name=".$id;
      $edit_attr = array('href' => $edit_url, 'class' => 'btn btn-primary btn-xs');
      return $this->html_generator->get_tag('a','resend',$edit_attr);
    }
    return parent::get_list_edit_link($key_field_name,$id);
  }  
}


?>
