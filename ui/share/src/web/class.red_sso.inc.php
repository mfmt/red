<?php

class red_sso {
  // Received from discourse
  private $sso = NULL;
  private $sig = NULL;
  // Should come from config file.
  private $sso_secret = NULL;
  // URL to discourse
  private $redirect_url = NULL;
  // Logged in user name.
  private $user_name = NULL;
  private $item_id = NULL;
  private $sso_confirm = NULL;
  private $sso_email_address = NULL;
  private $sso_email_address_other = NULL;
  private $template = NULL;
  private $html_generator = NULL;

  function set_template($template) {
    $this->template = $template;
  }
  function set_html_generator($html_generator) {
    $this->html_generator = $html_generator;
  }
  function set_user_name($user_name) {
    $this->user_name = $user_name;
  }
  function set_item_id($item_id) {
    $this->item_id = $item_id; 
  }
  function set_sso_secret($secret) {
    $this->sso_secret = $secret;
  }
  function set_redirect_url($url) {
    $this->redirect_url = $url;
  }
  function set_user_input($user_input) {
    $possible_values = array(
      'sso',
      'sig',
      'sso_confirm',
      'sso_email_address',
      'sso_email_address_other',
    );
    foreach ($possible_values as $value) {
      if (array_key_exists($value, $user_input)) {
        $this->$value = $user_input[$value];
      }
    }
  }

  function run() {
    $this->template->self_url_parts['sso'] = $this->sso;
    $this->template->self_url_parts['sig'] = $this->sig;
    // Ensure we have a user name and a item_id.
    if (empty($this->user_name)) {
      red_set_message(red_t("Failed to get your username."));
      return FALSE;
    }
    if (empty($this->item_id)) {
      red_set_message(red_t("Failed to get your user id. Is your account disabled?"));
      return FALSE;
    }

 		// validate sso
    $sso = urldecode($this->sso);
    if(hash_hmac('sha256', $sso, $this->sso_secret) !== $this->sig){
      red_set_message(red_t("Failed to validate SSO request."), 'error');
      //return FALSE;  
    }

    // extract the nonce
    $sso = base64_decode($sso);
    $query = array();
    parse_str($sso, $query);

    if (!array_key_exists('nonce', $query)) {
      red_set_message(red_t("Failed to find the nonce."), 'error');
      return FALSE;  
    }

    $nonce = $query['nonce'];

    // If they have not confirmed, prompt them to confirm.
    if ($this->sso_confirm != 'accept') {
      $this->confirm();  
      return;
    }
  
    if ($this->sso_email_address == 'other') {
      if (empty($this->sso_email_address_other)) {
        red_set_message("Please enter an alternative email address.");
        $this->confirm();
        return;
      }
      $this->send_confirmation_email();
      // Don't show confirm screen again, it confuses people who
      // think that something didn't work properly.
      return;
    }

    $email = $this->sso_email_address;
    // Ensure no shenanigans...
    $emails = $this->get_confirmed_email_addresses();
    if (!in_array($email, $emails)) {
      red_set_message("Something went wrong. It appears you selected an email address that is not confirmed as belonging t you.", 'error');
      $this->confirm();
      return;
    }
      
    // Redirect back to discourse...
    $payload =  base64_encode(
      http_build_query(
        array(
          'username' => $this->user_name,
          'external_id' => $this->item_id,
          'email' => $email,
          'nonce' => $nonce,
        )
      )
    );

    $redirect = array(
      'sso' => $payload,
      'sig' => hash_hmac('sha256', $payload, $this->sso_secret),
    );

    $redirect_query = http_build_query($redirect);

    header("Location: " . $this->redirect_url . '?' . $redirect_query);
  }

  function send_confirmation_email() {
    global $globals;
    $message = red_t("Someone (hopefully you) is trying to confirm the use of your email address to login to the May First Movement Technology control panel. If this is you, please click the link below to confirm.");
    $message = wordwrap($message) . "\n\n";

    $co = [];
    // Check for existing record.
    $rs = red_email_verify::get_existing($this->sso_email_address_other, $this->item_id);
    if (!empty($rs)) {
      // We have an existing record.
      $co['rs'] = $rs;
    }

    $email_verify = new red_email_verify($co);

    // See if we have already confirmed this one.
    if ($email_verify->get_email_verify_status() == "1") {
      red_set_message(red_t("This email has already been verified. Please select it from the list rather than typing it in.", 'error'));
      return FALSE;
    }
    $email_verify->set_email_verify_status(0);
    $hash = $email_verify->get_email_verify_hash();
    if (!empty($hash)) {
      red_set_message(red_t("An email verification has been sent already. I'm sending a new one now."));
      // Update the expiration.
      $email_verify->autogenerate_expiration();
    }
    else {
      $email_verify->autogenerate();
      $email_verify->set_item_id($this->item_id);
      $email_verify->set_email_verify_address($this->sso_email_address_other);
    }
    if (!$email_verify->commit_to_db()) {
      red_set_message("There was an error saving your email verification record.", 'error');
      red_set_message($email_verify->get_errors_as_string(), 'error');
      return FALSE;
    }

    $message .= 'https://' . $_SERVER['SERVER_NAME'] . '/' . 
      $globals['config']['url_path'] . 
      '?area=sso&sso=' . $this->sso . '&sig=' . $this->sig .
      '&email_verify_hash=' . $email_verify->get_email_verify_hash();
    if(!mail($this->sso_email_address_other, red_t("May First email verification"), $message, "From: " . "May First <members@mayfirst.org>")) {
      $this->set_error(red_t("Failed to send confirmation email."),'system');
      return false;
    }
    red_set_message(red_t("A confirmation email has been sent. Please click through the link sent to your email address to continue the process."));
    return TRUE;
  }

  function get_confirmed_email_addresses() {
    global $globals;
    $ret = array();
    $sql = "SELECT email_address FROM red_item JOIN red_item_email_address USING (item_id)
      WHERE item_status = 'active' AND email_address_recipient REGEXP @user_name";
    $result = red_sql_query($sql, ['@user_name' => '(^|,| )' . $this->user_name . '($|,| )']);
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }

    $sql = "SELECT red_item.item_id FROM red_item JOIN red_item_user_account USING (item_id)
      WHERE item_status = 'active' AND user_account_login = @user_name";
    $result = red_sql_query($sql, ['@user_name' => $this->user_name]);
    $row = red_sql_fetch_row($result);
    $item_id = intval($row[0]);

    $sql = "SELECT email_verify_address FROM red_email_verify
      WHERE email_verify_status = '1' AND item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $item_id]);
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;


  }

  function confirm() {
    $this->template->set_file('sso_file','sso.ihtml');
    
    $emails = $this->get_confirmed_email_addresses();
    $this->template->set_var('lang_select_an_email_address', red_t("Please select the email address you would like to use."));
    $confirmed_email_radio_options = '';
    $selected = FALSE;
    $count = count($emails);
    if ($count == 0) {
      $confirmed_email_radio_options = '<p><em>No confirmed email addresses in the control panel, you must enter an email address and then confirm it.</em></p>';
      $this->template->set_var('sso_other_email_checked', 'checked="checked"');
      $this->template->set_var('lang_other_email_address', red_t("Email address"));
      $this->template->set_var('sso_other_email_option_hidden', 'hidden');

    }
    else {
      $this->template->set_var('sso_other_email_checked', '');
      $this->template->set_var('lang_other_email_address', red_t("If you selected to use a new address, please enter it here."));
      for($i = 0; $i < $count; $i++) {
        // I have no idea which one should be default, so set TRUE on the first one.
        if ($i == 0) {
          $selected = array('checked' => 'checked');
        }
        else {
          $selected = array();
        }
        $input = $this->html_generator->get_input('sso_email_address', $emails[$i], 'radio', $selected);
        $label = $this->html_generator->get_tag('label', $input . $emails[$i]);
        $confirmed_email_radio_options .= $this->html_generator->get_tag('div', $label, array('class' => 'radio'));
      }
    }
    $this->template->set_var('lang_other', red_t("Enter a new address"));
    $this->template->set_var('confirmed_email_radio_options', $confirmed_email_radio_options);
    $this->template->set_var('lang_auth_request', red_t("Authorization request"));
    $this->template->set_var('lang_sso_explanation', red_t("The May First Movement Technology Discourse web site would like to login you in as <em>@user_name</em>. Do you accept (if you want to login as a different user, please log out from the control panel)?", array('@user_name' => $this->user_name)));
    $this->template->set_var('lang_your_decision', red_t("Please indicate if you accept or reject this request."));

    $accept_options = array(
      'accept' => red_t("Accept"),
    );
    $reject_options = array(
      'reject' => red_t("Reject"),
    );
    $sso_accept_input = $this->html_generator->get_radio('sso_confirm', $accept_options, TRUE);
    $sso_reject_input = $this->html_generator->get_radio('sso_confirm', $reject_options, NULL);
    $this->template->set_var('sso_accept_input', $sso_accept_input);
    $this->template->set_var('sso_reject_input', $sso_reject_input);
    $this->template->set_var('session_id',session_id());
    $this->template->set_var('sso',$this->sso);
    $this->template->set_var('sig',$this->sig);
    $this->template->parse('body_block','sso_file');
  }
}
