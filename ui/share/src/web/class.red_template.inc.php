<?php

class red_template extends Template {

  var $_css = array();
  var $_js = array();
  var $_on_load_js_functions = array();
  var $self_url;
  var $self_url_parts = array();
  var $constructed_self_url = null;

  function set_add_new_item_link($display = 'inline-block', $link = null) {
    if(is_null($link)) {
      $this->construct_self_url();
      $link = $this->constructed_self_url . '&amp;action=new';
    }
    $this->set_var('add_new_item_link_display',$display);
    $this->set_var('add_new_item_link',$link);
  }

  function set_area_navigation_list($items,$active) {
    if(!array_key_exists('top',$items)  && !array_key_exists('member',$items)) {
      // delete the whole block
      $this->set_block('area_navigation_file','area_navigation_list','delete_me');
      $this->set_var('delete_me','');
      return;
    } 
    $vars = array();
    foreach($items as $area => $values) {
      if($active == $area) {
        $vars[] = array(
          'list_item_class' => 'red-list-item',
          'list_item_display' => $values['friendly'],
        );
        
      } else {
        $add = array(
          'list_item_class' => 'red-list-item',
        );
        if(array_key_exists('id',$values) && empty($values['id'])) {
          // do not show as link
          $add['list_item_display'] = $values['friendly'];
        } else {
          $url = $this->self_url .  '?area=' . $area;
          if(array_key_exists('id',$values) && !empty($values['id'])) {
            $url .= "&amp;" . $values['field'] . "=" . $values['id'];
          }
          $add['list_item_display'] = '<a href="' . $url . '">'. $values['friendly'] . '</a>';
        }
        $vars[] = $add;
      }
    }

    $count = count($vars);
    if($count == 0) {
      $this->set_block('area_navigation_file','red_list_item','delete_me');
      $this->set_var('delete_me','');
    } else {  
      $vars[0]['list_item_class'] = $vars[0]['list_item_class'] . ' red-list-item-first';
      $vars[$count-1]['list_item_class'] = $vars[$count-1]['list_item_class'] . ' red-list-item-last';

      $this->set_var('area_label','Areas:');
      $this->set_block('area_navigation_file','red_list_item','red_list_items');
      reset($vars);
      foreach($vars as $k => $v) {
        #if($k != $count-1)  $v['list_item_display'] .= ' <span class="red-breadcrumb-arrow">&gt;&gt;</span> ';
        $this->set_var($v);
        $this->parse('red_list_items','red_list_item',true);
      }
    }
  }

  function display_member_parent_menu($parents,$selected) {
    $this->set_block('reports_file','red_stats_member_parent','red_stats_member_parents');
    foreach($parents as $k => $v) {
      $this->set_var('member_parent_id',$k);
      $this->set_var('member_parent_name',$v);
      if($k == $selected) {
        $this->set_var('selected','selected="selected"');
      } else {
        $this->set_var('selected','');
      }
      $this->parse('red_stats_member_parents','red_stats_member_parent',true);
          
    }
  }
  function display_stats($stats) {
    $this->set_block('reports_file','red_stats_item','red_stats_items');
    foreach($stats as $k => $v) {
      $this->set_var('stat_label',$k);
      $this->set_var('stat_value',number_format($v,0));
      $this->parse('red_stats_items','red_stats_item',true);
    }
  }

  function display_messages() {
    if(!array_key_exists('red',$_SESSION) || !array_key_exists('messages',$_SESSION['red'])) return;

    foreach($_SESSION['red']['messages'] as $type => $messages) {
      foreach($messages as $i => $message) {
        $message = red_htmlentities($message);
        $this->set_file('message_file','message.ihtml');
        $this->set_var('message_text',"$message");
        switch($type) {
          case 'error':
            $this->set_var('message_class','warning');
            break;
          case 'success':
            $this->set_var('message_class','success');
            break;
          default:
            $this->set_var('message_class','info');
        }

        $this->parse('message_block','message_file',true);
        unset($_SESSION['red']['messages'][$type][$i]);
      }
    }
  }

  function draw_theme() {
    $this->draw_header();
    $this->draw_footer();
  }

  function draw_header() {
    $this->set_file('header_file','header.ihtml');
    $this->parse('header_block','header_file');
  }

  function draw_footer() {
    $this->set_file('footer_file','footer.ihtml');
    $this->parse('footer_block','footer_file');
  }
  function add_on_load_js_function($function) {
    if(!in_array($function,$this->_on_load_js_functions)) {
      $this->_on_load_js_functions[] = $function;
    }
  }

  function parse_header_additions() {
    $this->parse_on_load_js_functions();
  }

  function parse_on_load_js_functions() {
    $on_load = '';
    if(count($this->_on_load_js_functions) > 0) {
      $functions = implode($this->_on_load_js_functions);
      $on_load = "onload=\"$functions\"";
    }
    $this->set_var('on_load',$on_load);
  }

  function add_css($file) {
    if(!in_array($file,$this->_css)) {
      $this->_css[] = $file;
    }
  }

  function add_js($files) {
    if(!is_array($files)) $files = array($files);
    foreach($files as $file) {
      if(!in_array($file,$this->_js)) {
        $this->_js[] = $file;
      }
    }
  }
  function parse_css($config) {
    // Now add any defined in the config file
    if(isset($config['css'])) $this->add_css($config['css']);
    $this->set_block('main_file','css_line','css_lines');
    foreach($this->_css as $file) {
      $this->set_var('css_file',$file);
      $this->parse('css_lines','css_line',TRUE);
    }
  }

  function parse_js() {
    reset($this->_js);
    $this->set_block('main_file','js_line','js_lines');
    if(count($this->_js) == 0) {
      $this->set_var('js_lines','');
    }
    else {
      foreach($this->_js as $file) {
        $this->set_var('js_file',$file);
        $this->parse('js_lines','js_line',TRUE);
      }
    }
  }
  function display_confirm_message($item_id,$service_key_field,$confirm_message,$session_id,$action) {
    $this->set_file('confirm_file','confirm.ihtml');
    $this->set_var(array(
        'confirm_message' => $confirm_message,
        'self_url' => $this->self_url,
        'service_key_field' => $service_key_field,
        'session_id' => $session_id,
        'action' => $action,
        'lang_yes' => red_t('Yes'),
        'lang_no' => red_t('No'),
        'item_id' => $item_id));
    $this->parse('confirm_block','confirm_file');
  }

  function construct_self_url($parts = null) {
    $this->constructed_self_url = $this->get_constructed_self_url($parts);
  }
  function get_constructed_self_url($parts = null, $format = 'urlencoded') {
    if(is_null($parts)) $parts = $this->self_url_parts;
    $fragments = array();
    reset($parts);
    foreach($parts as $k => $v) {
      if ($k && $v) {
        if (preg_match('/^[a-z_.0-9-]+$/', $k)) {
          $fragments[] = "$k=" . strip_tags($v);
        }
      }
    }
    $delimiter = '&amp;';
    if($format == 'raw') $delimiter = '&';
    return $this->self_url . '?' . implode($delimiter,$fragments);
  }

  function parse($target, $varname, $append = false) {
    // always set the self url
    $this->construct_self_url();
    // some templates want the full constructed url...
    $this->set_var('constructed_self_url',$this->constructed_self_url);
    // ... others just want the base url
    $this->set_var('self_url',$this->self_url);
    return parent::parse($target, $varname, $append);
  }

  function set_pager_block($total_records,$start,$limit,$search_keywords = '') {
    // if limit is greater or equal to count do nothing 
    if($limit >= $total_records) return;

    // if we have no records, return
    if($total_records == 0) return;

    $this->set_file('pager_file','pager.ihtml');
    $this->set_var('lang_total_records',red_t("Total records: @total_records",array('@total_records' => $total_records)));

    $total_pages = ceil($total_records/$limit);
    if($start == 0) {
      $current_page = 1;
    } else {
      $current_page = $start/$limit + 1;
    }

    if($current_page == 1) {
      $this->set_block('pager_file','previous','delete-me');
    } else {
      $this->set_var('previous_page_start',$start - $limit);
    }

    if($current_page == $total_pages) {
      $this->set_block('pager_file','next','delete-me');
    } else {
      $this->set_var('next_page_start',$start + $limit);
      $last_page_start = ($total_pages - 1) * $limit;
      $this->set_var('last_page_start',$last_page_start);
    }

    $this->set_var('delete-me','');
    $current_of_total_pages = red_t("Page @current_page of @total_pages pages",array('@current_page' => $current_page,'@total_pages' => $total_pages));
    $this->set_var('lang_current_of_total_pages',$current_of_total_pages);
    if(!empty($search_keywords)) {
      $this->set_var('keywords','&amp;search_keywords=' . urlencode($search_keywords));
    }
    $this->parse_page_links($current_page,$total_pages,$limit);
    $this->parse('pager_block','pager_file');

  }

  function parse_page_links($current_page,$total_pages,$limit) {
    // only run if we have more than one page
    if($total_pages < 2) {
      $this->set_block('pager_file','page_link_block','delete-me');
      $this->set_var('delete-me','');
      return;
    }

    $this->set_block('pager_file','page_link','page_links');

    // how many page links to create? default is 10. make sure it's an even number
    $count = 10;
    if($total_pages < $count) {
      // if we have less pages available, we have less page links
      $count = $total_pages;
    } 
    // we start with page $p
    $half = ceil($count/2);
    if($current_page <= $half) {
      // if our current page is less or equal than half our count, start with 1
      $p = 1;
    } elseif(($current_page + $half) > $total_pages) {
      // if our current page is too close to the end, start with total pages minus
      // count
      $p = $total_pages - $count + 1;
    } else {
      // otherwise start with the current page minus half of our total
      // (e.g. count is 10, half is 5, current page is 23, start with page
      // 18)
      $p = $current_page - $half;
    }

    // work out elipses
    if($p == 1) {  
      $this->set_var('page_link_earlier_elipses','');
    } else {
      $this->set_var('page_link_earlier_elipses','...');
    }
    if($p + $count > $total_pages) {
      $this->set_var('page_link_later_elipses','');
    } else {
      $this->set_var('page_link_later_elipses','...');
    }

    $i = 0;
    while($i < $count) {
      if($p == $current_page) {
        $this->set_var('page_link_class','red-current-page-link red-page-link');
      }
      $this->set_var('page_link_class','red-page-link');
      $start = ($p - 1) * $limit;
      $this->set_var('page_link_start',$start);
      $this->set_var('page',$p);

      $this->parse('page_links','page_link',true);
      $p++;
      $i++;
    }
  }
  function set_item_errors_message() {
    // set alert message at top of screen
    $this->set_file('item_errors_file','item_errors.ihtml');
    $this->parse('item_errors_block','item_errors_file');
  }
}

?>
