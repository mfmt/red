<?php

/**
 * Class for checking for and sending disk usage notifications.
 *
 * Instantiate with:
 *
 * $notifier = new red_disk_usage_notifier();
 * $notifyier->notify();
 *
 */

class red_disk_usage_notifier extends red_ado {

  public $debug = FALSE;
  protected $base_url;

  const NOTIFY_TYPE_WARNING = 'warning';
  const NOTIFY_TYPE_EXCEEDED = 'exceeded';

  function set_base_url() {
    global $globals;
    $this->base_url = $globals['config']['base_url'];
  }

  function log($val) {
    if ($this->debug) {
      if (is_array($val)) {
        print_r($val);
      }
      else {
        echo "$val\n";
      }
    }
  }
  function notify() {
    $this->set_base_url();
    $array = $this->get_notification_array(self::NOTIFY_TYPE_WARNING);
    //$this->log($items);
    foreach($array as $values ) {
      $this->send_notifications($values, self::NOTIFY_TYPE_WARNING);
      $this->update_notify_date($values, self::NOTIFY_TYPE_WARNING);
    }

    // Do it again for resources exceeding their quota.
    $array = $this->get_notification_array(self::NOTIFY_TYPE_EXCEEDED);
    //$this->log($items);
    foreach($array as $values ) {
      $this->send_notifications($values, self::NOTIFY_TYPE_EXCEEDED);
      $this->update_notify_date($values, self::NOTIFY_TYPE_EXCEEDED);
    }
  }

  function update_notify_date($values, $type) {
    if ($type == self::NOTIFY_TYPE_WARNING) {
      $field = 'item_quota_notify_warning_date';
    }
    elseif ($type == self::NOTIFY_TYPE_EXCEEDED) {
      $field = 'item_quota_notify_exceeded_date';
    }
    foreach($values['items'] as $item) {
      $item_id = intval($item['item_id']);
      $date = date('Y-m-d H:i:s', strtotime("+7 days"));
      $sql = "UPDATE red_item SET $field = @date WHERE item_id = #item_id";
      $params = [
        '@date' => $date,
        '#item_id' => $item_id
      ];
      red_sql_query($sql, $params);
    }
  }
  function send_notifications($values, $type = 'warning') {
    $body_template = NULL;
    $subject_template = NULL;
    if ($type == self::NOTIFY_TYPE_WARNING) {
      $body_template = 'disk_quota_warning.body';
      $subject_template = 'disk_quota_warning.subject';
    }
    elseif ($type == self::NOTIFY_TYPE_EXCEEDED) {
      $body_template = 'disk_quota_exceeded.body';
      $subject_template = 'disk_quota_exceeded.subject';
    }
    // $this->log($values);
    $member_friendly_name = $values['member_friendly_name'];
    $items = $values['items'];
    foreach($values['contacts'] as $contact) {
      $lang = $contact['contact_lang'];
      $subject = $this->get_email_template($subject_template, $lang);
      $body = $this->get_email_template($body_template, $lang);
      $replace = [
        '{member_friendly_name}' => $member_friendly_name,
        // Subject lines should not exceed 78 characters (and we have a 50 character prefix)
        '{member_friendly_name_subject}' => $this->truncate_for_subject($member_friendly_name),
        '{contact_first_name}' => $contact['contact_first_name'],
        '{disk_usage_summary}' => $this->format_disk_usage_summary($items),
        '{membership_disk_usage}' => red_human_readable_bytes(red_get_disk_usage_for_membership($values['member_id'])),
        '{membership_disk_allocation}' => red_human_readable_bytes(red_get_allocated_quota_for_membership($values['member_id'])),
        '{membership_disk_quota}' => red_human_readable_bytes(red_get_member_quota($values['member_id'])),
      ];
      $subject = str_replace(array_keys($replace), $replace, $subject );
      $body = str_replace(array_keys($replace), $replace, $body );
      $values = array(
        'member_id' => $values['member_id'],
        'correspondence_subject' => $subject,
        'correspondence_body' => $body,
        'correspondence_to' => $contact['contact_email']
      );
      
      $this->log($values);
      $ret = $this->create_related_object('red_correspondence',$values);
      if(!$ret) {
        red_set_message($this->get_errors_as_string(), 'error'); 
        return FALSE;
      }
    }
    return TRUE;
  }

  function truncate_for_subject($member_friendly_name) {
    if (strlen($member_friendly_name) > 25) {
      return substr($member_friendly_name, 0, 25) . '...';
    }
    return $member_friendly_name;
  }

  /**
   * Get contacts for member.
   */
  function get_contacts_for_member($member_id) {
    $ret = [];
    $sql = "SELECT contact_first_name, contact_last_name, contact_email, contact_lang
      FROM red_contact WHERE contact_status = 'active' AND member_id = #member_id";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    while ($row = red_sql_fetch_assoc($result)) {
      $ret[] = $row;
    }
    return $ret;
  }

  /**
   * Format array of items into a message
   *
   */
  function format_disk_usage_summary($items) {
    $ret = [];
    foreach($items as $item) {
      $quota = red_human_readable_bytes(($item['item_quota']));
      $disk_usage = red_human_readable_bytes(($item['item_disk_usage']));
      $link = $this->item_edit_link($item);
      $ret[] = ' * ' . $item['service_label'] . ' (' . $item['identifier'] . '): ' . $disk_usage . ' (in use) ' . $quota . ' (quota)' . "\n" .
        '   Update via this link: ' . $link . "\n\n";
    }
    return implode('', $ret);
  }

  /**
   * Generate link to edit the item.
   *
   */
  function item_edit_link($item) {
    return $this->base_url . 'index.php?area=hosting_order&hosting_order_id=' . 
      $item['hosting_order_id'] . '&service_id=' . $item['service_id'] . 
      '&action=edit&item_id=' . $item['item_id'];
  }

  /**
   * Return array of items that need warning notification.
   *
   * Create an array, indexed by the member_id, of each item
   * that is near quota.
   *
   **/
  function get_notification_array($type) {
    $ret = [];
    $now = date('Y-m-d H:i:s');
    $sql = "
      SELECT 
        red_member.member_id, member_friendly_name, red_service.service_id, 
        service_label, item_id, item_status, item_disk_usage, item_quota,
        red_hosting_order.hosting_order_id
      FROM red_member 
        JOIN red_hosting_order USING(member_id) 
        JOIN red_item USING(hosting_order_id)
        JOIN red_service USING(service_id)
      WHERE 
        item_status != 'deleted'
    ";
    $params = [];
    if ($type == self::NOTIFY_TYPE_WARNING) {
      $sql .= " AND (item_quota_notify_warning_date IS NULL OR item_quota_notify_warning_date < @now) AND
        item_quota > 0 AND
        item_quota_notify_warning_percent > 0 AND
        item_disk_usage / item_quota * 100 >= item_quota_notify_warning_percent
      ";
      $params['@now'] = $now;
    }
    if ($type == self::NOTIFY_TYPE_EXCEEDED) {
      // NOTE: database usage might exceed quotas because we can't stop them
      // via the file system.  Also, user disk usage may be just a few bytes
      // short of the quota and never equal the quota depending on how the app
      // that is writing the data behaves when it hits the limit. In this case
      // the user will never receive the final notice. 
      $sql .= " AND (item_quota_notify_exceeded_date IS NULL OR item_quota_notify_exceeded_date < @now) AND
        item_quota > 0 AND
        item_disk_usage >= item_quota
      ";
      $params['@now'] = $now;
    }

    // $this->log($sql);
    $result = red_sql_query($sql, $params);
    while ($row = red_sql_fetch_assoc($result)) {
      $member_id = $row['member_id'];
      if (!array_key_exists($member_id, $ret)) {
        $ret[$member_id] = [ 
          'member_id' => $member_id,
          'member_friendly_name' => $row['member_friendly_name'],
          'contacts' => $this->get_contacts_for_member($member_id),
          'items' => [],
        ];
      }

      if (count($ret[$member_id]['contacts']) == 0) {
        continue;
      }
      // Convert to friendlier units.
      $row['item_quota'] = red_human_readable_bytes($row['item_quota']);
      $row['item_disk_usage'] = red_human_readable_bytes($row['item_disk_usage']);

      // Depending on the service, add an identifier (e.g. database name, login name)
      $item_id = $row['item_id'];
      $sql = NULL;
      switch ($row['service_id']) {
        case 1:
          $sql = "SELECT user_account_login AS identifier FROM red_item_user_account WHERE item_id = #item_id";
          break;
        case 20:
          $sql = "SELECT mysql_db_name AS identifier FROM red_item_mysql_db WHERE item_id = #item_id";
          break;
        case 37:
          $sql = "SELECT nextcloud_login AS identifier FROM red_item_nextcloud WHERE item_id = #item_id";
          break;
        case 38:
          $sql = "SELECT mailbox_login AS identifier FROM red_item_mailbox WHERE item_id = #item_id";
          break;

      }
      if ($sql) {
        $id_result = red_sql_query($sql, ['#item_id' => $item_id]);
        $id_row = red_sql_fetch_assoc($id_result);
        $row['identifier'] = $id_row['identifier'];
      }
      else {
        // If we get a service id we don't expect, just label it with the id.
        $row['identifier'] = "Service: " . $row['service_id'];
      }

      $ret[$member_id]['items'][] = $row; 
    }
    //$this->log($ret);
    return $ret;
  }
  // Helper function for retrieving templates for email messages
  function get_email_template($name, $lang = 'en') {
    global $globals;
    $path = $globals['config']['src_path'] . "/../email";
    $p = "{$path}/{$lang}/{$name}";
    if(file_exists($p)) {
      return file_get_contents($p);
    }
  }
}


?>
