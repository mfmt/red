<?php

/**
 * This class is responsible for sending MX domain verification emails
 * and processing the results.
 *
 * When a member adds an MX domain name record pointing to our servers we
 * send a probe email to the given address to ensure they really are pointing
 * to our servers before we label the domain as being verified.
 *
 */

class red_mx_verify {
  protected $item_id = NULL;
  protected $errors = [];

  public function get_errors() {
    return implode(',', $this->errors);
  }

  public function set_item_id($item_id) {
    $this->item_id = $item_id;
  }

  // Given a domain, set the item id for it 
  // and return True if it exists or FALSE if it
  // doesn't
  public function set_item_id_for_domain($domain) {
    if (!preg_match(RED_DOMAIN_MATCHER, $domain)) {
      $this->set_errors[] = 'Invalid domain';
      return FALSE;
    }

    $sql = "SELECT red_item.item_id 
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE 
          dns_fqdn = @domain AND 
          dns_type = 'mx' AND 
          item_status = 'active'
        ORDER BY red_item.item_id ASC
        LIMIT 1";
    $result = red_sql_query($sql, ['@domain' => $domain]);
    # If there is more then one we set the first one we find.
    $row = red_sql_fetch_row($result);
    if (empty($row)) {
      $this->set_errors[] = 'unknown domain';
      return FALSE;
    }
    $this->set_item_id($row[0]);
    return TRUE;
  }

  public function send() {
    if (empty($this->item_id)) {
      // Set the item_id first.
      $this->errors[] = "Please set the item_id first";
      return FALSE;
    }
    // Lookup the domain name.
    $sql = "SELECT dns_fqdn FROM red_item_dns WHERE item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $this->item_id]);
    $row = red_sql_fetch_row($result);
    if (empty($row) or empty($row[0])) {
      $this->errors[] = "Failed to find the domain name from the item id.";
      return FALSE;
    }
    $email = 'mayfirst-verify-mx-domain@' . $row[0];
    $key = $this->get_or_generate_key();
    $headers = [];
    if (defined('RED_DEV') || defined('RED_TEST')) {
      $tld = 'dev';
    }
    else {
      $tld = 'org';
    }
    $extra = "-f members@mayfirst.{$tld}";
    $ret = mail($email, $key, "", $headers, $extra);
    if (!$ret) {
      $this->errors[] = "Failed to find the domain name from the item id.";
      return FALSE;
    }
    red_log("Sent domain verification email to $email");
    return TRUE;
  } 

  protected function get_or_generate_key() {
    $sql = "SELECT mx_verify_id, mx_verify_key FROM red_mx_verify WHERE item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $this->item_id]);
    $row = red_sql_fetch_row($result);
    $now = date('Y-m-d h:i:s');
    if (!empty($row)) {
      // We have already notified on this item. Update the last sent field and send back
      // the existing key.
      $id = $row[0];
      $key = $row[1];
      $sql = "UPDATE red_mx_verify SET mx_verify_last_sent = @now WHERE mx_verify_id = #id";
      red_sql_query($sql, ['@now' => $now, '#id' => $id]);
      return $key;
    }
    // We've never notified on this item id. Generate and save a key and then return the key.
    $key = red_generate_random_password(25);
    $sql = "INSERT INTO red_mx_verify VALUES(NULL, #item_id, @now, @now, @key)";
    red_sql_query($sql, ['#item_id' => $this->item_id, '@now' => $now, '@key' => $key]);
    return $key;
  }

  /*
   * Process the given key.
   *
   * Return FALSE if we encounter an error, NULL if there are no errors, but we
   * didn't update any DNS records, or TRUE if we updated a DNS record.
   */
  public function process_key(string $key): bool|null {
    // Our key is always the results of a base64 hash, so reject if any characters
    // are present that should not be.
    if (!preg_match('#^[A-Za-z0-9+/=]+$#', $key)) {
      $this->errors[] = "Illegal characters in key: '$key'.";
      return FALSE;
    }
    $sql = "SELECT mx_verify_id, item_id FROM red_mx_verify WHERE mx_verify_key = @key";
    $result = red_sql_query($sql, ['@key' => $key]);
    $row = red_sql_fetch_row($result);
    if (empty($row) || empty($row[0])) {
      // This is not necessarily an error, so we return NULL.
      // We often get multiple requests and the first one verifies the domain
      // and deletes the key. We'll end up here on requests that happened at
      // the same time, but came in after the key was deleted.
      return NULL;
    }

    // If we are here, then it's a successful verification.
    // fetch the domain name and update all red items matching the domain name.
    $mx_verify_id = $row[0];
    $item_id = $row[1];

    $sql = "SELECT dns_fqdn FROM red_item_dns WHERE item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $item_id]);
    $row = red_sql_fetch_row($result);
    if (empty($row)) {
      // Not sure how this could happen.
      $this->errors[] = "Failed to find domain after verifying the key and getting the item id: $item_id.";
      return FALSE;
    }
    $domain = $row[0];

    $sql = "UPDATE red_item_dns SET dns_mx_verified = 1 WHERE dns_fqdn = @domain AND dns_type = 'mx' AND
      item_id IN (SELECT item_id FROM red_item WHERE item_status != 'deleted')";
    red_sql_query($sql, ['@domain' => $domain]);

    // Now clear the red_mx_verify table.
    $sql = "DELETE FROM red_mx_verify WHERE mx_verify_id = #mx_verify_id";
    red_sql_query($sql, ['#mx_verify_id' => $mx_verify_id]);

    // Note: The API calling this funciton is responsible for triggering the re-build of all
    // the postfix databases.
    red_log("Verified domain $domain.");
    return TRUE;

  }

}
?>
