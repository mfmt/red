<?php

class red_web {
  private $template = NULL;
  private $html_generator = NULL;
  private $auth = NULL;
  private $authz = NULL;
  private $user_input = [];

  /**
   * Init
   *
   * Initialize all variables, login.
   *
   */
  private function init() {
    global $globals;
    $secure_cookies = true;
    session_set_cookie_params(0, NULL, NULL, $secure_cookies);
    session_start();

    $this->template = new red_template($globals['config']['ihtml_path']);
    $this->template->set_file('main_file','main.ihtml');
    $this->template->set_var('url_path', $globals['config']['url_path']);
    $this->template->set_var('html_title', $globals['config']['org_name']);
    $this->template->self_url = "index.php"; 

    // this is an html generator used by many of the objects 
    // being called
    $this->html_generator = new html_generator();

    // This class handles users authentication
    $this->auth = new red_auth();
    $this->auth->set_template_object($this->template);
    $this->auth->set_secure_cookie_flag($secure_cookies);

    // Provide a more trusted place to access user input.
    $this->user_input = $this->sanitize_user_input($_REQUEST);

    // Password resets happen with just the "h" parameter. We
    // need to explicitly set the area to "reset_pass" for it
    // to be properly reset.
    $area = $this->user_input['area'] ?? NULL;
    $h = $this->user_input['h'] ?? NULL;
    if (!$area) {
      if ($h) {
        $this->user_input['area'] = 'reset_pass';
      }
      else {
        // Default is hosting_order
        $this->user_input['area'] = 'hosting_order';
      }
    }

    // Check to ensure https cookies match the http protocol. 
    if ($secure_cookies && 
      (!array_key_exists('HTTPS',$_SERVER) || $_SERVER['HTTPS'] != 'on')) {
      red_set_message(red_t("Warning: secure_cookies are configured, but you are accessing the site via http. You may experience problems."),'error');
    }

    // Hash verification should take place prior to login in case they
    // get logged out along the way or open in a different browser.
    if (!empty($this->user_input['email_verify_hash'])) {
      red_email_verify::validate_hash($this->user_input['email_verify_hash']);
    }
  }

  function run() {
    $this->init();
    if ($this->authorize()) {
      $this->main();
    }
    $this->template->display_messages();
    $this->exit();
  }

  function authorize() {
    // reset_pass and change_pass are allowed without authentication.
    $allowed_without_auth = ['reset_pass', 'change_pass'];
    $area = $this->user_input['area'] ?? NULL;
    if (in_array($area, $allowed_without_auth)) {
      return TRUE;
    }
    if (!$this->auth->login()) {
      $this->display_login_form($this->auth->get_login_message());
      return FALSE;
    }
    else {
      if (!empty($this->user_input['logout'])) {
        $this->auth->logout();
        $this->display_login_form($this->auth->get_login_message());
        return FALSE;
      }
      else {
        // authorize the user
        $this->authz = new red_authz();
        $this->authz->set_user_name($this->auth->get_user_name());
        $this->template->set_var('user_name', $this->auth->get_user_name());

        // logged in user is disabled
        if($this->authz->is_disabled()) {
          $message = red_t("You login is disabled. Disabled accounts are typically the result of failure to pay your membership dues. Please contact info@mayfirst.org immediately.");
          red_set_message($message,'error');
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  private function exit() {
    global $globals;
    $this->template->parse_header_additions();
    $this->template->draw_theme();
    $this->template->parse_css($globals['config']);
    $this->template->parse_js();
    $this->template->pparse('out','main_file');
    exit;
  }

  function main() {
    global $globals;

    // store globals and user input in easier to reference variables
    $service_id = $this->user_input['service_id'];
    $action = $this->user_input['action'];
    $area = $this->user_input['area'];
    $session_id = $this->user_input['session_id'];
    $search_keywords = $this->user_input['search_keywords'];
    $filter_keywords = $this->user_input['filter_keywords'];
    
    // Add area and service_id to self_url parts.
    $this->template->self_url_parts['area'] = $area;
    if($service_id) {
      $this->template->self_url_parts['service_id'] = $service_id;
    }

    // Set core navigation
    $this->template->set_file('core_navigation_file','core_navigation.ihtml');
    $this->template->set_var('lang_home',red_t('Home'));
    $this->template->set_var('lang_my_settings',red_t('My Settings'));
    $this->template->set_var('lang_reports',red_t('Reports'));
    $this->template->set_var('lang_help',red_t('Help'));
    $this->template->set_var('lang_logout',red_t('Logout'));

    // Hide reports menu from non admins
    if (!$this->authz || !$this->authz->is_admin()) {
      $this->template->set_block('core_navigation_file', 'reports_nav', 'delete_me');
      $this->template->set_var('delete_me', '');
    }

    $this->template->parse('core_navigation_block','core_navigation_file');

    // down for maintenance
    if (array_key_exists('site_down_message',$globals['config'])) {
      if ($this->authz && $this->authz->is_admin()) {
        // admins can access site even when under maintainence
      }
      else {
        $message = $globals['config']['site_down_message'];
        if ($message) {
          red_set_message($message, 'error');
          $this->template->set_var('user_name', $this->auth->get_user_name());
          return;
        }
      }
    }

    // capture help, reports, settings and other special request
    switch($area) {
      case 'help':
        $this->help();
        return;

      case 'settings':
        $this->settings($this->auth->get_user_name());
        return;

      case 'reports':
        $this->reports();
        return;

      case 'sso':
        $sso = new red_sso();
        $sso->set_user_input($this->user_input);
        $sso->set_sso_secret($globals['config']['sso_secret']);
        $sso->set_redirect_url($globals['config']['sso_redirect_url']);
        $sso->set_user_name($this->auth->get_user_name());
        $sso->set_item_id($this->auth->get_item_id());
        $sso->set_template($this->template);
        $sso->set_html_generator($this->html_generator);
        $sso->run();
        return;

      case 'oauth':
        $oauth = new red_oauth();
        $oauth->set_user_input($this->user_input);
        $oauth->set_credentials($globals['config']['oauth_client_id'], $globals['config']['oauth_client_secret']);
        $oauth->set_user_name($this->auth->get_user_name());
        $oauth->set_server($globals['config']['oauth_server']);
        $oauth->set_template($this->template);
        $oauth->set_html_generator($this->html_generator);
        $oauth->run();
        return;

      case 'reset_pass':
        $password = new red_password();
        $password->set_template($this->template);
        $password->set_user_input($this->user_input);
        $password->reset_pass();
        return;
    }

    // the list object controls how the page will be displayed
    $area_object = red_area::get_red_object($area, $service_id);

    // set authorization info
    $area_object->is_admin = $this->authz->is_admin();
    $area_object->authz = $this->authz;
    $area_object->user_name = $this->auth->get_user_name();

    // the area_object sets the default service id if it's empty 
    if (empty($service_id)) {
      $service_id = $area_object->service_id;
    }

    // unit_key_field - e.g. hosting_order_id (if we're in the 
    // hosting_order area), member_id (if in the member area), etc.
    $unit_key_field = $area_object->unit_key_field;

    // service_key_field - e.g. item_id (if we're in the 
    // hosting_order area), hosting_order_id or item_id (if we're in the
    // member area), etc.
    $service_key_field = $area_object->service_key_field;

    $unit_id = '';
    if(array_key_exists($unit_key_field, $this->user_input))
      $unit_id = $this->user_input[$unit_key_field];

    $id = '';
    if(array_key_exists($service_key_field, $this->user_input)) {
      $id = $this->user_input[$service_key_field];
    }

    $area_object->id = $id;

    // figure out what units are available to this user
    $area_object->set_available_units();

    // if no unit_id is selected, grab the first one from the
    // list of available ones.
    $area_object->set_unit_id($unit_id);
    $unit_id = $area_object->unit_id;

    if (!empty($search_keywords)) {
      $area_object->parse_search_keywords($search_keywords);
    }
    if (!empty($filter_keywords)) {
      $area_object->filter_keywords = $filter_keywords;
    }

    // If they are submitting a new item to be written, make sure the id
    // has not been tampered with. It should come through with the sf_
    // prefix.
    $sf_key = 'sf_' . $service_key_field;
    if (array_key_exists($sf_key, $_POST)) {
      $sf_id = $_POST[$sf_key];
      if (!empty($sf_id)) {
        // Ensure we use the actual key being written when validating.
        $id = $sf_id;
      }
    }

    // make sure the user has access to the area, unit_id, and id
    // they are trying to access
    if (!$area_object->check_access($id, $action)) {
      // error_id 4 simply means that don't have access to anything
      // so send them to their settings page
      if (array_key_exists(4, $area_object->errors) && $area == 'hosting_order') {
        $this->settings($this->auth->get_user_name());
        return;
      } 
      else {
        red_set_message(red_t("Sorry, you don't seem to have access to this area."),'error');
        red_set_message($area_object->errors);
        return false;
      }
    }

    // set globals for the area_object
    $area_object->template =& $this->template;
    $area_object->html_generator =& $this->html_generator;
    if(!empty($unit_key_field)) {
      $this->template->self_url_parts[$unit_key_field] = $unit_id;
    }

    switch($action) {
      case 'delete':  
        $confirm = $this->user_input['confirm'];
        $cancel = $this->user_input['cancel'];
        $area_object->delete_child($id, $session_id, $confirm, $cancel);
        break;

      case 'disable':  
        $confirm = $this->user_input['confirm'];
        $cancel = $this->user_input['cancel'];
        $area_object->disable_child($id, $session_id, $confirm, $cancel);
        break;

      // Write the item specified to the database
      case 'enable':
        $confirm = $this->user_input['confirm'];
        $cancel = $this->user_input['cancel'];
        $area_object->enable_child($id, $session_id, $confirm, $cancel);
        break;

      case 'recalculate':
        $confirm = $this->user_input['confirm'];
        $cancel = $this->user_input['cancel'];
        $area_object->recalculate_child($id, $session_id, $confirm, $cancel);
        break;

      case 'write':
        if($area_object->write_child($session_id)) break;
        // there was an error, don't break, fall through to edit
        // which will display what the user filled in

      case 'edit':
        $area_object->edit_child($id);
        break;

      case 'new':
        $area_object->new_child();
        break;

      case 'print':
        $area_object->print_child($id);
        break;

    }

    $start = $this->user_input['start'];
    $limit = $this->user_input['limit'];
    if (!$area_object->list_children($start, $limit)) {
      return FALSE;
    }
    return TRUE;
  }

  private function sanitize_user_input($user_input) {
    // defaults
    $ret['area'] = '';
    $ret['action'] = '';
    $ret['password_action'] = '';
    $ret['hosting_order_id'] = '';
    $ret['member_id'] = '';
    $ret['service_id'] = '';
    $ret['payment_id'] = '';
    $ret['confirm'] = false;
    $ret['cancel'] = false;
    $ret['item_id'] = '';
    $ret['start'] = false;
    $ret['logout'] = false;
    $ret['limit'] = false;
    $ret['session_id'] = false;
    $ret['start'] = 0;
    $ret['limit'] = 50;
    $ret['search_keywords'] = '';
    $ret['filter_keywords'] = '';
    $ret['member_parent_id'] = 1;
    $ret['report_type'] = 'error-items';
    $ret['h'] = '';
    $ret['password'] = '';
    $ret['password_confirm'] = '';

    $tests = array(
      'area' => '/^(hosting_order|member|top|settings|help|reports|oauth|sso|reset_pass|change_pass)$/',
      'action' => '/^(new|write|edit|delete|print|disable|enable|recalculate)$/',
      'password_action' => '/^(submit_new_password|display_password_reset_form|submit_username|new)$/',
      'hosting_order_id' => '/^[0-9]+$/',
      'member_id' => '/^[0-9]+$/',
      'contact_id' => '/^[0-9]+$/',
      'vps_id' => '/^[0-9]+$/',
      'correspondence_id' => '/^[0-9]+$/',
      'start' => '/^[0-9]+$/',
      'limit' => '/^[0-9]+$/',
      'map_user_member_id' => '/^[0-9]+$/',
      'item_id' => '/^[0-9]+$/',
      'service_id' => '/^[0-9]+$/',
      'start' => '/^(false|[0-9]+)$/',
      'limit' => '/^(false|[0-9]+$)/',
      'logout' => '/^(true|1)$/',
      'confirm' => '/^(true|1|y|Y|yes|Yes)$/',
      'cancel' => '/^(false|0|n|N|no|No)$/',
      'session_id' => '/^[a-z0-9]+$/',
      'search_keywords' => '/.*/',
      'filter_keywords' => '/.*/',
      'member_parent_id' => '/^[0-9]+$/',
      'members' => '/^([0-9]+|)$/',
      // oauth 
      'consent_decision' => '/^(accept|reject)$/',
      'consent_email' => '/^(' . RED_EMAIL_REGEXP . '|)$/',
      'consent' => '/^[A-Z0-0\-]+$/',
      // sso
      'sso' => '/.*/',
      'sig' => '/.*/',
      'sso_confirm' => '/^(accept|reject)$/',
      'sso_email_address' => '/^(' . RED_EMAIL_REGEXP . '|(other))$/',
      'sso_email_address_other' => '/^(' . RED_EMAIL_REGEXP . '|)$/',
      'email_verify_hash' => '/.*/',
      'report_type' => '/^(error-items)$/',
      'user_name' => '/' . RED_LOGIN_REGEXP . '/',
      'h' => '/^[a-f0-9]+$/',
      'password' => '/.*/',
      'password_confirm' => '/.*/',
    );

    // indicate if a given variable should *only* come
    // from a particular input method (get, post, or cookie)
    $inputs = array(
      'session_id' => 'post',
      'members' => 'post',
      'sso_confirm' => 'post',
    );
    foreach($tests as $key => $test) {
      if(array_key_exists($key,$user_input)) {
        if(array_key_exists($key,$inputs)) {
          if($inputs[$key] == 'post') {
            if(!array_key_exists($key,$_POST)) {
              trigger_error("Invalid $key - should only come via POST.",E_USER_ERROR);
            }
          }
        }
        if(is_array($user_input[$key])) {
          foreach($user_input[$key] as $inner_key => $value) {
            if(!preg_match($test,$value)) {
              trigger_error("Invalid $key for index $inner_key ($value).",E_USER_ERROR);
            }
          }
        }
        else {
          if(!preg_match($test,$user_input[$key])) {
            trigger_error("Invalid $key.",E_USER_ERROR);
          }
        }
        $ret[$key] = $user_input[$key];
      }
    }
    return $ret;
  }
  private function display_login_form($login_message) {
    $this->template->set_file('login_file','login.ihtml');
    if ($this->user_input['area'] == 'sso') {
      $this->template->set_var('lang_sso_login_explanation',red_t("The May First Movement Technology Discourse application would like you to login. You can login with <em>any</em> May First Movement Technology login that you have, e.g. the username and password you use to check your email, or copy files to your web site, or login via Nextcloud. If you need to reset your password, please follow this <a href=\"resetpass\">link</a>."));
    }

    $this->template->set_var('lang_user_name',red_t("User name:"));
    $this->template->set_var('lang_password',red_t("Password:"));
    $this->template->set_var('lang_login',red_t("Login"));
    $this->template->set_var('lang_help',red_t("Help"));
    $this->template->set_var('lang_forgot_pass',red_t("Forgot Password"));
    if (count($_REQUEST) > 0) {
      $req = $_REQUEST;
      // Handle the case where someone has logged out, but needs to log in again.
      /// if we pass the the logout action, they will be logged right back out.
      if (array_key_exists('logout', $req) && $req['logout'] == 1) {
        $action = 'index.php';
      }
      // otherwise construct the request into a URL for the action
      else {
        // if the user and pass happen to be in the request, strip it out.
        if (array_key_exists('user_name', $req)) {
          unset($req['user_name']);
        }
        if (array_key_exists('user_pass', $req)) {
          unset($req['user_pass']);
        }
        $this->template->construct_self_url($req);
        $action = $this->template->constructed_self_url;
      }
    }
    else {
      // since no request is supplied, just use the index file.
      $action ='index.php';
    }
    $this->template->set_var('action', $action);

    if (!empty($login_message)) {
      red_set_message($login_message,'error');
    }
    $this->template->parse('body_block','login_file');
  }

  function help() {
    $this->template->set_file('help_file','help.ihtml');
    $this->template->parse('body_block','help_file');
  }

  function settings($user_name) {
    $edit_item = false;
    $user_account_edit_item = NULL;
    $mailbox_edit_item = NULL;

    // The presence of the session_id indicates they are posting
    // data so save.
    if ($this->user_input['session_id'] ?? FALSE) {
      $result = $this->write_settings($user_name);
      if($result !== TRUE) {
        // The save operation failed.
        if ($result !== FALSE) {
          $service_name = red_get_service_name_for_service_id($result->get_service_id());
          if ($service_name == 'user_account') {
            $user_account_edit_item = $result;
          }
          elseif ($service_name == 'mailbox') {
            $mailbox_edit_item = $result;
          }
        }
      }
    }

    // Get both user account and mailbox item ids to load the current
    // settings.
    $ua_item_id = $this->get_item_id_for_user_name($user_name);
    $ua_edit_item = red_item::get_red_object([
      'mode' => 'ui', 
      'id' => $ua_item_id
    ]);

    $m_item_id = $this->get_item_id_for_mailbox_login($user_name);
    $m_edit_item = NULL;
    if ($m_item_id) {
      $m_edit_item = red_item::get_red_object([
        'mode' => 'ui',
        'id' => $m_item_id
      ]);
    }
    if(!$ua_edit_item) {
      $message = red_t("Failed to load your user account: $user_name. Please contact support for help.");
      red_set_message($message,'error');
    }
    else {
      $ua_edit_item->set_pseudo_related("&nbsp");
      $ua_edit_item->set_html_generator($this->html_generator);
      $ua_js_includes = $ua_edit_item->get_javascript_includes();
      $this->template->set_file('settings_file','settings.ihtml');
      $this->template->add_js($ua_js_includes);
      $this->template->set_var('user_account_item_status', $ua_edit_item->get_item_status());
      $this->template->set_var('lang_my_settings',red_t("My Settings"));
      $this->template->set_var('lang_status',red_t("Status:"));
      $this->template->set_var('lang_refresh_status',red_t("Refresh Status"));
      $this->template->set_var('user_account_edit_block',$ua_edit_item->get_edit_block());
      $this->template->set_var('session_id',session_id());
      $this->template->parse('body_block','settings_file');
    }
    if(!$m_edit_item) {
      $this->template->set_var('mailbox_item_status', 'No mailbox configured');
    }
    else {
      $m_edit_item->set_html_generator($this->html_generator);
      $m_js_includes = $m_edit_item->get_javascript_includes();
      $this->template->set_file('settings_file','settings.ihtml');
      $this->template->add_js($m_js_includes);
      $this->template->set_var('mailbox_item_status',$m_edit_item->get_item_status());
      $this->template->set_var('lang_my_settings', red_t("My Settings"));
      $this->template->set_var('lang_status',red_t("Status:"));
      $this->template->set_var('lang_refresh_status',red_t("Refresh Status"));
      $this->template->set_var('mailbox_edit_block',$m_edit_item->get_edit_block());
      $this->template->set_var('session_id', session_id());
      $this->template->parse('body_block','settings_file');
    }
  }

  private function write_settings($user_name) {
    $sf_item_id = $_POST['sf_item_id'] ?? 0;
    $user_account_item_id = $this->get_item_id_for_user_name($user_name);
    $mailbox_item_id = $this->get_item_id_for_mailbox_login($user_name);
    if ($user_account_item_id == $sf_item_id) {
      $item_id = $user_account_item_id;
    }
    elseif ($mailbox_item_id == $sf_item_id) {
      $item_id = $mailbox_item_id;
    } 
    else {
      $item_id = NULL;
      $args = array('@item_id' => $item_id, '@sf_item_id' => $sf_item_id);
      $message = red_t("Failed to save settings - item_id mismatch ('@item_id' vs ".
                 "'@sf_item_id').",$args);
      red_set_message($message, 'error');
      return false;
    }
    $write_item = red_item::get_red_object([
      'mode' => 'ui', 
      'id' => $item_id
    ]);
    if (!$write_item) {
      $message = red_t("Failed to save! Please contact support for help.");
      red_set_message($message, 'error');
      return FALSE;
    }
    // Items are in charge of sanitizing their own input.
    $write_item->set_user_input($_POST);

    $session_id = $this->user_input['session_id'];
    if (empty($session_id) || $session_id != session_id()) {
      $msg = red_t("Missing or invalid session id. You are either experiencing a bug 
        or someting sneaky is going on. If you really want to edit/insert 
        this record, please try again. Otherwise, please contact support."); 
      red_set_message($msg,'error');
      return $write_item;
    }
    // If the user makes a input error - return this item as the
    // edit_item. with these values filled in
    if (!$write_item->validate()) {
      red_set_message($write_item->get_errors('validation'),'error');
      return $write_item;
    }
    elseif (!$write_item->commit_to_db()) {
      red_set_message($write_item->get_errors('system'),'error');
      return $write_item;
    }
    else {
      red_set_message(red_t('Record updated.'),'success');
      // Surprise, it all worked out, break
      return true;
    }
  }

  function get_item_id_for_user_name($user_name) {
    $sql = "SELECT red_item.item_id FROM red_item INNER JOIN ".
      "red_item_user_account USING (item_id) WHERE user_account_login ".
      "= @user_name AND (item_status = 'active' OR item_status = ".
      "'pending-insert' OR item_status = 'pending-update' OR item_status = ".
      "'pending-restore' OR item_status = 'soft-error' OR item_status = 'hard-error')";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? NULL;
  }

  function get_item_id_for_mailbox_login($user_name) {
    $sql = "SELECT red_item.item_id FROM red_item INNER JOIN ".
      "red_item_mailbox USING (item_id) WHERE mailbox_login ".
      "= @user_name AND (item_status = 'active' OR item_status = ".
      "'pending-insert' OR item_status = 'pending-update' OR item_status = ".
      "'pending-restore' OR item_status = 'soft-error' OR item_status = 'hard-error')";
    $result = red_sql_query($sql, ['@user_name' => $user_name]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? NULL;
  }

  private function reports() {
    $user_name = $this->auth->get_user_name();
    if ($this->authz->is_admin()) {
      $report_type = 'error-items';
      if (!empty($this->user_input['report_type'])) {
        $report_type = $this->user_input['report_type'];
      }
      if ($report_type == 'error-items') {
        // Display stats on members behind on payments.
        $this->template->set_file('reports_file','error-items-report.ihtml');
        $title = "Error Items";
        $data = red_get_error_items(); 
      
        $this->template->set_block('reports_file', 'line', 'lines');
        $this->template->set_var('total', count($data));
        foreach($data as $row) {
          $this->template->set_var($row);
          $this->template->parse('lines', 'line', TRUE);
        }
      }
    }
    $this->template->parse('body_block','reports_file');
  }
}
?>
