<?php

// We excpect to have access to the $config variable. before we are included.

$common_files = [
  '/red.lang.utils.inc.php',
  '/red.utils.inc.php',
  '/class.red_db.inc.php',
  '/class.red_ado.inc.php',
  '/class.red_item.inc.php',
  '/modules/class.red_item_cron.inc.php',
  '/modules/class.red_item_dns.inc.php',
  '/modules/class.red_item_email_address.inc.php',
  '/modules/class.red_item_list.inc.php',
  '/modules/class.red_item_mailbox.inc.php',
  '/modules/class.red_item_mysql_db.inc.php',
  '/modules/class.red_item_mysql_user.inc.php',
  '/modules/class.red_item_nextcloud.inc.php',
  '/modules/class.red_item_psql.inc.php',
  '/modules/class.red_item_server_access.inc.php',
  '/modules/class.red_item_user_account.inc.php',
  '/modules/class.red_item_xmpp.inc.php',
  '/modules/class.red_item_web_app.inc.php',
  '/modules/class.red_item_web_conf.inc.php',
];

foreach ($common_files as $file) {
  require_once($config['common_src_path'] . $file);
}

$ui_files = [
  '/class.red_api.inc.php',
  '/class.red_auth.inc.php',
  '/class.red_authz.inc.php',
  '/class.red_disk_usage_notifier.inc.php',
  '/class.red_mx_verify.inc.php',
  '/class.red_queue.inc.php',
  '/class.red_web.inc.php',
  '/web/class.red_password.inc.php',
  '/web/class.red_sso.inc.php',
  '/web/class.red_oauth.inc.php',
  '/web/class.html_generator.inc.php',
  '/web/class.red_area.inc.php',
  '/web/class.red_area_hosting_order.inc.php',
  '/web/class.red_area_member.inc.php',
  '/web/class.red_area_top.inc.php',
  '/web/class.red_oauth.inc.php',
  '/web/class.red_sso.inc.php',
  '/web/template.inc.php',
  '/web/class.red_template.inc.php',
  '/modules/class.red.contact.inc.php',
  '/modules/class.red.correspondence.inc.php',
  '/modules/class.red.email_verify.inc.php',
  '/modules/class.red.hosting_order.inc.php',
  '/modules/class.red_item_cron_ui.inc.php',
  '/modules/class.red_item_dns_ui.inc.php',
  '/modules/class.red_item_email_address_ui.inc.php',
  '/modules/class.red_item_hosting_order_access_ui.inc.php',
  '/modules/class.red_item_list_ui.inc.php',
  '/modules/class.red_item_mailbox_ui.inc.php',
  '/modules/class.red_item_mysql_db_ui.inc.php',
  '/modules/class.red_item_mysql_user_ui.inc.php',
  '/modules/class.red_item_nextcloud_ui.inc.php',
  '/modules/class.red_item_pass_reset_ui.inc.php',
  '/modules/class.red_item_psql_ui.inc.php',
  '/modules/class.red_item_server_access_ui.inc.php',
  '/modules/class.red_item_user_account_ui.inc.php',
  '/modules/class.red_item_xmpp_ui.inc.php',
  '/modules/class.red_item_web_app_ui.inc.php',
  '/modules/class.red_item_web_conf_ui.inc.php',
  '/modules/class.red.map_user_member.inc.php',
  '/modules/class.red.member.inc.php',
  '/modules/class.red.search.inc.php',
  '/modules/class.red.unique_unix_group.inc.php',
  '/modules/class.red.vps.inc.php',
];

foreach ($ui_files as $file) {
  require_once($config['src_path'] . $file);
}

$sql_resource = red_db::init_db($config);  
if(!$sql_resource) {
  // This is not recoverable - and should trigger output
  red_log("Failed to get sql_resource when bootstrapping.");
  exit;
}

// Set our two global variables.
$globals['config'] = $config;
$globals['sql_resource'] = $sql_resource;
global $globals;

