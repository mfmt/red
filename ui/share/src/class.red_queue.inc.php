<?php

/**
 *
 * Class for managing a queue of tasks that have to be executed in the
 * background. This class represents a single queue job (in the red_queue
 * table), that might have many related queue tasks (in the red_queue_task
 * table).
 *
 * In addition, there is the run() method which provides more of a queue job
 * manager role of iterating through all run-able queue jobs and runs them.
 *
 **/

class red_queue  {

  /**
   * $tasks
   *
   * An array of tasks to either save or execute for the given
   * queue job.
   */
  private $tasks = [];

  /**
   * $item_id
   *
   * The item id of the corresponding item in the red_item table.
   */
  private $item_id = NULL;
  private $errors = [];
  private $job_count = 0;
  private $log_level = NULL;
  const LOG_LEVEL_INFO = 0;
  const LOG_LEVEL_ERROR = 1;

  // The following are used when executing a job.
  // The status of the item when the queue job was saved.
  private $initial_item_status = NULL;
  // The status of the item now.
  private $current_item_status = NULL;
  // The status of the most recent queue job for this item.
  private $queue_status = NULL;
  private $queue_message = NULL;
  private $queue_last_updated = NULL;

  // Possible queue and queue task statuses
  const QUEUE_STATUS_RUNNING = 'running';
  const QUEUE_STATUS_COMPLETED = 'completed';
  const QUEUE_STATUS_ERROR = 'error';
  const QUEUE_STATUS_PENDING = 'pending';

  /**
   *
   * $reset_status
   *
   * When running a job, you may optionally reset the item_status prior to
   * re-running the job to force the item to be re-processed.
   *
   * This is useful if the item is in an error state.
   *
   */
  private $reset_status = NULL;
  const RESET_STATUS_DISABLE = 'pending-disable';
  const RESET_STATUS_RESTORE = 'pending-restore';
  const RESET_STATUS_DELETE = 'pending-delete';

  private $queue_id = NULL;

  public function __construct() {
    $this->log_level = self::LOG_LEVEL_INFO;

  }

  public function set_quiet() {
    $this->log_level = self::LOG_LEVEL_ERROR;
  }

  public function set_restore() {
    $this->reset_status = self::RESET_STATUS_RESTORE;
  }
  public function set_disable() {
    $this->reset_status = self::RESET_STATUS_DISABLE;
  }
  public function set_delete() {
    $this->reset_status = self::RESET_STATUS_DELETE;
  }

  public function add_task($function, $args = [], $order = 0, $callback = NULL) {
    if (empty($function)) {
      throw new RedException("The function cannot be empty when adding queue tasks.");
    }
    $this->tasks[] = [
      'function' => $function,
      'args' => $args,
      'order' => $order,
      'callback' => $callback,
    ];
  }

  public function set_item_id($item_id) {
    $this->item_id = $item_id;
  }

  public function get_item_id() {
    return $this->item_id;
  }

  private function print($msg, $ending="\n") {
    if ($this->log_level < 1) {
      echo "{$msg}{$ending}";
    }
  }

  public function save(){
    if (count($this->tasks) == 0) {
      // Nothing to save.
      return TRUE;
    }

    if (!$this->get_item_id()) {
      throw new RedException("Cannot save queue without an item_id.");
    }

    // Ensure there isn't a pending or running task.
    $sql = "SELECT COUNT(*) FROM red_queue WHERE item_id = #item_id AND
      queue_status IN (@pending, @running)";
    $result = red_sql_query($sql, [
      '#item_id' => $this->get_item_id(),
      '@pending' => self::QUEUE_STATUS_PENDING,
      '@running' => self::QUEUE_STATUS_RUNNING
    ]);
    $row = red_sql_fetch_row($result);
    if ($row[0] > 0) {
      throw new RedException("A running or pending queue exists for item " . $this->get_item_id());
    }
    // Retrieve the current status.
    $sql = "SELECT item_status FROM red_item WHERE item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $this->get_item_id()]);
    $row = red_sql_fetch_row($result);
    $initial_status = $row[0];
    $queue_status = self::QUEUE_STATUS_PENDING;
    // Save the parent queue.
    $sql = "INSERT INTO red_queue
      SET
        item_id = #item_id,
        queue_status = @queue_status,
        queue_initial_item_status = @initial_status";
    red_sql_query($sql, [
      '#item_id' => $this->get_item_id(),
      '@queue_status' => $queue_status,
      '@initial_status' => $initial_status
    ]);
    $queue_id = red_sql_insert_id();

    // Create queue tasks.
    foreach ($this->tasks as $task) {
      $sql = "INSERT INTO red_queue_task SET queue_id = #queue_id,
        queue_task_function = @function, queue_task_args = @args,
        queue_task_order = #order, queue_task_status = @queue_status,
        queue_task_callback = @callback";
      red_sql_query($sql, [
        '#queue_id' => $queue_id,
        '@function' => $task['function'],
        '@callback' => $task['callback'],
        '@args' => serialize($task['args']),
        '#order' => $task['order'],
        '@queue_status' => $queue_status,
      ]);
    }
  }

  private function set_error($err) {
    $this->errors[] = $err;
  }

  private function get_errors() {
    return $this->errors;
  }

  private function acquire_lock() {
    // Obtain a lock just to ensure we are the only process running.
    $sql = "SELECT GET_LOCK('red-queue', 1)";
    $result = red_sql_query($sql);
    $row = red_sql_fetch_row($result);
    if ($row[0] == 1) {
      return TRUE;
    }
    $this->set_error("Failed to acquire a lock. There is probably another red-queue job running. Wait a few seconds and try again.");
    return FALSE;
  }

  private function dot() {
    $this->print(".", '');
  }

  /**
   * Clean up
   *
   * Only one run() process can run at a time, so convert any queue jobs
   * set to "running" to "error".
   */
  private function cleanup_running_jobs() {
    $sql = "UPDATE red_queue SET
      queue_status = @error,
      queue_message = 'Job was in running state, but was no longer running.'
      WHERE queue_status = @running";
    red_sql_query($sql, [
      '@error' => self::QUEUE_STATUS_ERROR,
      '@running' => self::QUEUE_STATUS_RUNNING,
    ]);
    // Same for queue tasks
    $sql = "UPDATE red_queue_task SET
      queue_task_status = @error
      WHERE queue_task_status = @running";
    red_sql_query($sql, [
      '@error' => self::QUEUE_STATUS_ERROR,
      '@running' => self::QUEUE_STATUS_RUNNING,
    ]);
  }

  /**
   * set_state
   *
   * Figure out and record what state this item is in.
   */
  private function set_state(int $item_id) {
    // Get the current item status.
    $sql = "SELECT item_status FROM red_item WHERE item_id = #item_id";
    $result = red_sql_query($sql, ['#item_id' => $item_id]);
    $row = red_sql_fetch_row($result);
    $this->current_item_status = $row[0] ?? NULL;

    if (!$this->current_item_status) {
      $this->set_error("Failed to find an item matching that id.");
      return FALSE;
    }

    // Check the queue status of the last time this task has run.
    $sql = "SELECT
        queue_id, queue_status, queue_initial_item_status, queue_message, queue_last_updated
      FROM red_queue
      WHERE item_id = #item_id
      ORDER BY queue_last_updated DESC LIMIT 1";
    $result = red_sql_query($sql, ['#item_id' => $item_id]);
    $row = red_sql_fetch_row($result);
    $this->queue_id = $row[0] ?? NULL;

    if (!$this->queue_id) {
      // It's possible this item has never been queued.
      return TRUE;
    }
    $this->queue_status = $row[1];
    $this->initial_item_status = $row[2];
    $this->queue_message = $row[3];
    $this->queue_last_updated = $row[4];

    return TRUE;
  }

  /**
   * We may have to rebuild and re-save the item before
   * running the queue.
   */
  private function force_item_status(int $item_id) {
    $this->set_state($item_id);
    // If there are any pending or running jobs we switch them to error.
    $sql = "UPDATE red_queue SET queue_status = @error_status WHERE
      queue_status IN (@running_status, @pending_status) AND item_id
      = #item_id";
    red_sql_query($sql, [
      '@error_status' => self::QUEUE_STATUS_ERROR,
      '@running_status' => self::QUEUE_STATUS_RUNNING,
      '@pending_status' => self::QUEUE_STATUS_PENDING,
      '#item_id' => $item_id,
    ]);

    // We can't change an item in a pending, hard error or deleted state so we have to first switch it
    // to something completed, and then we will build and update the item properly.
    $pending_or_error = [
      'pending-update',
      'pending-disable',
      'pending-restore',
      'pending-insert',
      'pending-update',
      'pending-delete',
      'hard-error',
      'deleted',
    ];
    if (in_array($this->current_item_status, $pending_or_error)) {
      $temp_status = NULL;
      if ($this->reset_status == SELF::RESET_STATUS_DISABLE) {
        $temp_status = 'disabled';
      }
      elseif ($this->reset_status == SELF::RESET_STATUS_RESTORE) {
        // All items in soft-error revert to pending restore, to handle
        // either recovering from pending-insert or pending-update.
        $temp_status = 'soft-error';
      }
      elseif ($this->reset_status == SELF::RESET_STATUS_DELETE) {
        $temp_status = 'disabled';
      }
      $sql = "UPDATE red_item SET item_status = @status WHERE item_id = #item_id";
      red_sql_query($sql, ['@status' => $temp_status, '#item_id' => $item_id]);
    }

    // Now we rebuild the item as if it was being updated via the control panel.
    $co = [
      'mode' => 'ui',
      'id' => $item_id,
    ];
    $item = red_item::get_red_object($co);
    if (!$item) {
      throw new RedException("Failed to build item from construction options");
    }
    $item->reset_to_db_values();
    if ($this->reset_status == SELF::RESET_STATUS_DISABLE) {
      $item->set_disable_flag();
    }
    elseif ($this->reset_status == SELF::RESET_STATUS_DELETE) {
      $item->set_delete_flag();
    }
    if (!$item->validate()) {
      $this->print("Failed to validate the item.");
      foreach ($item->get_errors() as $error) {
        $this->print(implode(' ', $error));
      }
      return FALSE;
    }
    if (!$item->commit_to_db()) {
      $this->print("Failed to re-save the item.");
      foreach ($item->get_errors() as $error) {
        $this->print(implode(' ', $error));
      }
      return FALSE;
    }
    return TRUE;
  }

  public function run() {
    if (!$this->acquire_lock()) {
      // Technically an error, but we return TRUE so it doesn't count as
      // an error, so the systemd job won't fail when this happens. Note:
      // the systemd job might do this if one red-queue run is long and over
      // laps with another or if an admin is running red-queue.
      return TRUE;
    }

    // Any jobs in the running status should be switched to error.
    $this->cleanup_running_jobs();

    if ($this->reset_status) {
      if (!$this->get_item_id()) {
        // Avoid accidentally deleting or disabling a lot of records.
        $this->set_error("You cannot pass --disable, --delete, --restore \
        unless you pass a single item id.");
        return FALSE;
      }
      else {
        $this->force_item_status($this->get_item_id());
      }
    }

    // Collect queue jobs to run.
    $sql = "SELECT queue_id, item_id FROM red_queue WHERE queue_status = @pending";
    $params = [
      '@pending' => self::QUEUE_STATUS_PENDING,
    ];
    $result = red_sql_query($sql, $params);
    while ($row = red_sql_fetch_row($result)) {
      $queue_id = $row[0];
      $item_id = $row[1];

      if ($this->get_item_id()) {
        if ($this->get_item_id() != $item_id) {
          continue;
        }
      }

      $this->set_state($item_id);
      $this->run_job($queue_id, $item_id);
      $this->job_count++;
    }
    return TRUE;
  }

  private function run_job($queue_id, $item_id) {
    if ($this->log_level < 1) {
      $this->print("Item ID: {$item_id}");
    }
    // Update queue job to indicate that we have started.
    $sql = "UPDATE red_queue SET queue_status = @running WHERE queue_id = #queue_id";
    red_sql_query($sql, [
      '#queue_id' => $queue_id,
      '@running' => self::QUEUE_STATUS_RUNNING,
    ]);

    // Fetch queue tasks.
    $sql = "SELECT
        queue_task_id,
        queue_task_function,
        queue_task_args,
        queue_task_callback
      FROM red_queue_task WHERE queue_id = #queue_id
      ORDER BY queue_task_order";
    $results = red_sql_query($sql, ['#queue_id' => $queue_id]);
    while ($row = red_sql_fetch_row($results)) {
      $this->dot();
      $queue_task_id = $row[0];
      // Update queue task to indicate that we have started.
      $sql = "UPDATE red_queue_task SET queue_task_status = @running
        WHERE queue_task_id = #queue_task_id";
      red_sql_query($sql, [
        '#queue_task_id' => $queue_task_id,
        '@running' => self::QUEUE_STATUS_RUNNING,
      ]);

      // Try to run it.
      $function = $row[1];
      $args = unserialize($row[2]);
      $callback = $row[3];
      try {
        $err = NULL;
        $red_error_table_updated = FALSE;
        // These functions should always throw a RedException on error.
        $result = call_user_func_array($function, $args);

        // Special check for errors for red_notify_host because that
        // process sets it own errors.
        if ($function == 'red_notify_host') {
          $sql = "SELECT item_status FROM red_item WHERE item_id = #item_id";
          $notify_result = red_sql_query($sql, ['#item_id' => $item_id]);
          $notify_row = red_sql_fetch_row($notify_result);
          if (preg_match('/-error/', $notify_row[0])) {
            $err = "Unexpected status after notify host: " . $notify_row[0];
            $sql = "SELECT error_log_message FROM red_error_log
              WHERE item_id = #item_id
              ORDER BY error_log_datetime
              DESC LIMIT 1";
            $err_result = red_sql_query($sql, ['#item_id' => $item_id]);
            $err_row = red_sql_fetch_row($err_result);
            $err .= " Last error recorded: " . $err_row[0] ?? NULL;
            $red_error_table_updated = TRUE;
          }
        }

        if (!$err && $callback) {
          // Run any callback functions on the results. This is used
          // to updated disk usage, etc.
          $callback($result);
        }
      }
      catch (RedException $e) {
        $err = $e->getMessage();
      }

      if ($err) {
        // Update queue job to reflect error.
        $sql = "UPDATE red_queue SET queue_status = @error_status, queue_message = @error
          WHERE queue_id = #queue_id";
        red_sql_query($sql, [
          '@error_status' => self::QUEUE_STATUS_ERROR,
          '@error' => $err,
          '#queue_id' => $queue_id
        ]);

        // Update the queue task_id status
        $sql = "UPDATE red_queue_task SET queue_task_status = @error_status
          WHERE queue_task_id = #queue_task_id";
        red_sql_query($sql, [
          '#queue_task_id' => $queue_task_id,
          '@error_status' => self::QUEUE_STATUS_ERROR,
        ]);

        // Update the red_item table as well.
        $sql = "UPDATE red_item SET item_status = 'soft-error'
          WHERE item_id = #item_id";
        red_sql_query($sql, ['#item_id' => $item_id]);

        $this->set_error($err);

        if (!$red_error_table_updated) {
          // red_notify_host inserts errors into the red_error table. But other calls
          // do not, so insert that record if necessary so it shows up in the UI.
          $sql = "INSERT INTO red_error_log SET
            item_id = #item_id,
            error_log_severity = 'soft',
            error_log_type = 'system',
            error_log_message = @err";
          red_sql_query($sql, ['#item_id' => $item_id, '@err' => $err]);
        }
        // Skip to the next job.
        return;
      }
      else {
        // Update the queue task_id status
        $sql = "UPDATE red_queue_task SET queue_task_status = @completed
          WHERE queue_task_id = #queue_task_id";
        red_sql_query($sql, [
          '#queue_task_id' => $queue_task_id,
          '@completed' => self::QUEUE_STATUS_COMPLETED,
        ]);
      }
    }
    // Update queue job to indicate that we have completed
    $sql = "UPDATE red_queue SET queue_status = @completed
      WHERE queue_id = #queue_id";
    red_sql_query($sql, [
      '#queue_id' => $queue_id,
      '@completed' => self::QUEUE_STATUS_COMPLETED,
    ]);
    // Update the item_id table.
    $item_status = NULL;
    if ($this->initial_item_status == 'pending-disable') {
      $item_status = 'disabled';
    }
    elseif (in_array($this->initial_item_status, [
      'pending-insert',
      'pending-update',
      'pending-restore'
    ])) {
      $item_status = 'active';
    }
    elseif ($this->initial_item_status == 'pending-delete') {
      $item_status = 'deleted';
    }
    else {
      throw new RedException("Unknown item status: {$this->initial_item_status}");
    }

    $sql = "UPDATE red_item SET item_status = @item_status
      WHERE item_id = #item_id";
    red_sql_query($sql, [
      '#item_id' => $item_id,
      '@item_status' => $item_status,
    ]);

    if ($this->log_level < 1) {
      $this->print('');
      $this->print("New state: {$item_status}");
    }
  }

  public function show() {
    $item_id = $this->get_item_id();
    if ($item_id) {
      if (!$this->set_state($item_id)) {
        return;
      }
      $this->print("Item ID: {$item_id}");
      if ($this->queue_id) {
        $this->print("Initial item status: {$this->initial_item_status}");
        $this->print("Current item status: {$this->current_item_status}");
        if ($this->reset_status) {
          $this->print("Will reset item status to: {$this->reset_status}");
        }
        $sql = "SELECT COUNT(*) FROM red_queue_task WHERE queue_id = #queue_id";
        $result = red_sql_query($sql, ['#queue_id' => $this->queue_id]);
        $row = red_sql_fetch_row($result);
        $count = $row[0];
        $this->print("Queue status: {$this->queue_status}");
        $this->print("Number of tasks in queue: {$count}");
        $this->print("Queue message: {$this->queue_message}");
        $this->print("Queue last updated: {$this->queue_last_updated}");
      }
      else {
        $this->print("No record in queue");
      }
    }
    else {
      $sql = "SELECT item_id FROM red_queue WHERE queue_status = @pending";
      $result = red_sql_query($sql, ['@pending' => self::QUEUE_STATUS_PENDING]);
      $item_ids = [];
      $display_item_ids = [];
      $count = 0;
      while ($row = red_sql_fetch_row($result)) {
        $count++;
        $item_ids[] = $row[0];
        if ($count < 10) {
          $display_item_ids[] = $row[0];
        }
      }
      $this->print("Pending items in queue: $count");
      $this->print("Item ids: " . implode(',', array_slice($display_item_ids, 0, 10)));
      if ($count > 10) {
        $more = $count - 10;
        $this->print("  (and {$more} more...)");
      }
    }
    $this->print('');
  }

  public function summary() {
    $this->print("Completed {$this->job_count} jobs.");
    $err_count = count($this->errors);
    if ($err_count == 0) {
      $this->print("No errors.");
    }
    else {
      $this->print("{$err_count} error(s).");
      foreach ($this->errors as $err) {
        $this->print("{$err}");
      }
    }
  }

  public function task_count() {
    return count($this->tasks);
  }

}
