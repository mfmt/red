<?php

if (!class_exists('red_item_xmpp_ui')) {
  class red_item_xmpp_ui extends red_item_xmpp {
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to deleete the xmpp account for " . 
        $this->_html_generator->get_tag('span', $this->get_xmpp_login(), $attr) . "?";
    }

    function get_edit_xmpp_login() {
      if ($this->exists_in_db()) {
        return $this->get_auto_constructed_read_field('xmpp_login');
      }
      return $this->get_auto_constructed_edit_field('xmpp_login');
    }

    function _post_commit_to_db() {
      $this->_notify_host = FALSE;
      $action = NULL;
      $item_id = $this->get_item_id();
      if ($this->get_item_status() == 'pending-delete') {
        $action = 'delete';
      }
      else if ($this->get_item_status() == 'pending-delete') {
        $action = 'disable';
      }
      else {
        $action = 'enable';
      }
      $this->queue->add_task('red_item_xmpp::apply', [$item_id, $action], 10);
      return parent::_post_commit_to_db();
    }
  }
}

?>
