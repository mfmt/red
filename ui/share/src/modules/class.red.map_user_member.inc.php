<?php

class red_map_user_member extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'map_user_member_id';
  var $_key_table = 'red_map_user_member';

  function get_red_construction_options() {
    
    return $this->_construction_options;

  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete this member access record?");
  }

  function _initialize_from_id($id) {
    $sql = "SELECT * FROM red_map_user_member ". 
      "WHERE map_user_member_id = #id";
    $params = ['#id' => $id];
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_assoc($result);
    return $this->_initialize_from_recordset($row);
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_status('deleted');
  }

  var $_map_user_member_id;
  function set_map_user_member_id($value) {
    $this->_map_user_member_id = $value;
  }

  function get_map_user_member_id() {
    return $this->_map_user_member_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_status;
  function get_status() {
    return $this->_status;
  }

  function set_status($value) {
    $this->_status = $value;
  }

  var $_login;
  function get_login() {
    return $this->_login;
  }

  function set_login($value) {
    $this->_login = $value;
  }

  // constructor
  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->member_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_status('active');
    }
  }


  function _set_datafields() {
    $this->_datafields = array(
      'map_user_member_id' => array(
        'fname' => red_t('Map user member id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_map_user_member',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => TRUE,
        'tblname' => 'red_map_user_member',
      ),
      'status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_map_user_member',
      ),
      'login' => array (
        'req' => true,
        'pcre'   => RED_LOGIN_MATCHER,
        'pcre_explanation'   => RED_LOGIN_EXPLANATION,
        'type'  => 'varchar',
        'fname'  => red_t('Login name'),
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'user_visible' => TRUE,
        'input_type' => 'text',
        'text_length' => 20,
        'text_max_length' => 128,
        'tblname'   => 'red_map_user_member'),

    );
  }

  function additional_validation() {
    if ($this->_delete) {
      return;
    } 
    // Ensure user submitted exists and is active
    $sql = "SELECT COUNT(*) AS count FROM red_item JOIN red_item_user_account USING (item_id)
      WHERE item_status IN ('active', 'pending-insert', 'pending-update', 'pending-restore')
      AND user_account_login = @login";
    $result = red_sql_query($sql, ['@login' => $this->get_login()]);
    $row = red_sql_fetch_row($result);
    if ($row[0] == 0) {
      $this->set_error(red_t("Please choose a user name that is active in the system."), 'validation');
    }
  }
}
