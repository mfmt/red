<?php

if(!class_exists('red_item_user_account_ui')) {
  class red_item_user_account_ui extends red_item_user_account {
   var $is_admin = false;
   var $_javascript_includes = array('scripts/password_generator.js','scripts/item_user_account.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the user account " . 
        $this->_html_generator->get_tag('span',$this->get_user_account_login(),$attr) . 
        "? The user's home directory will be deleted.";
    }

    function get_edit_user_account_password() {
      // display confirm box
      $value = $this->get_user_account_password();

      $pass_input = $this->_html_generator->get_input('sf_user_account_password','','password', array('size' => 30,'id' => 'user_account_password')) . '<br />';

      // create link for user to generate random password
      $link_attr = array(
        'href' => '#',
        'onClick' => "pass=generateRandomPassword(12);parsePassword(pass);return false;"
      );
      $password_link = $this->_html_generator->get_tag('a','Create random password',$link_attr);
      // Create tag to display the password when the users clicks it
      $display_attr = array('id' => 'red_display_password');
      $password_display = $this->_html_generator->get_tag('span','',$display_attr) . '<br />';

      // password confirm box
      $pass_confirm_input = $this->_html_generator->get_input('sf_user_account_password_confirm','','password', array('size' => 30,'id' => 'user_account_password_confirm')) . '<br />';

      // Directions for password confirm box
      $message_em = $this->_html_generator->get_tag('em',red_t('Please confirm your password (or leave blank if not changing)'));
      $message_small = $this->_html_generator->get_tag('small', $message_em);
      $message = $this->_html_generator->get_tag('div', $message_small);
      return $pass_input . $password_link . $password_display .
        $pass_confirm_input . $message;
    }

    // Return an array with the item_id and mailbox status of any mailbox record linked
    // to this user account.
    function get_mailbox() {
      $login = $this->get_user_account_login();
      $sql = "SELECT * 
        FROM red_item JOIN red_item_mailbox USING(item_id)  
        WHERE mailbox_login = @login AND item_status != 'deleted'";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_assoc($result);
      if (empty($row)) {
        return [];
      }
      return $row;
    }
    function get_server_access() {
      $login = $this->get_user_account_login();
      $sql = "SELECT * 
        FROM red_item JOIN red_item_server_access USING(item_id)  
        WHERE server_access_login = @login AND item_status != 'deleted'";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_assoc($result);
      if (empty($row)) {
        return [];
      }
      return $row;
    }

    function get_video_meeting() {
      $login = $this->get_user_account_login();
      $sql = "SELECT red_item.*, red_item_xmpp.*
        FROM red_item JOIN red_item_xmpp USING(item_id)  
        JOIN red_service USING (service_id)
        WHERE xmpp_login = @login AND
        service_name = 'video_meeting' AND
        item_status != 'deleted'";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_assoc($result);
      if (empty($row)) {
        return [];
      }
      return $row;
    }

    function get_chat() {
      $login = $this->get_user_account_login();
      $sql = "SELECT red_item.*, red_item_xmpp.* 
        FROM red_item JOIN red_item_xmpp USING(item_id)  
        JOIN red_service USING (service_id)
        WHERE xmpp_login = @login AND
        service_name = 'chat' AND
        item_status != 'deleted'";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_assoc($result);
      if (empty($row)) {
        return [];
      }
      return $row;
    }

    function get_nextcloud() {
      $login = $this->get_user_account_login();
      $sql = "SELECT * 
        FROM red_item JOIN red_item_nextcloud USING(item_id)  
        WHERE nextcloud_login = @login AND item_status != 'deleted'";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_assoc($result);
      if (empty($row)) {
        return [];
      }
      return $row;
    }
    function get_read_pseudo_related() {
      global $globals;
      $url_path = $globals['config']['url_path'];

      $overriden = $this->get_pseudo_related();

      if ($overriden) {
        return $overriden;
      }
      $login = $this->get_user_account_login();
      $possible_items = [
        'mailbox' => [
          'name' => red_t('Mailbox'),
          'title' => red_t("Add a mailbox to this account"),
          'description' => red_t("When configured with a mailbox, you can receive email at @login@mail.mayfirst.org and/or set your own email address via the Email Address tab", ['@login' => $login]),
          'link_text' => red_t("Set the auto responder here."),
        ],
        'nextcloud' => [
          'name' => red_t('Nextcloud'),
          'title' => red_t("Add a nextcloud login to this account"),
          'description' => red_t("When configured with a nextcloud item, you can access our cloud server via https://share.mayfirst.org."),
          'link_text' =>  red_t("Set quota here."),
        ],
        'server_access' => [
          'name' => red_t('Server Access'),
          'title' => red_t("Add a server access login to this account"),
          'description' => red_t("When configured with a server acccess item, you can login via ssh or secure FTP to make changes to your web site."),
          'link_text' => red_t("Upload or modify public ssh key here."),
        ],
        'video_meeting' => [
          'name' => red_t('Video Meeting'),
          'title' => red_t("Add a video meeting login to this account"),
          'description' => red_t("When configured with a video meeting login, you can create web-based video meetings via https://meet.mayfirst.org."),
          'link_text' => '',
        ],
        'chat' => [
          'name' => red_t('Chat'),
          'title' => red_t("Add a chat login to this account"),
          'description' => red_t("When configured with a chat user, you can securely chat with other XMPP chat users via https://im.mayfirst.org."),
          'link_text' => '',
        ],
      ];
      $hosting_order_id = $this->get_hosting_order_id();
      $access_level = red_authz::get_membership_level_for_id($hosting_order_id, 'hosting_order');

      // Get current service access levels and put into easy to manage array.
      $sql = "SELECT service_name, service_basic_access, service_standard_access FROM red_service";
      $result = red_sql_query($sql);
      $service_access_levels = [];
      while ($row = red_sql_fetch_row($result)) {
        $service_name = $row[0];
        $basic = $row[1];
        $standard = $row[2];
        $service_access_levels[$service_name] = [
          'basic' => $basic,
          'standard' => $standard,
        ];
      }
      $items = [];
      // Check which services this hosting order has access to.
      $allowed_levels = ['full', 'single'];
      foreach ($possible_items as $service_name => $val) {
        $access = $service_access_levels[$service_name][$access_level];
        if (in_array($access, $allowed_levels)) {
          $items[$service_name] = $val;
        }
      }
      $ret = '';
      $div_attr = [ 'class' => 'red-related-user-account-item' ];
      if (empty($login)) {
        // Might be a new item.
        $login = 'your-selected-login';
      }
      foreach ($items as $item => $item_props) {
        $func = "get_{$item}";
        $value = $this->$func();
        $label = $item_props["title"]; 
        $options = [
          '1' => $this->_html_generator->get_tag('label', $label),
        ];
        $checkbox_attributes = [ 'class' => 'red-checkbox', 'id' => "pseudo_add_{$item}" ];
        if (empty($value)) {
          $selected = $_POST["pseudo_add_{$item}"] ?? [];
          if ($selected) {
            $selected = [ 1 ];
          }
          $checkbox = $this->_html_generator->get_checkboxes("pseudo_add_{$item}[]",$options, $selected, $checkbox_attributes);
          $explanation_text = $item_props["description"];
          $explanation_em = $this->_html_generator->get_tag('em', $explanation_text);
          $explanation_small = $this->_html_generator->get_tag('small', $explanation_em);
          $explanation = $this->_html_generator->get_tag('div', $explanation_small);
          $ret .= $this->_html_generator->get_tag('div', $checkbox . $explanation, $div_attr);
        }
        else {
          $initial_text = red_t("This user account is linked to a @item item.", [ '@item' => $item_props['name'] ]);
          $selected = TRUE;
          $checkbox_attributes['disabled'] = 'disabled';
          $checkbox = $this->_html_generator->get_checkboxes("disabled_pseudo_add_{$item}[]",$options, $selected, $checkbox_attributes);
          $link_text = $item_props['link_text'];
          if ($value['item_status'] == 'active') {
            $query_params = [
              'area' => 'hosting_order',
              'hosting_order_id' => $this->get_hosting_order_id(),
              'service_id' =>  red_get_service_id_for_service_name($item),
              'action' => 'edit',
              'item_id' => $value['item_id'],
            ];
            $link_attributes = [
              'href' => $url_path . 'index.php?' . http_build_query($query_params),
            ];
            $link = $this->_html_generator->get_tag('a', $link_text, $link_attributes );
            $content = $checkbox . $initial_text . ' ' . $link;
            $ret .= $this->_html_generator->get_tag('div', $content, $div_attr);
          }
          else {
            $status = red_t("But, the status is @status.", [ '@status' => $value['item_status'] ]);
            $link_text = red_t("Click here to review.");
            $post_text = red_t("Contact support if you need help!");
            $query_params = [
              'area' => 'hosting_order',
              'hosting_order_id' => $this->get_hosting_order_id(),
              'service_id' => red_get_service_id_for_service_name($item),
            ];
            $link_attributes = [
              'href' => $url_path . 'index.php?' . http_build_query($query_params),
            ];
            $link = $this->_html_generator->get_tag('a', $link_text, $link_attributes );
            $content = $checkbox . $initial_text . ' ' .  ' ' . $status . ' ' . $link . ' ' . $post_text;
            $ret .= $this->_html_generator->get_tag('div', $content, $div_attr);
          }
        }
      }
      return $ret;
    }

    function set_user_input($post) {
      $this->set_item_id($post['sf_item_id']);
      $this->reset_to_db_values();
      foreach ($post as $k => $v) {
        if (preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          // This field should not be set by the class - it's a  
          // helper field
          if($field == 'user_account_password_confirm') continue;

          // We need to auto create/manipulate the password field 
          if($field == 'user_account_password') {
            // this is hacky - validation errors should happen
            // when validation... but then the post variable is not
            // available there and by then the password will be
            // encrypted ...
            $login = '';
            if(array_key_exists('sf_user_account_login',$post)) {
              // it's a new account
              $login = $post['sf_user_account_login'];
            }
            else {
              $login = $this->get_user_account_login();
            }
            if($v == '') {
              // Continue - if this record is being updated, then the
              // value from the db will be used. If it is a new record
              // then the empty password will be caught by the validate
              // script
              continue;
            }
            elseif($v != $post['sf_user_account_password_confirm']) {
              $this->set_error(red_t("Your passwords do not match."),'validation');
            }
            elseif(!red_is_good_password($v)) {
              $this->set_error(red_t('Please use a password that is longer and has a greater variety of letters and numbers.'),'validation');
            }
            elseif($v == 'b@mzit5' || $v == 'NuX@gwb' || $v == 'no$n1ch') {
              $this->set_error(red_t("Use your imagination. Come up with your own password."),'validation');
            }
            elseif($v == $login) {
              $this->set_error(red_t("Please don't set your password to the same value as your username. That's one of the first guesses of any EvilGenius."),'validation');
            }
          }
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }

    function _pre_commit_to_db() {
      if (!parent::_pre_commit_to_db()) return FALSE;
      if ($this->_delete) {
        // Replace the password for privacy reasons and to avoid login trouble
        // with usernames that have the same password.
        // See: https://support.mayfirst.org/ticket/7507
        $password = red_generate_random_password(25);
        if(FALSE === $password) {
            $this->set_error(red_t("Failed to generate random password."),'system');
            return FALSE;
        }
        $this->set_field_value('user_account_password', $password);

        // Remove the user from sitewide admins if they are a sitewide admin and being deleted.
        // See: https://support.mayfirst.org/ticket/7191
        $sql = "DELETE FROM red_sitewide_admin WHERE user_name = @user_name";
        red_sql_query($sql, ['@user_name' => $this->get_user_account_login()]);
      }
      return TRUE;
    }

    function _post_commit_to_db() {
      if (!$this->clear_loginservice_cache()) {
        $this->set_error(red_t("Failed to clear login service cache."), "system", "soft");
        return FALSE;
      }
      if ($this->_delete) {
        // If this is a delete, then we delete all related password reset requests, hosting_order_access
        // and member access records to avoid validation errors.
        $login = $this->get_user_account_login();
        $sql = "UPDATE red_item SET item_status = 'deleted' WHERE item_id IN (
          SELECT item_id FROM red_item_pass_reset WHERE pass_reset_login = @login)";
        red_sql_query($sql, ['@login' => $login]);

        $sql = "UPDATE red_map_user_member SET status = 'deleted' WHERE login = @login";
        red_sql_query($sql, ['@login' => $login]);

        $sql = "UPDATE red_item SET item_status = 'deleted'
          WHERE item_id IN (
            SELECT item_id 
            FROM red_item_hosting_order_access 
            WHERE hosting_order_access_login = @login
          )
        ";
        red_sql_query($sql, ['@login' => $login]);
      }
      
      // Run post commit so the user account is saved before we create
      // the related records.
      if (!parent::_post_commit_to_db()) {
        return FALSE;
      }
      if (!$this->_delete) {
        if ($this->on_new_infrastructure()) {
          // Update the password on the shell server if a record exists.
          $sql = "SELECT item_host FROM red_item join red_item_server_access USING (item_id)
            WHERE server_access_login = @login AND item_status != 'deleted'";
          $result = red_sql_query($sql, ['@login' => $this->get_user_account_login()]);
          $row = red_sql_fetch_row($result);
          $server_access_host = $row[0] ?? NULL;
          if ($server_access_host) {
            $setPasswordParams = [
              $this->get_user_account_login(),
              $this->get_user_account_password(),
              $server_access_host,
            ];
            $this->queue->add_task(
              'red_item_server_access:set_password',
              $setPasswordParams,
              10
            );
          }
        }
        // Check if we should be creating related records.
        $related_services = [ 'server_access', 'mailbox', 'nextcloud', 'video_meeting', 'chat' ];
        foreach ($related_services as $service_name) {
          $item_type = $service_name;
          if (in_array($service_name, ['video_meeting', 'chat'])) {
            $item_type = 'xmpp';
          }

          // Check if we already have the related item.
          $func = "get_{$service_name}";
          $existing_item = $this->$func();

          $create = $_POST["pseudo_add_{$service_name}"] ?? FALSE;
          if ($create) {
            // Create the record if it doesn't already exist.
            if (!$existing_item) {
              $co = [
                'mode' => 'ui',
                'service_name' => $service_name, 
                "hosting_order_id" => $this->get_hosting_order_id(),
              ];
              $obj = red_item::get_red_object($co);
          
              $set_login_func = "set_{$item_type}_login";
              $obj->$set_login_func($this->get_user_account_login());
              if (!$obj->validate() || !$obj->commit_to_db()) {
                $params = [
                  '@item' => $item_type,
                  '@errors' => $obj->get_errors_as_string(),
                ];
                $msg = red_t("Faled to create @item record. Errors: @errors", $params); 
                $this->set_error($msg, "system", "soft");
                $this->auto_set_item_status();
                return FALSE;
              }
            }
          }
          if ($this->exists_in_db() && $existing_item) {
            // The "create" checkbox is not available when editing records.
            // So, this block kicks in when editing.
            //
            // Check if we already have the item and it's host is different
            // from the user account host (won't be necessary after we move
            // everyone to the new infrastructure), in which case we should
            // toggle it to pending update so any password change is
            // propagated.
            if ($service_name == 'nextcloud') {
              // Nextcloud doesn't set a password.
              continue;
            }
            $host = $this->get_item_host(); 
            if ($existing_item['item_host'] != $host && $existing_item['item_status'] == 'active') {
              $co = ['rs' => $existing_item, 'mode' => 'ui'];
              $obj = red_item::get_red_object($co);
              if (!$obj) {
                $msg = red_t("Faled to create the related @item object.", [ '@item' => $item_type ]);
                $this->set_error($msg, "system", "soft");
                $this->auto_set_item_status();
                return FALSE;
              }
              if (!$obj->commit_to_db()) {
                $params = [ '@err' => $obj->get_errors_as_string(), '@item' => $item_type ];
                $msg = red_t("Faled to update related @item record: @err.", $params );
                $this->set_error($msg, "system", "soft");
                $this->auto_set_item_status();
                return FALSE;
              }
            }
          }
        }
      }
      return TRUE;
    }

    function set_user_account_password($value) {
      // If there is a value and it does not begin with $1$ (md5) or $6$
      // (sha512), then encrypt it 
      if(!empty($value) && substr($value,0,3) != '$1$' && substr($value,0,3) != '$6$') {
        $value = $this->crypt($value);
      }
        return parent::set_user_account_password($value);
    }

    public static function crypt($value,$salt = '') {
      // Auto generate the salt passed on the configuration parameter.
      if(empty($salt)) {
        if(!defined('RED_PASSWORD_HASH_TYPE') || RED_PASSWORD_HASH_TYPE == 'md5') {
          $prefix = '$1$';
        } elseif (RED_PASSWORD_HASH_TYPE == 'sha512') {
          // rounds= tells php how many times the hashing loop should be executed.
          // Should be an integer between 1000 and 999,999,999. Default is 5000.
          // We calculated that on dkg's machine, 100,000 rounds took 100 ms to
          // verify. We are using an odd number in case someone has created a
          // table already.
          $prefix = '$6$rounds=199999$';
        } else {
          // Use default
          $prefix = '$1$';
        }
        // Our password function includes the '+' character which is not allowed
        // as a salt, but the period is, so make the replacement.
        $salt = $prefix . str_replace('+', '.', red_generate_random_password(16));
      }
      return crypt($value,$salt);
    }


    function get_edit_user_account_mountpoint() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('user_account_mountpoint');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    function additional_validation() {
      parent::additional_validation();
      // Only admins are allowed to change the mountpoint parameter.
      if (!$this->is_admin) {
        $item_id = $this->get_item_id();
        $mountpoint = $this->get_user_account_mountpoint();
        if (!empty($mountpoint)) {
          // We are going to throw an error if the submitted value is not
          // the same as the value in the database.
          $throw_error = TRUE;
          if ($this->exists_in_db()) {
            $sql = "SELECT user_account_mountpoint FROM red_item_user_account WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] == $mountpoint) {
              // You are saved, no error.
              $throw_error = FALSE;
            }
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set mountpoint.'), 'validation');
          }
        }
        

        // Max user disk usage is 50gb. This ensures that anyone who needs more space
        // has to notify an admin, who can make sure there is enough space on the partition
        // to support it.
        $max_quota = 50 * 1024 * 1024 * 1024;
        $user_set_value = red_machine_readable_bytes($this->get_item_quota());
        if ($user_set_value > $max_quota) {
          $error_message = red_t('Only administrators can set a quota higher then 50GB. This restriction ensures we have enough disk space on the server to accomodate your needs. Please contact support and we can help.');
          // They have exceeded the limit. This is going to throw a
          // validation error unless they are editing the record and
          // the have not changed the value that was stored in the db
          // by an admin on an earlier save.
          if ($this->exists_in_db()) {
            $item_id = intval($this->get_item_id());
            $sql = "SELECT item_quota FROM red_item WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // Only if they are different do we have a problem. 
            if($row[0] != $user_set_value) {
              $this->set_error($error_message, 'validation');
            }
          }
          else {
            // It's a new record, always throw the error.
            $this->set_error($error_message, 'validation');
          }
        }
      }
    }
  }  
}

?>
