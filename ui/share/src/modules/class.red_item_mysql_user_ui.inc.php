<?php

if(!class_exists('red_item_mysql_user_ui')) {
  class red_item_mysql_user_ui extends red_item_mysql_user {
    // provides password generator
    var $_javascript_includes = array('scripts/password_generator.js','scripts/item_mysql_user.js');
    var $is_admin = false;
    var $_max_allowed_user_set_connections = 25;

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the MySQL user " . 
         $this->_html_generator->get_tag('span',$this->get_mysql_user_name(),$attr) . "?";
    }
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
    function get_edit_mysql_user_db() {
      $dbs = $this->get_mysql_user_db();
      if (!$dbs) {
        $dbs = [];
      }
      else {
        $dbs = explode(':', $dbs);
      }
      $options = [];
      $available = $this->get_available_databases();
      foreach($available as $db => $host) {
        $options[$db] = $db . " ($host)";
      }
      // Reformat for use as option list.

      return $this->_html_generator->get_checkboxes('sf_mysql_user_db[]', $options, $dbs);
    }

    function get_read_mysql_user_name() {
      $user_name = $this->get_mysql_user_name();
      $dbs = explode(':', $this->get_mysql_user_db());
      $db = array_pop($dbs);

      return $user_name . ' ' . 
        $this->_html_generator->get_tag('em', "(mysql://{$user_name}:[password]@localhost/{$db})");

    }

    function get_edit_mysql_user_priv() {
      $priv = $this->get_mysql_user_priv();
      $options = $this->get_priv_options();
      return $this->_html_generator->get_select('sf_mysql_user_priv',$options,$priv);
    }
    function get_edit_mysql_user_password() {
      // display confirm box
      $value = $this->get_mysql_user_password();

      $pass_input = $this->_html_generator->get_input('sf_mysql_user_password','','password', array('size' => 30,'id' => 'mysql_user_password')) . '<br />';

      // create link for user to generate random password
      $link_attr = array(
        'href' => '#',
        'onClick' => "pass=generateRandomPassword(10);parsePassword(pass);return false;"
      );
      $password_link = $this->_html_generator->get_tag('a','Create Random Pass',$link_attr);
      // Create tag to display the password when the users clicks it
      $display_attr = array('id' => 'red_display_password');
      $password_display = $this->_html_generator->get_tag('span','',$display_attr) . '<br />';

      // password confirm box
      $pass_confirm_input = $this->_html_generator->get_input('sf_mysql_user_password_confirm','','password', array('size' => 30,'id' => 'mysql_user_password_confirm')) . '<br />';

      // Directions for password confirm box
      $explanation_attr = array('class' => 'red_explanation');
      $message = $this->_html_generator->get_tag('span',red_t('Please confirm the database user\'s password (or leave blank if not changing)'),$explanation_attr) . '<br />';
      return $pass_input . $password_link . $password_display .
        $pass_confirm_input . $message;
    }

    function set_user_input($post) {
      $this->set_item_id($post['sf_item_id']);
      $this->reset_to_db_values();
      foreach($post as $k => $v) {
        if(preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          // This field should not be set by the class - it's a  
          // helper field
          if($field == 'mysql_user_password_confirm') continue;

          // We need to auto create/manipulate the password field 
          if($field == 'mysql_user_password') {
            // this is hacky - validation errors should happen
            // when validation... but then the post variable is not
            // available there and by then the password will be
            // encrypted ...
            $login = '';
            if(array_key_exists('sf_mysql_user_name',$post)) {
              // it's a new account
              $login = $post['sf_mysql_user_name'];
            } else {
              $login = $this->get_mysql_user_name();
            }
            if($v == '') {
              // Continue - if this record is being updated, then the
              // value from the db will be used. If it is a new record
              // then the empty password will be caught by the validate
              // script
              continue;
            } elseif($v != $post['sf_mysql_user_password_confirm']) {
              $this->set_error(red_t("The database user\'s passwords do not match."),'validation');
            } elseif(!red_is_good_password($v)) {
              $this->set_error(red_t('Please use a password that is longer and has more variation in letters and numbers.'),'validation');
            } elseif($v == 'b@mzit5' || $v == 'NuX@gwb' || $v == 'no$n1ch') {
              $this->set_error(red_t("Use your imagination. Come up with your own password."),'validation');
            } elseif($v == $login) {
              $this->set_error(red_t("Please don't set the database user\'s password to the same value as the account\'s name. That's one of the first guesses of any EvilGenius."),'validation');
            } else {
              $sql = "SELECT PASSWORD(@v)";
              $result = red_sql_query($sql, ['@v' => $v]);
              $row = red_sql_fetch_row($result);
              $v = $row[0];
            }
          } 
          elseif($field == 'mysql_user_db') {
            $dbs = array();
            foreach($v as $checkbox) {
              $dbs[] = array_pop($checkbox);
            }
            $v = implode(':', $dbs);
          }
          if ($this->accept_user_input($field)) {
            $this->set_field_value($field, $v);
          }
        }
      }
    }

    function additional_validation() {
      parent::additional_validation();
      if ($this->get_mysql_user_priv() == 'full') {
        $dbs = $this->get_over_quota_databases();
        if ($dbs) {
          $dbs = implode(':', $dbs);
          $this->set_error(red_t("This user has full access to a database that is over quota. Please remove access to the following database or set to read only: @dbs", ['@dbs' => $dbs]),'validation');
        }
      }
      if ($this->get_mysql_user_max_connections() > $this->_max_allowed_user_set_connections && !$this->is_admin) {
          // This is only allowed if the value is the same as the value currently in the database
        // (otherwise, regular users could not edit a record that was increased by an admin).
        if($this->exists_in_db()) {
          // Get the value of max_connections currently saved in the db.
          $item_id = intval($this->get_item_id());
          $sql = "SELECT mysql_user_max_connections FROM red_item_mysql_user WHERE item_id = #item_id";
          $result = red_sql_query($sql, ['#item_id' => $item_id]);
          $row = red_sql_fetch_row($result);
          // If they are different, then this user is trying to change this value. We don't
          // allow that.
          if($row[0] != $this->get_mysql_user_max_connections()) {
            $this->set_error(red_t('Only administrators are allowed to set Max Connections greater than @max_connections.',array('@max_connections' => $this->_max_allowed_user_set_connections)), 'validation');
          }
        }
      }
    }
    function _pre_commit_to_db() {
      if (!parent::_pre_commit_to_db()) return FALSE;
      // If the hosting order host is using proxysql, than every time a mysql
      // user account changes we have to notify the host to pull in the new
      // mysql user info from all network databases so we stay up to date.
      $host = $this->get_hosting_order_host();
      $sql = "SELECT server_proxysql FROM red_server WHERE server = @host";
      $result = red_sql_query($sql, ['@host' => $host]); 
      $row = red_sql_fetch_row($result);
      if ($row[0] == 1) {
        $this->queue->add_task('red_update_network_mysql_users', [$host], 20);
      }
      return TRUE;
    }

    function _post_commit_to_db() {
      if ($this->on_new_infrastructure()) {
        // We do not notify the host - we handle everything via the queue
        $this->_notify_host = FALSE;
        $status = $this->get_item_status();
        $action = NULL;
        if (in_array($status, ['pending-insert', 'pending-update', 'pending-restore'])) {
          $action = 'create';
        }
        elseif (in_array($status, ['pending-delete', 'pending-disable'])) {
          $action = 'delete';
        }
        if (!$action) {
          red_log("Mising action for status {$status}");
        }
        else {
          $item_id = $this->get_item_id();
          $this->queue->add_task('red_item_mysql_user::apply', [$item_id, $action], 10);
        }
      }
      return parent::_post_commit_to_db();
    }
  }  
}

?>
