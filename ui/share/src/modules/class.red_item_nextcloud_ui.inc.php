<?php

if(!class_exists('red_item_nextcloud_ui')) {
  class red_item_nextcloud_ui extends red_item_nextcloud {
    var $is_admin = FALSE;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to deleete the nextcloud account for " . 
        $this->_html_generator->get_tag('span',$this->get_nextcloud_login(),$attr) . 
        "? All Nextcloud documents owned by this user will be purged.";
    }

    function get_edit_nextcloud_login() {
      if($this->exists_in_db()) return $this->get_auto_constructed_read_field('nextcloud_login');
      return $this->get_auto_constructed_edit_field('nextcloud_login');
    }

    function get_edit_nextcloud_abandoned() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('nextcloud_abandoned');
      }
      else {
        if ($this->get_nextcloud_abandoned() == 0) {
          return red_t("Not abandoned");
        }
        else {
          return red_t("Yes, over 6 months without a login");
        }
      }
    }

    function additional_validation() {
      parent::additional_validation();
      // Only applies to non-admins.
      if (!$this->is_admin) {
        // Max user disk usage is 50gb. This ensures that anyone who needs more space
        // has to notify an admin, who can make sure there is enough space on the partition
        // to support it.
        $max_quota = 50 * 1024 * 1024 * 1024;
        $user_set_value = red_machine_readable_bytes($this->get_item_quota());
        if ($user_set_value > $max_quota) {
          $error_message = red_t('Only administrators can set a quota higher then 50GB. This restriction ensures we have enough disk space to accomodate your needs. Please contact support and we can help.');
          // They have exceeded the limit. This is going to throw a
          // validation error unless they are editing the record and
          // the have not changed the value that was stored in the db
          // by an admin on an earlier save.
          if ($this->exists_in_db()) {
            $item_id = $this->get_item_id();
            $sql = "SELECT item_quota FROM red_item WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // Only if they are different do we have a problem. 
            if($row[0] != $user_set_value) {
              $this->set_error($error_message, 'validation');
            }
          }
          else {
            // It's a new record, always throw the error.
            $this->set_error($error_message, 'validation');
          }
        }
      }
    }

    function _post_commit_to_db() {
      // We do not notify the host - we handle everything via the queue
      $this->_notify_host = FALSE;
      $quota = NULL;
      $check_disk_usage = FALSE;
      if ($this->get_item_status() == 'pending-insert') {
        $action = 'add';
        $quota = $this->get_item_quota();
      }
      elseif ($this->get_item_status() == 'pending-disable') {
        $action = 'disable';
      }
      elseif ($this->get_item_status() == 'pending-delete') {
        $action = 'delete';
      }
      else {
        $action = 'enable';
        $quota = $this->get_item_quota();
        // Only check disk usage on enable, When adding we get an error
        // if the user has not yet logged in.
        $check_disk_usage = TRUE;
      }
      $item_id = $this->get_item_id();
      $this->queue->add_task('red_item_nextcloud::apply', [$item_id, $action], 10);
      if ($check_disk_usage) {
        // Check the disk usage and update.
        $service = 'nextcloud';
        $server = NULL;
        $this->queue->add_task('red_item::get_disk_usage_for_service', [$service, $server, $item_id], 20, 'red_item_nextcloud::save_disk_usage');
      }

      return parent::_post_commit_to_db();
    }
  }  
}

?>
