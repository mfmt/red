<?php

if(!class_exists('red_item_mysql_db_ui')) {
  class red_item_mysql_db_ui extends red_item_mysql_db {
    var $is_admin = FALSE;
    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the MySQL database " . 
        $this->_html_generator->get_tag('span',$this->get_mysql_db_name(),$attr) . 
        "? All tables will be dropped.";
    }
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
    function additional_validation() {
      parent::additional_validation();
      // Only applies to non-admins.
      if (!$this->is_admin) {
        // Max user disk usage is 3gb. This ensures that anyone who needs more space
        // has to notify an admin, who can make sure there is enough space on the partition
        // to support it.
        $max_quota = 3 * 1024 * 1024 * 1024;
        $user_set_value = red_machine_readable_bytes($this->get_item_quota());
        if ($user_set_value > $max_quota) {
          $error_message = red_t('Only administrators can set a quota higher then 3GB. This restriction ensures we have enough disk space on the database server to accomodate your needs. Please contact support and we can help.');
          // They have exceeded the limit. This is going to throw a
          // validation error unless they are editing the record and
          // the have not changed the value that was stored in the db
          // by an admin on an earlier save.
          if ($this->exists_in_db()) {
            $item_id = $this->get_item_id();
            $sql = "SELECT item_quota FROM red_item WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // Only if they are different do we have a problem. 
            if($row[0] != $user_set_value) {
              $this->set_error($error_message, 'validation');
            }
          }
          else {
            // It's a new record, always throw the error.
            $this->set_error($error_message, 'validation');
          }
        }
      }
    }

    function _post_commit_to_db() {
      if ($this->on_new_infrastructure()) {
        // We do not notify the host - we handle everything via the queue
        $this->_notify_host = FALSE;
        $status = $this->get_item_status();
        $action = NULL;
        $item_id = $this->get_item_id();
        if (in_array($status, ['pending-insert', 'pending-update', 'pending-restore'])) {
          $action = 'create';
        }
        elseif ($this->get_item_status() == 'pending-delete') {
          $action = 'delete';
        }
        // Note: pending-disable does nothing - we only really disable mysql usernames.
        if ($action) {
          $this->queue->add_task('red_item_mysql_db::apply', [$item_id, $action], 10);
        }
        // Check the disk usage and update.
        $service = 'mysql_db';
        $server = NULL;
        $this->queue->add_task('red_item::get_disk_usage_for_service', [$service, $server, $item_id], 20, 'red_item_mysql_db::save_disk_usage');
      }
      return parent::_post_commit_to_db();
    } 
  }  
}

?>
