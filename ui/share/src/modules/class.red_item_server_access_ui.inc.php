<?php

if(!class_exists('red_item_server_access_ui')) {
  class red_item_server_access_ui extends red_item_server_access {

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete server access for " . 
        $this->_html_generator->get_tag('span',$this->get_server_access_login(),$attr) . 
        "?";
    }

    function get_edit_server_access_login() {
      if($this->exists_in_db()) return $this->get_auto_constructed_read_field('server_access_login');
      return $this->get_auto_constructed_edit_field('server_access_login');
    }

    function get_edit_server_access_public_key() {
      $field = $this->get_auto_constructed_edit_field('server_access_public_key');
      if ($this->on_new_infrastructure()) {
        // This message doesn't apply on the new infrastructure.
        return $field;
      }
      $explanation_attr = array('class' => 'red_explanation');
      $message = $this->_html_generator->get_tag('span',red_t('This form will add keys but not delete them. To delete keys please manually edit your .ssh/authorized_keys file.'),$explanation_attr) . '<br />';
      return $field . '<br />' . $message;
    }

    function _post_commit_to_db() {

      if ($this->on_new_infrastructure()) {
        $this->_notify_host = FALSE;
        $hosting_order_id = $this->get_hosting_order_id();
        $sql = "SELECT item_id, item_host FROM red_item JOIN red_item_web_conf USING(item_id)
          WHERE item_status != 'deleted' AND hosting_order_id = #hosting_order_id";
        $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
        $row = red_sql_fetch_row($result);
        $site_id = intval($row[0]);
        $site_host = $row[1];
        if (in_array($this->get_item_status(), ['pending-disable', 'pending-delete'])) {
          // What it the web site is already deleted? If $site_id or $site_host
          // don't exist just move on. Authentication will get deleted on the
          // weborigin server.
          if ($site_id && $site_host) {
            $this->queue->add_task('red_item_web_conf::revoke_site_access', [$site_id, $this->get_server_access_login(), $site_host], 10);
          }
          if ($this->get_item_status() == 'pending-disable') {
            $this->queue->add_task('red_item_server_access::ensure_disabled', [$this->get_server_access_login(), $this->get_item_host()], 20);
          }
          else {
            $this->queue->add_task('red_item_server_access::ensure_gone', [$this->get_server_access_login(), $this->get_item_host()], 20);
          }
        }
        else {
          global $globals;
          $config = $globals['config'];
          $auth = new red_auth();
          // If the usre is logged in, then grant site acceess.
          if ($auth->user_session_exists($this->get_server_access_login())) {
            $this->queue->add_task('red_item_web_conf::grant_site_access', [$site_id, $this->get_server_access_login(), $site_host], 10);
          }
          $sql = "SELECT user_account_uid, user_account_password FROM red_item_user_account JOIN red_item USING (item_id)
            WHERE item_status != 'deleted' AND user_account_login = @login";
          $result = red_sql_query($sql, ['@login' => $this->get_server_access_login()]);
          $row = red_sql_fetch_row($result);
          $uid = $row[0];
          $password = $row[1];
          $ensureEnabledParams = [
            $this->get_server_access_login(),
            $uid,
            $this->get_item_host(),
          ];
          $this->queue->add_task(
            'red_item_server_access::ensure_enabled',
            $ensureEnabledParams,
            20
          );
          $setPasswordParams = [
            $this->get_server_access_login(),
            $password,
            $this->get_item_host(),
          ];
          $this->queue->add_task(
            'red_item_server_access::set_password',
            $setPasswordParams,
            30
          );
          if ($this->get_server_access_public_key()) {
            $authKeyParams = [
              $this->get_server_access_login(),
              base64_encode($this->get_server_access_public_key()),
              $this->get_item_host(),
            ];
            $this->queue->add_task(
              'red_item_server_access::set_ssh_authorized_keys',
              $authKeyParams,
              30
            );
          }
          $setWeboriginParams = [
            $this->get_server_access_login(),
            $site_id,
            $site_host,
            $this->get_item_host(),
          ];
          $this->queue->add_task(
            'red_item_server_access::save_weborigin_login',
            $setWeboriginParams,
            40
          );
        }
      }
      return parent::_post_commit_to_db();
    }
  }  
}

?>
