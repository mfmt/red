<?php

if(!class_exists('red_item_web_conf_ui')) {
  class red_item_web_conf_ui extends red_item_web_conf {
    var $_max_allowed_user_set_processes = 12;
    var $_max_allowed_user_set_memory = 512;
    var $is_admin = false;
    var $_javascript_includes = array('scripts/text_field_split.js','scripts/item_web_conf.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
      $this->_delete_confirmation_message = red_t('Are you sure you want to delete this web configuration? All web files and configurations will be deleted.');

    }

    function get_read_web_conf_tls() {
      if ($this->get_web_conf_tls() == 0) {
        return 'http only';
      }
      return 'https enabled';
    }

    // We do this manually so we can run it through htmlentities.
    function get_edit_web_conf_settings() {
      $settings_value = $this->get_htmlentities($this->get_web_conf_settings());
      $attr = array('cols' => '100','rows' => '10','wrap' => 'nowrap', 'class' => 'form-control');
      $settings_input = $this->_html_generator->get_form_element('sf_web_conf_settings',$settings_value,'textarea',$attr);
      return $settings_input;
    }

    function get_edit_web_conf_php_version() {
      // By default, $this->_web_conf_php_version_options contains every
      // possible option. Depending on the servers' default option, and whether
      // or not the user is an admin, we pair them down.
      $server = addslashes($this->get_item_host());
      $default_php_version = $this->get_default_php_version_for_host($server);
      $selected_php_version = $this->get_web_conf_php_version();

      // The 5.6 should always be displayed for admins and only
      // displayed for non-admins if the site is already running it.
      if (!$this->is_admin && $selected_php_version != '5.6') {
        // Hide the 5.6 options.
        unset($this->_web_conf_php_version_options['5.6']);
      }
      if (!$this->on_new_infrastructure()) {
        // We don't offer 8.3 on old infrastructure.
        unset($this->_web_conf_php_version_options['8.3']);
      }

      // Notify users that we recommend whatever is the current default.
      $this->_web_conf_php_version_options[$default_php_version] = "{$default_php_version} Recommended";

      // Reset the options.
      $this->_datafields['web_conf_php_version']['options'] = $this->_web_conf_php_version_options;
      return $this->get_auto_constructed_edit_field('web_conf_php_version');
    }
    
    function get_edit_web_conf_cache_type() {
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('web_conf_cache_type');
      }
      else {
        if ($this->get_web_conf_cache_type() == 'custom') {
          // Non-admins can't modify a cache type if it's set to custom.
          return $this->get_auto_constructed_read_field('web_conf_cache_type');
        }
        // Hide the "custom" option.
        $options = $this->get_web_conf_cache_type_options();
        unset($options['custom']);
        return $this->_html_generator->get_select('sf_web_conf_cache_type', $options, $this->get_web_conf_cache_type());
      }
    }

    function get_edit_web_conf_cache_settings() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('web_conf_cache_settings');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    function get_edit_web_conf_mountpoint() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('web_conf_mountpoint');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    function additional_validation() {
      parent::additional_validation();
      // both max_processes and max_memory have limits that regular users
      // are not allowed to exceed.
      $user_limited_resources = array('processes', 'memory');
      // Only applies to non-admins.
      if (!$this->is_admin) {
        $item_id = intval($this->get_item_id());
        foreach($user_limited_resources as $limited_resource) {
          $getter = 'get_web_conf_max_' . $limited_resource;
          $parameter = '_max_allowed_user_set_' . $limited_resource;
          $user_set_value = $this->$getter();
          $max_allowed = $this->$parameter;
          if ($user_set_value > $max_allowed) {
            // They have exceeded the limit. This is going to throw a
            // validation error unless they are editing the record and
            // the have not changed the value that was stored in the db
            // by an admin on an earlier save.
            if ($this->exists_in_db()) {
              $field = 'web_conf_max_' . $limited_resource;
              $sql = "SELECT !field FROM red_item_web_conf WHERE item_id = #item_id";
              $result = red_sql_query($sql, ['!field' => $field, '#item_id' => $item_id]);
              $row = red_sql_fetch_row($result);
              // If they are the same, then it's no problem.
              if($row[0] == $user_set_value) {
                continue;
              }
            }
            $this->set_error(red_t('Only administrators are allowed to set @resource greater than @max.',
              array('@max' => $this->$parameter, '@resource' => $limited_resource)), 'validation');
          }
        }
      }

      // Also, only admins are allowed to change the cache_settings and mountpoint parameters.
      if (!$this->is_admin) {
        $item_id = $this->get_item_id();
        $cache_settings = $this->get_web_conf_cache_settings();
        $mountpoint = $this->get_web_conf_mountpoint();
        if (!empty($cache_settings)) {
          // We are going to throw an error if the submitted value is not
          // the same as the value in the database.
          $throw_error = TRUE;
          if ($this->exists_in_db()) {
            $sql = "SELECT web_conf_cache_settings FROM red_item_web_conf WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] == $cache_settings) {
              // You are saved, no error.
              $throw_error = FALSE;
            }
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set cache settings.'), 'validation');
          }
        }
        if (!empty($mountpoint)) {
          if ($this->exists_in_db()) {
            // We are going to throw an error if the submitted value is not
            // the same as the value in the database.
            $sql = "SELECT web_conf_mountpoint FROM red_item_web_conf WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] != $mountpoint) {
              $this->set_error(red_t('Only administrators are allowed to set mountpoint.'), 'validation');
            }
          }
          else {
            // Only allow mounts points on new hosting items on new infrastructure
            if (!$this->on_new_infrastructure()) {
              $this->set_error(red_t('Mountpoints should only be set on new infrastructure. Please contact support for help.'), 'validation');
            }
          }
        }
      }
    }     

    // We add our queue items in _post_commit_to_db becausee
    // we need an item_id and we don't have one on new items
    // in _pre_commit_to_db.
    function _post_commit_to_db() {
      // During the transition from moshes, we have to decide whether this record
      // needs to have a proxy record setup on our web proxy servers or not.
      // If the item host matches weborigin[0-9]+ it needs to have a web proxy
      // setup. Otherwise it does not.
      if ($this->on_new_infrastructure()) {
        // We do not notify the host - we handle everything via the queue
        $this->_notify_host = FALSE;

        if ($this->_delete) {
          $this->queue->add_task('red_item_web_conf::ensure_php', [$this->get_item_id(), 'gone'], -40);
          $this->queue->add_task('red_item_web_conf::ensure_apache_conf', [$this->get_item_id(), 'gone'], -30);
          $this->queue->add_task('red_item_web_conf::ensure_directories', [$this->get_item_id(), 'gone'], -20);
          $this->queue->add_task('red_item_web_conf::ensure_user', [$this->get_item_id(), 'gone'], -10);

          // Remove proxy and tls certs after the web configuration is removed.
          $this->queue->add_task('red_item_web_conf::delete_proxy_configuration', [$this->get_item_id()], 10);
          if ($this->get_web_conf_tls() == 1) {
            $this->queue->add_task('red_item_web_conf::update_certs_file', [], 20);
          }
        }
        else if ($this->_disable) {
          $this->queue->add_task('red_item_web_conf::ensure_disabled', [$this->get_item_id()], 0);
          // Remove proxy and tls certs after the web configuration is removed.
          $this->queue->add_task('red_item_web_conf::delete_proxy_configuration', [$this->get_item_id()], 10);
          if ($this->get_web_conf_tls() == 1) {
            // We have to remove tls cert because renewals will fail and alert us.
            $this->queue->add_task('red_item_web_conf::update_certs_file', [], 20);
          }
        }
        else {
          $values = [];
          $values['item_id'] = $this->get_item_id();
          $values['item_host'] = $this->get_item_host();
          $values['domains'] = $this->get_web_conf_domain_names();
          $values['tls'] = $this->get_web_conf_tls();
          $values['tls_redirect'] = $this->get_web_conf_tls_redirect();
          $values['logging'] = $this->get_web_conf_logging();
          $values['cache_type'] = $this->get_web_conf_cache_type();
          $values['cache_settings'] = $this->get_web_conf_cache_settings();
          // Add proxy and tls certs *before* the web configuration is created.
          $this->queue->add_task('red_item_web_conf::set_proxy_configuration', [$values], -20);
          // We do the tls stuff whether or not tls is enabled because we might be deleting
          // tls configurations.
          $this->queue->add_task('red_item_web_conf::update_default_web_proxies_file', [], -11);
          $this->queue->add_task('red_item_web_conf::update_certs_file', [], -10);
          $this->queue->add_task('red_item_web_conf::ensure_user', [$this->get_item_id(), 'exists'], 10);
          $this->queue->add_task('red_item_web_conf::ensure_directories', [$this->get_item_id(), 'exists'], 20);
          $this->queue->add_task('red_item_web_conf::ensure_apache_conf', [$this->get_item_id(), 'exists'], 30);

          if ($this->get_web_conf_max_processes() != 0) {
            $this->queue->add_task('red_item_web_conf::ensure_php', [$this->get_item_id(), 'exists'], 40);
          }
          else {
            $this->queue->add_task('red_item_web_conf::ensure_php', [$this->get_item_id(), 'gone'], -40);
          }
          // Check the disk usage and update.
          if (!defined('RED_TEST')) {
            // We don't have quotas enabled in testing mode because of docker.
            $service = 'web_conf';
            $server = NULL;
            $this->queue->add_task('red_item::get_disk_usage_for_service', [$service, $server, $this->get_item_id()], 50, 'red_item_web_conf::save_disk_usage');
          }

        }
      }
      // Ensure we delete, disable or enable any related web apps
      $hosting_order_id = intval($this->get_hosting_order_id());
      $sql = "SELECT red_item.item_id, item_status 
        FROM 
          red_item JOIN red_item_web_app USING (item_id) 
        WHERE 
          item_status IN ('active', 'disabled') AND
          hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $row = red_sql_fetch_row($result);
      if ($row) {
        $item_id = $row[0];
        $status = $row[1];
        if ($this->_delete || $this->_disable) {
          // If deleting or disabling, ensure web app is also deleted or disabled.
          if ($this->_delete) {
            $new_status = 'deleted';
          }
          elseif ($this->_disable) {
            $new_status = 'disabled';
          }
        }
        else {
          // Otherwise, ensure it's active.
          $new_status = 'active';
        }
        if ($new_status != $status) {
          $sql = "UPDATE red_item SET item_status = @new_status WHERE item_id = #item_id";
          red_sql_query($sql, ['@new_status' => $new_status, '#item_id' => $item_id]);
          red_set_message(red_t("Your related web application was also set to @status.", [ "@status" => $new_status ]));
        }
      }
      return parent::_post_commit_to_db();
    }

           
    function add_proxy_configuration_to_queue() {
      
    }
  }  
}



?>
