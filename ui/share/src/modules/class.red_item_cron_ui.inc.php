<?php

if(!class_exists('red_item_cron_ui')) {
  class red_item_cron_ui extends red_item_cron {

    var $_javascript_includes = array('scripts/item_cron.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // Set language dependent properties
    }

    function get_edit_cron_schedule() {
      $input = $this->get_auto_constructed_edit_field('cron_schedule');  

      // create links for user to generate predefined schedules 
      $daily_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('daily');return false;"
      );
      $hourly_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('hourly');return false;"
      );
      $weekly_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('weekly');return false;"
      );
      $forever_attr = array(
        'href' => '#',
        'onClick' => "pass=generateCronSchedule('" . $this::$FOREVER . "');return false;",
        'id' => 'red-forever-schedule',
      );
      $links = "Auto fill schedule field: " . 
        $this->_html_generator->get_tag('a',red_t('Forever'),$forever_attr) . ' | ' .
        $this->_html_generator->get_tag('a',red_t('Daily'),$daily_attr) . ' | ' . 
        $this->_html_generator->get_tag('a',red_t('Hourly'),$hourly_attr) . ' | ' . 
        $this->_html_generator->get_tag('a',red_t('Weekly'),$weekly_attr);


      $legacy_explanation_attr = [
        'class' => 'red-explanation', 
        'id' => 'red-help-hidden-fieldset-message'
      ];
      $format_block = $this->_html_generator->get_tag('blockquote',red_t('minute hour day-of-month month day-of-week'));
      $hourly_block = $this->_html_generator->get_tag('blockquote','34 * * * *');
      $daily_block = $this->_html_generator->get_tag('blockquote','15 4 * * *');
      $weekly_block = $this->_html_generator->get_tag('blockquote','47 2 * * sun');
      $monthly_block = $this->_html_generator->get_tag('blockquote','32 18 17 * *');
        
      $legacy_explanation = red_t("Please enter schedules in five space separated fields: 
        !format_block An asterisk means always run at the specified interval. For example, 
        once an hour at 34 minutes after the hour would be: !hourly_block Once a day at 4:15 am 
        would be: !daily_block Every sunday at 2:47 am would be !weekly_block And, on the 17th 
        of the month at 6:32 pm would be: !monthly_block", 
        [
          '!format_block' => $format_block, 
          '!hourly_block' => $hourly_block, 
          '!weekly_block' => $weekly_block, 
          '!monthly_block' => $monthly_block
        ]
      );
      $legacy_help_message = $this->_html_generator->get_tag(
        'div',
        $legacy_explanation,
        $legacy_explanation_attr
      ) . '<br />';

      $legacy_help_attr = [
        'href' => '#', 
        'onClick' => "toggleVisibilityFieldset('red-help-hidden-fieldset','red-help-hidden-fieldset-message'); return false"
      ];
      $legacy_help_link = $this->_html_generator->get_tag('a','Help',$legacy_help_attr);
      $legacy_help_legend = $this->_html_generator->get_tag('legend',$legacy_help_link);
      $legacy_fieldset_attr = array('id' => 'red-help-hidden-fieldset');
      $legacy_help_fieldset = $this->_html_generator->get_tag('fieldset',$legacy_help_legend . $legacy_help_message, $legacy_fieldset_attr);

      $help_explanation = red_t("Choose 'Forever' to launch a command and run it forever 
        (e.g. to launch a nodejs or django web server). Otherwise, choose a time interval to 
        run a command repeatedly according to a schedule. You may also enter any calendar specification 
        by hand that is supported by systemd time.");
      $help_div_attributes = [ 'id' => 'red-systemd-timer-explanation', 'class' => 'red-item-description' ];
      $help_div = $this->_html_generator->get_tag('div', $help_explanation, $help_div_attributes);

      $generator_div = $this->_html_generator->get_tag('div', $links, [ 'id' => 'red-generate-cron-schedule' ]);
      return $input . $generator_div . $legacy_help_fieldset . $help_div;

    }

    function _post_commit_to_db() {
      if ($this->on_new_infrastructure()) {
        // We do not notify the host - we handle everything via the queue
        $this->_notify_host = FALSE;
        if ($this->_delete || $this->_disable) {
          $this->queue->add_task('red_item_cron::ensure', [$this->get_item_id(), 'gone'], 0);
        }
        else {
          $this->queue->add_task('red_item_cron::ensure', [$this->get_item_id(), 'exists'], 0);
        }
      }
      return parent::_post_commit_to_db();
    }
  }

}

?>
