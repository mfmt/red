<?php

class red_contact extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'contact_id';
  var $_key_table = 'red_contact';

  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_contact_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_contact_status('active');
    }
    $this->_human_readable_description = red_t("Contacts are individuals related to a member");
    $this->_human_readable_name = red_t('Contacts');

  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the contact: @email?", array('@email' => $this->get_contact_email()));
  }

  function _initialize_from_id($id) {
    $sql = "SELECT * FROM red_contact ". 
      "WHERE contact_id = #id";
    $params = ['#id' => $id];
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_assoc($result);
    return $this->_initialize_from_recordset($row);
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_contact_status('deleted');
  }

  var $_contact_id;
  function set_contact_id($value) {
    $this->_contact_id = $value;
  }

  function get_contact_id() {
    return $this->_contact_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_contact_first_name;
  function set_contact_first_name($value) {
    $this->_contact_first_name = $value;
  }

  function get_contact_first_name() {
    return $this->_contact_first_name;
  }


  var $_contact_last_name;
  function set_contact_last_name($value) {
    $this->_contact_last_name = $value;
  }

  function get_contact_last_name() {
    return $this->_contact_last_name;
  }

  var $_contact_status;
  function set_contact_status($value) {
    $this->_contact_status = $value;
  }

  function get_contact_status() {
    return $this->_contact_status;
  }

  var $_contact_email;
  function set_contact_email($value) {
    $this->_contact_email = $value;
  }

  function get_contact_email() {
    return $this->_contact_email;
  }
  
  var $_contact_lang;
  function set_contact_lang($value) {
    $this->_contact_lang = $value;
  }

  function get_contact_lang() {
    return $this->_contact_lang;
  }
  function _set_datafields() {
    $this->_datafields = array(
      'contact_id' => array(
        'fname' => red_t('Member ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_contact',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Parent ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_contact',
        'req' => FALSE 
      ),
      'contact_first_name' => array(
        'fname' => red_t('First Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_contact',
        'text_length' => 50,
        'req' => TRUE
      ),
      'contact_last_name' => array(
        'fname' => red_t('Last Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_contact',
        'text_length' => 50,
        'req' => TRUE
      ),
      'contact_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE, 
        'tblname' => 'red_contact',
      ),
      'contact_email' => array(
        'fname' => red_t('Email address'),
        'type' => 'text',
        'pcre' => RED_EMAIL_MATCHER,
        'pcre_explanation' => RED_EMAIL_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE,
        'text_length' => 50,
        'tblname' => 'red_contact',
      ),
      'contact_lang' => array(
        'fname' => red_t('Preferred language for notices'),
        'type' => 'text',
        'pcre' => RED_INTERFACE_LANG_MATCHER,
        'pcre_explanation' => RED_INTERFACE_LANG_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => TRUE, 
        'tblname' => 'red_contact',
      ),
    );
  }

  function get_edit_contact_lang() {
    $default = $this->get_contact_lang();
    if(empty($default)) $default = 'en_US';
    return $this->_html_generator->get_select('sf_contact_lang',array('en_US' => red_t('English'),'es_MX' => red_t('Spanish')),$default);
  }
}
