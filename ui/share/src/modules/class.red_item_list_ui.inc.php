<?php

if(!class_exists('red_item_list_ui')) {
  class red_item_list_ui extends red_item_list {

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the list " . 
        $this->_html_generator->get_tag('span',$this->get_list_name(),$attr) . 
        "? All list archives will be deleted.";
    }

    function get_read_list_name() {
      $list = $this->get_list_name();
      // Try to determine the URL preferred by this server
      global $globals;
      $lists_domain_map = $globals['config']['lists_domain_map'] ?? NULL;
      if ($lists_domain_map) {
        $domain = $lists_domain_map[$this->get_item_host()] ?? NULL;
        if ($domain) {
          $url = "https://{$domain}/";
          $attributes = array('href' => $url . "mailman/admin/$list");
          return $this->_html_generator->get_tag('a', $list,$attributes);
        }
      }
      return $list;
    }

    function get_edit_list_domain() {
      $selected_domain = $this->get_list_domain();
      $options = $this->get_list_domain_options();
      if(!empty($selected_domain) && !in_array($selected_domain,$options))
        $options[$selected_domain] = $selected_domain;
      $multiple = false;
      $attributes = array();
      $prepend_choose_one_option = false;
      return $this->_html_generator->get_select('sf_list_domain',$options,$selected_domain,$multiple,$attributes,$prepend_choose_one_option);
    }

    function get_edit_list_owner_email() {
      // Unset whatever was set before. It may not be the list owner any more.
      $this->set_list_owner_email(NULL);
      $msg = red_t("Leave blank to preserve existing list owner. Enter a new email address to overwrite list owner address and send new password.");
      $msg_tag = $this->_html_generator->get_tag('p', $msg);
      return $this->get_auto_constructed_edit_field('list_owner_email') . $msg_tag;
    }

    function _pre_commit_to_db() {
      if (!parent::_pre_commit_to_db()) return FALSE;
      if (!$this->exists_in_db() || $this->_delete || $this->_disable) {
        // Rebuild the email databases.
        $this->queue->add_task('red_item_mailbox::rebuild_email_databases', [], 10); 
      }
      return TRUE;
    }

  }  
}

?>
