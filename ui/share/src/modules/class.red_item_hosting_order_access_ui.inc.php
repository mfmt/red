<?php

if(!class_exists('red_item_hosting_order_access_ui')) {
  class red_item_hosting_order_access_ui extends red_item {
    var $_hosting_order_access_login;


    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
      $this->_set_child_datafields();
    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to remove access to the hosting order from " . 
        $this->_html_generator->get_tag('span',$this->get_hosting_order_access_login(),$attr) . "?";
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('hosting_order_access_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Login'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_hosting_order_access'),
                                 ));

    }

    function additional_validation() {
      if ($this->_delete || $this->_disable) {
        return;
      }
      $hosting_order_id = $this->get_hosting_order_id();
      $item_id = $this->get_item_id();
      $login = $this->get_hosting_order_access_login();
      $params = [
        '#hosting_order_id' => $hosting_order_id,
        '@login' => $login,
      ];
      $sql = "SELECT COUNT(red_item.item_id) AS count FROM red_item JOIN red_item_hosting_order_access
        USING(item_id) WHERE hosting_order_id = #hosting_order_id AND item_status != 'deleted'
        AND hosting_order_access_login = @login";
      if ($item_id) {
        $sql .= " AND red_item.item_id != #item_id";
        $params['#item_id'] = $item_id;
      }
      $result = red_sql_query($sql, $params);
      $row = red_sql_fetch_row($result);
      if ($row[0] > 0) {
        $this->set_error(red_t('The login has already been granted hosting order access.'),'validation');
      }
      // You don't have to select a user account from your hosting order or membership, 
      // but it does have to be an active account or disabled (depending on this state).
      $status = $this->get_item_status();
      $user_account_statuses = [ 'active', 'pending-insert', 'pending-update' ];
      if (in_array($status, [ 'disabled', 'pending-disabled' ])) {
        $user_account_statuses[] = 'disabled';
        $user_account_statuses[] = 'pending-disable';
      }
      $login = addslashes($this->get_hosting_order_access_login());
      $user_account_statuses = '"' . implode('","', $user_account_statuses) . '"';
      $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_user_account USING (item_id) 
        WHERE user_account_login = @login AND 
        item_status IN ($user_account_statuses)";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_row($result);
      if ($row[0] == 0) {
        $this->set_error(red_t('The login does not exist or is not enabled. Did you make a typo?'),'validation');
      }
    }
    function set_hosting_order_access_login($value) {
      $this->_hosting_order_access_login = $value;
    }
    function get_hosting_order_access_login() {
      return $this->_hosting_order_access_login;
    }
    
  }  
}

?>
