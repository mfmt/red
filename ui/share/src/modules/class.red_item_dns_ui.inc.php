<?php

if (!class_exists('red_item_dns_ui')) {
  class red_item_dns_ui extends red_item_dns {
    var $_javascript_includes = array('scripts/item_dns.js');
    var $is_admin = FALSE;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
  
    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the " . 
        $this->_html_generator->get_tag('span',$this->get_dns_type().  
          "/" . $this->get_dns_fqdn(),$attr) . 
        " dns entry?";
    }

    function get_edit_dns_type() {
      // You cannot change a DNS type once it is created - on the server we update records
      // based on fqdn + type so editing a type will have unexpected consequences.
      $attr = [];
      $multiple = FALSE;
      $edits_allowed = [ 'a', 'alias' ];
      $options = $this->get_dns_type_options();
      $type = $this->get_dns_type();
      if ($this->exists_in_db()) {
        if (!in_array($type, $edits_allowed)) {
          // Most records you cannot edit.
          $attr['disabled'] = 'disabled';
        }
        else {
          // But you can change an A to an ALIAS and vice versa. That ensures you can make
          // the change without editing the A record first.
          $options = [ 'a' => 'a', 'alias' => 'alias'];
        }
      }
      return $this->_html_generator->get_select('sf_dns_type', $options, $type, $multiple, $attr);
    }    
    function get_edit_dns_ttl() {
      return $this->get_auto_constructed_edit_field('dns_ttl') . ' seconds';
    }
    function get_read_dns_fqdn() {
      $non_active = '';
      // add a warning if it's an mx record, there's not dns server set
      // (it could be a mx record pointing lists
      if(!$this->name_service_active()) {
        $non_active = $this->_html_generator->get_tag('span','*',
                      array('class' => 'red-non-local-mx'));
      }
      return $this->get_auto_constructed_read_field('dns_fqdn') . $non_active;
    }

    // deterine whether name service is active for the given domain
    function name_service_active() {
      if (self::is_local($this->get_dns_fqdn())) {
        return TRUE;
      }
      // We can't use a class variable because a new instance of this class
      // is created with every record.
      static $dns_lookup_cache = array();

      if(!red_single_ping_result('1.1.1.1')) {
        return FALSE;
      }

      // In theory you can create a ptr record that is not handled by us
      // but it's more obvious and I'd like to make this return faster
      // by skipping the lookup.
      if ($this->get_dns_type() == 'ptr') {
        return TRUE;
      }

      $hostname = $this->get_dns_zone(); 
      // Check cache.
      if (array_key_exists($hostname,$dns_lookup_cache)) {
        return $dns_lookup_cache[$hostname];
      }

      $assigned = self::get_nameservers($hostname);
      $records = @dns_get_record($hostname, DNS_NS);
      if($records) {
        foreach($records as $k => $record) {
          if(in_array($record['target'], $assigned)) {
            // store for faster lookups
            $dns_lookup_cache[$hostname] = TRUE;
            return TRUE;
          }
        }
      }

      // store for faster lookups
      $dns_lookup_cache[$hostname] = FALSE;
      return FALSE;
    }
    function get_edit_dns_mx_verified() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('dns_mx_verified');
      }
      else {
        return red_t("Only available to administrators");
      }
    }

    function _post_commit_to_db() {
      // We do not notify the host - we handle everything via the queue
      $this->_notify_host = FALSE;
      $active_states = ['pending-insert', 'pending-update', 'pending-restore'];
      $status = $this->get_item_status();

      // If this is an MX domain and another mx domain matching this domain is verified
      // then this one should be considered verified as well.
      if ($this->get_dns_type() == 'mx') {
        // We have to rebuild the unverified file - to either add this domain or remove it
        // if necessary.
        $this->queue->add_task('red_item_mailbox::rebuild_unverified_domains_database', [], 10);
        if ($this->get_dns_mx_verified() == 0) {
          if (red_domain_is_verified($this->get_dns_fqdn())) {
            $this->set_dns_mx_verified(1);
          }
          else {
            // Every time a MX record that is unverified is changed, we need to trigger
            // an email to verify.
            if (in_array($this->get_item_status(), ['pending-delete', 'pending-disable'])) {
              // Clean up the red_mx_verify table. No need to re-verify.
              $item_id = $this->get_item_id();
              $sql = "DELETE FROM red_mx_verify WHERE item_id = #item_id";
              red_sql_query($sql, ['#item_id' => $item_id]);
            }
            else {
              // Send verification.
              $this->queue->add_task('red_send_mx_verification_after_delay', [$this->get_item_id()], 15);
            }
          }
        }
        else {
          // If it's verified, then we rebuild the email databases so we will accept email sent to these
          // domains.
          $this->queue->add_task('red_item_mailbox::rebuild_email_databases', [], 15);
        }
      }
      elseif ($this->get_dns_type() == 'alias') {
        // We maintain a record linking each alias fqdn to the target in a sqlite database on the nsstage
        // server so we can update them if the target IP address changes.
        if (in_array($status, $active_states)) {
          $this->queue->add_task('red_item_dns::link_alias_to_target', [$this->get_item_id()], -10);
        }
        else {
          // If we are deleting this record, clean up the link.
          $this->queue->add_task('red_item_dns::unlink_alias_from_target', [$this->get_item_id()], -10); 
        }
      }
      elseif ($this->get_dns_type() == 'dkim') {
        // With DKIM records, we have to first generate a dkim key, copy it to
        // our mail relay servers and then output the public key (or, if the
        // key has been generated, just output the public key). We pass this
        // output to a call back function, which will ensure the DKIM TXT record
        // is generated.
        if (in_array($status, $active_states)) {
          $this->queue->add_task('red_item_dns::get_dkim_public_key', [$this->get_item_id()], -10, 'red_item_dns::set_dkim_txt_record'); 
        }
        else {
          // If deleting or disabling, delete the dkim files.
          $this->queue->add_task('red_item_dns::purge_dkim', [$this->get_item_id()], 10); 
        }
      }
      if (in_array($status, $active_states)) {
        $this->queue->add_task('red_item_dns::initialize_zone', [$this->get_item_id()], -20); 
      }
      else {
        if (!$this->zone_has_records()) {
          // After our update, purge the zone configuration file.
          $this->queue->add_task('red_item_dns::purge_zone', [$this->get_item_id()], 20); 
        }
      }
      if ($this->get_dns_type() != 'dkim') {
        // DKIM records handle updates in the callback function, so we only run on other types.
        $this->queue->add_task('red_item_dns::update_record', [$this->get_item_id()], 0); 
      }
      return parent::_post_commit_to_db();
    }
  }
}

?>
