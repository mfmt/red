<?php

if(!class_exists('red_item_web_app_ui')) {
  class red_item_web_app_ui extends red_item_web_app {
    var $_delete_confirmation_message = "Deleting a web application only deletes this record in the database. You must delete and re-create your web configuration to fully remove it.";
    private $run_install = FALSE;
    private $secure_install = FALSE;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }
    
    function get_edit_web_app_install_ip() {
      // Prepopulate with the IP address of the current user.
      $install_ip = $this->get_web_app_install_ip();
      if (empty($install_ip) && !$this->exists_in_db()) {
        $this->set_web_app_install_ip($_SERVER['REMOTE_ADDR']);
      }
      return $this->get_auto_constructed_edit_field('web_app_install_ip');
    }

    function get_edit_web_app_name() {
      // We tag on to this field to add a checkbox allowing the user
      // to choose not to attempt installation when creating a new record.
      // This allows users to add a web app when their web app is already
      // installed so they can benefit from the upgrade system.
      $ret = $this->get_auto_constructed_edit_field('web_app_name');
      if ($this->exists_in_db()) {
        // No OP - if they are editing a record, we don't provide this option
        // because it would allow a user to change a web app from soft-error
        // to active without resolving the soft-error.
        return $ret;
      }
      $div_attributes = array( 'class' => 'red_web_app_install');
      $options = [
        'install_web_app_y' => red_t("Please download this web app into my web directory"),
        'install_web_app_n' => red_t("My app is already downloaded, just keep it up to date"),
      ];
      $selected = 'install_web_app_y';
      $install_radio = $this->_html_generator->get_radio('sf_install', $options, $selected);
      $ret .= $this->_html_generator->get_tag('div',$install_radio, $div_attributes);
      return $ret;
    }

    function _pre_commit_to_db() {
      if(!parent::_pre_commit_to_db()) return FALSE;
      $sf_install = $_POST['sf_install'] ?? '';

      // disable and delete are no-ops - have to be done at the web conf level.
      if ($this->_delete || $this->_disable) {
        if ($this->_delete) {
          $this->set_item_status('deleted');
        }
        else {
          $this->set_item_status('disabled');
        }
        $this->_notify_host = FALSE;
        $this->run_install = FALSE;
        $this->secure_install = FALSE;
      }
      elseif ($sf_install == 'install_web_app_n') {
        // Set the status to active if they don't want to install the web app
        // and indicate that we don't want to notify the server.
        $this->set_item_status('active');
        // Neither install nor secure the web app.
        $this->_notify_host = FALSE;
        $this->run_install = FALSE;
        $this->secure_install = FALSE;
      }
      else {
        // Otherwise we always secure it.
        $this->secure_install = TRUE;
        if (!$this->exists_in_db()) {
          // Set flag for the post commit hook below. We only install if we are submitting
          // for the first time.
          $this->run_install = TRUE;
        }
      }
      // The rest of the installation will happen in post commit
      return TRUE;
    }

    function _post_commit_to_db() {
      if ($this->on_new_infrastructure()) {
        $this->_notify_host = FALSE;
        if ($this->run_install) {
          $this->queue->add_task('red_item_web_app::ensure_exists', [$this->get_item_id()], 0);
        }
        if ($this->secure_install) {
          $this->queue->add_task('red_item_web_app::secure_install_script', [$this->get_item_id()], 0);
        }
      }
      return parent::_post_commit_to_db();
    } 
  }
}

?>
