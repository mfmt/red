<?php

class red_vps extends red_ado {
  var $_key_field = 'vps_id';
  var $_key_table = 'red_vps';
  var $is_admin = FALSE;
  var $type_options = [];
  var $location_options = [];
  var $managed_options = [];

  function __construct($construction_options) {
    $this->type_options = [
      'physical' => red_t("Physical"),
      'virtual' => red_t("Virtual"),
    ];
    $this->location_options = [
      'telehouse' => 'Telehouse',
      'koumbit' => 'Koumbit',
      'monkeybrains' => 'Monkeybrains',
      'greenhost' => 'Greenhost',
      'reliableservers' => 'Reliable Servers',
      'awknet' => 'Awknet',
      'webarch' => 'Webarch',
      'linode' => 'Linode',
    ];
    $this->managed_options = [
      'n' => red_t("No"),
      'y' => red_t("Yes"),
    ];

    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - the following fields must be set
    if(empty($this->_vps_id)) {
      if(array_key_exists('member_id',$construction_options)) {
        $this->set_member_id($construction_options['member_id']);
      }
      $this->set_vps_status('active');
    }


  }

  function get_delete_confirmation_message() {
    return red_t("Are you sure you want to delete the vps: @vps_server?", array('@vps_server' => $this->get_vps_server()));
  }

  function _initialize_from_id($id) {
    $sql = "SELECT * FROM red_vps ". 
      "WHERE vps_id = #id";
    $params = ['#id' => $id];
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_assoc($result);
    return $this->_initialize_from_recordset($row);
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_vps_status('deleted');
  }

  var $_vps_id;
  function set_vps_id($value) {
    $this->_vps_id = $value;
  }
  function get_vps_id() {
    return $this->_vps_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }
  function get_member_id() {
    return $this->_member_id;
  }

  var $_vps_cpu;
  function set_vps_cpu($value) {
    $this->_vps_cpu = $value;
  }
  function get_vps_cpu() {
    return $this->_vps_cpu;
  }

  var $_vps_ram;
  function set_vps_ram($value) {
    $this->_vps_ram = $value;
  }
  function get_vps_ram() {
    return $this->_vps_ram;
  }
  
  var $_vps_hd;
  function set_vps_hd($value) {
    $this->_vps_hd = $value;
  }
  function get_vps_hd() {
    return $this->_vps_hd;
  }
  var $_vps_ssd = 0;
  function set_vps_ssd($value) {
    $this->_vps_ssd = $value;
  }
  function get_vps_ssd() {
    return $this->_vps_ssd;
  }

  var $_vps_server;
  function set_vps_server($value) {
    $this->_vps_server = $value;
  }
  function get_vps_server() {
    return $this->_vps_server;
  }

  var $_vps_status;
  function set_vps_status($value) {
    $this->_vps_status = $value;
  }
  function get_vps_status() {
    return $this->_vps_status;
  }

  var $_vps_type;
  function set_vps_type($value) {
    $this->_vps_type = $value;
  }
  function get_vps_type() {
    return $this->_vps_type;
  }
  var $_vps_location;
  function set_vps_location($value) {
    $this->_vps_location = $value;
  }
  function get_vps_location() {
    return $this->_vps_location;
  }
  var $_vps_managed;
  function set_vps_managed($value) {
    $this->_vps_managed = $value;
  }
  function get_vps_managed() {
    return $this->_vps_managed;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'vps_id' => array(
        'fname' => red_t('VPS Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_vps',
        'req' => FALSE 
      ),
      'member_id' => array(
        'fname' => red_t('Member Id'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_INT_EXPLANATION,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_vps',
        'req' => TRUE 
      ),
      'vps_server' => array(
        'fname' => red_t('Server Domain Name'),
        'type' => 'varchar',
        'pcre' => RED_DOMAIN_MATCHER,
        'pcre_explanation' => RED_DOMAIN_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 30,
        'req' => TRUE 
      ),
      'vps_cpu' => array(
        'fname' => red_t('Number of CPUs'),
        'type' => 'int',
        'pcre' => RED_TINYINT_MATCHER,
        'pcre_explanation' => RED_TINYINT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 1,
        'req' => TRUE 
      ),
      'vps_ram' => array(
        'fname' => red_t('Amount of RAM (in GB)'),
        'type' => 'int',
        'pcre' => RED_TINYINT_MATCHER,
        'pcre_explanation' => RED_TINYINT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 2,
        'req' => TRUE 
      ),
      'vps_hd' => array(
        'fname' => red_t('Spinning Disk Size'),
        'type' => 'int',
        'pcre' => RED_BYTES_MATCHER,
        'pcre_explanation' => RED_BYTES_EXPLANATION,
        'description' => red_t("Valid values include: 10b, 100kb, 500mb, or 3gb"),
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'req' => TRUE 
      ),
      'vps_ssd' => array(
        'fname' => red_t('Solid State Disk Size'),
        'type' => 'int',
        'pcre' => RED_BYTES_MATCHER,
        'pcre_explanation' => RED_BYTES_EXPLANATION,
        'description' => red_t("Valid values include: 10b, 100kb, 500mb, or 3gb"),
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'req' => TRUE 
      ),
      'vps_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_ACTIVE_DELETED_MATCHER,
        'pcre_explanation' => RED_ACTIVE_DELETED_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE, 
        'tblname' => 'red_vps',
      ),
      'vps_type' => array(
        'fname' => red_t('Type'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 30,
        'input_type' => 'select',
        'options' => $this->type_options, 
        'req' => TRUE 
      ),
      'vps_location' => array(
        'fname' => red_t('Location'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_TEXT_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'text_length' => 30,
        'input_type' => 'select',
        'options' => $this->location_options, 
        'req' => TRUE 
      ),
      'vps_managed' => array(
        'fname' => red_t('Managed by May First?'),
        'type' => 'text',
        'pcre' => RED_YES_NO_MATCHER,
        'pcre_explanation' => RED_YES_NO_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_vps',
        'options' => $this->managed_options,
        'input_type' => 'select',
        'req' => TRUE 
      ),
    );

  }

  function additional_validation() {
    if(!$this->is_admin) {
      $this->set_error(red_t("Sorry, only administrators can create/edit/delete VPS records."),'validation');
      return;
    }
    if ($this->_delete) {
      return;
    }
    // If the member quota is set, we can't exceed it.
    $member_quota = red_get_member_quota($this->get_member_id());

    if ($member_quota > 0) {
      $total = red_get_allocated_quota_for_membership($this->get_member_id()); 

      // If this record is in the database already, we have to subtract the saved value.
      $saved_vps_hd = 0;
      if ($this->exists_in_db()) {
        $vps_id = $this->get_vps_id();
        $sql = "SELECT vps_hd FROM red_vps WHERE vps_id = #vps_id";
        $result = red_sql_query($sql, ['#vps_id' => $vps_id]);
        $row = red_sql_fetch_row($result);
        if ($row) {
          $saved_vps_hd = $row[0];
        }
      }
      $diff = $total - $saved_vps_hd + intval($this->get_vps_hd()) - $member_quota;

      if ($diff > 0) {
        $quota_over = red_human_readable_bytes($diff);
        $this->set_error(red_t("Setting the hard disk for this VPS exceeds your membership quota by @quota_over", [ '@quota_over' => $quota_over ]),'validation');
      }
    }
    if (!in_array($this->get_vps_type(), array_keys($this->type_options))) {
        $this->set_error(red_t("Your VPS type option is invalid."),'validation');
    }
    if (!in_array($this->get_vps_location(), array_keys($this->location_options))) {
        $this->set_error(red_t("Your VPS location option is invalid."),'validation');
    }
    if (!in_array($this->get_vps_managed(), array_keys($this->managed_options))) {
        $this->set_error(red_t("Your VPS managed option is invalid."),'validation');
    }
    if ($this->duplicate_vps_name_exists()) {
        $vps_name = addslashes($this->get_vps_server());
        $this->set_error(red_t("A VPS with the name '$vps_name' already exists."),'validation');
    }
    
  }

  function duplicate_vps_name_exists() {
    $sql = "SELECT COUNT(*) AS count FROM red_vps WHERE vps_server = @vps_server AND vps_status = 'active'";
    $params = [
      '@vps_server' => $this->get_vps_server(),
    ];
    if ($this->exists_in_db()) {
      $sql .= " AND vps_id != #vps_id";
      $params['#vps_id'] = $this->get_vps_id();
    }
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_row($result);
    if ($row[0] > 0) {
      return TRUE;
    }
    return FALSE;
  }

  function _pre_commit_to_db() {
    $this->set_vps_hd(red_machine_readable_bytes($this->get_vps_hd()));
    $this->set_vps_ssd(red_machine_readable_bytes($this->get_vps_ssd()));
    return parent::_pre_commit_to_db();
  }

  function get_edit_vps_ssd() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_vps_ssd());
    $attributes = [ 'class' => 'form-control', 'id' => 'vps_ssd' ];
    $type = 'text';
    return $this->_html_generator->get_input('sf_vps_ssd',$value, $type, $attributes);
  }

  function get_read_vps_ssd() {
    // Convert to human readable before displaying.
    return red_human_readable_bytes($this->get_vps_ssd());
  }

  function get_edit_vps_hd() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_vps_hd());
    $attributes = [ 'class' => 'form-control', 'id' => 'vps_hd' ];
    $type = 'text';
    return $this->_html_generator->get_input('sf_vps_hd',$value, $type, $attributes);
  }

  function get_read_vps_hd() {
    // Convert to human readable before displaying.
    return red_human_readable_bytes($this->get_vps_hd());
  }
}
