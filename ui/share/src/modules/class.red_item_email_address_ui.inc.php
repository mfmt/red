<?php

if(!class_exists('red_item_email_address_ui')) {

  class red_item_email_address_ui extends red_item_email_address {
    var $is_admin = false;
    var $_javascript_includes = array('scripts/item_email_address.js');

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the email address " . $this->_html_generator->get_tag('span',$this->get_email_address(),$attr) . "?";
    }

    // return a custom edit field for email addresses that only
    // allows certain domain names
    function get_edit_email_address() {
      // display the proper form
      $domains = $this->get_email_domains();
      $domain_options = array();
      foreach($domains as $domain) {
        $domain_options[$domain] = $domain;
      }
      $username_value = $this->get_username_portion_of_address(); 
      $domain_value = $this->get_domain_portion_of_address(); 
      $attributes = array('size' => '12', 'maxlength' => '100');
      $user_input = $this->_html_generator->get_input('sf_email_address_username',$username_value,'text',$attributes);
      $domain_input = $this->_html_generator->get_select('sf_email_address_domain',$domain_options,$domain_value);
      return $user_input . '@' . $domain_input;
    }
      
    // customize to ensure spaces between username and email address
    function get_read_email_address_recipient() {
      $recipient = $this->get_email_address_recipient();
      $array = explode(',',$recipient);
      return implode(', ',$array);
    }

    // customize to add a mailto: link 
    function get_read_email_address() {
      $email = $this->get_email_address();
      return '<a href="mailto:' . $email . '">' . $email . '</a>'; 
    }
    // Custom email address recipient that allows either an existing
    // user account or a free form email address
    function get_edit_email_address_recipient() {
      $recipient = $this->get_email_address_recipient();

      // Create the list of user account mailboxes via a select drop down
      $mailbox_logins = $this->get_related_mailbox_logins();
      if (count($mailbox_logins) == 0) {
        $mailbox_select_div = "";
        $description_mailbox_login = red_t("No mailboxes are associated with your account. Click the mailbox tab to add one, or enter an external email address above to forward your email to another provider.");
      }
      else {
        $description_mailbox_login = red_t("For convenience, you can add email addresses from your list of existing mailboxes.");
        $span_attributes = array('class' => 'red_explanation');
        $div_attributes = array(
          'id' => 'available_mailboxes',
          'class' => 'red_email_recipient_input'
        );
        // The select function won't produce a drop down if there is only one option,
        // so we have to force it by adding a new option.
        $prepend_choose_one_option = TRUE;
        if (count($mailbox_logins) == 1) {
          $prepend_choose_one_option = FALSE;
          $mailbox_logins = ['' => "--Choose Recipient--"] + $mailbox_logins;
        }
        $mailbox_logins_attr = [];
        $mailbox_select = $this->_html_generator->get_select('available_mailboxes', $mailbox_logins, NULL, false, $mailbox_logins_attr, $prepend_choose_one_option);
        $attr = [];
        $mailbox_select_div = $this->_html_generator->get_tag('div', $mailbox_select, $div_attributes);
      }

      // Create the actual text box used to record the recipients. 
      $recipient_input_div = $this->get_auto_constructed_edit_field('email_address_recipient');
      $description_recipient = red_t("Enter one or more comma separated email addresses, OR a user account followed by @mail.mayfirst.org, that should receive email sent to this address.");
      $description_mailbox_login_div = $this->_html_generator->get_tag('div', $description_mailbox_login, [ 'class' => 'red-item-description' ]);
      return $description_recipient . 
        $recipient_input_div .
        $description_mailbox_login_div .
        $mailbox_select_div;
    }

    // override parent function
    // Add an explanation for the mx_warning_indicator
    function get_enumerate_header_block() {
      $mx_explanation = $this->get_mx_warning_explanation();
      //return $mx_explanation . parent::get_enumerate_header_block();
      // not working yet...
      return parent::get_enumerate_header_block();
    }

    function get_mx_warning_explanation() {
      return "<p class=\"red_explanation\"><a name=\"mx_warning\">" .
        red_t("An @mx_warning_indicator  next to an email address indicates that the domain name used ".
          "by the email address is not configured to have email delivered ".
          "to May First Movement Technology servers.",array('@mx_warning_indicator' => $this->get_mx_warning_indicator() )) . 
        "</a></p>";
    }
    // override parent function 
    // this function is called when the values for this item are being
    // enumerated. We want to add a check to see if the domain is
    // pointing to us and set a warning if it is not
    function get_enumerate_data_block() {
      $row = $this->get_enumerate_data_block_row();
      $email_address = $row['email_address']['value'];
      $domain = strstr($email_address,'@');
      $domain = substr($domain,1);
      if(red_single_ping_result('1.1.1.1') && !$this->mx_set_to_primary_host($domain)) {
        $row['email_address']['value'] = $row['email_address']['value'] . 
          $this->get_mx_warning_indicator();
      }
      return $this->_html_generator->get_table_cells($row);
    }

    function get_mx_warning_indicator() {
      // not working yet
      return '';
      return '<span class="red_mx_warning"><a href="#red_mx_warning">'.
        '*</a></span>';
    }

     // override parent to properly insert email address 
    function set_user_input($post) {
      // first load the db values
      $this->set_item_id($post['sf_item_id']);
      $this->reset_to_db_values();
      foreach($post as $k => $v) {
        if(preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          // Build the address based on username and domain
          if($field == 'email_address_username') {
            $v = $post['sf_email_address_username'] . '@' . $post['sf_email_address_domain'];
            // Change the field name so it is properly set below
            $field = 'email_address';
          }
          if($this->accept_user_input($field)) {
            $this->set_field_value($field,$v);
          }
        }
      }
    }

    function _pre_commit_to_db() {

      // During the transition from moshes, we have to decide whether this
      // record needs to be set on a MOSH (old style) or whether it is being
      // routed through our new mail network (new style). If the domain name's
      // MX record points to (a|b|c|d).mayfirst.org, then we must rebuild the
      // email databases and it does not need to be set on a mosh, so remove
      // the item_host and called remote_tasks(). Otherwise leave the item_host
      // as is, and the item host will call remote tasks.
      $domain = addslashes($this->get_domain_portion_of_address());
      if ($this->on_new_infrastructure() || red_domain_uses_mx_servers($domain)) {
        $this->queue->add_task('red_item_mailbox::rebuild_email_databases', [], 10); 
        $this->set_item_host('');
      }
      return parent::_pre_commit_to_db();
    }
  }
}



?>
