<?php

class red_member extends red_ado {

  // determines the default host
  // for new hosting orders
  var $_key_field = 'member_id';
  var $_key_table = 'red_member';
  var $member_parent_id_options = array();

  function get_delete_confirmation_message() {
    $attr = array('class' => 'red-message-variable');
    return "Are you sure you want to delete the member '" .
      $this->_html_generator->get_tag('span',$this->get_member_friendly_name(),$attr) .
      "'? All related hosting orders will be deleted.";
  }

  function _initialize_from_id($id) {
    $sql = "SELECT * FROM red_member ".
      "WHERE member_id = #id";
    $params = ['#id' => $id];
    $result = red_sql_query($sql, $params);
    $row = red_sql_fetch_assoc($result);
    return $this->_initialize_from_recordset($row);
  }

  function set_delete_flag() {
    parent::set_delete_flag();
    $this->set_member_status('inactive');
  }

  var $_unique_unix_group_id;
  function set_unique_unix_group_id($value) {
    $this->_unique_unix_group_id = $value;
  }

  function get_unique_unix_group_id() {
    return $this->_unique_unix_group_id;
  }

  var $_member_id;
  function set_member_id($value) {
    $this->_member_id = $value;
  }

  function get_member_id() {
    return $this->_member_id;
  }

  var $_member_parent_id;
  function set_member_parent_id($value) {
    $this->_member_parent_id = $value;
  }

  function get_member_parent_id() {
    return $this->_member_parent_id;
  }

  var $_member_friendly_name;
  function set_member_friendly_name($value) {
    $this->_member_friendly_name = $value;
  }

  function get_member_friendly_name() {
    return $this->_member_friendly_name;
  }

  var $_member_status;
  function set_member_status($value) {
    $this->_member_status = $value;
  }

  function get_member_status() {
    return $this->_member_status;
  }

  var $_member_benefits_level;
  function set_member_benefits_level($value) {
    $this->_member_benefits_level = $value;
  }
  function get_member_benefits_level() {
    return $this->_member_benefits_level;
  }

  var $_member_type;
  function set_member_type($value) {
    $this->_member_type = $value;
  }

  function get_member_type() {
    return $this->_member_type;
  }

  var $_member_start_date;
  function set_member_start_date($value) {
    // remove any time garbage that will cause the
    // validation to fail
    if(strlen($value) > 10)
      $value = substr($value,0,10);
    $this->_member_start_date = $value;
  }

  function get_member_start_date() {
    // set default value of today
    if(empty($this->_member_start_date))
      $this->_member_start_date = date('Y-m-d');

    return $this->_member_start_date;
  }
  var $_member_end_date;
  function set_member_end_date($value) {
    // remove any time garbage that will cause the
    // validation to fail
    if(!is_null($value) && strlen($value) > 10) {
      $value = substr($value,0,10);
    }
    $this->_member_end_date = $value;
  }

  function get_member_end_date() {
    return $this->_member_end_date;
  }

  var $_member_quota;
  function set_member_quota($value) {
    $this->_member_quota = $value;
  }
  function get_member_quota() {
    return $this->_member_quota;
  }

  function _set_datafields() {
    $this->_datafields = array(
      'member_id' => array(
        'fname' => red_t('Member ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => FALSE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_member',
        'req' => FALSE
      ),
      'member_parent_id' => array(
        'fname' => red_t('Member Parent ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_member',
        'req' => FALSE
      ),
      'unique_unix_group_id' => array(
        'fname' => red_t('Unique Unix Group ID'),
        'type' => 'int',
        'pcre' => RED_ID_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'tblname' => 'red_member',
        'req' => false,  // actually it is required - check performed in additional_validation
      ),
      'member_friendly_name' => array(
        'fname' => red_t('Friendly Name'),
        'type' => 'varchar',
        'pcre' => RED_TEXT_MATCHER,
        'pcre_explanation' => RED_ID_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'tblname' => 'red_member',
        'text_length' => 50,
        'req' => TRUE
      ),
      'member_status' => array(
        'fname' => red_t('Status'),
        'type' => 'text',
        'pcre' => RED_MEMBER_STATUS_MATCHER,
        'pcre_explanation' => RED_MEMBER_STATUS_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_type' => array(
        'fname' => red_t('Type'),
        'type' => 'text',
        'pcre' => RED_MEMBER_TYPE_MATCHER,
        'pcre_explanation' => RED_MEMBER_TYPE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_benefits_level' => array(
        'fname' => red_t('Benefits Level'),
        'type' => 'text',
        'pcre' => RED_MEMBER_BENEFITS_LEVEL_MATCHER,
        'pcre_explanation' => RED_MEMBER_BENEFITS_LEVEL_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_start_date' => array(
        'fname' => red_t('Start Date'),
        'type' => 'date',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_end_date' => array(
        'fname' => red_t('End Date'),
        'type' => 'date',
        'pcre' => RED_DATE_MATCHER,
        'pcre_explanation' => RED_DATE_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => FALSE,
        'user_update' => FALSE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),
      'member_quota' => array(
        'fname' => red_t('Quota'),
        'type' => 'int',
        'pcre' => RED_BYTES_MATCHER,
        'pcre_explanation' => RED_BYTES_EXPLANATION,
        'user_visible' => TRUE,
        'user_insert' => TRUE,
        'user_update' => TRUE,
        'req' => FALSE,
        'tblname' => 'red_member',
      ),

    );

  }

  var $_unique_unix_group_name;
  function get_unique_unix_group_name() {
    // when validating without committing, we may set the group name
    // to avoid validation errors (the identifier, for example, must
    // be group.mayfirst.org for individual accounts or it won't
    // validate). When actually commiting, the calling script should
    // not set the unique_unix_group_name
    if(empty($this->_unique_unix_group_name)) {
      $unix_group_id = $this->get_unique_unix_group_id();
      if(empty($unix_group_id)) return false;
      $sql = "SELECT unique_unix_group_name FROM red_unique_unix_group ".
        "WHERE unique_unix_group_id = #unix_group_id";
      $result = red_sql_query($sql, ['#unix_group_id' => $unix_group_id]);
      $row = red_sql_fetch_row($result);
      $this->_unique_unix_group_name = $row[0];
    }
    return $this->_unique_unix_group_name;
  }

  function set_unique_unix_group_name($value) {
    $this->_unique_unix_group_name = $value;
  }

  var $_contact_email;
  function get_contact_email() {
    return $this->_contact_email;
  }
  function set_contact_email($v) {
    $this->_contact_email = $v;
  }

  var $_contact_lang;
  function get_contact_lang() {
    return $this->_contact_lang;
  }
  function set_contact_lang($v) {
    $this->_contact_lang = $v;
  }

  var $_contact_first_name;
  function get_contact_first_name() {
    return $this->_contact_first_name;
  }
  function set_contact_first_name($v) {
    $this->_contact_first_name = $v;
  }

  var $_contact_last_name;
  function get_contact_last_name() {
    return $this->_contact_last_name;
  }
  function set_contact_last_name($v) {
    $this->_contact_last_name = $v;
  }

  function additional_validation() {
    if (!$this->_delete) {
      // Ensure we can parse the quota
      if (red_machine_readable_bytes($this->get_member_quota()) === FALSE) {
        $this->set_error(red_t("The member quota does not seem to be in a readable format."),'validation');
      }
      // Ensure the quota is not less then what is already allocated.
      if($this->exists_in_db()) {
        $member_quota = $this->get_member_quota();
        // Set quota to 0 to have no quota.
        if ($member_quota > 0) {
          $total = red_get_allocated_quota_for_membership($this->get_member_id()); 
          if ($total > red_machine_readable_bytes($this->get_member_quota())) {
            $total_readable = red_human_readable_bytes($total);
            $this->set_error(red_t("The member quota you chose is less then the total quota already allocated. Please edit some of your resources to lower the quota allocated or ensure your member quota is over @total_readable.", [ '@total_readable' => $total_readable ]),'validation');
          }
        }
      }
    }

    if(!$this->member_friendly_name_unique()) {
      $error = red_t("The member name you have chosen is already in use.");
      $this->set_error($error,'validation');
    }

    if (!$this->exists_in_db()) {
      // if a unique_unix_group_id is being passed, make sure it exists
      $unix_id = $this->get_unique_unix_group_id();
      if(!empty($unix_id)) {
        $sql = "SELECT unique_unix_group_name FROM red_unique_unix_group ".
          "WHERE unique_unix_group_id = #unix_id";
        $result = red_sql_query($sql, ['#unix_id' => $unix_id]);
        if(red_sql_num_rows($result) == 0){
          $this->set_error(red_t("No matching unique unix group found."),'validation');
        }
      } else {
        // otherwise, make sure we can create unique_unix_group
        global $globals;
        $values = array(
          'unique_unix_group_name' => $this->get_unique_unix_group_name(),
        );
        // (this function sets its own errors)
        $this->create_related_object('red_unique_unix_group',$values,$validate_only = true);
      }

      // if we are passing in contact names to create a related contact,
      // make sure we have valid contact values
      $email = $this->get_contact_email();
      if(!empty($email)) {
        $first_name = $this->get_contact_first_name();
        $last_name = $this->get_contact_last_name();
        if(empty($first_name)) {
          $this->set_error(red_t("First name is required."),'validation');
        }
        if(empty($last_name)) {
          $this->set_error(red_t("Last name is required."),'validation');
        }
        if(!preg_match(RED_EMAIL_MATCHER,$email)) {
          $this->set_error(red_t("That didn't look like a valid email address."),'validation');
        }
      }
    } else {
      // only validate unix_group_id on non-insert actions. On insert, the group is created
      // later.
      $unix_id = $this->get_unique_unix_group_id();
      if(empty($unix_id)) {
        $this->set_error(red_t("Unix group id was left empty."),'validation');
      }
      if($this->_delete) {
        $member_id = $this->get_member_id();
        if($this->still_has_hosting_order()) {
          $this->set_error(red_t("This member has one or more hosting orders. Please deal with those before deleting."),'validation');
        }
      }
    }
    if(count($this->get_errors('validate')) != 0) return false;
    return true;
  }

  function member_friendly_name_unique() {
    // only use this rule if the member has not been submitted
    if(empty($this->_member_id)) {
      $member_friendly_name = addslashes($this->get_member_friendly_name());
      $sql = "SELECT member_id FROM red_member ".
        "WHERE member_friendly_name = @member_friendly_name AND member_status = 'active'";
      $result =  red_sql_query($sql, ['@member_friendly_name' => $member_friendly_name]);
      if(red_sql_num_rows($result) != 0) {
        $row = red_sql_fetch_assoc($result);
        return false;
      }
    }
    return true;
  }

  // constructor
  function __construct($construction_options) {
    // call parent
    parent::__construct($construction_options);
    $this->_set_datafields();
    // when creating a new item - set defaults
    if(empty($this->_member_id)) {
      $this->set_member_status('active');
      $this->set_member_type('organization');
      $this->set_member_parent_id(0);
    }
    if(empty($this->_member_parent_id))
      $this->set_member_parent_id(0);

  }

  function _pre_commit_to_db() {
    global $globals;
    $this->set_member_quota(red_machine_readable_bytes($this->get_member_quota()));
    // if deleting, first attempt to delete hosting orders before we delete the member
    if($this->_delete) {
      // delete unix group
      $unix_group_id = $this->get_unique_unix_group_id();
      $member_id = $this->get_member_id();
      if(!red_unix_group_id_in_use($unix_group_id, $member_id)) {
        $sql = "SELECT * FROM red_unique_unix_group WHERE unique_unix_group_id = #unix_group_id";
        if (!$this->delete_objects($sql, ['#unix_group_id' => $unix_group_id], 'red_unique_unix_group')) {
          $this->set_error("Failed to delete unix group.",'system');
          return false;
        }
      }

      // delete contacts
      $sql = "SELECT * FROM red_contact ".
        "WHERE member_id = #member_id AND contact_status != 'deleted'";
      if(!$this->delete_objects($sql, ['#member_id' => $member_id], 'red_contact')) {
        $this->set_error(red_t("Failed to delete contact."),'system');
        return false;
      }

      // Update member_end_date field
      $end_date = date('Y-m-d');
      $this->set_member_end_date($end_date);
    } 
    elseif(!$this->exists_in_db()) {
      // create the unique_unix_group if we are inserting
      $unix_id = $this->get_unique_unix_group_id();
      if(empty($unix_id)) {
        $values = array(
          'unique_unix_group_name' => $this->get_unique_unix_group_name(),
        );
        if(false === $obj = $this->create_related_object('red_unique_unix_group', $values)) {
          return false;
        }
        $this->set_unique_unix_group_id($obj->get_unique_unix_group_id());
      }
    }
    return true;
  }

  function _post_commit_to_db() {
    if($this->_sql_action == 'insert') {
      // This object has just been added to the database
      $this->_construction_options['member_id'] = $this->get_member_id();
      $email = $this->get_contact_email();
      if(!empty($email)) {
        // create a related contact record
        $values = array(
          'member_id' => $this->get_member_id(),
          'contact_email' => $this->get_contact_email(),
          'contact_first_name' => $this->get_contact_first_name(),
          'contact_lang' => $this->get_contact_lang(),
          'contact_last_name' => $this->get_contact_last_name(),
        );
        $ret = $this->create_related_object('red_contact',$values);
        if(!$ret) return $ret;
      }
    }
    return true;
  }

  function get_edit_member_parent_id() {
    return $this->_html_generator->get_select('sf_member_parent_id',$this->member_parent_id_options,$this->get_member_parent_id(),false,array(),false);
  }
  
  function get_edit_member_benefits_level() {
    return $this->_html_generator->get_select('sf_member_benefits_level',$this->get_member_benefits_level_options(),$this->get_member_benefits_level(), false);
  }
  function get_edit_member_type() {
    return $this->_html_generator->get_select('sf_member_type',$this->get_member_type_options(),$this->get_member_type(), false);
  }
  function get_edit_member_quota() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_member_quota());
    return $this->_html_generator->get_input('sf_member_quota',$value);
  }
  function get_read_member_quota() {
    // Convert to human readable before displaying.
    $value = red_human_readable_bytes($this->get_member_quota());
    if ($value == 0) {
      return red_t("Not set");
    }
    return $value;
  }
  function get_member_benefits_level_options() {
    return array('basic' => red_t('Basic'), 'standard' => red_t('Standard'), 'extra' => red_t("Extra"));
  }
  function get_member_type_options() {
    return array('individual' => red_t('Individual'),'organization' => red_t('Organization'));
  }
  function get_edit_block_table_elements() {
    $elements = parent::get_edit_block_table_elements();
    if(!$this->exists_in_db()) {
      // pop off the last row
      $last = array_pop($elements);
      // add a special field to ask for the user login name

      // first login
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Group short name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_group_name',$this->get_unique_unix_group_name(),'text');
      $elements[] = $row;

      // email address for related contact
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Email address for contact');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_contact_email',$this->get_contact_email(),'text');
      $elements[] = $row;

      // first name for related contact
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('First name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_contact_first_name',$this->get_contact_first_name(),'text');
      $elements[] = $row;

      // last name for related contact
      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = red_t('Last name');
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $this->_html_generator->get_form_element('sf_contact_last_name',$this->get_contact_last_name(),'text');
      $elements[] = $row;

      // lang preference for related contact
      $default = $this->get_contact_lang();
      if(empty($default)) $default = 'en_US';
      $lang_select = $this->_html_generator->get_select('sf_contact_lang',array('en_US' => red_t('English'),'es_MX' => red_t('Spanish')),$default);

      $row = array(0 => array(), 1 => array());
      $row[0]['value'] = 'Language';
      $row[0]['attributes'] = array('class' => 'red-edit-field-label');
      $row[1]['value'] = $lang_select;
      $elements[] = $row;

      // put the last element back on.
      $elements[] = $last;
    }
    return $elements;
  }

  /**
   * Check to see if this membership has any hosting orders
   * that may need to be deleted before removing the membership.
   **/
  function still_has_hosting_order() {
    $member_id = $this->get_member_id();
    $sql = "SELECT count(*) FROM red_hosting_order WHERE ".
      "member_id = #member_id AND " .
      "(hosting_order_status = 'active' OR hosting_order_status = 'disabled')";

    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    $row = red_sql_fetch_row($result);
    if($row[0] > 0) return TRUE;
    return FALSE;
  }

  function set_user_input($post) {
    // first load the db values
    $key_field = $this->get_key_field();
    $submit_key_var = 'sf_' . $key_field;
    $this->set_field_value($key_field,$post[$submit_key_var]);
    $this->reset_to_db_values();
    foreach ($post as $k => $v) {
      if(preg_match('/sf_(.*)$/',$k,$matches)) {
        $field = $matches[1];
        // This should not be set by the class - it's a
        // helper
        if($field == 'group_name') {
          // set the unique unix group - which will be created in _pre_commit_to_db
          $this->set_unique_unix_group_name($v);
        } elseif($field == 'contact_email') {
          $this->set_contact_email($v);
        } elseif($field == 'contact_first_name') {
          $this->set_contact_first_name($v);
        } elseif($field == 'contact_lang') {
          $this->set_contact_lang($v);
        } elseif($field == 'contact_last_name') {
          $this->set_contact_last_name($v);
        } else {
          // normal fields
          if($this->accept_user_input($field))
            $this->set_field_value($field,$v);
        }
      }
    }
  }

  // Memberships can be deleted when all items have been disabled
  // for more than 3 months.
  public static function active_hosting_orders($member_id) {
    $sql = "SELECT COUNT(*) AS count FROM red_hosting_order
      WHERE member_id = #member_id AND hosting_order_status = 'active'";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }
  
  // Available items are items that are not deleted or disabled.
  public static function available_items($member_id) {
    $sql = "SELECT COUNT(*) AS count FROM red_hosting_order
      JOIN red_item USING(hosting_order_id)
      WHERE red_hosting_order.member_id = #member_id AND 
      item_status NOT IN ('deleted', 'disabled', 'pending-delete', 'pending-disable')";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  public static function date_of_last_disabled_item($member_id) {
    $sql = "SELECT SUBSTRING(MAX(item_modified), 1, 10) AS date FROM red_hosting_order
      JOIN red_item USING(hosting_order_id)
      WHERE red_hosting_order.member_id = #member_id AND 
      item_status = 'disabled'";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? NULL;
  }
}
