Hello {contact_first_name},

This email is an automated notice from the May First tech support team about
the membership: {member_friendly_name}.

We are letting you know that the following hosting resources are getting
close to their alloted disk space quota:

{disk_usage_summary}

Currently, you membership is allowed {membership_disk_quota} of disk space. You have already
allocated {membership_disk_allocation}. And, you are currently using {membership_disk_usage}. 

If you have not yet allocated all the disk space you are allowed, you are
welcome to click on the link to any item that has exceeded it's disk usage and
increase the quota. 

If you notice that you have allocated a lot more disk space that you are using,
you may login to the control panel (https://members.mayfirst.org/cp) and lower
the disk allocation for items that are not using the space they have been
allocated.

Lastly, if you need to have more disk space, we have plenty to go around! Just
reply to this email and let us know you would like to change your hosting plan.

Note: Once you exceed your quota, your services won't work properly.

Remember: we collectively own all our servers, so maintaining quotas is
an important way to ensure we are sharing them responsibly.

If you would like to silence these notifications, you can click on the link to
the item, and either postpone notifications by setting the next notification
date or change the percentage threshhold that triggers notifications.
