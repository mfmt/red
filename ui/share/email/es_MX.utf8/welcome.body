Hola {member_friendly_name},

Gracias por ayudarnos a construir May First Movement Technology y apoyar
nuestra misión de crear una infraestructura tecnológica resiliente y
socialmente justa al alcance de cada persona.

A continuación encontrarás información importante sobre los servicios de
alojamiento.

== NUESTRA MISIÓN ==

Somos mucho más que un proveedor de servicios web, May First es su
membresía. Gracias al trabajo junto a las membresías fortalecemos las
luchas locales y los movimientos sociales de transformación global
mediante la gestión colectiva de nuestras tecnologías.


== ACTIVA TU CONTRASEÑA ==

Tu contraseña aún no está configurada.

Para establecerla, haz clic en el siguiente enlace. Se solicitará crear
una nueva contraseña para esta cuenta. No será posible acceder a ningún
servicio de alojamiento hasta que se haya establecido una contraseña.

{password_reset_url}

Este enlace expira el {password_reset_expire_date}.


== PANEL DE CONTROL DE LAS MEMBRESÍAS ==

Para acceder al panel de control de May First, ingresa a:

https://members.mayfirst.org/cp

e inicia sesión con:

usuarie: {user_name}
contraseña: [ingresa la contraseña ingresada en el paso anterior]

El panel de control permite crear y modificar usuaries, cuentas de
correo, listas de correo, sitios web y proporcionar acceso SSH/SFTP,
etc.

== NOMBRE DE DOMINIO ==

Si tu membresía posee los beneficios de alojamiento estándar, los
siguientes apartados son aplicables para ti.

Si tienes tu propio nombre de dominio, visita este enlace para obtener
información sobre cómo configurarlo utilizando los servidores de May
First:
https://help.mayfirst.org/es/guide/how-to-configure-domain-to-use-may-first-dns-nameservers


== CREAR UN SITIO WEB ==

Opcionalmente puedes crear un sitio web. Para ello, inicia sesión en el
panel de control, haz clic en la pestaña "Configuración web" y haz clic
en "Añadir un nuevo elemento" (o Add new item). Para obtener más
información, visita nuestra página de ayuda:

https://help.mayfirst.org/es/guide/how-to-add-web-site

== COPIAR ARCHIVOS A TU SITIO WEB==

Para instalar automáticamente WordPress u otra aplicación web, consulta
las instrucciones aquí:

https://help.mayfirst.org/es/guide/how-to-automatically-install-web-app

Para conocer cómo utilizar manualmente el acceso SSH/SFTP para copiar
archivos puedes revisar:

https://help.mayfirst.org/es/guide/how-to-connect-to-your-website-host-with-ssh-or-sftp

== CONTACTAR AL SOPORTE TÉCNICO ==

Si tienes duds sobre los servicios, puedes consultar nuestra página de
ayuda https://help.mayfirst.org (disponible en inglés y español).

O bien contactar al soporte técnico escribiendo a: support@mayfirst.org

¡Muchas gracias!
May First Movement Technology
