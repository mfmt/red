// Check if we are on new infrastructure and hide elements as
// necessary (until we completely move to new infrastructure).
var on_new_infrastructure = $('#red-edit-table').data("red-on-new-infrastructure");
if (on_new_infrastructure) {
  $('#red-cron-login-table-row').hide();
  $('#red-help-hidden-fieldset').hide();
}
else {
  $('#red-forever-schedule').hide();
  $('#red-systemd-timer-explanation').hide();
  $('#red-cron-working-directory-table-row').hide();
  $('#red-cron-environment-table-row').hide();
}

function generateCronSchedule(type)
{
	// we randomize by choosing the current date, hour, min
	var now = new Date();
	var min = now.getMinutes();
	var hour = now.getHours();
	var day = now.getDay();

	var schedule = '';
	if(type == 'hourly') {
    if (on_new_infrastructure) {
      schedule = 'hourly';
    }
    else {
		  schedule = min + ' * * * *';
    }
	} else if (type == 'daily') {
    if (on_new_infrastructure) {
      schedule = 'daily';
    }
    else {
	  	schedule = min + ' ' + hour + ' * * *';
    }
	} else if (type == 'weekly') {
    if (on_new_infrastructure) {
      schedule = 'weekly';
    }
    else {
		  schedule = min + ' ' + hour + ' * * ' + day;
    }
	} else if (type == 'forever') {
    schedule = 'forever';
  }
	if(schedule != '') {
		document.getElementById('cron_schedule').value = schedule;
	}
  
}


