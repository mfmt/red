function generateRandomPassword(len)
{
	var letters="abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
	var numbers="23456789";
	var ret = '';
	var keylist;
	var bool;
	var number_checker = new RegExp("[0-9]");
	var letter_checker = new RegExp("[a-zA-Z]");
	for(i=0;i<len;i++) {
		bool=Math.round(Math.random());
		if(bool == 0) {
			keylist=letters;
		} else {
			keylist=numbers;
		}
		if(i==len-1) {
			// make sure we have at least one number and one letter
			if(!number_checker.exec(ret)) {
				keylist=numbers;
			} 
			if(!letter_checker.exec(ret)) {
				keylist=letters;
			}
		}
		ret+=keylist.charAt(Math.floor(Math.random()*keylist.length));
	}
	return ret; 
}
