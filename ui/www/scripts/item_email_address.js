function addEmailAddressFromAvailableMailboxes(el) {
  var mailboxLogin = event.target.value || event.srcElement.value;
  if (!mailboxLogin) {
    // No mailbox selected, do nothing.
    return;
  }

  // Convert the mailbox login to an email address.
  newAddress = mailboxLogin + '@mail.mayfirst.org';
  if (document.getElementById("email_address_recipient").value == "") {
    document.getElementById("email_address_recipient").value = newAddress;
  }
  else {
    document.getElementById("email_address_recipient").value += ", " + newAddress;
  }

  // Remove this email address from the list of options.
  var mailboxOptions = document.getElementById("available_mailboxes");
  for (var i=0; i < mailboxOptions.children[0].length; i++) {
    if (mailboxOptions.children[0][i].value == mailboxLogin) {
       mailboxOptions.children[0].remove(i);
    }
  }
}
document.getElementById("available_mailboxes").addEventListener('change', addEmailAddressFromAvailableMailboxes);
