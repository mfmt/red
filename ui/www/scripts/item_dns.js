/* Only show DNS fields relevant to the type selected. */ 

// Get the value of the type field.
$(document).ready(function() {

  // Set available fields based on current value of dns type.
  var dns_type = $("[name|='sf_dns_type'] option:selected").val();
  update_fields_based_on_dns_type(dns_type);
  
  // If the dns type changes, update the fields available.
  $("[name|='sf_dns_type']").change(function () {
    dns_type = $("[name|='sf_dns_type'] option:selected").val();
    update_fields_based_on_dns_type(dns_type);
  });
  $("#dns-form").on('submit', function () {
    if ($('#dns_type').val() == 'alias') {
      $('#dns_ip').val('');
    }
    else if ($('#dns_type').val() == 'a') {
      $('#dns_server_name').val('');
    }
  });

  function update_fields_based_on_dns_type(dns_type) {
    // If we are editing a record, disable non-editable fields.
    if ($('#item_id').val() && $('#item_id').val() != 0) {
      if (dns_type == 'ptr') {
        $('#dns_ip').prop('disabled', true);
        $('#dns_fqdn').prop('disabled', false);
      }
      else {
        $('#dns_fqdn').prop('disabled', true);
        $('#dns_ip').prop('disabled', false);
      }
    }
    if ( dns_type == '' ) {
      // Initially hide all fields we might not need.
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'alias' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }

    else if ( dns_type == 'mx' ) {
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").show();
      $("#red-dns-mx-verified-table-row").show();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'txt' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").show();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'srv' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").show();
      $("#red-dns-weight-table-row").show();
      $("#red-dns-port-table-row").show();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'a' || dns_type == 'aaaa' || dns_type == 'ptr') {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").show();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'cname' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").show();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'sshfp' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").show();
      $("#red-dns-sshfp-type-table-row").show();
      $("#red-dns-sshfp-fpr-table-row").show();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'caa' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").show();
      $("#red-dns-caa-tag-table-row").show();
      $("#red-dns-caa-value-table-row").show();
      $("#red-dns-dkim-selector-table-row").hide();
      $("#red-dns-dkim-signing-domain-table-row").hide();
    }
    else if ( dns_type == 'dkim' ) {
      $("#red-dns-mx-verified-table-row").hide();
      $("#red-dns-ip-table-row").hide();
      $("#red-dns-server-name-table-row").hide();
      $("#red-dns-text-table-row").hide();
      $("#red-dns-dist-table-row").hide();
      $("#red-dns-weight-table-row").hide();
      $("#red-dns-port-table-row").hide();
      $("#red-dns-sshfp-algorithm-table-row").hide();
      $("#red-dns-sshfp-type-table-row").hide();
      $("#red-dns-sshfp-fpr-table-row").hide();
      $("#red-dns-caa-flags-table-row").hide();
      $("#red-dns-caa-tag-table-row").hide();
      $("#red-dns-caa-value-table-row").hide();
      $("#red-dns-dkim-selector-table-row").show();
      $("#red-dns-dkim-signing-domain-table-row").show();
    }
  }
});

