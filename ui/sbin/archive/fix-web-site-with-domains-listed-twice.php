#!/usr/bin/php
<?php

/*
 * Fix web sites with domain names listed more than once. 
 *
 */

$config_paths = [ '../etc/config.inc.php', '/usr/local/share/red/ui/etc/config.inc.php' ];
foreach($config_paths as $path) {
  if (is_file($path)) {
    require($path);
    break;
  }
}

$globals['config'] = $config;
global $globals;

require($config['common_src_path'] . '/red.lang.utils.inc.php');
require($config['common_src_path'] . '/red.utils.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');
require($config['common_src_path'] . '/class.red_ado.inc.php');
require($config['common_src_path'] . '/class.red_item.inc.php');

if(!$sql_resource = red_db::init_db($config)) {
  echo "Failed to initialize db\n";
  exit(2);
}


$sql = "SELECT item_id, web_conf_domain_names
  FROM 
    red_item JOIN red_item_web_conf USING (item_id) 
  WHERE
    item_status in ('active', 'disabled')";

$result = red_sql_query($sql, $sql_resource);
$count = 0;
while ($row = red_sql_fetch_row($result)) {
  $item_id = $row[0];
  $domain_names_str = trim($row[1]);
  $domain_names_unique_str = trim(implode(' ', array_unique(array_filter(explode(' ', $domain_names_str)))));
  if ($domain_names_str != $domain_names_unique_str) {
    echo "Was: $domain_names_str\n";
    $sql = "UPDATE red_item_web_conf SET web_conf_domain_names = '$domain_names_unique_str' WHERE item_id = $item_id";
    echo "sql: $sql\n";
    red_sql_query($sql, $sql_resource);
    $count++;
  }
}
echo "Total: $count\n";
   


?>
