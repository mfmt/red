#!/bin/bash

# Ensure /etc/knot/knot.conf.d/knot.zones.conf.d/$zone exists
# set SOA and NS records as needed.
PATH="/bin:/usr/bin:/usr/sbin:/sbin"

zone="$1"
nameservers="$2"

if [[ ! "$zone" =~ ^[0-9a-z.-]+$ ]]; then
  printf "Illegal characters in or empty zone: %s\n" "$zone"
  exit 1
fi

dest="/etc/knot/knot.conf.d/knot.zones.conf.d/${zone}.conf"
if [ ! -f "$dest" ]; then
  # Create the zone configuration file.
  if ! printf "zone:\n  - domain: %s.\n" "$zone" > "$dest"; then
    printf "Failed to create zone file %s.\n" "$dest"
    # Remove dest file so we start fresh next time.
    rm -f "$dest"
    exit 1
  fi

  if ! knotc reload; then
    printf "Failed to reload knotc after creating %s.\n" "$dest"
    # Remove dest file so we start fresh next time.
    rm -f "$dest"
    exit 1
  fi
fi

dest="/var/lib/knot/${zone}.zone"

# If any of our knotc commands fail, ensure we exit cleanly.
die () {
  printf "%s\n" "$1"
  knotc zone-abort "$zone"
  rm -f "$dest"
  exit 1
}

if [ ! -f "$dest" ]; then
  # First the SOA
  ttl=86400
  # Peel off the first name server.
  ns=$(echo "$nameservers" | cut -d: -f1)
  email="hostmaster.${zone}."
  # For serial, use a timestamp.
  serial=$(date +%s)
  # Time between refresh from the secondary server (not really used
  # since the primary server notifies the secondary when there is an
  # update).
  refresh=2h
  # How often for the secondary to retry if first attempt fails.
  retry=3m
  # How long secondary values are valid if we can't reach the primary
  expire=4w
  # How long the secondary should cache a "no response for this domain"
  negative_response_ttl=1h

  knotc zone-begin "$zone" || die "Failed to run zone-begin on ${zone}"
  knotc zone-set "$zone"  @ "$ttl" SOA "${ns}." "$email" "$serial" "$refresh" "$retry" "$expire" "$negative_response_ttl" || die "Failed to set SOA for ${zone}"
  oldifs="$IFS"
  IFS=":"
  for nameserver in $nameservers; do
    knotc zone-set "$zone" "${zone}." 86400 NS "${nameserver}." || die "Failed to set name server ${nameserver}"
  done
  IFS="$oldifs"
  knotc zone-commit "$zone" || die "Failed to commit zone ${zone}"
  if ! knotc zone-check "$zone"; then
    printf "Failed to check zone %s.\n" "$zone"
    exit 1
  fi

  touch /var/lib/red/zone-changed.txt
fi
