#!/bin/bash

PATH="/bin:/usr/bin:/usr/sbin:/sbin"

service_item_id="$1"
user_item_id="$2"
service_file="$3"
timer_file="$4"

# When we exit, ensure we clean up the temp files.
die() {
  rm -f "$service_file"
  if [ -n "$timer_file" ]; then
    rm -f "$timer_file"
  fi
  exit 1
}

if [[ ! "$service_item_id" =~ ^[0-9]+$ ]]; then
  printf "Illegal characters in or empty cron_item_id: %s\n" "$service_item_id"
  die
fi

if [[ ! "$user_item_id" =~ ^[0-9]+$ ]]; then
  printf "Illegal characters in or empty user_item_id: %s\n" "$user_item_id"
  die
fi

if [[ ! "$service_file" =~ ^/tmp/red-cron[A-Za-z0-9]+$ ]]; then
  printf "Illegal characters service file path: %s\n" "$service_file"
  die
fi

if ! test -f "$service_file"; then
  printf "service file does not exist: %s\n" "$service_file"
  die
fi

if [ -n "$timer_file" ]; then
  if [[ ! "$timer_file" =~ ^/tmp/red-cron[A-Za-z0-9]+$ ]]; then
    printf "Illegal characters timer file path: %s\n" "$timer_file"
    die
  fi
  if ! test -f "$timer_file"; then
    printf "timer file does not exist: %s\n" "$timer_file"
    die
  fi
fi

user_name="site${user_item_id}writer"
service_name="red-item-${service_item_id}"
home=$(getent passwd "$user_name" | cut -d: -f6)
uid=$(getent passwd "$user_name" | cut -d: -f3)
if [ ! -e "$home" ]; then
  printf "Failed to find home directory %s\n" "$home"
  die
fi
service_dir="${home}/.config/systemd/user"
mkdir -p "$service_dir" || die

if ! /usr/bin/systemctl start dbus; then
  printf "Failed to start dbus\n"
  die
fi
if ! /usr/bin/loginctl enable-linger "$user_name"; then
  printf "Failed to enable linger.\n"
  die
fi

# There is always a service file.
mv "$service_file"  "${service_dir}/${service_name}.service" || die

if [ -n "$timer_file" ]; then
  mv "$timer_file"  "${service_dir}/${service_name}.timer" || die
  # We are starting and enabling the timer.
  start_service="${service_name}.timer"
  stop_service="${service_name}.service"
  target_dir=default.target.wants
else
  # We are starting and enabling the service.
  start_service="${service_name}.service"
  stop_service="${service_name}.timer"
  target_dir=multi-user.target.wants
fi
target_path="${service_dir}/${target_dir}"
if [ ! -d "$target_path" ]; then
  mkdir -p "$target_path" || die
fi

# Chowning this directory allows us to enable/disable as the user.
chown "$user_name" "$target_path" || die

xdg_runtime_dir="/run/user/${uid}"
if [ ! -e "$xdg_runtime_dir" ]; then
  # Sometimes it takes a few second to show up after we enable linger.
  sleep 5
fi
if [ ! -e "$xdg_runtime_dir" ]; then
  printf "Run time dir is not available: %s\n" "$xdg_runtime_dir"
  die
fi

systemctl --user --machine "${user_name}@.host" daemon-reload || die
systemctl --user --machine "${user_name}@.host" "enable" "$start_service" || die
systemctl --user --machine "${user_name}@.host" "restart" "$start_service" || die

# Clean up. We might be transitioning from a scheduled cron job to a forever
# service or vice versa. Ensure the other service is disabled and stopped.
# Ignore errors if the service does not exist.
systemctl --user --machine "${user_name}@.host" "disable" "$stop_service" 2>/dev/null
systemctl --user --machine "${user_name}@.host" "stop" "$stop_service" 2>/dev/null

if [ -z "$timer_file" ]; then
  # Ensure we don't leave a timer file behind.
  rm -f "${service_dir}/${service_name}.timer" || die
fi
exit 0
