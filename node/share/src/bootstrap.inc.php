<?php

$config_file = '/usr/local/etc/red/red_node.conf';
if(!file_exists($config_file)) {
  die("The config file, '$config_file', does not exist.");
}
require_once($config_file);

// Generate a usable $config variable.
$config = array(
  'red_backends' => '',
  'src_path' => '',
  'common_src_path' => '',
  'conf_path' => '',
  'node' => '',
  'lock_file' => '',
  'db_host' => '',
  'db_user' => '',
  'db_pass' => '',
  'db_name' => '',
  'site_dir_template' => '',
  'mysql_cnf' => '',
  'mysql_cert_file' => '',
);
// All variables must be set except mysql_cnf and mysql_cert_file. 
// With those two, at least one needs to be et.
if (!isset($mysql_cnf) && !isset($mysql_cert_file)) {
  die("Please set either mysql_cert_file or mysql_cnf in your config file.");
}
foreach($config as $k => $v) {
  if ($k != 'mysql_cnf' && $k != 'mysql_cert_file') {
    if (empty($$k)) {
      die("The variable $k is required in your config file.");
    }
  }
  elseif (!isset($$k)) {
    // It's ok if mysql_cnf is unset OR mysql_cert_file is unset, as long as one of them
    // is set.
    unset($config[$k]);
    continue;
  }
  $config[$k] = $$k;
}

// Load up all files.
$common_files = [
  '/red.lang.utils.inc.php',
  '/red.utils.inc.php',
  '/class.red_db.inc.php',
  '/class.red_ado.inc.php',
  '/class.red_item.inc.php',
  '/modules/class.red_item_cron.inc.php',
  '/modules/class.red_item_dns.inc.php',
  '/modules/class.red_item_email_address.inc.php',
  '/modules/class.red_item_list.inc.php',
  '/modules/class.red_item_mailbox.inc.php',
  '/modules/class.red_item_mysql_db.inc.php',
  '/modules/class.red_item_mysql_user.inc.php',
  '/modules/class.red_item_nextcloud.inc.php',
  '/modules/class.red_item_server_access.inc.php',
  '/modules/class.red_item_user_account.inc.php',
  '/modules/class.red_item_web_app.inc.php',
  '/modules/class.red_item_web_conf.inc.php',
];

foreach ($common_files as $file) {
  require_once($config['common_src_path'] . $file);
}

$node_files = [
  '/class.red_item_cron_node_systemd.inc.php',
  '/class.red_item_cron_node_vixie.inc.php',
  '/class.red_item_email_address_node_postfix.inc.php',
  '/class.red_item_list_node_mailman.inc.php',
  '/class.red_item_mailbox_node_postfix.inc.php',
  '/class.red_item_mysql_db_node_mysql.inc.php',
  '/class.red_item_mysql_user_node_mysql.inc.php',
  '/class.red_item_server_access_node_ssh.inc.php',
  '/class.red_item_user_account_node_unix.inc.php',
  '/class.red_item_web_app_node_unix.inc.php',
  '/class.red_item_web_conf_node_apache2_fpm.inc.php',
  '/class.red_item_web_conf_node_apache2.inc.php',
];

foreach ($node_files as $file) {
  require_once($config['src_path'] . "/modules/{$file}");
}

$sql_resource = red_db::init_db($config);  
if(!$sql_resource) {
  // This is not recoverable - and should trigger output
  red_log("Failed to get sql_resource when bootstrapping.");
  die("Failed to get sql_resource when bootstrapping.");
}
// Set our two global variables.
$globals['config'] = $config;
$globals['sql_resource'] = $sql_resource;
global $globals;

