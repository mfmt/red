<?php
if(!class_exists('red_item_mysql_user_node_mysql')) {
  class red_item_mysql_user_node_mysql extends red_item_mysql_user {
    var $_path_to_cnf = '/root/.my.cnf';
    var $_server_sql_resource;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      // If we are transitioning to new infrastructure...
      $custom_path = "/root/.my.legacy.cnf";
      if (file_exists($custom_path)) {
        $this->_path_to_cnf = $custom_path;
      }
    }

    // Get the host for the corresponding database that this mysql user
    // has access to. Pick the first one if there are more than one.
    function get_mysql_db_host() {
      $user_db = $this->get_mysql_user_db();
      if (empty($user_db)) {
        return NULL;
      }
      $dbs = array_filter(explode(':', $user_db));
      if (!is_array($dbs)) {
        return NULL;
      }

      $first_db = array_pop($dbs);
      // Active database records should come first, but we will also take deleted ones
      // in case we are deleting a record and all the dbs have already been deleted.
      $sql = "SELECT item_host FROM red_item JOIN red_item_mysql_db USING(item_id)
        WHERE mysql_db_name = @first_db ORDER BY item_status LIMIT 1";
      $result = red_sql_query($sql, ['@first_db' => $first_db]);
      $row = red_sql_fetch_row($result);
      return $row[0];
    }

    function _server_db_connect() {
      // require the mysql_db class so we can create the static function
      if(!$sql_resource = red_item_mysql_db_node_mysql::server_db_connect($this->_path_to_cnf)) {
        $this->set_error("Failed to connect to local MySQL server",'system');
        return false;
      }
      $this->_server_sql_resource = $sql_resource;
      return true;

    }

    function node_sanity_check() {
      if(!file_exists($this->_path_to_cnf)) {
        $message = "The mysql configuration file doesn't exist. ".
          "Trying: " .  $this->_path_to_cnf . ".";
        $this->set_error($message,'system');
        return false;
      }
      if(!is_readable($this->_path_to_cnf))  {
        $message = 'The mysql configuration file is not readable. '.
          'Trying: ' . $this->_path_to_cnf;
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->_server_db_connect()) {
        $message = 'Failed to establish a MySQL connection on the server.';
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete() {
      if(!$this->revoke_privs()) return false;
      if(!$this->delete_user()) return false;
      return true;
    }

    function disable() {
      return $this->delete();
    }

    function insert() {
      if(!$this->insert_user()) return false;
      if(!$this->grant_max_connections()) return false;
      if(!$this->grant_privs()) return false;
      return true;
    }

    function update() {
      // special exception - in case we are updating after 
      // being disabled, we'll need to re-insert the user
      if(!$this->user_exists()) {
        if(!$this->insert_user()) return false;
      }
      if(!$this->update_password()) return false;
      if(!$this->grant_max_connections()) return false;
      if(!$this->revoke_privs()) return false;
      if(!$this->grant_privs()) return false;
      return true;
    }

    function restore() {
      if(!$this->user_exists()) {
        if(!$this->insert()) return false;
      }
      if(!$this->update()) return false;
      return true;
    }

    function server_db_query($sql, $params = []) {
      if (!$result = red_sql_query($sql, $params, $this->_server_sql_resource)) {
        $error = red_sql_error($this->_server_sql_resource);
        if (!empty($error)) {
          $this->set_error("SQL error: $sql,  error: $error", 'system', 'soft');
          return false;
        }
      }
      return $result;
    }

    function user_exists() {
      $sql = "SELECT User FROM mysql.user WHERE User = @mysql_user_name";
      $result = $this->server_db_query($sql, ['@mysql_user_name' => $this->get_mysql_user_name()]);
      if(mysqli_num_rows($result) == 0) return false;
      return true;
    }

    function get_database_host() {
      // If the host is listed in the red_server table, then it's a mosh and we give
      // permission to localhost. Otherwise, it's a network server, and we give permission
      // to our local network.
      $sql = "SELECT COUNT(*) AS count FROM red_server WHERE server = @server";
      $result = red_sql_query($sql, ['@server' => $this->get_item_host()]);
      $row = red_sql_fetch_row($result);
      if($row[0] == 0) {
        return '10.9.67.%';
      }
      else {
        return 'localhost';
      }
    }
    function insert_user() {
      $sql = "CREATE USER @mysql_user_name@@database_host 
        IDENTIFIED BY PASSWORD @password";
      $params = [
        '@mysql_user_name' => $this->get_mysql_user_name(),
        '@database_host' => $this->get_database_host(),
        '@password' => $this->get_mysql_user_password()
      ];
      if (false === $this->server_db_query($sql, $params)) {
        return false;
      }
      return true;
    }

    function grant_max_connections() {
      $sql = "GRANT USAGE ON *.* TO @mysql_user_name@@database_host 
        WITH MAX_USER_CONNECTIONS #max_connections";
      $params = [
        '@mysql_user_name' => $this->get_mysql_user_name(),
        '@database_host' => $this->get_database_host(),
        '#max_connections' => $this->get_mysql_user_max_connections(),
      ];
      if(false === $this->server_db_query($sql, $params)) return false;
      return true;
    }

    function get_grant_statement($db) {
      $ret = "GRANT ";
      $ret .= $this->get_sql_priv();  
      $ret .= $this->get_grant_predicate($db, 'TO');
      return $ret;
    }

    function get_grant_predicate($db, $prep) {
      return "ON `$db`.* $prep " . $this->get_user_string();
    }

    function get_user_string() {
      $user = $this->get_mysql_user_name();
      return "'$user'@'" . $this->get_database_host() . "'";
    }

    function get_sql_priv() {
      if($this->get_mysql_user_priv() == 'full') {
        return "ALL ";
      } else {
        return "SELECT ";
      }
    }
    function grant_privs()  {
      $dbs = explode(':',$this->get_mysql_user_db());
      if(empty($dbs)) return true;
      foreach($dbs as $db) { 
        $sql = $this->get_grant_statement($db);
        if(false === $this->server_db_query($sql)) return false;
      }
      return true;
    }

    function revoke_privs()  {
      // for some reason REVOKE ALL ON *.* FROM 'user'@'localhost' doesn't work
      $sql = "DELETE FROM mysql.db WHERE User = @mysql_user_name";
      $params = [
        '@mysql_user_name' => $this->get_mysql_user_name()
      ];
      if (false === $this->server_db_query($sql, $params)) return false;
      if (!$this->flush_privileges()) return false;
      return true;
    }

    function delete_user() {
      $sql = "DELETE FROM mysql.user WHERE User = @mysql_user_name";
      $params = [
        '@mysql_user_name' => $this->get_mysql_user_name()
      ];
      if (false === $this->server_db_query($sql, $params)) return false;
      if (!$this->flush_privileges()) return false;
      return true;

    }
      
    function flush_privileges() {
      $sql = "FLUSH PRIVILEGES";
      if(false === $this->server_db_query($sql)) return false;
      return true;
    }

    function update_password() {
      $sql = "SET PASSWORD FOR " . $this->get_user_string() .
        "= @password";
      $params = [
        '@password' => $this->get_mysql_user_password(),
      ]; 
      if (false === $this->server_db_query($sql, $params)) return false;
      return true;

    }
  }  
}


?>
