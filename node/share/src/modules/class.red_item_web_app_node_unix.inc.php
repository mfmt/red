<?php
if(!class_exists('red_item_web_app_node_unix')) {
  class red_item_web_app_node_unix extends red_item_web_app {
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;
    }

    function node_sanity_check() {
      return true;
    }

    function delete() {
      // No-op. You have to delete the web conf to delete the web app.
      return TRUE; 
    }

    function disable() {
      // No-op. You have to disable the web conf to disable the web app;
      return TRUE;
    }

    function is_deprecated_web_app() {
      // We can no longer handle these old web apps.
      $deprecated = ['drupal5', 'drupal6', 'drupal7', 'mediawiki'];
      $name = $this->get_web_app_name();
      if (in_array($name, $deprecated)) {
        return TRUE;
      }
      return FALSE;
    }

    function insert() {
      if ($this->is_deprecated_web_app()) {
        return TRUE;
      }
      $name = $this->get_web_app_name();
      $install_func = NULL;
      switch ($name) {
        case 'drupal9':
          $install_func = 'install_drupal9';
          break;
        case 'wordpress':
          $install_func = 'install_wordpress';
          break;
        case 'backdrop':
          $install_func = 'install_backdrop';
          break;
      }
      if (empty($install_func)) {
        $this->set_error("Unknown web app name.", 'validation');
        return FALSE;
      }
      if (!$this->$install_func()) {
        return FALSE;
      }
      if (!$this->secure_installation_scripts()) {
        return FALSE;
      }
      return TRUE;
    }

    function update() {
      if ($this->is_deprecated_web_app()) {
        return TRUE;
      }

      // If we are updating after a soft error (which happened before any install files
      // were dropped), then we should insert. 
      if (!$this->install_files_present()) {
        return $this->insert(); 
      }
      // Otherwise, they may be updating the install IP address.
      return $this->secure_installation_scripts();
    }

    function install_files_present() {
      $name = $this->get_web_app_name();
      if ($name == 'wordpress') {
        $check_path = $this->get_site_dir() . '/web/wp-admin';
      }
      else {
        $check_path = $this->get_site_dir() . '/web/core';
      }
      return file_exists($check_path);
    }

    function restore() {
      return $this->update(); 
    }

    function get_web_conf_execute_as_user() {
      $sql = "SELECT web_conf_execute_as_user, red_item.item_id, item_host 
        FROM red_item JOIN red_item_web_conf
        USING (item_id) WHERE item_status = 'active'
        AND hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, [
        '#hosting_order_id' => $this->get_hosting_order_id()
      ]);
      $row = red_sql_fetch_row($result);
      if(empty($row)) return false;
      $execute_user = $row[0];
      $item_id = $row[1];
      $item_host = $row[2];
      if (preg_match('/^weborigin/', $item_host)) {
        return "site{$item_id}writer";
      }
      return $execute_user;
    }

    function install_drupal9() {
      return $this->install_web_app('drupal9');
    }
    function install_wordpress() {
      return $this->install_web_app('wordpress');
    }
    function install_backdrop() {
      return $this->install_web_app('backdrop');
    }

    function install_web_app($name) {
      $user = $this->get_web_conf_execute_as_user();
      if (empty($user)) {
        $this->set_error("I failed to get the execute as user. Is your web configuration properly enabled?", 'validation');
        return FALSE;
      }
      $args = [
        $name,
        $this->get_site_dir() . '/web',
      ];
      $cmd = '/usr/local/share/red/node/sbin/red-install-web-app';
      $env = [];
      $exit_status = red_fork_exec_wait($cmd, $args, $env, $user);
      if($exit_status == 0) {
        return TRUE;
      }
      elseif ($exit_status == 1) {
        $this->set_error("The web directory is not empty. Please delete and re-create your web configuration and try again.", 'validation');
        return FALSE;
      }
      else {
        $this->set_error("There was an error installing the web app (error: $exit_status). Please contact support for help.", 'validation');
        return FALSE;
      }
      return TRUE;
    }

    /*
     * Ensure install can only happen from install_ip
     */
    function secure_installation_scripts() {
      $htaccess_path = NULL; 
      $match = NULL;
      switch ($this->get_web_app_name()) {
        case 'drupal9':
        case 'backdrop':
          $htaccess_path = $this->get_site_dir() . '/web/core/.htaccess';
          $match = '^install\.php';
          break;
        case 'wordpress':
          $htaccess_path = $this->get_site_dir() . '/web//wp-admin/.htaccess';
          $match = '^(install|setup-config)\.php';
          break;
      }

      $ip = trim($this->get_web_app_install_ip());
      if (empty($ip)) {
        // If no IP is specified, just remove the htaccess file.
        if (file_exists($htaccess_path)) {
          if (!unlink($htaccess_path)) {
            $this->set_error("Failed to delete htaccess file.", 'system');
            return FALSE;
          };
          return TRUE;
        }
      }
      // Ensure the htaccess is up to date with the right IP address.
      $content = <<<EOC
<FilesMatch "$match">
Order deny,allow
deny from all
allow from $ip 
ErrorDocument 403 '<html><head><title>No access</title></head><body><p style="margin-left:10%; margin-top:20px; font-family: sans-serif;">Woops! Sorry, you seem to be installing this web app from a different IP address then you used to initiate the process. See our <a href="https://support.mayfirst.org/wiki/unauthorized-to-install-web-app">full explanation</a> for how to get around this problem.</p></body></html>'
</FilesMatch>
EOC;
      if (!file_put_contents($htaccess_path, $content)) {
        $this->set_error("Failed to create htaccess file.", 'system');
        return FALSE;
      };
      $user = $this->get_web_conf_execute_as_user();
      if (!red_chown($htaccess_path, $user)) {
        $this->set_error("Failed to chown htaccess file.", 'system');
        return FALSE;
      }
      return TRUE;
    }
  }  
}


?>
