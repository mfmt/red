<?php
if(!class_exists('red_item_email_address_node_postfix')) {
  class red_item_email_address_node_postfix extends red_item_email_address {
    var $_virtual_alias_maps_file = "/etc/postfix/virtual_alias_maps.red";
    var $_virtual_alias_domains_file = "/etc/postfix/virtual_alias_domains.red";

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;
    }

    function delete() {
      // See if the email address exists 
      if(!$this->email_address_exists()) {
        return TRUE; 
      }

      // Kill the postfix settings
      if (!$this->create_virtual_alias_domains_file()) return FALSE;
      if (!$this->create_virtual_alias_maps_file()) return FALSE;
      return TRUE; 
    }

    function disable() {
      return $this->delete();
    }

    function insert() {
      // Make sure this email address does not already exist
      if($this->email_address_exists()) {
        $message = 'Cannot insert new email account, an email account with '.
          'the same address already exists.';
        $this->set_error($message,'system','soft');
        return false;
      }

      if (!$this->create_virtual_alias_domains_file()) return FALSE;
      if (!$this->create_virtual_alias_maps_file()) return FALSE;
      return true;

    }

    function update() {
      // we have to be able to restore from being disabled.
      return $this->restore();
    }

    function restore() {
      if (!$this->create_virtual_alias_domains_file()) return FALSE;
      if (!$this->create_virtual_alias_maps_file()) return FALSE;

      return true;
    }

    function email_address_exists() {
      return red_key_exists_in_file($this->get_email_address(), '\s', $this->_virtual_alias_maps_file);
    }
      
    function node_sanity_check() {
      if(!is_writable($this->_virtual_alias_maps_file))  {
        $message = 'Virtual alias maps file not writable or does not '.
          'exist. Trying: ' . $this->_virtual_alias_maps_file;
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_virtual_alias_domains_file))  {
        $message = 'Virtual alias domains file not writable or does not '.
          'exist. Trying: ' . $this->_virtual_alias_domains_file;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function create_virtual_alias_maps_file() {
      if (!red_write_virtual_alias_maps_red_file($this->get_item_host(), $this->_virtual_alias_maps_file)) {
        $this->set_error("Failed to write the virtual alias maps file.", "system", "soft");
        return FALSE;
      }
      return TRUE;
    }

    function create_virtual_alias_domains_file() {
      if (!red_write_virtual_alias_domains_red_file($this->get_item_host(), $this->_virtual_alias_domains_file)) {
        $this->set_error("Failed to write the virtual alias domains file.", "system", "soft");
        return FALSE;
      }
      return TRUE;
    }  
  }
}


?>
