<?php
if(!class_exists('red_item_mysql_db_node_mysql')) {
  class red_item_mysql_db_node_mysql extends red_item_mysql_db {
    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('path_to_cnf');
    var $_path_to_cnf = '/root/.my.cnf';
    var $_server_sql_resource;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      global $globals;
      $conf_file = $globals['config']['conf_path'] . 
        '/red_mysql.conf';
      if(!$this->_set_config_values($conf_file)) {
        return;
      }

      // We may need to use the legacy my.cnf file if we are
      // transitioning from old mosh to new infrastructure.
      $custom_paths = [
        "/root/.my.legacy.cnf",
      ];
      foreach ($custom_paths as $custom_path) {
      if (file_exists($custom_path)) {
          $this->_path_to_cnf = $custom_path;
        }     
      }
    }

    function _server_db_connect() {
      if(!$sql_resource = $this->server_db_connect($this->_path_to_cnf)) {
        $this->set_error("Failed to connect to local MySQL server",'system');
        return false;
      }
      $this->_server_sql_resource = $sql_resource;
      return true;
    }

    /*
     * static function - designed to also be called
     * by class.red_item_mysql_user_node_mysql.inc.php
     */
    static function server_db_connect($path_to_cnf) {
      if(!file_exists($path_to_cnf)) return false;

      $file = file($path_to_cnf);
      $user = '';
      $pass = '';
      $host = '';
      $port = NULL;
      foreach($file as $line) {
        if(preg_match("/^#/",$line)) continue;

        $parts = explode('=',$line);
        if(trim($parts[0]) == 'user') {
          $user = trim($parts[1]);
        } elseif(trim($parts[0]) == 'password' || $parts[0] == 'pass') {
          $pass = trim($parts[1]);
        }
        else if(trim($parts[0]) == 'host') {
          $host = trim($parts[1]);
        }
        else if(trim($parts[0]) == 'port') {
          $port = intval(trim($parts[1]));
        }
        if(!empty($user) && !empty($pass) && !empty($host) &&!empty($port)) break;
      }
      if(empty($user) || empty($pass)) {
        return false;
      }

      if (empty($host)) {
        $host = 'localhost';
      }
      $database = NULL;
      if(!$sql_resource = mysqli_connect($host, $user, $pass, $database, $port)) {
        return false;
      }
      return $sql_resource;
    }

    function node_sanity_check() {
      if(!file_exists($this->_path_to_cnf)) {
        $message = "The mysql configuration file doesn't exist. ".
          "Trying: " .  $this->_path_to_cnf . ".";
        $this->set_error($message,'system');
        return false;
      }
      if(!is_readable($this->_path_to_cnf))  {
        $message = 'The mysql configuration file is not readable. '.
          'Trying: ' . $this->_path_to_cnf;
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->_server_db_connect()) {
        $message = 'Failed to establish a MySQL connection on the server.';
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete() {
      if(!$this->delete_db()) return false;
      return true;
    }

    function disable() {
      // no reasonable way to disable a database, so we
      // do nothing, and rely on the db user being disabled
      return true;
    }

    function insert() {
      // See if the db exists 
      if($this->db_exists()) {
        $message = 'I was asked to add a new database, but '.
          'a database with the same name already exists.';
        // this must be a hard error. If it's a soft error, then they
        // will be able to delete a database that is not theirs
        $this->set_error($message,'system','hard');
        return false;
      }
      if(!$this->insert_db()) return false;
      return true;
    }

    function update() {
      $db = $this->get_mysql_db_name();
      // We track disk usage - but not using filesystem. We have to do it manually
      // via the update disk usage command.
      if(!$this->update_disk_usage()) return false;
      return true;
    }

    function restore() {
      // See if the db exists 
      if(!$this->db_exists()) {
        return $this->insert();
      }
      else {
        $this->update();
      }
      return true;
    }

    function server_db_query($sql, $params = []) {
      $result = red_sql_query($sql, $params, $this->_server_sql_resource);
      if(false === $result) {
        $this->set_error(red_sql_error($this->_server_sql_resource), 'system', 'soft');
        return false;
      }
      return $result;
    }

    function db_exists() {
      $sql = "SHOW DATABASES LIKE @mysql_db_name";
      $result = $this->server_db_query($sql, ['@mysql_db_name' => $this->get_mysql_db_name()]);
      // on error, better to indicate db exists 
      if(!$result) return true;
      if(mysqli_num_rows($result) == 0) return false;
      return true;
    }

    function insert_db()  {
      $sql = 'CREATE DATABASE `!mysql_db_name`';
      return $this->server_db_query($sql, ['!mysql_db_name' => $this->_mysql_db_name]);
    }

    function delete_db() {
      if(!$this->db_exists()) return true;
      $sql = 'DROP DATABASE `!mysql_db_name`';
      return $this->server_db_query($sql, ['!mysql_db_name' => $this->_mysql_db_name]);
    }
    
  }  
}


?>
