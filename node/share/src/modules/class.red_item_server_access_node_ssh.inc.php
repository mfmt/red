<?php
if(!class_exists('red_item_server_access_node_ssh')) {
  class red_item_server_access_node_ssh extends red_item_server_access {
    var $_addgroup_cmd = '/usr/sbin/addgroup';
    var $_deluser_cmd = '/usr/sbin/deluser';
    var $_usermod_cmd = '/usr/sbin/usermod';
    var $_ssh_group = 'sshusers';
    var $_config_variables = [];

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      global $globals;
      $conf_file = $globals['config']['conf_path'] . 
        '/red_server_access.ssh.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }
    }

    function delete() {
      if ($this->on_new_infrastructure()) {
        if (!$this->delete_unix_account()) return FALSE;
        if (!$this->delete_weborigin_login()) return FALSE;
      }
      else {
        if(!$this->remove_user_from_ssh_group()) return false;
      }
      return true;
    }

    function disable() {
      if ($this->on_new_infrastructure()) {
        if(!$this->disable_unix_account()) return false;
      }
      else {
        if(!$this->remove_user_from_ssh_group()) return false;
      }
      return TRUE;
    }

    function insert() {
      // See if the user exists 
      if ($this->on_new_infrastructure()) {
        if (!$this->create_unix_account()) return FALSE;
      }
      else {
        if (!$this->unix_account_exists()) {
          $message = 'I was asked to add server access for a user, but '.
            'the account does not exist in the unix config files. ';
          $this->set_error($message,'system');
          return false;
        }
      }
      if (!$this->set_shell()) return false;
      if (!$this->add_user_to_ssh_group()) return false;
      if (!$this->create_public_key()) return false;
      if (!$this->set_weborigin_login()) return FALSE;
      if (!$this->create_private_key()) return false;
      return true;
    }

    function update() {
      return $this->insert(); 
    }

    function restore() {
      return $this->insert(); 
    }

    function node_sanity_check() {
      $cmds_to_check = array('addgroup','deluser','usermod');
      foreach($cmds_to_check as $cmd) {
        $cmd_variable_name = "_{$cmd}_cmd"; 
        if(!file_exists($this->$cmd_variable_name)) {
          $message = "$cmd command does not exist. ".
            'Trying: ' . $this->$cmd_variable_name;
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }

    function delete_weborigin_login() {
      if (!$this->on_new_infrastructure()) {
        return TRUE;
      }
      $login = $this->get_server_access_login();
      $base = "/var/lib/red/access/";
      $path = "{$base}/$login";
      if (file_exists($path)) {
        if (!unlink($path)) {
          $this->set_error("Failed to unlink access file. Please contact support.", 'system', 'soft');
          return FALSE;
        }
      }
      return TRUE;
    }

    function set_weborigin_login() {
      if (!$this->on_new_infrastructure()) {
        return TRUE;
      }
      $login = $this->get_server_access_login();
      $base = "/var/lib/red/access/";
      if (!red_create_directory_recursively($base)) {
        $this->set_error("Failed to create directory $base.", 'system', 'soft');
        return FALSE;
      }
      $sql = "SELECT item_id, item_host FROM red_item JOIN red_item_web_conf USING (item_id)
        WHERE hosting_order_id = #hosting_order_id AND item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
      $row = red_sql_fetch_row($result);
      $id = $row[0];
      $host = $row[1];
      $content = "site{$id}writer@$host";
      $path = "{$base}/$login";
      if (file_put_contents($path, $content) === FALSE) {
        $this->set_error("Failed to save to $path");
        return FALSE;
      }
      return TRUE;
    }

    function create_private_key() {
      if (!$this->on_new_infrastructure()) {
        return TRUE;
      }
      $login = $this->get_server_access_login();
      $cmd = "/usr/local/sbin/mf-ssh-generate-key";
      $args = [
        $login
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to generate ssh keys for $login.";
        $this->set_error($error, 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }
    function disable_unix_account() {
      // set expiration date to 1  
      $login = $this->get_server_access_login();
      $cmd = '/sbin/usermod';  
      $args = [
        '--expiredate',
        1,
        $login
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to disable the shell user.";
        $this->set_error($error, 'system', 'soft');
        return FALSE;
      }
      return true;
    }

    function unix_account_exists() {
      $login = $this->get_server_access_login();
      return red_key_exists_in_file($login, ':', '/etc/passwd');
    }

    function delete_unix_account() {
      if (!$this->unix_account_exists()) {
        return TRUE;
      }
      $login = $this->get_server_access_login();
      // First try to umount the site directory - we won't be able to
      // delete the user's home directory if it's mounted.
      $site_dir = "/home/users/{$login}/site";
      if (is_dir($site_dir)) {
        $cmd = "/usr/bin/umount";
        $args = [
          $site_dir,
        ];

        // Ignore any errors - if the dir is not mounted we get an
        // error but that doesn't matter.
        red_fork_exec_wait($cmd, $args);
      }
      $cmd = '/usr/sbin/userdel';
      $args = [
        '--force',
        '--remove',
        $login
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to delete the shell user.";
        $this->set_error($error, 'system', 'soft');
        return FALSE;
      }
      return TRUE;

    }
    function create_unix_account() {
      $login = $this->get_server_access_login();
      $sql = "SELECT user_account_uid, user_account_password 
        FROM red_item JOIN red_item_user_account USING(item_id) WHERE 
        item_status != 'deleted' AND user_account_login = @login";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_row($result);
      $uid = intval($row[0]);
      $password = $row[1];

      if (!file_exists("/home/users")) {
        mkdir("/home/users");
      }

      if ($this->unix_account_exists()) {
        // Update the password.
        $cmd = "/usr/sbin/usermod";
        $args = [
          '--password',
          $password,
          // Unset expire date.
          '--expiredate',
          "",
          $login,
        ];
        $error = "Failed to update the password.";
        
      }
      else {
        $cmd = '/sbin/useradd';
        $args = [
          '--uid', $uid,
          '--shell', '/usr/bin/bash',
          '--password', $password,
          '--create-home',
          '--home-dir', "/home/users/{$login}",
          $login
        ];
        $error = "Failed to create the new user.";
      }
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $this->set_error($error, 'system', 'soft');
        return FALSE;
      }
      // We lock down everything on the shell server by ensuring
      // it is all root owned with some exceptions.
      if (!red_chown("/home/users/{$login}", "root")) {
        $this->set_error("Failed to chown user home directory. Please contact support.", 'system', 'soft');
        return FALSE;
      } 
      if (!red_chgrp("/home/users/{$login}", $login)) {
        $this->set_error("Failed to chgrp user home directory. Please contact support.", 'system', 'soft');
        return FALSE;
      }
      if (!chmod("/home/users/{$login}", 0750)) {
        $this->set_error("Failed to chmod user home directory. Please contact support.", 'system', 'soft');
        return FALSE;
      }
      if (!file_exists("/home/users/{$login}/.ssh")) {
        if (!mkdir("/home/users/{$login}/.ssh")) {
          $this->set_error("Failed to ssh directory. Please contact support.", 'system', 'soft');
          return FALSE;
        }
      }
      // Except the known_hosts file.
      if (!touch("/home/users/{$login}/.ssh/known_hosts")) {
        $this->set_error("Failed to touch known_hosts directory. Please contact support.", 'system', 'soft');
        return FALSE;
      }
      if (!red_chown("/home/users/{$login}/.ssh/known_hosts", $login)) {
        $this->set_error("Failed to chown known_hosts directory. Please contact support.", 'system', 'soft');
        return FALSE;
      }
      // When using sftp, the site will be mounted over the site directory.
      $chown_site_dir = TRUE;
      if (!file_exists("/home/users/{$login}/site")) {
        // Ignore errors. When the site dir is sshfs mounted it becomes in accessible
        // to other users so it seems to be there, but you can't mkdir it.
        if (!@mkdir("/home/users/{$login}/site")) {
          $chown_site_dir = FALSE;
        }
      }
      if ($chown_site_dir) {
        if (!red_chown("/home/users/{$login}/site", $login)) {
          $this->set_error("Failed to chown site directory. Please contact support.", 'system', 'soft');
          return FALSE;
        }
        if (!chmod("/home/users/{$login}/site", 0700)) {
          $this->set_error("Failed to chmod site directory. Please contact support.", 'system', 'soft');
          return FALSE;
        }
      }
      return TRUE;
    }

    function set_shell() {
      if ($this->on_new_infrastructure()) {
        return TRUE;
      }
      $cmd = $this->_usermod_cmd;  
      $user = $this->get_server_access_login();
      $args = '-s /bin/bash ' . escapeshellarg($user) .  " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to update the user shell. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }

    function get_home_dir() {
      $lines = file("/etc/passwd"); 
      $login = $this->get_server_access_login();
      foreach ($lines as $line) {
        $parts = explode(':', $line);
        if ($parts[0] == $login) {
          return $parts[5];
        }
      }
      return FALSE;
    }

    function create_public_key() {
      $home_dir = $this->get_home_dir();
      if (!$home_dir) {
        $login = $this->get_server_access_login();
        $this->set_error("Failed to find home directory for $login.", 'system', 'soft');
        return FALSE;
      }
      $ssh_dir = "$home_dir/.ssh";
      $auth_keys = "$ssh_dir/authorized_keys";

      // iterate over every line, only add lines 
      // that do not already exist
      $existing_lines = array();
      if(file_exists($auth_keys)) {
        $existing_lines = file($auth_keys, FILE_IGNORE_NEW_LINES);
      }
      $user_keys = $this->get_server_access_public_key();  
      if(!empty($user_keys)) {
        if(!file_exists($ssh_dir)) {
          if(!mkdir($ssh_dir)) {
            $this->set_error("Failed to create ssh directory. Trying: $ssh_dir.",'system');
            return false;
          }
          if(!$this->set_file_ownership($ssh_dir)) return false;
        }

        if (!file_exists($auth_keys)) {
          if(!touch($auth_keys)) {
            $this->set_error("Failed to create your auth keys file. Trying: $auth_keys.",'system');
            return false;
          }
          if(!$this->set_file_ownership($auth_keys)) return false;
        }
        if (!$this->on_new_infrastructure()) {
          $user_keys_arr = array_map('trim', explode("\n",$user_keys));
          foreach($user_keys_arr as $user_key) {
            $user_key = trim($user_key);
            if(!in_array($user_key,$existing_lines)) {
              // we can't be sure if the preceding line has a line break
              // so add one in the beginning.
              $append = "\n# added by mfpl members control panel\n" . 
                $user_key . "\n";
              if(!red_append_to_file($auth_keys,$append)) {
                $this->set_error("Failed to append to authorized_keys file. Trying: $auth_keys.",'system');
                return false;
              }
            }
          }
        }
        else {
          if (!file_put_contents($auth_keys, $user_keys)) {
            $this->set_error("Failed to copy authorized_keys file. Please contact support.",'system', 'soft');
            return false;
          }
        }
      }
      return true;
    }

    function set_file_ownership($file) {
      if ($this->on_new_infrastructure()) {
        // On new infrastructure, authorized_keys remains under root
        // ownership.
        return TRUE;
      }
      $unix_user = $this->get_server_access_login();
      if ($this->on_new_infrastructure()) {
        $unix_group_id = $unix_user;
      }
      else {
        $unix_group_id = $this->get_hosting_order_unix_group_id();
      }
      if(!red_chown($file,$unix_user)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!red_chgrp($file,$unix_group_id)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }
      return true;
    }

    function get_login_uid() {
      $sql = "SELECT user_account_uid FROM red_item_user_account WHERE ".
        "user_account_login = @unix_user_name";
      $result = red_sql_query($sql, [
        '@unix_user_name' => $this->get_server_access_login()
      ]); 
      if (!$result || red_sql_num_rows($result) == 0) {
        $message = "Unable to get user uid from user name";
        $this->set_error($message,'system');
        return false;
      }
      $row = red_sql_fetch_row($result);
      return $row[0];
    }
    function add_user_to_ssh_group() {
      if ($this->on_new_infrastructure()) {
        return TRUE;
      }
      $user = $this->get_server_access_login();
      $group = $this->_ssh_group;
      $cmd = $this->_addgroup_cmd;  
      $args = escapeshellarg($user) . ' ' . escapeshellarg($group) . " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed to add the use to the ssh group. The output was: ".
          "$output.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return true;
    }

    function remove_user_from_ssh_group() {
      $user = $this->get_server_access_login();
      $group = $this->_ssh_group;
      $cmd = $this->_deluser_cmd;  
      $args = escapeshellarg($user) . ' ' . escapeshellarg($group) . " 2>&1";
      exec("$cmd $args",$output,$return_value);
      // Don't check for error. If the user does not exist, this will
      // return an error, which is ok.  
      return true;
    }
  }
}


?>
