<?php
if(!class_exists('red_item_list_node_mailman')) {
  class red_item_list_node_mailman extends red_item_list {
    // If you want to extend this class in a way the requires an
    // addition to the config file, then add a value to this
    // array in your constructor
    var $_config_variables = array('mailman_bin_dir',
                             'owner_email_template',
                             'web_url',
                             'postmap_cmd',
                             'from_email',
                             'mailman_list_dir',
                             'postfix_transport_file',
    );
    var $_mailman_bin_dir;
    var $_owner_email_template;
    var $_mailman_list_dir = '/var/lib/mailman/lists';
    var $_postfix_disabled_email_file = '/etc/postfix/disabled-email';
    var $_web_url;
    var $_postfix_transport_file;
    var $_postmap_cmd;
    var $_from_email;
    var $_initial_password;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;

      global $globals;

      $conf_file = $globals['config']['conf_path'] .
        '/red_list.mailman.conf';
      if(!$this->_set_config_values($conf_file)) {
        return False;
      }
    }

    function node_sanity_check() {
      if(!is_dir($this->_mailman_bin_dir)) {
        $message = 'Mailman bin directory does not exist. Trying: ' . $this->_mailman_bin_dir;
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_postfix_transport_file))  {
        $message = 'Transport file not writable or does not '.
          'exist. Trying: ' . $this->_postfix_transport_file;
        $this->set_error($message,'system');
        return false;
      }
      if(!file_exists($this->_postmap_cmd)) {
        $message = 'Postmap command command does not exist. Trying: ' .
          $this->_postmap_cmd;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete() {
      if(!$this->remove_email_from_disabled_file()) return false;
      if(!$this->delete_list()) return false;
      if (!$this->build_mailman_aliases()) return FALSE;
      return true;
    }

    function disable() {
      if (!$this->build_mailman_aliases()) return FALSE;
      return $this->add_email_to_disabled_file();
    }

    function insert() {
      // See if the user exists
      if($this->list_exists()) {
        $message = 'I was asked to add a new Mailman list, but '.
          'a list with the same name already exists.';
        $this->set_error($message,'system','soft');
        return false;
      }
      if(!$this->insert_list()) return false;
      if (!$this->build_mailman_aliases()) return FALSE;
      if(!$this->notify_owner()) return false;
      return true;
    }

    function update() {
      if (!$this->configure_domain_if_necessary()) return FALSE;
      if (!$this->remove_email_from_disabled_file()) return false;
      // reset the list password
      if (!$this->set_email_domain()) return FALSE;
      if (!$this->set_owner()) return FALSE;
      if (!$this->build_mailman_aliases()) return FALSE;
      return TRUE;
    }

    function restore() {
      if(!$this->remove_email_from_disabled_file()) return false;

      // See if the list exists
      if(!$this->list_exists()) {
        return $this->insert();
      }
      else {
        if (!$this->configure_domain_if_necessary()) return FALSE;
        if (!$this->remove_email_from_disabled_file()) return false;
        if (!$this->build_mailman_aliases()) return FALSE;
      }
      return TRUE;
    }

    function set_email_domain() {
      $tmp = tempnam(sys_get_temp_dir(), 'redmm');
      $domain = $this->get_list_domain();
      file_put_contents($tmp, "host_name = '{$domain}'\n");
      $cmd = '/var/lib/mailman/bin/config_list';
      $args = [
        '-i',
        $tmp,
        $this->get_list_name(),
      ]; 
      $ret == red_fork_exec_wait($cmd, $args);
      unlink($tmp);
      if ($ret == 0) {
        return TRUE;
      }
      $message = "Failed to update list email domain.";
      $this->set_error($message, 'system', 'soft');
      return FALSE;
    }

    function set_owner() {
      $tmp = tempnam(sys_get_temp_dir(), 'redmm');
      $owner = $this->get_list_owner_email();
      if (!$owner) {
        return TRUE;
      }
      file_put_contents($tmp, "owner = ['{$owner}']\n");
      $cmd = '/var/lib/mailman/bin/config_list';
      $args = [
        '-i',
        $tmp,
        $this->get_list_name(),
      ]; 
      $ret == red_fork_exec_wait($cmd, $args);
      unlink($tmp);
      if ($ret == 0) {
        if (!$this->reset_password()) return FALSE;
        return TRUE;
      }
      $message = "Failed to update list owner email.";
      $this->set_error($message, 'system', 'soft');
      return FALSE;
    }

    function list_exists() {
      $dir = $this->_mailman_list_dir . '/' . $this->get_list_name();
      if(file_exists($dir)) {
        return true;
      }
      return false;
    }

    function build_mailman_aliases() {
      $sql = "SELECT list_name, list_domain
          FROM red_item JOIN red_item_list USING (item_id)
          WHERE
            item_status != 'deleted' AND
            item_status != 'pending-delete' AND
            item_status != 'disabled' AND
            item_status != 'pending-disable' AND
            item_host = @host";

      $results = red_sql_query($sql, ['@host' => $this->get_item_host()]);
      $lines = [];
      while ($row = red_sql_fetch_row($results)) {
        $name = $row[0];
        $domain = $row[1];
        $lines[] = "{$name}: {$name}@{$domain}";
        foreach(red_item_mailbox::$mailman2_email_address_fragments as $fragment) {
          $lines[] = "{$name}-{$fragment}: {$name}-{$fragment}@{$domain}";
        }

      }
      $file = "/etc/postfix/mailman-aliases";
      if (!file_put_contents($file, implode("\n", $lines))) {
        $message = "Failed to save mailman-aliases file";
        $this->set_error($message, 'system', 'soft');
        return FALSE;
      };
      $cmd = '/usr/sbin/postalias';
      $args = array($file);
      if(0 == red_fork_exec_wait($cmd,$args)) {
        return TRUE;
      }
      $message = "Failed to run postalias.";
      $this->set_error($message,'system');
      return false;
    }

    function add_email_to_disabled_file() {
      $file = $this->_postfix_disabled_email_file;
      $list_email = $this->get_list_name() . '@' . $this->get_list_domain();
      if(!red_key_exists_in_file($list_email," ",$file)) {
        $action = "REJECT This email list has been disabled. If you are the list owner, please contact support at https://support.mayfirst.org/newticket.";
        $map_line = "$list_email $action\n";
        if(!red_key_exists_in_file($list_email," ",$file)) {
          if(!red_append_to_file($file,$map_line)) {
            $message = "Failed to add email to disabled email list.";
            $this->set_error($message,'system');
            return false;
          }
        }
        if(!$this->postmap_file($file)) return false;
      }
      return true;
    }

    function remove_email_from_disabled_file() {
      $file = $this->_postfix_disabled_email_file;
      $list_email = $this->get_list_name() . '@' . $this->get_list_domain();
      if(red_key_exists_in_file($list_email," ",$file)) {
        if(!red_delete_from_file($file,$list_email," ")) {
          $message = "Failed to remove list email from disabled file.";
          $this->set_error($message,'system');
          return false;
        }
        if(!$this->postmap_file($file)) return false;
      }
      return true;
    }

    function reset_password() {
      $cmd = $this->_mailman_bin_dir . "/change_pw";
      $new_password = red_generate_random_password();
      if(FALSE === $new_password) {
        $message = "Failed to generate random password.";
        $this->set_error($message,'system');
        return FALSE;
      }
      $args = array('-l', $this->get_list_name(), '-p', $new_password);
      if(0 == red_fork_exec_wait($cmd, $args)) {
        return TRUE;
      }
      $message = "Failed to change the password.";
      $this->set_error($message,'system');
      return false;
    }

    function insert_list()  {
      if(!$this->configure_domain_if_necessary()) return false;
      $domain = $this->get_list_domain();
      $cmd = $this->_mailman_bin_dir . "/newlist";
      $this->_initial_password = red_generate_random_password();
      if(FALSE === $this->_initial_password) {
        $message = "Failed to generate random password.";
        $this->set_error($message,'system');
        return FALSE;
      }
      $url_host = parse_url($this->_web_url, PHP_URL_HOST);
      $args = array(
        '-q',
        '--urlhost',
        $url_host,
        '--emailhost',
        $domain,
        $this->get_list_name(),
        $this->get_list_owner_email(),
        $this->_initial_password
      );
      if(0 == red_fork_exec_wait($cmd, $args)) {
        return TRUE;
      }

      $message = "Failed to create the list.";
      $this->set_error($message,'system');
      return false;
    }

    function delete_list() {
      if(!$this->delete_domain_if_necessary()) return false;
      $cmd = $this->_mailman_bin_dir . "/rmlist";
      $args = array('-a', $this->get_list_name());
      if(0 == red_fork_exec_wait($cmd, $args)) {
        return TRUE;
      }
      $message = "Failed to delete the list.";
      $this->set_error($message,'system');
      return false;

    }

    function configure_domain_if_necessary() {
      // if this is the first time this domain name has been
      // used, make sure postfix is configured to use it
      $domain = $this->get_list_domain();
      if(!red_key_exists_in_file($domain,"\t",$this->_postfix_transport_file)) {
        $map_line = "$domain\tmailman:\n";
        if(!red_append_to_file($this->_postfix_transport_file,$map_line)) {
          $message = "Failed to create transport account for the domain.";
          $this->set_error($message,'system');
          return false;
        }

        if(!$this->postmap_file($this->_postfix_transport_file)) return false;
      }
      return true;
    }

    function postmap_file($file) {
      $cmd = $this->_postmap_cmd;
      $args = array($file);
      if(0 == red_fork_exec_wait($cmd,$args)) {
        return TRUE;
      }
      $message = "Failed to run postmap.";
      $this->set_error($message,'system');
      return false;
    }

    function delete_domain_if_necessary() {
      // if the domain name is not used by any other lists,
      // remove it from the postfix conf files.
      $domain = $this->get_list_domain();
      if(red_key_exists_in_file($domain,"\t",$this->_postfix_transport_file)) {
        $item_id = $this->get_item_id();
        $sql = "SELECT item_id FROM red_item_list JOIN red_item USING(item_id) ".
          "WHERE list_domain = @domain AND red_item.item_id != #item_id ".
          "AND (item_status = 'active' OR item_status LIKE 'pending-%')";
        $result = red_sql_query($sql, ['@domain' => $domain, '#item_id' => $item_id]);
        if(red_sql_num_rows($result) == 0) {
          // remove
          $file = $this->_postfix_transport_file;
          if(!red_delete_from_file($file,$domain,"\t")) {
            $message = "Failed to delete transport account for the domain.";
            $this->set_error($message,'system');
            return false;
          }
          if(!$this->postmap_file($file)) return false;
        }
      }
      return true;
    }

    function notify_owner() {
      $template = $this->_owner_email_template['default'];
      $content = file_get_contents($template);
      $list_name = $this->get_list_name();
      $find = array(
        '{list_name}',
        '{initial_password}',
        '{web_url}',
        '{list_owner}',
        '{list_address}'
      );
      $list_owner = $this->get_list_owner_email();
      $domain = $this->get_list_domain();
      $list_address = "$list_name@$domain";
      $replace = array(
        $list_name,
        $this->_initial_password,
        $this->_web_url . 'admin/' . $list_name,
        $list_owner,
        $list_address,
      );
      $content = str_replace($find,$replace,$content);
      $subject = 'New email list instructions';
      $from_email = $this->_from_email['default'];
      $headers = null;
      if($from_email) {
        $headers = "From: $from_email";
      }
      if(!red_mail($list_owner,$subject,$content,$headers)) {
        $message = "Failed to send email.";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }
  }
}


?>
