#!/bin/bash

PATH="/bin:/usr/bin:/usr/sbin:/sbin"

item_id="$1"
mountpoint="$2"
id="$3"
quota="$4"
inodes="$5"

if [[ ! "$item_id" =~ ^[0-9]+$ ]]; then
  printf "Illegal characters in or empty item_id: %s\n" "$item_id"
  exit 1
fi

if [[ ! "$mountpoint" =~ ^data[0-9]+$ ]]; then
  printf "Illegal characters in or empty mountpoint: %s\n" "$mountpoint"
  exit 1
fi
if [[ ! "$id" =~ ^[0-9]+$ ]]; then
  printf "Illegal characters in or empty id: %s\n" "$id"
  exit 1
fi

if [[ ! "$quota" =~ ^[0-9.]+$ ]]; then
  printf "Illegal characters in or empty quota: %s\n" "$quota"
  exit 1
fi

if [[ ! "$inodes" =~ ^[0-9.]+$ ]]; then
  printf "Illegal characters in or empty inodes: %s\n" "$inodes"
  exit 1
fi

name="site${item_id}writer"
home="/home/sites/${item_id}"

# First create the group.
if ! getent group "$name" > /dev/null; then
  groupadd \
    --gid "$id" \
    "$name" || exit 1
fi

if ! getent passwd "$name" > /dev/null; then
  useradd \
    --uid "$id" \
    --gid "$id" \
    --no-create-home \
    --home-dir "$home" \
    --shell /usr/bin/bash \
    "$name" || exit 1
fi

# Add to users so security limits are applied
if ! getent group users | grep "$name" > /dev/null; then
  adduser "$name" users || exit 1
fi

# Check for quota on production servers.
if [ -f "/media/${mountpoint}/aquota.user" ]; then 
  current_quota=$(quota -u "$name" | grep '/dev/sd' | awk '{print $4}')
  if [ -z "$current_quota" ] || [ "$current_quota" -ne "$quota" ]; then
    setquota -u "$name" 0 "$quota" 0 "$inodes" "/media/${mountpoint}" || exit 1 
  fi
fi

# The www-data users has to be in every user's group to ensure
# they have read access to the their web directory.
if ! groups www-data | grep "$name" >/dev/null; then
  adduser www-data "$name" || exit 1
fi

exit 0
