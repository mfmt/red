# Based on https://github.com/AsavarTzeth/docker-php5-fpm
FROM my-stretch:latest

MAINTAINER Jamie McClelland <jamie@progressivetech.org>

# Just install enough packages for red to work.
# (maildrop provides maildirmake which ensures Maildir creation works).
RUN apt-get update && \
    apt-get install -y \
    --no-install-recommends \
    php-cli \
    mysql-client \
    maildrop \
    php-mysql && \
    rm -rf /var/lib/apt/lists/*

# Create directories used by red
RUN mkdir -p /etc/postfix \
  /etc/apache2/sites-enabled \
  /etc/apache2/sites-available \
  /etc/php5/fpm/pool.d \
  /home/members \
  /var/lib/knot \
  /etc/knot/knot.conf.d/knot.zones.conf.d \
  /etc/php/7.0/fpm/pool.d/

# Create fake binaries that will be "reloaded"
RUN ln -s /bin/true /usr/sbin/apache2
RUN ln -s /bin/true /usr/bin/certbot 
RUN ln -s /bin/true /usr/local/sbin/mf-knot-zone-transfer
RUN ln -s /bin/true /usr/local/sbin/mf-certbot
RUN ln -s /bin/true /usr/sbin/postmap
RUN  ln -s /bin/true /bin/systemctl
RUN touch /etc/apache2/envvars
RUN touch /etc/postfix/virtual_alias_maps.red
RUN touch /etc/postfix/virtual_alias_domains.red

# Help us out a bit..
RUN ln -s /usr/local/share/red/node/sbin/red-node-update /usr/local/sbin/red-node-update
# Ensure we are using animal as the DNS server
RUN echo nameserver 172.17.42.1 > /etc/resolv.conf

RUN touch /etc/red-dns-soa-exceptions.conf
RUN addgroup sshusers

COPY ./docker-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
