#!/bin/bash
set -e

target="/usr/local/etc/red"
defaults="/usr/local/share/red/node/etc/red"
mkdir -p "$target"
for foo in $(ls "$defaults"/*.sample); do
  basename=$(basename "$foo" .sample)
  cp "${foo}" "${target}/${basename}"
done

if [ -z "$1" ]; then
  # Run red-node-update repeatedly forever so we don't have to notify, which is
  # tricky in this docker setup.
  # If you want to run it manually, turn off by touching /red-node-update-off
  while [ 1 ]; do 
    if [ ! -f /red-node-update-off ]; then
      red-node-update
    fi
    sleep 1 
  done
else
  exec "$@"
fi
