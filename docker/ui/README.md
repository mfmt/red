# UI Docker container

This docker container is designed to run the UI interface for red.

It's only the PHP part - you should run the web part from your host.

# Build the image 

`docker build -t red-ui .`

# Create container

Note: These values are set below, change as needed:

 * path to red: /home/jamie/projects/mfpl/red
 * local port is 8807
 * DB container: dbdev
 * SMTP container: smtp

    docker create --name "red-ui" \
          -v "/home/jamie/projects/mfpl/red":/var/www/html \
          -p 127.0.0.1:8807:9000 \
          -e "DOCKER_UID=$UID" \
          --link "dbdev:dbdev" \
          --link "smtp:smtp" \
          red-ui:latest "php-fpm7.3" 

