#!/bin/bash
set -e

# If the command passed to docker run/exec starts with a - or -- then
# set the command to php-fpm7.3 and pass whatever follows as an argument to
# php-fpm7.3.
if [ "${1:0:1}" = '-' ]; then
	set -- "php-fpm7.3" "$@"
fi

if [ "$1" = 'php-fpm7.3' ]; then
  set -- "$@" --nodaemonize
fi

# Check for a passed in DOCKER_UID environment variable. If it's there
# then ensure that the www-data user is set to this UID. That way we can
# easily edit files from the host.
if [ -n "$DOCKER_UID" ]; then
  printf "Updating UID...\n"
  # First see if it's already set.
  current_uid=$(getent passwd www-data | cut -d: -f3)
  if [ "$current_uid" -eq "$DOCKER_UID" ]; then
    printf "UIDs already match.\n"
  else
    printf "Updating UID from %s to %s.\n" "$current_uid" "$DOCKER_UID"
    usermod -u "$DOCKER_UID" www-data && chown -R "$DOCKER_UID" /var/www
    mkdir /run/php
    chown -R "$DOCKER_UID" /run/php
  fi
fi
exec "$@"
