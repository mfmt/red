# Docker based development environment

These files help you build a docker based development environment for red.

There are three main pieces:

 * Web server: this should be run on your host machine
 * Red UI: one container runs PHP and servers the PHP user interface web site.
 * Red Node: a second container acts like a MOSH. It's not a real MOSH. In fact
   it simply runs /bin/true for many of the commands that should set things up
   on a node, so it is not a perfect test for ensuring that everything is properly
   working, however, it covers most functionality.

   Additionally, the node container runs red-node-update once a second so all
   configuration changes in the UI get propagated right away.

# Setup

There are a number of pieces that should be enabled on your development machine for this to work:

 * Pick a domain name. In all examples and sample configuration files, the domain name: red.loc.cx 
   is used. I own that domain name so you will never have to worry about it conflicting with a real
   domain name. Probably a good idea to use it.
 * Pick an IP address. I have picked 10.11.13.1. Easiest if you use that one too.
 * Create an entry in /etc/hosts (or you local DNS server) that points red.loc.cx -> 10.11.13.1
 * Generate a x509 cert and key for red.loc.cx
 * Install and run nginx on your host machine. Copy red.nginx.conf to /etc/nginx/sites-enabled
   and edit to match your setup.
 * Install docker.

# Containers

Now, you can follow the README files in red-ui and red-node to build and launch the containers.

# Testing

You can run tests by:

 * Run the ./recreate script on the host to get a fresh node.
 * Entering the red-ui container
 * Go to ui/sbin/
 * Run ./reset-data to reset the data
 * Run: ./red-test

