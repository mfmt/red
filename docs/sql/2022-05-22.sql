CREATE TABLE `red_item_disk_usage_snapshot` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `item_disk_usage` bigint(20) unsigned NOT NULL DEFAULT 0,
  `item_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (item_id),
  INDEX item_disk_usage_index(item_disk_usage)
);

CREATE INDEX item_disk_usage_index ON red_item (item_disk_usage);
