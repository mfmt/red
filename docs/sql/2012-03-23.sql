ALTER TABLE red_member ADD COLUMN member_currency varchar(8) DEFAULT 'USD';
ALTER TABLE red_invoice ADD COLUMN invoice_currency varchar(8) DEFAULT 'USD';
