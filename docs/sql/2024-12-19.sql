DROP TABLE red_invoice;
DELETE FROM red_error_log WHERE item_id NOT IN (SELECT item_id FROM red_item);
DELETE FROM red_item WHERE item_status = 'deleted' AND hosting_order_id = 0;
ALTER TABLE red_item
ADD CONSTRAINT fk_red_item_hosting_order
FOREIGN KEY (hosting_order_id)
REFERENCES red_hosting_order(hosting_order_id)
ON DELETE CASCADE;
ALTER TABLE red_error_log
ADD CONSTRAINT fk_red_item
FOREIGN KEY (item_id)
REFERENCES red_item(item_id)
ON DELETE CASCADE;
