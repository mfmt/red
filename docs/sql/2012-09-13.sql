CREATE TABLE `red_tag` (
  `tag_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `tag` varchar(64) NOT NULL,
  `tag_status` enum('active', 'deleted'),
  `tag_modified` timestamp DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`tag_id`),
  KEY `member_id` (`member_id`)
);

INSERT INTO red_service SET service_table = 'red_tag', service_area = 'member', service_name = 'Tag', service_description = 'Tag', service_delete_order = 240, service_order_by = 'tag DESC', service_status = 'active';

