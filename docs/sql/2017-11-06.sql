CREATE TABLE `red_email_verify` (
  `email_verify_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT '0' COMMENT 'The item id of the corresponding red_user_account record',
  `email_verify_address` varchar(128) NOT NULL DEFAULT '',
  `email_verify_hash` varchar(32) DEFAULT NULL,
  `email_verify_expire` datetime DEFAULT NULL,
  `email_verify_status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`email_verify_id`),
  UNIQUE KEY `id_and_email` (`item_id`, `email_verify_address`),
  KEY `email_verify_hash` (`email_verify_hash`),
  CONSTRAINT `red_item_user_account_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
