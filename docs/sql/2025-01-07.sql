UPDATE red_service SET service_name = 'web_conf' WHERE service_name = 'web_configuration';
UPDATE red_service SET service_name = 'web_app' WHERE service_name = 'web_application';
UPDATE red_service SET service_name = 'map_user_member' WHERE service_name = 'member_access';
UPDATE red_service SET service_name = 'contact' WHERE service_name = 'contacts';

ALTER TABLE red_service ADD COLUMN service_basic_access enum('full','read-only','single', 'none') DEFAULT 'none';
ALTER TABLE red_service ADD COLUMN service_standard_access enum('full','read-only','single','none') DEFAULT 'full';
UPDATE red_service SET service_basic_access = 'full' WHERE service_name = 'contact';
UPDATE red_service SET service_basic_access = 'single' WHERE service_name IN ('user_account', 'mailbox', 'email_address', 'nextcloud', 'hosting_order');
UPDATE red_service SET service_standard_access = 'single' WHERE service_name IN('web_conf', 'web_app');
UPDATE red_service SET service_standard_access = 'read-only' WHERE service_name = 'vps';
UPDATE red_service SET service_standard_access = 'none' WHERE service_name IN ('video_meeting', 'chat');
