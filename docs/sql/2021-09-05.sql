ALTER TABLE red_service ADD COLUMN service_display_order smallint(6);
UPDATE red_service SET service_display_order = service_id;
-- Readjust order so nextcloud is closer to the top.
UPDATE red_service SET service_display_order = 0 WHERE service_id = 1;
UPDATE red_service SET service_display_order = 1 WHERE service_id = 2;
UPDATE red_service SET service_display_order = 2 WHERE service_id = 37;

-- Move infrequently used items to the bottom - high volume email list
-- and hosting order access
UPDATE red_service SET service_display_order = 100 WHERE service_id = 11;
UPDATE red_service SET service_display_order = 100 WHERE service_id = 10;
