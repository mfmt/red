ALTER TABLE red_item_web_conf ADD COLUMN web_conf_writer_uid mediumint(8) unsigned DEFAULT NULL;
ALTER TABLE red_item_web_conf ADD COLUMN web_conf_reader_uid mediumint(8) unsigned DEFAULT NULL;
CREATE TABLE `red_uid_seq` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT "Source of unique UIDs for all users created.",
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB;
ALTER TABLE red_item_user_account CHANGE COLUMN `user_account_uid` `user_account_uid` mediumint(8) unsigned NOT NULL;
SET @user_account_auto_increment = (SELECT MAX(user_account_uid) + 1 FROM red_item_user_account);
SET @sql = CONCAT("ALTER TABLE red_uid_seq AUTO_INCREMENT = ", @user_account_auto_increment);
PREPARE st FROM @sql;
EXECUTE st;

