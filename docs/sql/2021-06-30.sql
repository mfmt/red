ALTER TABLE red_item ADD COLUMN item_disk_usage bigint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item ADD COLUMN item_quota bigint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item ADD COLUMN item_quota_notify_percent tinyint unsigned NOT NULL DEFAULT 85;
ALTER TABLE red_item ADD COLUMN item_quota_notify_date datetime DEFAULT NULL;

