CREATE TABLE `red_password_cache` (
  `item_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `added` (`added`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

