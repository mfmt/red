-- MariaDB dump 10.19  Distrib 10.11.3-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: red
-- ------------------------------------------------------
-- Server version	10.11.3-MariaDB-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `red_contact`
--

DROP TABLE IF EXISTS `red_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_contact` (
  `contact_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `contact_first_name` varchar(64) NOT NULL DEFAULT '',
  `contact_last_name` varchar(64) NOT NULL DEFAULT '',
  `contact_email` varchar(64) NOT NULL DEFAULT '',
  `contact_status` enum('active','deleted') DEFAULT 'active',
  `contact_lang` varchar(5) DEFAULT 'en_US',
  PRIMARY KEY (`contact_id`),
  KEY `contact_status` (`contact_status`),
  KEY `contact_email` (`contact_email`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_contact_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_correspondence`
--

DROP TABLE IF EXISTS `red_correspondence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_correspondence` (
  `correspondence_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned DEFAULT 0,
  `correspondence_from` varchar(128) DEFAULT NULL,
  `correspondence_to` varchar(128) DEFAULT NULL,
  `correspondence_subject` varchar(128) DEFAULT NULL,
  `correspondence_date` datetime DEFAULT NULL,
  `correspondence_body` text DEFAULT NULL,
  PRIMARY KEY (`correspondence_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_correspondence_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci COMMENT='Email correspondence';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_email_verify`
--

DROP TABLE IF EXISTS `red_email_verify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_email_verify` (
  `email_verify_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0 COMMENT 'The item id of the corresponding red_user_account record',
  `email_verify_address` varchar(128) NOT NULL DEFAULT '',
  `email_verify_hash` varchar(32) DEFAULT NULL,
  `email_verify_expire` datetime DEFAULT NULL,
  `email_verify_status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`email_verify_id`),
  UNIQUE KEY `id_and_email` (`item_id`,`email_verify_address`),
  KEY `email_verify_hash` (`email_verify_hash`),
  CONSTRAINT `red_item_user_account_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_error_log`
--

DROP TABLE IF EXISTS `red_error_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_error_log` (
  `error_log_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(11) unsigned DEFAULT NULL,
  `error_log_severity` enum('hard','soft') DEFAULT NULL,
  `error_log_type` enum('system','validation') DEFAULT NULL,
  `error_log_message` text NOT NULL,
  `error_log_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`error_log_id`),
  KEY `error_item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_file_sync`
--

DROP TABLE IF EXISTS `red_file_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_file_sync` (
  `file_sync_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_sync_key` varchar(128) DEFAULT NULL,
  `file_sync_hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_sync_id`),
  UNIQUE KEY `file_sync_key` (`file_sync_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_hosting_order`
--

DROP TABLE IF EXISTS `red_hosting_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_hosting_order` (
  `hosting_order_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `unique_unix_group_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `hosting_order_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'E.g. 1 yr hosting for domain.org',
  `hosting_order_status` enum('active','deleted','disabled') DEFAULT 'active',
  `hosting_order_host` varchar(128) DEFAULT NULL,
  `hosting_order_identifier` varchar(255) NOT NULL DEFAULT '',
  `hosting_order_notes` text NOT NULL,
  `hosting_order_disabled_by_admin` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`hosting_order_id`),
  KEY `member_id` (`member_id`),
  KEY `hosting_order_host` (`hosting_order_host`),
  KEY `unique_unix_group_id` (`unique_unix_group_id`),
  CONSTRAINT `red_hosting_order_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_invoice`
--

DROP TABLE IF EXISTS `red_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_invoice` (
  `invoice_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned DEFAULT NULL,
  `account_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `invoice_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `invoice_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `invoice_status` enum('paid','unpaid','eaten','void','review') DEFAULT 'unpaid',
  `invoice_description` text NOT NULL,
  `invoice_private_notes` text NOT NULL,
  `invoice_currency` varchar(8) DEFAULT 'USD',
  `invoice_date_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `invoice_type` enum('benefits','membership') DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `account_id` (`account_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_invoice_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci COMMENT='Record of earning the money';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item`
--

DROP TABLE IF EXISTS `red_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item` (
  `item_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `hosting_order_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `member_id` mediumint(11) unsigned DEFAULT 0,
  `service_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `item_host` varchar(255) NOT NULL DEFAULT '',
  `item_status` enum('active','pending-delete','pending-update','pending-insert','pending-restore','deleted','hard-error','soft-error','transfer-limbo','pending-disable','disabled') DEFAULT NULL,
  `item_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `item_disk_usage` bigint(20) unsigned NOT NULL DEFAULT 0,
  `item_quota` bigint(20) unsigned NOT NULL DEFAULT 0,
  `item_quota_notify_warning_percent` tinyint(3) unsigned NOT NULL DEFAULT 85,
  `item_quota_notify_warning_date` datetime DEFAULT NULL,
  `item_quota_notify_exceeded_date` datetime DEFAULT NULL,
  `item_disabled_by_hosting_order` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`item_id`),
  KEY `service_id` (`service_id`),
  KEY `hosting_order_id` (`hosting_order_id`),
  KEY `item_status` (`item_status`),
  KEY `member_id` (`member_id`),
  KEY `item_host` (`item_host`),
  KEY `item_disk_usage_index` (`item_disk_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci COMMENT='Each config item, such as email account, etc.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_cron`
--

DROP TABLE IF EXISTS `red_item_cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_cron` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `cron_login` varchar(128) NOT NULL DEFAULT '',
  `cron_schedule` varchar(128) NOT NULL DEFAULT '',
  `cron_cmd` varchar(255) NOT NULL DEFAULT '',
  `cron_working_directory` varchar(128) NOT NULL DEFAULT '',
  `cron_environment` text NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_cron_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_disk_usage_snapshot`
--

DROP TABLE IF EXISTS `red_item_disk_usage_snapshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_disk_usage_snapshot` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `item_disk_usage` bigint(20) unsigned NOT NULL DEFAULT 0,
  `item_modified` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`item_id`),
  KEY `item_disk_usage_index` (`item_disk_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_dns`
--

DROP TABLE IF EXISTS `red_item_dns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_dns` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `dns_zone` varchar(255) DEFAULT NULL,
  `dns_type` enum('mx','a','txt','cname','srv','aaaa','ptr','mailstore','sshfp','dkim','alias','caa') DEFAULT NULL,
  `dns_fqdn` varchar(255) DEFAULT NULL,
  `dns_ip` varchar(79) DEFAULT NULL,
  `dns_ttl` mediumint(9) DEFAULT NULL,
  `dns_server_name` varchar(255) DEFAULT NULL,
  `dns_text` text DEFAULT NULL,
  `dns_dist` tinyint(3) unsigned DEFAULT NULL,
  `dns_port` smallint(5) unsigned DEFAULT 0,
  `dns_weight` smallint(5) unsigned DEFAULT 0,
  `dns_sshfp_algorithm` tinyint(4) NOT NULL DEFAULT 0,
  `dns_sshfp_type` tinyint(4) NOT NULL DEFAULT 0,
  `dns_sshfp_fpr` text DEFAULT NULL,
  `dns_caa_flags` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `dns_caa_tag` text DEFAULT NULL,
  `dns_caa_value` text DEFAULT NULL,
  `dns_dkim_selector` varchar(32) DEFAULT NULL,
  `dns_dkim_signing_domain` varchar(255) DEFAULT NULL,
  `dns_mx_verified` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`item_id`),
  KEY `dns_type` (`dns_type`),
  KEY `dns_fqdn` (`dns_fqdn`),
  KEY `dns_zone` (`dns_zone`),
  CONSTRAINT `red_item_dns_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_email_address`
--

DROP TABLE IF EXISTS `red_item_email_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_email_address` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `email_address` varchar(255) NOT NULL DEFAULT '',
  `email_address_recipient` varchar(768) NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`),
  KEY `email_address_recipient` (`email_address_recipient`),
  KEY `email_address` (`email_address`),
  CONSTRAINT `red_item_email_address_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_hosting_order_access`
--

DROP TABLE IF EXISTS `red_item_hosting_order_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_hosting_order_access` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `hosting_order_access_login` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `hosting_order_login` (`hosting_order_access_login`),
  CONSTRAINT `red_item_hosting_order_access_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_list`
--

DROP TABLE IF EXISTS `red_item_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_list` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `list_name` varchar(64) DEFAULT NULL,
  `list_owner_email` varchar(128) DEFAULT NULL,
  `list_domain` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_list_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_mailbox`
--

DROP TABLE IF EXISTS `red_item_mailbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_mailbox` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `mailbox_login` varchar(128) DEFAULT NULL,
  `mailbox_auto_response` text DEFAULT NULL,
  `mailbox_auto_response_reply_from` varchar(128) DEFAULT NULL,
  `mailbox_auto_response_action` varchar(24) DEFAULT NULL,
  `mailbox_abandoned` tinyint(1) DEFAULT 0,
  `mailbox_mountpoint` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `mailbox_abandoned` (`mailbox_abandoned`),
  KEY `mailbox_login` (`mailbox_login`),
  CONSTRAINT `red_item_mailbox_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_mysql_db`
--

DROP TABLE IF EXISTS `red_item_mysql_db`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_mysql_db` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `mysql_db_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_mysql_db_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_mysql_user`
--

DROP TABLE IF EXISTS `red_item_mysql_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_mysql_user` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `mysql_user_name` varchar(16) DEFAULT NULL,
  `mysql_user_password` varchar(41) DEFAULT NULL,
  `mysql_user_db` text DEFAULT NULL,
  `mysql_user_priv` enum('full','read') DEFAULT NULL,
  `mysql_user_max_connections` smallint(6) DEFAULT 25,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_mysql_user_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_nextcloud`
--

DROP TABLE IF EXISTS `red_item_nextcloud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_nextcloud` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `nextcloud_login` varchar(128) DEFAULT NULL,
  `nextcloud_abandoned` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`item_id`),
  KEY `nextcloud_abandoned` (`nextcloud_abandoned`),
  KEY `nextcloud_login` (`nextcloud_login`),
  CONSTRAINT `red_item_nextcloud_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_pass_reset`
--

DROP TABLE IF EXISTS `red_item_pass_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_pass_reset` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `pass_reset_login` varchar(255) DEFAULT NULL,
  `pass_reset_hash` varchar(32) DEFAULT NULL,
  `pass_reset_expires` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pass_reset` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_pass_reset_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_psql`
--

DROP TABLE IF EXISTS `red_item_psql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_psql` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `psql_name` varchar(16) DEFAULT NULL,
  `psql_password` varchar(256) DEFAULT NULL,
  `psql_max_connections` smallint(6) DEFAULT 25,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_psql_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_server_access`
--

DROP TABLE IF EXISTS `red_item_server_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_server_access` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `server_access_login` varchar(128) DEFAULT NULL,
  `server_access_public_key` text NOT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_server_access_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_user_account`
--

DROP TABLE IF EXISTS `red_item_user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_user_account` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `user_account_firstname` varchar(128) DEFAULT NULL,
  `user_account_lastname` varchar(128) DEFAULT NULL,
  `user_account_login` varchar(255) DEFAULT NULL,
  `user_account_password` varchar(255) DEFAULT NULL,
  `user_account_uid` mediumint(8) unsigned NOT NULL,
  `user_account_gpg_ids` text DEFAULT NULL,
  `user_account_gpg_public_key` text DEFAULT NULL,
  `user_account_mountpoint` varchar(8) DEFAULT NULL,
  `user_account_recovery_email` varchar(254) NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `user_account_uid` (`user_account_uid`),
  KEY `user_account_login` (`user_account_login`),
  CONSTRAINT `red_item_user_account_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_web_app`
--

DROP TABLE IF EXISTS `red_item_web_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_web_app` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_app_name` varchar(128) DEFAULT NULL,
  `web_app_update` varchar(32) DEFAULT 'none',
  `web_app_install_ip` varchar(48) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_web_app_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_web_conf`
--

DROP TABLE IF EXISTS `red_item_web_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_web_conf` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `web_conf_login` varchar(128) DEFAULT NULL,
  `web_conf_execute_as_user` varchar(128) DEFAULT NULL,
  `web_conf_settings` text DEFAULT NULL,
  `web_conf_max_processes` smallint(6) DEFAULT 12,
  `web_conf_cgi` tinyint(4) DEFAULT 0,
  `web_conf_domain_names` text DEFAULT NULL,
  `web_conf_logging` tinyint(1) DEFAULT 2,
  `web_conf_document_root` varchar(255) DEFAULT NULL,
  `web_conf_tls` tinyint(1) DEFAULT 1,
  `web_conf_tls_redirect` tinyint(1) DEFAULT 1,
  `web_conf_tls_cert` varchar(255) DEFAULT NULL,
  `web_conf_tls_key` varchar(255) DEFAULT NULL,
  `web_conf_cache_type` enum('none','performance','custom') DEFAULT 'none',
  `web_conf_cache_settings` text DEFAULT NULL,
  `web_conf_max_memory` mediumint(11) DEFAULT 256,
  `web_conf_php_version` decimal(3,1) DEFAULT 7.0,
  `web_conf_mountpoint` varchar(8) DEFAULT NULL,
  `web_conf_writer_uid` mediumint(8) unsigned DEFAULT NULL,
  `web_conf_reader_uid` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_web_conf_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_item_xmpp`
--

DROP TABLE IF EXISTS `red_item_xmpp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_item_xmpp` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `xmpp_login` varchar(128) DEFAULT NULL,
  `xmpp_domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_xmpp_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_login_session`
--

DROP TABLE IF EXISTS `red_login_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_login_session` (
  `login_session_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_hash` varchar(32) DEFAULT NULL,
  `stamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`login_session_id`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_map_user_member`
--

DROP TABLE IF EXISTS `red_map_user_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_map_user_member` (
  `map_user_member_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`map_user_member_id`),
  KEY `fk_login` (`login`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_map_user_member_ibfk_1` FOREIGN KEY (`login`) REFERENCES `red_item_user_account` (`user_account_login`) ON DELETE CASCADE,
  CONSTRAINT `red_map_user_member_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_map_user_server`
--

DROP TABLE IF EXISTS `red_map_user_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_map_user_server` (
  `map_user_server_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `server` varchar(128) NOT NULL DEFAULT '0',
  `status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`map_user_server_id`),
  KEY `fk_login` (`login`),
  KEY `fk_server` (`server`),
  CONSTRAINT `red_map_user_server_ibfk_1` FOREIGN KEY (`login`) REFERENCES `red_item_user_account` (`user_account_login`) ON DELETE CASCADE,
  CONSTRAINT `red_map_user_server_ibfk_2` FOREIGN KEY (`server`) REFERENCES `red_server` (`server`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_member`
--

DROP TABLE IF EXISTS `red_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_member` (
  `member_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `unique_unix_group_id` mediumint(8) unsigned DEFAULT NULL,
  `member_friendly_name` varchar(128) NOT NULL DEFAULT '',
  `member_parent_id` mediumint(11) unsigned DEFAULT NULL,
  `member_status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `member_type` enum('individual','organization') NOT NULL DEFAULT 'organization',
  `member_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `member_end_date` datetime DEFAULT NULL,
  `member_benefits_level` enum('basic','standard','extra') DEFAULT NULL,
  `member_quota` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci COMMENT='Additional info about members is stored in other tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_member_dns_status`
--

DROP TABLE IF EXISTS `red_member_dns_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_member_dns_status` (
  `member_dns_status_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `member_dns_status` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_dns_status_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_member_dns_status_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_mx_verify`
--

DROP TABLE IF EXISTS `red_mx_verify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_mx_verify` (
  `mx_verify_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0 COMMENT 'The item id of the corresponding red_item_dns MX record',
  `mx_verify_first_sent` datetime DEFAULT NULL,
  `mx_verify_last_sent` datetime DEFAULT NULL,
  `mx_verify_key` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`mx_verify_id`),
  KEY `mx_verify_key` (`mx_verify_key`),
  KEY `mx_verify_last_sent` (`mx_verify_last_sent`),
  KEY `red_item_dns_ibfk_2` (`item_id`),
  CONSTRAINT `red_item_dns_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_online_payment`
--

DROP TABLE IF EXISTS `red_online_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_online_payment` (
  `online_payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `online_payment_identifier` varchar(128) DEFAULT NULL,
  `invoice_id` mediumint(11) unsigned DEFAULT NULL,
  `bank_id` mediumint(11) unsigned DEFAULT NULL,
  `online_payment_amount` decimal(8,2) DEFAULT NULL,
  `online_payment_date` datetime DEFAULT NULL,
  `online_payment_email` varchar(64) DEFAULT NULL,
  `online_payment_notes` text DEFAULT NULL,
  `member_id` mediumint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`online_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='Chart of accounts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_password_cache`
--

DROP TABLE IF EXISTS `red_password_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_password_cache` (
  `item_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `added` timestamp NOT NULL DEFAULT current_timestamp(),
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `added` (`added`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_payment`
--

DROP TABLE IF EXISTS `red_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_payment` (
  `payment_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `bank_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `payment_date` date DEFAULT NULL,
  `payment_method` enum('credit','check','cash','other','transfer') DEFAULT 'credit',
  `payment_identifier` varchar(128) NOT NULL DEFAULT '' COMMENT 'If check, check number, if credit, last 4 digits',
  `payment_amount` decimal(8,2) NOT NULL DEFAULT 0.00,
  `payment_notes` varchar(255) NOT NULL DEFAULT '',
  `payment_status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`payment_id`),
  KEY `invoice_id` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci COMMENT='Record of receiving the money';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_postal_address`
--

DROP TABLE IF EXISTS `red_postal_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_postal_address` (
  `postal_address_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `postal_address_street_number` varchar(16) NOT NULL,
  `postal_address_street_name` varchar(255) NOT NULL,
  `postal_address_street_extra` varchar(255) NOT NULL,
  `postal_address_city` varchar(255) NOT NULL,
  `postal_address_province` varchar(255) NOT NULL,
  `postal_address_country` char(2) NOT NULL,
  `postal_address_code` varchar(32) NOT NULL,
  `postal_address_status` enum('active','deleted') DEFAULT NULL,
  `postal_address_is_primary` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`postal_address_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `red_postal_address_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_queue`
--

DROP TABLE IF EXISTS `red_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_queue` (
  `queue_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `queue_status` enum('running','pending','error','completed') DEFAULT 'pending',
  `queue_initial_item_status` enum('pending-insert','pending-update','pending-restore','pending-delete','pending-disable') DEFAULT NULL,
  `queue_message` text DEFAULT NULL,
  `queue_last_updated` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`queue_id`),
  KEY `item_id` (`item_id`),
  KEY `queue_status` (`queue_status`),
  CONSTRAINT `red_queue_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_queue_task`
--

DROP TABLE IF EXISTS `red_queue_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_queue_task` (
  `queue_task_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `queue_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `queue_task_status` enum('running','pending','error','completed') DEFAULT 'pending',
  `queue_task_function` varchar(255) DEFAULT NULL,
  `queue_task_callback` varchar(255) DEFAULT NULL,
  `queue_task_args` text NOT NULL DEFAULT '',
  `queue_task_order` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`queue_task_id`),
  KEY `queue_id` (`queue_id`),
  KEY `queue_task_status` (`queue_task_status`),
  CONSTRAINT `red_queue_id` FOREIGN KEY (`queue_id`) REFERENCES `red_queue` (`queue_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_server`
--

DROP TABLE IF EXISTS `red_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_server` (
  `server_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `accepting` tinyint(1) DEFAULT 0,
  `server` varchar(128) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT 0,
  `mountpoint` varchar(8) DEFAULT NULL,
  `server_default_php_version` varchar(8) DEFAULT '7.0',
  `server_proxysql` tinyint(4) DEFAULT 0,
  `server_dedicated_mail` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`server_id`),
  KEY `server` (`server`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_service`
--

DROP TABLE IF EXISTS `red_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_service` (
  `service_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `service_table` varchar(64) NOT NULL DEFAULT '',
  `service_area` varchar(32) DEFAULT NULL,
  `service_name` varchar(32) DEFAULT NULL,
  `service_label` varchar(64) NOT NULL,
  `service_description` text NOT NULL,
  `service_help_link` varchar(128) DEFAULT NULL,
  `service_default_host` varchar(255) NOT NULL DEFAULT '',
  `service_delete_order` smallint(6) NOT NULL DEFAULT 0,
  `service_order_by` varchar(64) DEFAULT NULL,
  `service_status` enum('active','inactive') DEFAULT 'active',
  `service_item` tinyint(3) unsigned DEFAULT 1,
  `service_display_order` smallint(6) DEFAULT NULL,
  `service_standard_access` enum('full','read-only','single','none') DEFAULT 'full',
  `service_basic_access` enum('full','read-only','single','none') DEFAULT 'none',
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci COMMENT='E.g. Apache config, Email alias, SIP Phone, etc.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_sitewide_admin`
--

DROP TABLE IF EXISTS `red_sitewide_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_sitewide_admin` (
  `sitewide_admin_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sitewide_admin_id`),
  KEY `user_name` (`user_name`),
  CONSTRAINT `red_sitewide_admin_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `red_item_user_account` (`user_account_login`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_stripe_cc_log`
--

DROP TABLE IF EXISTS `red_stripe_cc_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_stripe_cc_log` (
  `stripe_cc_log_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `stripe_cc_log_ip` varchar(32) DEFAULT NULL,
  `stripe_cc_log_fail_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`stripe_cc_log_id`),
  KEY `fail_date` (`stripe_cc_log_fail_date`),
  KEY `ip` (`stripe_cc_log_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_uid_seq`
--

DROP TABLE IF EXISTS `red_uid_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_uid_seq` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Source of unique UIDs for all users created.',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_unique_unix_group`
--

DROP TABLE IF EXISTS `red_unique_unix_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_unique_unix_group` (
  `unique_unix_group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `unique_unix_group_name` varchar(128) DEFAULT NULL,
  `unique_unix_group_status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY (`unique_unix_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_user`
--

DROP TABLE IF EXISTS `red_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_user` (
  `user_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `user_name` varchar(64) NOT NULL DEFAULT '',
  `user_pass` varchar(128) NOT NULL DEFAULT '',
  `user_admin` enum('y','n') DEFAULT 'n',
  `user_hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `red_vps`
--

DROP TABLE IF EXISTS `red_vps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red_vps` (
  `vps_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `vps_status` enum('active','deleted') DEFAULT 'active',
  `vps_server` varchar(128) DEFAULT NULL,
  `vps_cpu` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `vps_ram` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `vps_hd` bigint(20) unsigned NOT NULL DEFAULT 0,
  `vps_ssd` bigint(20) unsigned NOT NULL DEFAULT 0,
  `vps_type` varchar(32) DEFAULT NULL,
  `vps_location` varchar(32) DEFAULT NULL,
  `vps_managed` char(1) DEFAULT 'n',
  PRIMARY KEY (`vps_id`),
  KEY `vps_server` (`vps_server`),
  KEY `vps_status` (`vps_status`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_vps_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-01-11 13:49:20
