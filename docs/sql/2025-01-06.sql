ALTER TABLE red_service CHANGE COLUMN service_name service_label varchar(64) NOT NULL;
ALTER TABLE red_service ADD COLUMN service_name VARCHAR(32) AFTER service_area;
UPDATE red_service SET service_name = REPLACE(LOWER(service_label), ' ', '_');
