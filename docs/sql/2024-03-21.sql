ALTER TABLE red_item_dns CHANGE COLUMN `dns_type` `dns_type` enum('mx','a','txt','cname','srv','aaaa','ptr','mailstore','sshfp','dkim','alias','caa') DEFAULT NULL;
ALTER TABLE red_item_dns ADD COLUMN dns_caa_flags tinyint UNSIGNED DEFAULT 0 NOT NULL;
ALTER TABLE red_item_dns ADD COLUMN dns_caa_tag text;
ALTER TABLE red_item_dns ADD COLUMN dns_caa_value text;
