-- enable proper sorting by invoice type.
ALTER TABLE red_invoice CHANGE COLUMN invoice_type invoice_type enum('benefits', 'membership') NULL DEFAULT NULL;
