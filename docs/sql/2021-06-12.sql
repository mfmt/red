ALTER TABLE red_member ADD COLUMN member_quota mediumint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item_user_account ADD COLUMN user_account_disk_usage bigint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item_user_account ADD COLUMN user_account_quota mediumint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item_web_conf ADD COLUMN web_conf_quota mediumint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item_web_conf ADD COLUMN web_conf_disk_usage bigint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item_mysql_db ADD COLUMN mysql_db_quota mediumint unsigned NOT NULL DEFAULT 0;
ALTER TABLE red_item_mysql_db ADD COLUMN mysql_db_disk_usage bigint unsigned NOT NULL DEFAULT 0;


