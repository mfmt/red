-- removing unique keys to enable deleted items to remain 

ALTER TABLE red_item_email_address DROP KEY email_address;
ALTER TABLE red_item_user_account DROP KEY user_account_login;

