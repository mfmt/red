DROP FUNCTION IF EXISTS `get_salt`;
DROP FUNCTION IF EXISTS `valid_hash`;
DELIMITER |
CREATE DEFINER=`root`@`localhost`
FUNCTION `get_salt`(name VARCHAR(255)) 
  RETURNS varchar(255) 
    CHARSET utf8
    BEGIN
      DECLARE algosalt VARCHAR(255);
      SELECT REVERSE( SUBSTRING( REVERSE( user_account_password ), LOCATE( '$', REVERSE( user_account_password ) ) +1 ) ) INTO algosalt
      FROM red.red_item_user_account
      JOIN red.red_item USING (item_id)
      WHERE user_account_login = name AND item_status = 'active';
      RETURN algosalt;
    END|

CREATE DEFINER=`root`@`localhost`
FUNCTION `valid_hash`(salthash VARCHAR(255), name VARCHAR(255))
  RETURNS int(11)
    BEGIN  
      DECLARE hash VARCHAR(255);
      SELECT user_account_password INTO hash FROM red.red_item_user_account
      JOIN red_item USING(item_id)
      WHERE user_account_login = name
        AND item_status = 'active'
        AND user_account_password = salthash;
     RETURN IF(hash IS NOT NULL, 1, 0);
    END|

