ALTER TABLE red_item_dns ADD COLUMN dns_mx_verified tinyint(4) DEFAULT 0;
CREATE TABLE `red_mx_verify` (
  `mx_verify_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0 COMMENT 'The item id of the corresponding red_item_dns MX record',
  `mx_verify_first_sent` datetime DEFAULT NULL,
  `mx_verify_last_sent` datetime DEFAULT NULL,
  `mx_verify_key` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`mx_verify_id`),
  KEY `mx_verify_key` (`mx_verify_key`),
  KEY `mx_verify_last_sent` (`mx_verify_last_sent`),
  CONSTRAINT `red_item_dns_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

