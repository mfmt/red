CREATE TABLE `red_postal_address` (
  `postal_address_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `postal_address_street_number` varchar(16) NOT NULL,
  `postal_address_street_name` varchar(255) NOT NULL,
  `postal_address_street_extra` varchar(255) NOT NULL,
  `postal_address_city` varchar(255) NOT NULL,
  `postal_address_province` varchar(255) NOT NULL,
  `postal_address_country` char(2) NOT NULL,
  `postal_address_code` varchar(32) NOT NULL,
  `postal_address_status` enum('active', 'deleted'),
  `postal_address_is_primary` enum('y','n') DEFAULT 'y',
  PRIMARY KEY (`postal_address_id`),
  KEY `member_id` (`member_id`)
);

INSERT INTO red_service SET service_table = 'red_postal_address', service_area = 'member', service_name = 'Postal Address', service_description = 'Member mailing address', service_delete_order = 240, service_order_by = 'postal_address_country DESC', service_status = 'active';

