ALTER TABLE red_item_user_account DROP COLUMN user_account_quota;
ALTER TABLE red_item_user_account DROP COLUMN user_account_disk_usage;
ALTER TABLE red_item_mysql_db DROP COLUMN mysql_db_quota;
ALTER TABLE red_item_mysql_db DROP COLUMN mysql_db_disk_usage;
ALTER TABLE red_item_web_conf DROP COLUMN web_conf_quota;
ALTER TABLE red_item_web_conf DROP COLUMN web_conf_disk_usage;
ALTER TABLE red_member CHANGE COLUMN member_quota member_quota bigint(20) unsigned NOT NULL DEFAULT 0;


