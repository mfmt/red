CREATE TABLE `red_note` (
  `note_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  `note_status` enum('active', 'deleted'),
  `note_modified` timestamp DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`note_id`),
  KEY `member_id` (`member_id`)
);

INSERT INTO red_service SET service_table = 'red_note', service_area = 'member', service_name = 'Notes', service_description = 'Notes', service_delete_order = 240, service_order_by = 'note_modified DESC', service_status = 'active';

UPDATE red_service SET service_name = 'Tags' WHERE service_name = 'Tag';

