ALTER TABLE red_item_nextcloud ADD COLUMN nextcloud_abandoned tinyint(1) DEFAULT 0;
CREATE INDEX nextcloud_abandoned ON red_item_nextcloud (nextcloud_abandoned);

