ALTER TABLE red_contact ADD COLUMN contact_lang VARCHAR(5) DEFAULT 'en_US';
ALTER TABLE red_invoice CHANGE COLUMN `invoice_status` `invoice_status` enum('paid','unpaid','eaten','void','review') DEFAULT 'unpaid';
