CREATE TABLE `red_item_xmpp` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `xmpp_login` varchar(128) DEFAULT NULL,
  `xmpp_domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_xmpp_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
);

UPDATE red_service SET service_display_order = 90 WHERE service_id = 39;
UPDATE red_service SET service_display_order = 80 WHERE service_id = 24;
UPDATE red_service SET service_display_order = 70 WHERE service_id = 23;
UPDATE red_service SET service_display_order = 60 WHERE service_id = 23;
UPDATE red_service SET service_display_order = 50 WHERE service_id = 21;
UPDATE red_service SET service_display_order = 40 WHERE service_id = 20;

INSERT INTO red_service SET service_table = 'red_item_xmpp', service_area = 'hosting_order', service_name = 'Video Meeting', service_description = 'Create your own web-based video conference at room <a href="https://meet.mayfirst.org/">meet.mayfirst.org</a> and invite people to join you', service_delete_order = 50, service_order_by = 'xmpp_login', service_status = 'active', service_item = 1, service_display_order = 20, service_id = 40, service_help_link = 'https://help.mayfirst.org/en/guide/how-to-use-Jitsi-Meet-video-conference-platform', service_default_host = 'jitsimeet002.mayfirst.org';
INSERT INTO red_service SET service_table = 'red_item_xmpp', service_area = 'hosting_order', service_name = 'Chat', service_description = 'Create an instant message account for one-on-one or group chats using the XMPP protocol, login via <a href="https://im.mayfirst.org/">im.mayfirst.org</a>', service_delete_order = 50, service_order_by = 'xmpp_login', service_status = 'active', service_item = 1, service_display_order = 30, service_id = 41, service_default_host = 'prosody001.mayfirst.org';
