<?php
if(!class_exists('red_item')) {
  class red_item extends red_ado {
    var $_key_field = 'item_id';
    var $_key_table = 'red_item';

    // Variables common to ui and node classes
    var $_src_path;
    var $_mode;   // either ui or node 
    var $_hosting_order_unix_group_id;
    var $_hosting_order_unix_group_name;
    var $_site_dir_template;
    var $_member_parent_id;

    // We maintain a public var so we can statically check if a class
    // tracks disk usage and also a private non static one so we can
    // determine if an instatiated item should support it.
    public static $track_disk_usage = FALSE;
    var $_track_disk_usage = FALSE;
      
    // These are from the red_item database:
    var $_item_id;
    var $_hosting_order_id;
    var $_member_id;
    var $_service_id;
    var $_item_status;
    var $_item_host;
    var $_item_disk_usage = 0;
    var $_item_quota = 0;
    var $_item_quota_notify_warning_date;
    var $_item_quota_notify_exceeded_date;
    var $_item_quota_notify_warning_percent = 85;
    var $_item_modified;
    var $_item_disabled_by_hosting_order = 0;

    // Server only variables
    var $_hosting_order_identifier;    // usually domain name, from 
    // red_hosting_order table
    var $_member_unix_group_name;  // usually same as hosting_order, 
    // but can be different. This value
    // is used when building directories, e.g.
    // /home/members/$member_group_name/

    // Client only variables
    var $queue = NULL;
    var $_child_table;
    var $_notify_host = TRUE;            // Tell the ui whether or not 
    // to notify the host that there is
    // a pending change
    var $_notify_user = 'root';          // username to ssh in as when
    // notifying host  
    var $_notify_cmd = '/usr/bin/ssh';
    var $_notify_background_process = true;  // Whether to background the
    // the notification process

    // Never create unix UID less than this number.
    var $_minimum_unix_user_id = 500;

    // Factory function to build a red object.
    public static function get_red_object($options = []) {
      global $globals;

      // Mode is required, no way around that.
      $mode = $options['mode'] ?? NULL;
      if (!$mode) {
        throw new RedException("You must pass mode when calling get_red_object");
      }

      // Somehow we have to get the child table so we know what kind of
      // red_item to create and we need a hosting_order_id so we can properly
      // save this item and we need a service_id (although it can be derived 
      // from the child table). They can come from a variety of options.
     
      $child_table = $options['child_table'] ?? NULL;
      $service_id = $options['service_id'] ?? NULL;
      $service_name = $options['service_name'] ?? NULL;
      $id = $options['id'] ?? NULL;
      $rs = $options['rs'] ?? NULL;
      $hosting_order_id = $options['hosting_order_id'] ?? NULL;

      // $id trumps all other options.
      if ($id) {
        $sql = "SELECT service_id, hosting_order_id FROM red_item WHERE item_id = #item_id";
        $result = red_sql_query($sql, ['#item_id' => $id]);
        $row = red_sql_fetch_row($result);
        $service_id = $row[0] ?? NULL;
        $hosting_order_id = $row[1] ?? NULL;
        $child_table = red_get_service_table_for_service_id($service_id);
      }
      elseif ($rs) {
        $service_id = $rs['service_id'] ?? NULL;
        $hosting_order_id = $rs['hosting_order_id'] ?? NULL;
      }
      if (!$child_table && $service_id) {
        $child_table = red_get_service_table_for_service_id($service_id);
      }
      if (!$child_table && $service_name) {
        $child_table = red_get_service_table_for_service_name($service_name);
      }
      if ($service_name && !$service_id) {
        $service_id = red_get_service_id_for_service_name($service_name);
      }
      if ($child_table && !$service_id) {
        $service_id = red_get_service_id_for_service_table($child_table);
      }
      // Now we should have a child_table, a service_id and a hosting_order_id
      if (empty($child_table))  {
        throw new RedException("Failed to find child table when creating red item object.");
      }
      if (empty($hosting_order_id))  {
        throw new RedException("Failed to find hosting_order_id when creating red item object.");
      }
      if (empty($service_id))  {
        throw new RedException("Failed to find service_id when creating red item object.");
      }
      // Make sure these are set.
      $options['service_id'] = $service_id;
      $options['hosting_order_id'] = $hosting_order_id;

      $backend_name = '';
      if ($mode == 'node') {
        $backend_name = $globals['config']['red_backends'][$child_table] ?? NULL;
        if (!$backend_name) {
          red_log("Failed to find backend when creating red item for {$child_table}.");
          return FALSE;
        }
        $backend_name = "_{$backend_name}";
      }

      $child_class = $child_table . '_' . $mode . $backend_name;

      $object = new $child_class($options);
      return $object;
    }

    // constructor
    function __construct($construction_options) {
      // Call parent.
      parent::__construct($construction_options);
      global $globals;

      // Required by both ui and node.
      $this->set_site_dir_template();

      // Set default value - this will be overridden when
      // the object is commited to the db
      if (empty($this->_item_modified)) {
        $this->set_item_modified(date('Y-m-d H:i:s'));
      }

      // What are we, ui or node?
      $this->_mode = $construction_options['mode'];

      // All items have a set of data fields in common.
      $this->_set_common_datafields();

      // Both a service_id and a hosting_order_id are required.
      $this->set_service_id($construction_options['service_id']);
      $this->set_hosting_order_id($construction_options['hosting_order_id']);

      if ($this->_mode == 'ui') {
        // mode is ui
        $this->_child_table = red_get_service_table_for_service_id($this->get_service_id());
        $this->set_queue();
        if (!$this->exists_in_db()) {
          $host = $this->_determine_appropriate_host();
          $this->set_item_host($host);
        }
        if (!$this->get_item_status()) {
          $this->set_item_status('pending-insert');
        }
      }

      $this->_set_hosting_order_identifier();
      $this->_set_hosting_order_unix_group();
      $this->_set_member_unix_group();
    }

    // Shared methods common to both ui and nodes
    //
    // If the host starts with weborigin001, etc. then we
    // are on new infrastructure.
    function on_new_infrastructure($host = NULL) {
      if (is_null($host)) {
        $host = $this->get_item_host();
        if (!$host) {
          // Might be a new item.
          $host = $this->get_hosting_order_host();
        }
      }
      return preg_match('/^(weborigin|shell|mailstore|nextcloud|mysql)[0-9]+\./', $host);
    }

    // Check if we are in the process of moving to new infrastructure.
    // That may require converting certain values from ones compatible
    // with the old infra to ones compatible with the new infra.
    function transitioning_to_new_infrastructure() {
      if ($this->exists_in_db()) {
        if ($this->on_new_infrastructure()) {
          $item_id = $this->get_item_id();
          $sql = "SELECT item_host FROM red_item WHERE item_id = #item_id";
          $result = red_sql_query($sql, ['#item_id' => $item_id]);
          $row = red_sql_fetch_row($result);
          $host = $row[0];
          if (!$this->on_new_infrastructure($host)) {
            // Our current host is on new infra, our previous host is not.
            return TRUE;
          }
        }
      }
      return FALSE;
    }

    function get_site_dir() {
      $find = array(
        '{unix_group_name}',
        '{identifier}',
        '{item_id}'
      );
      $hosting_order_id = $this->get_hosting_order_id();
      // Get the item id associated with this hosting order. We need it when 
      // creating and deleting.
      $sql = "SELECT item_id FROM red_item JOIN red_item_web_conf USING(item_id)
        WHERE hosting_order_id = #hosting_order_id AND 
        item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $row = red_sql_fetch_row($result);
      if(empty($row))  {
        $this->set_error("Couldn't get the item id for this hosting order.",'system');
        return false;
      }
      $item_id = $row[0];

      $replace = array(
        $this->get_member_unix_group_name(),
        $this->get_hosting_order_identifier(),
        $item_id
      );
        
      $site_dir = str_replace($find,$replace,$this->get_site_dir_template());
      if(substr($site_dir,-1) == '/') $site_dir = substr($site_dir,0,-1);
      return $site_dir;
    }

    function _initialize_from_id($item_id) {
      $service_table = red_get_service_table_for_item_id($item_id);
      $sql = "SELECT * FROM red_item INNER JOIN !service_table ".
        "ON red_item.item_id = !service_table.item_id ".
        "WHERE red_item.item_id = #item_id";
      $params = [
        '!service_table' => $service_table,
        '#item_id' => $item_id,
      ];
      $result = red_sql_query($sql, $params);
      $rs = red_sql_fetch_assoc($result);
      return $this->_initialize_from_recordset($rs);
    }

    function _initialize_from_recordset($rs) {
      parent::_initialize_from_recordset($rs);
      if($rs['item_status'] == 'pending-delete') {
        $this->set_delete_flag();
      }
      if($rs['item_status'] == 'pending-disable') {
        $this->set_disable_flag();
      }
    }

    function _set_common_datafields() {
      $this->_datafields = array_merge($this->_datafields, [
        'item_id' => [ 
          'req' => false,
          'pcre'   => RED_ID_MATCHER,
          'pcre_explanation'   => RED_ID_EXPLANATION,
          'type'  => 'int',
          'fname'  => 'Id',
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => TRUE,
          'weight' => 0,
          'tblname'   => 'red_item'
        ],
        'hosting_order_id' => [
          'req' => false,
          'pcre'   => RED_ID_MATCHER,
          'pcre_explanation'   => RED_ID_EXPLANATION,
          'type'  => 'int',
          'fname'  => 'Hosting Order ID',
          'user_update' => FALSE,
          'user_insert' => FALSE,
          'user_visible' => FALSE,
          'weight' => 1,
          'tblname'   => 'red_item'
        ],
        'member_id' => [
          'req' => false,
          'pcre'   => RED_ID_MATCHER,
          'pcre_explanation'   => RED_ID_EXPLANATION,
          'type'  => 'int',
          'fname'  => 'Member ID',
          'user_update' => FALSE,
          'user_insert' => FALSE,
          'user_visible' => FALSE,
          'weight' => 2,
          'tblname'   => 'red_item'
        ],
        'service_id' => [
          'req'   => TRUE,
          'pcre'   => RED_ID_MATCHER,
          'pcre_explanation'   => RED_ID_EXPLANATION,
          'type'  => 'int',
          'fname'  => 'Service ID',
          'user_update' => FALSE,
          'user_insert' => FALSE,
          'user_visible' => FALSE,
          'weight' => 3,
          'tblname' => 'red_item'
         ],
         'item_host' => [
          // item_host can be empty to signify a db only item
          'req'   => FALSE,
          'pcre'   => RED_HOST_MATCHER,
          'pcre_explanation'   => RED_HOST_EXPLANATION,
          'type'  => 'text',
          'fname'  => 'Host',
          'user_update' => FALSE,
          'user_insert' => FALSE,
          'user_visible' => FALSE,
          'weight' => 4,
          'tblname' => 'red_item'
        ],
        'item_status' => [
          'req'   => TRUE,
          'pcre'   => RED_STATUS_MATCHER,
          'pcre_explanation'   => RED_STATUS_EXPLANATION,
          'type'  => 'text',
          'fname'  => 'Item Status',
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => TRUE,
          'weight' => 5,
          'tblname' => 'red_item'
        ],
        'item_quota' => [
          'req' => false,
          'pcre'   => RED_BYTES_MATCHER,
          'pcre_explanation'   => RED_BYTES_EXPLANATION,
          'description' => red_t("Restrict this item to a maximum disk quota. Valid values include: 10b, 100kb, 500mb, or 3gb"),
          'type'  => 'int',
          'fname'  => red_t('Quota allocated'),
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => FALSE,
          'input_type' => 'text',
          'weight' => 101,
          'tblname'   => 'red_item'
         ],
        'item_disk_usage' => [
          'req' => false,
          'pcre'   => RED_INT_MATCHER,
          'pcre_explanation'   => RED_INT_EXPLANATION,
          'type'  => 'int',
          'fname'  => red_t('Disk Usage') . '<sup>&dagger;</sup>',
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => FALSE,
          'input_type' => 'text',
          'weight' => 100,
          'tblname'   => 'red_item'
        ],
        'item_quota_notify_warning_percent' => [
          'req' => false,
          'pcre'   => RED_INT_MATCHER,
          'pcre_explanation'   => RED_INT_EXPLANATION,
          'type'  => 'int',
          'fname'  => red_t('Notification percent'),
          'description'  => red_t('Notify member contacts when disk usage reaches this percent of quota'),
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => FALSE,
          'input_type' => 'text',
          'weight' => 102,
          'tblname'   => 'red_item'
        ],
        'item_quota_notify_warning_date' => [
          'req'   => FALSE,
          'pcre'   => RED_DATETIME_MATCHER,
          'pcre_explanation'   => RED_DATETIME_EXPLANATION,
          'type'  => 'datetime',
          'fname'  => red_t('Notification date'),
          'description'  => red_t('Silence notifications until this date, e.g. 2021-06-30 20:30 (leave blank for immediate notification).'),
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => FALSE,
          'weight' => 103,
          'tblname' => 'red_item'
        ],
        'item_quota_notify_exceeded_date' => [
          'req'   => FALSE,
          'pcre'   => RED_DATETIME_MATCHER,
          'pcre_explanation'   => RED_DATETIME_EXPLANATION,
          'type'  => 'datetime',
          'fname'  => red_t('Notification date'),
          'description'  => red_t('Silence notifications until this date, e.g. 2021-06-30 20:30 (leave blank for immediate notification).'),
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => FALSE,
          'weight' => 104,
          'tblname' => 'red_item'
        ],
        'item_modified' => [
          'req'   => TRUE,
          'pcre'   => RED_DATETIME_MATCHER,
          'pcre_explanation'   => RED_DATETIME_EXPLANATION,
          'type'  => 'datetime',
          'fname'  => 'Last Modified',
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => TRUE,
          'weight' => 6,
          'tblname' => 'red_item'
        ],
        'item_disabled_by_hosting_order' => [
          'req'   => TRUE,
          'pcre'   => RED_ZERO_ONE_MATCHER,
          'pcre_explanation'   => RED_ZERO_ONE_EXPLANATION,
          'type'  => 'int',
          'fname'  => 'Item disabled by hosting order',
          'user_insert' => FALSE,
          'user_update' => FALSE,
          'user_visible' => FALSE,
          'weight' => 110,
          'tblname' => 'red_item'
        ],
      ]);

      $this->adjust_disk_usage_visibility();
      
    }

    function adjust_disk_usage_visibility() {
      // Now adjust the visibility of the disk usage and quota related fields
      // depending on whether this class is using them or not.
      if ($this->_track_disk_usage) {
        $this->_datafields['item_disk_usage']['user_visible'] = TRUE;
        $this->_datafields['item_quota']['user_visible'] = TRUE;
        $this->_datafields['item_quota']['user_update'] = TRUE;
        $this->_datafields['item_quota']['user_insert'] = TRUE;
        $this->_datafields['item_quota_notify_warning_percent']['user_update'] = TRUE;
        $this->_datafields['item_quota_notify_warning_percent']['user_insert'] = TRUE;
        $this->_datafields['item_quota_notify_warning_date']['user_update'] = TRUE;
        $this->_datafields['item_quota_notify_warning_date']['user_insert'] = TRUE;
      }
    }

    function set_item_id($value) {
      $this->_item_id = intval($value);
    }
    function get_item_id() {
      return $this->_item_id;
    }

    function set_service_id($value) {
      $this->_service_id = intval($value);
    }
    function get_service_id() {
      return $this->_service_id;
    }

    function set_hosting_order_id($value) {
      $this->_hosting_order_id = intval($value);
    }
    function get_hosting_order_id() {
      return $this->_hosting_order_id;
    }

    function set_member_id($value) {
      $this->_member_id = $value;
    }

    function set_item_status($value) {
      $this->_item_status = $value;
    }
    function get_item_status() {
      return $this->_item_status;
    }

    function set_item_host($value) {
      if (empty($value)) {
        // When an item has no host, it should no longer have a quota or use
        // disk space. This handy short cut makes the transition easier.
        $this->set_item_quota(0);
        $this->set_item_disk_usage(0);
      }
      $this->_item_host= $value;
    }
    function get_item_host() {
      return $this->_item_host;
    }

    /**
     * Set quota to percent over usage
     *
     * Given an integer representing a percentage, set the quota to that
     * percentage over the current usage. For example, if given 10, and disk
     * usage is 100, set the quota to 110.
     */
    function set_item_quota_to_percent_over_usage($value) {
      $usage = $this->get_item_disk_usage();
      $quota = $this->get_item_quota();
      $target_quota = ($value/100 * $usage) + $usage;
      if ($quota > $target_quota) {
        if ($target_quota == 0) {
          // You can't set the quota to 1MB.
          $target_quota = 1024 * 1024;
        }
        $this->set_item_quota($target_quota);
      }
    }

    function set_item_quota($value) {
      if (empty($value)) {
        $value = 0;
      }
      $this->_item_quota = $value;
    }
    function get_item_quota() {
      return $this->_item_quota;
    }

    function set_item_disk_usage($value) {
      $this->_item_disk_usage = intval($value);
    }
    function get_item_disk_usage() {
      return $this->_item_disk_usage;
    }

    function set_item_quota_notify_warning_percent($value) {
      if (empty($value)) {  
        $value = 0;
      }
      $this->_item_quota_notify_warning_percent = intval($value);
    }
    function get_item_quota_notify_warning_percent() {
      return $this->_item_quota_notify_warning_percent;
    }
    
    function set_item_quota_notify_warning_date($value) {
      $this->_item_quota_notify_warning_date = $value;
    }
    function get_item_quota_notify_warning_date() {
      return $this->_item_quota_notify_warning_date;
    }

    function set_item_quota_notify_exceeded_date($value) {
      $this->_item_quota_notify_exceeded_date = $value;
    }
    function get_item_quota_notify_exceeded_date() {
      return $this->_item_quota_notify_exceeded_date;
    }

    function get_item_modified() {
      return $this->_item_modified;
    }
    function set_item_modified($value) {
      $this->_item_modified = $value;
    }

    function get_item_disabled_by_hosting_order() {
      return $this->_item_disabled_by_hosting_order;
    }
    function set_item_disabled_by_hosting_order($value) {
      $this->_item_disabled_by_hosting_order = $value;
    }

    function get_notify_user() {
      return $this->_notify_user;
    }

    function set_notify_background_process($value) {
      $this->_notify_background_process = $value;
    }
    function get_notify_background_process() {
      return $this->_notify_background_process;
    }

    function set_notify_user($value) {
      $this->_notify_user = $value;
    }

    function get_notify_cmd() {
      return $this->_notify_cmd;
    }

    function set_notify_cmd($value) {
      $this->_notify_cmd = $value;
    }

    function set_site_dir_template()  {
      global $globals;
      $this->_site_dir_template = $globals['config']['site_dir_template'];
    }

    function get_site_dir_template() {
      if ($this->on_new_infrastructure()) {
        return '/home/sites/{item_id}';
      }
      return $this->_site_dir_template;
    }

    function write_error_to_database($index): void {
      $error = $this->_errors[$index]['error'];
      $severity = $this->_errors[$index]['severity'];
      $type = $this->_errors[$index]['type'];
      $item_id = $this->get_item_id();

      // If item id is empty, don't write to database. We now have
      // constraints to prevent errors without item ids.
      if (empty($item_id)) {
        return;
      }
      $sql = "INSERT INTO red_error_log SET error_log_type = @type,
        error_log_severity = @severity, error_log_message = @error,
        item_id = #item_id";
      $params = [
        '@type' => $type,
        '@severity' => $severity,
        '@error' => $error,
        '#item_id' => $item_id,
      ];

      if(FALSE === red_sql_query($sql, $params)) {
        throw new RedException("Logging SQL statement failed. The DB error is: " . red_sql_error() . " The SQL statement was: $sql.");
      }
    }

    function _set_hosting_order_unix_group() {
      $hosting_order_id = $this->get_hosting_order_id();
      if (empty($hosting_order_id))  {
        // We don't need to set the hosting order unix id if it is an item inherited directly
        // by the member.
        return true;
      }
      $sql = "SELECT red_hosting_order.unique_unix_group_id,".
        "unique_unix_group_name FROM ".
        "red_hosting_order INNER JOIN red_unique_unix_group ON ".
        "red_hosting_order.unique_unix_group_id = ".
        "red_unique_unix_group.unique_unix_group_id WHERE ".
        "hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $row = red_sql_fetch_row($result);
      if(empty($row))  {
        $this->set_error("Hosting order: $hosting_order_id does not have a properly set unique_unix_group_id value.",'system');
        return false;
      }
      $this->_hosting_order_unix_group_id = $row[0];
      $this->_hosting_order_unix_group_name = $row[1];
      return true;
    }

    function get_member_id() {
      // An item might have either a member_id set (and no hosting_order_id),
      // or it might have a hosting_order_id and no member_id. If it has a
      // hosting_order_id and no member_id, then we derive the member_id.

      // FIXME - the member_id is sometimes populated in the red_item table and is often
      // wrong. This should be fixed, but for now we work around it.

      $hosting_order_id = $this->get_hosting_order_id();
      if(empty($hosting_order_id))  {
        if(!empty($this->_member_id)) {
          return $this->_member_id;
        };
        $this->set_error("Failed to retreive the hosting_order_id when getting member id.",'system');
        return false;
      }
      $sql = "SELECT member_id FROM red_hosting_order WHERE ".
        "hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $row = red_sql_fetch_row($result);
      $derived_member_id = $row[0] ?? NULL;
      if ($derived_member_id) { 
        $this->_member_id = $row[0];
        return $this->_member_id;
      }
      if(!empty($this->_member_id)) {
          return $this->_member_id;
      };
    }

    function get_member_parent_id() {
      if(!empty($this->_member_parent_id)) return $this->_member_parent_id;

      $hosting_order_id = $this->get_hosting_order_id();
      $member_id = $this->get_member_id();
      if (empty($hosting_order_id) && empty($member_id)) {
        $this->set_error("Failed to retreive the hosting_order_id when getting member parent id.",'system');
      }
      if(!empty($hosting_order_id))  {
        $sql = "SELECT member_parent_id FROM red_hosting_order JOIN red_member ".
          "USING(member_id)  WHERE hosting_order_id = #hosting_order_id";
        $params = ['#hosting_order_id' => $hosting_order_id];
      }
      else {
        $sql = "SELECT member_parent_id FROM red_member ".
          "WHERE member_id = #member_id";
        $params = ['#member_id' => $member_id];
      }
      $result = red_sql_query($sql, $params);
      $row = red_sql_fetch_row($result);
      $this->_member_parent_id = $row[0];
      return $this->_member_parent_id;
    }

    function _set_member_unix_group() {
      $member_id = $this->get_member_id();
      $sql = "SELECT unique_unix_group_name FROM ".
        "red_member INNER JOIN ".
        "red_unique_unix_group ON ".
        "red_member.unique_unix_group_id = ".
        "red_unique_unix_group.unique_unix_group_id WHERE ".
        "member_id = #member_id";
      $result = red_sql_query($sql, ['#member_id' => $member_id]);
      $row = red_sql_fetch_row($result);
      if(empty($row))  {
        $hosting_order_id = $this->get_hosting_order_id();
        $this->set_error("Hosting order: $hosting_order_id is not related to a red_member record with a valid unique_unix_group_id.",'system');
        return false;
      }
      $this->_member_unix_group_name = $row[0];
      return true;
    }

    function get_member_unix_group_name() {
      return $this->_member_unix_group_name;
    }

    function get_hosting_order_unix_group_name() {
      return $this->_hosting_order_unix_group_name;
    }

    function get_hosting_order_unix_group_id() {
      return intval($this->_hosting_order_unix_group_id);
    }

    function _set_hosting_order_identifier() {
      $hosting_order_id = $this->get_hosting_order_id();
      if(empty($hosting_order_id))  {
        return null;
      }
      $sql = "SELECT hosting_order_identifier FROM  ".
        "red_hosting_order WHERE ".
        "red_hosting_order.hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $row = red_sql_fetch_row($result);
      $this->_hosting_order_identifier = $row[0];
    }

    function get_hosting_order_identifier() {
      return $this->_hosting_order_identifier;
    }

    function get_next_uid() {
      $sql = "INSERT INTO red_uid_seq  VALUES(NULL)";
      $result = red_sql_query($sql);
      return red_sql_insert_id();
    }

    // Server only methods
    function execute() {
      if(!$this->node_sanity_check()) {
        $message = 'Cannot do anything. Failed sanity check.';
        $this->set_error($message,'system');
        $this->set_item_status('hard-error');
        $this->_commit_to_db();
        return false;
      }
      $status = $this->get_item_status();

      // Validate to ensure nobody snuck something 
      // into the database and to run any node mode
      // only validation tests
      if(!$this->validate()) {
        $this->auto_set_item_status();
      }
      else {
        // If we validate, proceed
        switch($status)  {
          case 'pending-delete':
            if (!$this->delete()) { 
              $this->auto_set_item_status();
            }
            break;
          case 'pending-update':
            if (!$this->update()) {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-insert':
            if (!$this->insert()) {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-restore':
            if (!$this->restore()) {
              $this->auto_set_item_status();
            }
            break;
          case 'pending-disable':
            if (!$this->disable()) {
              $this->auto_set_item_status();
            }
            break;
          }
      }  
      // Don't run the public commit_to_db() function
      // because it will refuse to commit if we fail
      // to validate. However when running in node
      // mode we want to commit the item_status change
      // to "error" regardless of validation problems.
      if(!$this->_commit_to_db()) return false;

      return true;
    }

    function delete() {
      // should be overriden in child class
      return false;

    }

    function insert() {
      // should be overriden in child class
      return false;

    }

    function update() {
      // should be overriden in child class
      return false;

    }

    function restore() {
      // should be overriden in child class
      // FIXME this should be set to false
      // Only set to true while moving sites
      return true;

    }

    // Call the script to report the current amount of disk space used by the
    // named resource and update the item with the results.
    //
    // NOTE: This function call is deprecated. This deprecated call is run
    // directly on the node and the node updates the disk usage by making a
    // remote network call to the database. 
    //
    // Instead, we should now update the disk usage of a single item or service
    // by adding a a task to the queue that calls get_disk_usage and those
    // results should be passed to the save_disk_usage.
    function update_disk_usage($disk_usage = NULL) {
      if (is_null($disk_usage)) {
        $service = NULL;
        $name = NULL;
        $service_name = red_get_service_name_for_service_id($this->get_service_id());
        if ($service_name == 'user_account') {
          $service = 'user_account';
          $name = $this->get_user_account_login();
        }
        else if ($service_name == 'mailbox') {
          $service = 'user_account';
          $name = $this->get_mailbox_login();
        }
        else if ($service_name == 'web_conf') {
          $service = 'user_account';
          $name = $this->get_web_conf_login();
        }
        else if ($service_name == 'mysql_database') {
          $service = 'mysql';
          $name = $this->get_mysql_db_name();
        }
        else if ($service_name == 'nextcloud') {
          $service = 'nextcloud';
          $name = $this->get_nextcloud_login();
        }
        else {
          $this->set_error("Unknown service: {$service_name}");
          return FALSE;
        }
        $cmd = '/usr/local/share/red/node/sbin/red-update-disk-usage';
        $service = escapeshellarg($service);
        $name = escapeshellarg($name);
        $output = [];
        exec("$cmd $service $name --report", $output, $retval);
        $disk_usage = NULL;
        if ($retval != 0 || count($output) > 1) {
          $this->set_error('Failed to update disk usage. Check /var/log/red.log','system', 'soft');
          red_log("Failed to update disk usage: " . print_r($output, TRUE));
          return FALSE;
        }
        // If no output, the resource might not be created yet, so set value
        // to 0.
        if (count($output) == 0) {
          $disk_usage = 0;
        }
        else {
          // Parse the output, which should be in the format:
          // item_id:size_on_disk:size_in_database.
          // We want to return the size on disk.
          $out = array_pop($output);
          if (preg_match('/([0-9]+):([0-9]+):([0-9]+)/', $out, $matches)) {
            $disk_usage = $matches[2];
          }
          else {
            $this->set_error('Failed to parse output of disk usage','system', 'soft');
            red_log("Failed to parse: $out");
            return FALSE;
          }
        }
      }
      $this->set_item_disk_usage($disk_usage);
      return TRUE;
    }

    function node_sanity_check() {
      // This should be overridden in the child class
      // if relevant (how could it not be relevant?).
      // True if the necessary files or database are
      // writable, false otherwise
      return true;
    }

    /*
     * This function can be called by node classes if 
     * the item modifies the data for another item and needs
     * the other item to be updated to get the new info
     */
    function node_update_related_item($item_id,$backend) {
      // Build generic construction options from existing object
      $construction_options = ['mode' => 'node'];
      $item_id = $item_id;
      // include errors in case we are reloading after a boo boo
      $sql = "SELECT * FROM red_item WHERE item_id = #item_id ".
        "AND (item_status = 'active' OR item_status = 'soft-error' ".
        "OR item_status = 'hard-error')";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);

      $rs = red_sql_fetch_assoc($result);

      // artificially set to pending update to trigger update
      $rs['item_status'] = 'pending-update';
      $construction_options['rs'] =& $rs;

      $item = red_item::get_red_object($construction_options);

      if(!isset($item) || empty($item)) return false;

      $item->set_site_dir_template();
      return $item->execute();
    }

    // This function should only be called if a function
    // returns false. Set the item_status based on
    // existence of errors
    function auto_set_item_status() {
      if(count($this->get_errors('all','soft')) > 0) {
        $this->set_item_status('soft-error');
        // overwrite if there are also hard errors
      }
      elseif(count($this->get_errors('all','hard')) > 0) {
        $this->set_item_status('hard-error');
      }
      else {
        // shouldn't be here
        $this->set_error('Something went wrong, but no hard or soft errors were set','system');
        $this->set_item_status('hard-error');
      }
    }

    // Child node classes can use this function to set the
    // user quota on the node.
    function set_user_quota() {
      $user = NULL;
      // Mountpoint will remain NULL on moshes, but get updated if
      // we are on new infrastructure.
      $mountpoint = NULL;

      // Note: nextcloud overrides this function to set the quota
      // in it's own way. And mysql just calls update_disk_usage
      // which handles enforcement. We only deal with services that
      // use the ext4 filesystem to enforce the quota.
      $service_name = red_get_service_name_for_service_id($this->get_service_id());
      if ($service_name == 'user_account') {
        // user account should only be caled on old MOSH's
        if ($this->on_new_infrastructure()) {
          return TRUE;
        }
        $user = $this->get_user_account_login();
      }
      elseif ($service_name == 'mailbox') {
        $user = $this->get_mailbox_login();
        if ($this->on_new_infrastructure()) {
          $mountpoint = $this->get_mailbox_mountpoint();
        }
        else {
          // On moshes, quotas are only set on user accounts, not mailboxes, to
          // avoid double counting.
          return TRUE;
        }
      }
      elseif ($service_name == 'web_conf') {
        $user = $this->get_web_conf_login();
        if ($this->on_new_infrastructure()) {
          $mountpoint = $this->get_web_conf_mountpoint();
        }
        else {
          // On moshes, quotas are only set on user accounts, not web_conf, to
          // avoid double counting.
          return TRUE;
        }
      }
      $quota = $this->get_item_quota();
      if (empty($quota)) {
        return TRUE;
      }

      // And if we are running in a docker container, then all best are off
      // and we should politely return.
      if (file_exists("/.dockerenv")) {
        return TRUE;
      }
      if ($this->on_new_infrastructure()) {
        if ($mountpoint) {
          $partition = "/media/{$mountpoint}";
        }
        else {
          $this->set_error('Missing mountpoint when setting quota.', 'system', 'soft');
          return FALSE;
        }
      }
      else {
        $partition = '/home';
      }
      if (!file_exists("{$partition}/aquota.user")) {
        $this->set_error('User quotas not enabled on this server','system','soft');
        return FALSE;
      }
      // Quotas are set in 1024 byte blocks - so we have to divide bytes by 1024
      $quota_hard = ceil(intval($quota) / 1024);

      // By default ext4 assigns 1 inode for every 15 KB - so let's use that ratio
      $inodes_hard = ceil($quota / 15);

      // Note: we don't seem to get feedback on soft limits so we set soft limit to 0.
      $cmd = '/usr/sbin/setquota';
      $args = [
        '-u',
        $user,
        0,
        $quota_hard,
        0,
        $inodes_hard,
        $partition
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $this->set_error('Failed to set quota','system');
        return FALSE;
      }
      if (!$this->update_disk_usage()) return false;
      return TRUE;
    }

    // Child node classes can use this function to read
    // in a config file. The child class should define an 
    // array called _config_variables that includes each
    // config value in the conf file. In addition, the
    // child class should create an internal variable
    // (prefaced with an underscore) for each of these 
    // variables.
    function _set_config_values($conf_file) {
      if (!file_exists($conf_file)) {
        // If the file doesn't exist, continue - we will take the default
        // values.
        return TRUE;
      }
      if(!red_check_file($conf_file)) {
        echo("Failed to load $conf_file. When running as root the file must owned and only be writable by root.\n");
        return false;
      }
      // FIXME the config file gets loaded once for every instance
      // seems wasteful, especially since the script could create
      // many instances per every time it is run...
      require($conf_file);
      foreach($this->_config_variables as $k => $v) {
        $variable = "_$v";
        if(isset($$v))  { 
          $this->$variable = $$v;
        }
      }
      return true;
    }
    // UI only methods

    function set_queue() {
      $this->queue = new red_queue();
    }

    function _determine_appropriate_host() {
      // Special case for mysql databases.
      $service_name = red_get_service_name_for_service_id($this->get_service_id());
      if ($service_name == 'mysql_database') {
        // Check this hosting order host and see if proxysql is enabled
        // on this host.
        $host = $this->get_hosting_order_host();
        $sql = "SELECT server_proxysql FROM red_server WHERE server = @host";
        $result = red_sql_query($sql, ['@host' => $host]);
        $row = red_sql_fetch_row($result);
        if ($row[0] == 1) {
          // We have proxysql enabled. We can fall through and we will take the
          // service_default configured for the service.
        }
        else {
          // We have to intervene and return the hosting_order_host instead so the
          // database is installed on the mosh itself.
          return $host;
        }
      }
      // Special case for server access. 
      if ($service_name == 'server_access' && $this->on_new_infrastructure() ) {
        // Fix me - this shouldn't be hard-coded. After the transition, update
        // red_service for id 3 and set the service_default_host.
        return 'shell001.mayfirst.' . red_get_top_level_domain();
      }
      // special case for user account
      if ($service_name == 'user_account' && $this->on_new_infrastructure() ) {
        // If we are on new infrastructure, don't set the host, the record only
        // exists in the database.
        return '';
      }
      // Check the service
      $sql = "SELECT service_default_host FROM red_service WHERE ".
        "service_id = #service_id";
      $result = red_sql_query($sql, ['#service_id' => $this->get_service_id()]);
      $row = red_sql_fetch_row($result);
      $default_host = $row[0];

      if ($default_host != 'default') {
        return $default_host;
      }

      $sql = "SELECT hosting_order_host FROM red_hosting_order WHERE ".
        "hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
      $row = red_sql_fetch_row($result);
      return $row[0];
    }

    function notify_host() {
      if ($this->_notify_host) {
        $service_name = red_get_service_name_for_service_id($this->get_service_id());
        $cmd = $this->get_notify_cmd(); 
        $host = $this->get_item_host();
        if ($host == '') {
          return true;
        }
        $user = $this->get_notify_user();
        if ((defined('RED_DEV') || defined('RED_TEST')) && $service_name == 'email_list') {
          // Noop. Make testing easier. Skip email list creation in dev/testing mode.
        }
        else {
          $this->queue->add_task('red_notify_host', [ $user, $host, $cmd ], 0);
        }
      }
      return true;
    }

      

    function is_in_modifiable_state() {
      if(preg_match('/^(pending-|hard-error|deleted|transfer-limbo)/',$this->get_item_status()) > 0) 
        return false;
      return true;
    }

    function is_in_deletable_state() {
      if(preg_match('/^(hard-error|deleted|pending-)/',$this->get_item_status()) > 0) 
        return false;
      return true;
    }
    function is_in_disableable_state() {
      if(preg_match('/^(hard-error|deleted|pending-)/',$this->get_item_status()) > 0) 
        return false;
      return true;
    }
    function commit_to_db() {
      // Update the last modified time
      $this->set_item_modified(date('Y-m-d H:i:s'));
      return parent::commit_to_db();
    }
          
    // This function actually commits to db
    function _commit_to_db() {
      // call parent
      if(!parent::_commit_to_db()) return false;
        
      // update child table
      // The node should never touch the child table,
      // it should only update red_item (this is a security
      // precaution - the db user should not have update
      // privs to the child tables).
      if($this->_mode == 'ui') {
        $sql = $this->_get_sql($this->_child_table);
        if(!red_sql_query($sql)) return FALSE;
      }

      return TRUE;
    }

    function _pre_commit_to_db() {
      $this->set_item_quota(red_machine_readable_bytes($this->get_item_quota()));
      if (!$this->_disable) {
        // When enabling, always ensure this flag is unset.
        $this->set_item_disabled_by_hosting_order(0);
      }
      $host = $this->get_item_host();
      if (defined('RED_DEV') || ($this->queue->task_count() == 0 && empty($host))) {
        // If we are in DEV mode there is no back end, so automatically update.
        // Also, If host is empty, then it means it is a database only item, so
        // there should be no node notification and the item will be
        // automatically active or deleted
        $this->_notify_host = false;
        if ($this->_delete) {
          $this->set_item_status('deleted');
        }
        elseif ($this->_disable) {
          $this->set_item_status('disabled');
        } else {
          $this->set_item_status('active');
        }
      }
      elseif($this->exists_in_db()) {
        // It already exists - set appropriate item_status

        // Only set item_status if we're in ui mode
        // node mode functions will set it otherwise
        if($this->_mode == 'ui') {
          // If we're deleting an existing record
          // this flag is set by the ui (not set internally)
          // with the set_delete_flag().
          if($this->_delete) {
            if (!$this->is_in_deletable_state()) {
              $message = 'You cannot delete a record that is '.
                'already deleted or has a status set to '.
                'error or pending. Please wait until the '.
                'status changes to make your change. '.
                'If the status is set to soft-error, please '.
                'read the error message and re-edit your record '.
                'to fix it. Then, if you want, you can delete it. '.
                'If the status is set to hard-error please '.
                'notify support.';
              $this->set_error($message,'system');
              return false;
            }
            else {
              $this->set_item_status('pending-delete');
            }
          } 
          elseif($this->_disable) {
            if(!$this->is_in_disableable_state()) {
              $message = 'You cannot disable a record that is '.
                'already deleted or has a status set to '.
                'error or pending. Please wait until the '.
                'status changes to make your change. '.
                'If the status is set to soft-error, please '.
                'read the error message and re-edit your record '.
                'to fix it. Then, if you want, you can disable it. '.
                'If the status is set to hard-error please '.
                'notify support.';
              $this->set_error($message,'system');
              return false;
            }
            else {
              $this->set_item_status('pending-disable');
            }
          }
          elseif ($this->get_item_status() == 'soft-error') {
            // convert all soft-errors to pending-restore because
            // we have no idea whether it was set to pending-insert
            // or pending-update before the node switched it to 
            // soft-error.
            $this->set_item_status('pending-restore');
          }
          // Otherwise, make sure it is modifiable 
          elseif(!$this->is_in_modifiable_state()) {
            $status = $this->get_item_status();
            if($status == 'hard-error') {
              $message = "The record has status set to hard-error. ".
                "Please open a support ticket!";
            } else {
              $message = "You cannot modify a record that with ".
                "the status set to $status.";
            }
            $this->set_error($message,'system');
            return false;
          }
          else {
            $this->set_item_status('pending-update');
          }
        }
      }
      return true;
    }

    function _post_commit_to_db() {
      if($this->_sql_action == 'insert' || $this->_mode == 'ui') {
        // Some items are instantly active (like user accounts in the new
        // infrastructure). These have no need for any queueing of tasks.
        $completed_statuses = ['active', 'deleted', 'disabled'];
        if (!in_array($this->get_item_status(), $completed_statuses)) {
          $this->notify_host();
          $this->queue->set_item_id($this->get_item_id());
          $this->queue->save();
          // Never run the queue from the web UI unless we are testing. Otherwise
          // we will have multiple queue runs going at the same time and it will
          // create a mess. When testing and running via the web, then we want to
          // execute immediately since we won't have a cron job running to do it.
          if (defined('RED_TEST') && php_sapi_name() != 'cli') {
            $this->queue->set_quiet();
            $this->queue->run();
          }
        }
      }
      return TRUE;
    }


    function is_field_value_unique($field, $table, $value) {
      // don't count deleted items or items in limbo
      $sql = "SELECT red_item.item_id, !field FROM !table INNER JOIN red_item 
        ON !table.item_id = red_item.item_id WHERE !field = @value AND 
        red_item.item_status != 'deleted' AND item_status != 
        'transfer-limbo'";

      $params = [
        '@value' => $value,
        '!table' => $table,
        '!field' => $field,
      ];
      $result = red_sql_query($sql, $params);

      // If no records exist...
      if(red_sql_num_rows($result) == 0) return true;
      if(red_sql_num_rows($result) == 1) {
        $row = red_sql_fetch_row($result);
        // If the record that exists is the record we're dealing with
        // return true
        if($row[0] == $this->get_item_id()) return true;
      }
      // otherwise return false
      return false;
    }

    function in_limbo() {
      // check to see if we're in transfer-limbo
      // we're in transfer limbo if there's a hosting order
      // record with hosting_order_status set to transfer-limbo
      // and identifier set to our identifier
      $identifier = $this->get_hosting_order_identifier();
      $sql = "SELECT hosting_order_id FROM red_hosting_order WHERE ".
        "hosting_order_identifier = @identifier AND " .
        "hosting_order_status = 'transfer-limbo'";
      $result = red_sql_query($sql, ['@identifier' => $identifier]);
      if(red_sql_num_rows($result) == 0) return false;
      return true;
    }

    function get_hosting_order_host() {
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT hosting_order_host FROM red_hosting_order ".
        "WHERE hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $row = red_sql_fetch_row($result);
      return $row[0];
    }

    function validate() {
      // don't validate on delete when status is transfer-limbo - 
      // they values won't validate because of duplicates, but that's ok.
      if($this->_delete && $this->get_item_status() == 'transfer-limbo') 
        return true;

      if ($this->_mode == 'ui') {
        // We allow some disk usage validation errors go by via the UI (to help when
        // moving items between servers). We don't want those to get tripped up on
        // the node.
        $this->track_disk_usage_validation();
      }
      return parent::validate();
    }

    // Ensure disk usage relatd values are sane.
    function track_disk_usage_validation() {
      if (!$this->_track_disk_usage) return TRUE;
      if ($this->_delete) return TRUE;

      $service_name = red_get_service_name_for_service_id($this->get_service_id());
      // A non-zero quota must be set.
      if ($this->get_item_quota() == 0) {
        if ($service_name == 'user_account' && $this->get_item_host() == '') {
          // Speical case when we transfer a user account to the new infrastructure.
          // We allow an item quota of zero!
          // no-op
        }
        else {
          $this->set_error(red_t("Please set a disk quota higher than 0."),'validation');
        }
      }
      // quota can't be set lower then current disk usage.
      $disk_quota = red_machine_readable_bytes($this->get_item_quota());
      if ($this->get_item_quota() != 0 && $this->get_item_disk_usage() > $disk_quota) {
        $disk_usage_readable = red_human_readable_bytes($this->get_item_disk_usage());
        $this->set_error(red_t("You cannot set the quota lower then your current disk usage, which is @disk_usage", [ '@disk_usage' => $disk_usage_readable ]),'validation');
      } 

      // If the member quota is set, we can't exceed it.
      if ($this->get_member_quota() > 0) {
        $amount_over_quota = $this->get_amount_over_quota(); 
        if ($amount_over_quota > 0) {
          $notify = TRUE;
          // We make a very teeny exception here to assist with transitioning to new infrastructure.
          if ($service_name == 'web_conf') {  // we are a web item.
            if ($this->on_new_infrastructure()) { // we are on new infra
              if ($this->on_new_infrastructure($this->get_hosting_order_host())) {
                // Our hosting order is still on old infrastructure. We can get away with this
                // because once we delete the data used by our old users we will be under
                // quota again.
                $notify = FALSE;
              }
            }
          }
          if ($notify) {
            $quota_over = red_human_readable_bytes(($amount_over_quota));
            $this->set_error(red_t("Setting the quota for this item exceeds your membership quota by @quota_over", [ '@quota_over' => $quota_over ]),'validation');
          }
        }
      }

      // Notification percent should be between 0 and 100.
      if ($this->get_item_quota_notify_warning_percent() < 0 || $this->get_item_quota_notify_warning_percent() > 100) {
        $this->set_error(red_t("Please set your notification percent to a number between 0 and 100"),'validation');
      }

      // Ensure the notification is in the future. 
      if (!empty($this->get_item_quota_notify_warning_date())) {
        $date = strtotime($this->get_item_quota_notify_warning_date());
        // The validation above should catch improperly formatted dates
        if ($date !== FALSE) {
          // The cron job runs once an hour, so let's give ourselves an hour of leeway here.
          if ($date + 3600 < time()) {
            // This is not a validation error - otherwise you can't enable a disabled
            // item with a notification in the past. So, instead, simply issue a warning
            if ($this->_mode == 'ui') {
              red_set_message(red_t("Your notification date is in the past."));
            }
          }
        }
      }

      // Ensure the quota is parseable
      if (red_machine_readable_bytes($this->get_item_quota()) === FALSE) {
        $this->set_error(red_t("The quota does not seem to be in a readable format. Please enter a number followed by g, m, or k."),'validation');
      }
    }
    function get_edit_item_quota() {
      if (!$this->_datafields['item_quota']['user_insert'] && !$this->exists_in_db()) {
        return NULL;
      }
      elseif (!$this->_datafields['item_quota']['user_update'] && $this->exists_in_db()) {
        return NULL;
      }
      // Convert to human readable before displaying.
      $value = red_human_readable_bytes($this->get_item_quota());
      $attributes = [ 'class' => 'form-control', 'id' => 'item_quota' ];
      $type = 'text';
      return $this->_html_generator->get_input('sf_item_quota',$value, $type, $attributes);
    }

    function get_read_item_quota() {
      // Convert to human readable before displaying.
      $value = red_human_readable_bytes($this->get_item_quota());
      if ($value == 0) {
        return red_t("Not set");
      }
      return $value;
    }

    function get_read_item_disk_usage() {
      // Convert to human readable before displaying.
      return red_human_readable_bytes($this->get_item_disk_usage());
    }

    // Some modules allow a variable to be set in the
    // configuration file that will be different depending
    // on the parent_id, member_id, or hosting_order_id.
    //
    // These variables are set in the form of:
    // $var['default'] = 'some value';
    // $var['hosting_order_id'][123] = 'some other value';
    // $var['hosting_order_id'][434] = 'some other value';
    // $var['member_id'][242] = 'some other value';
    // $var['member_parent_id'][2222] = 'some other value';
    //
    // If we are using one of these variables, and it is
    // providing a file path, this function will determine
    // if all file paths exist
    function variable_conf_files_exists($var,&$message) {
      if(!is_array($var)) {
        if(!file_exists($var)) {
          $message = "I was not able to locate the following file: " . 
            $var . ".";
          return false;
        }
      } else {
        foreach($var as $k => $v) {
          if($k == 'default') {
            if(!file_exists($var[$k])) {
              $message = "I was not able to find the default conf file. Trying: " . 
                $var[$k] . ".";
              return false;
            }
          } else {
            if(is_array($v)) {
              foreach($v as $id => $path) {
                if(!file_exists($path)) {
                  $message = "I was not able to find the conf file with $k set to $id doesn't exist. Trying: " . 
                    $path . ".";
                  return false;
                }
              }
            }
          }
        }
      }
      return true;
    }

    function get_mountpoint_for_host() {
      $host = $this->get_item_host();
      if (!$host) {
        return '';
      }
      $sql = "SELECT mountpoint FROM red_server WHERE server = @host";
      $result = red_sql_query($sql, ['@host' => $host]);
      $row = red_sql_fetch_row($result);
      return $row[0];
    }

    function get_member_quota() {
      return red_get_member_quota($this->get_member_id());
    }

    // If a quota is set for this membership, calculate their quotas for
    // user accounts and web sites and return the amount that
    // they are over or 0 if they are not over.
    function get_amount_over_quota() {
      $total = red_get_allocated_quota_for_membership($this->get_member_id()); 
      $saved_item_quota = 0;
      if ($this->exists_in_db()) {
        $item_id = $this->get_item_id();
        $sql = "SELECT item_quota FROM red_item WHERE item_id = #item_id";
        $result = red_sql_query($sql, ['#item_id' => $item_id]);
        $row = red_sql_fetch_row($result);
        $saved_item_quota = $row[0];
      }
      $member_quota = $this->get_member_quota();
      $diff = $total - $saved_item_quota + intval(red_machine_readable_bytes($this->get_item_quota())) - $member_quota;
      if ($diff <= 0) {
        return 0;
      }
      return $diff;
    }
    function get_available_user_account_logins() {
      $hosting_order_id = $this->get_hosting_order_id();
      $status = $this->get_item_status();
      $allowed_user_account_statuses = [ 'active', 'pending-insert', 'pending-update', 'pending-restore' ];
      if (in_array($status, [ 'pending-disabled' ]) || $this->_disable) {
        $allowed_user_account_statuses[] = 'disabled';
        $allowed_user_account_statuses[] = 'pending-disable';
        $allowed_user_account_statuses[] = 'soft-error';
        $allowed_user_account_statuses[] = 'hard-error';
      }
      $status_query = '"' . implode('","', $allowed_user_account_statuses) . '"';

      $sql = "SELECT user_account_login 
        FROM red_item INNER JOIN red_item_user_account USING (item_id) 
        WHERE 
          hosting_order_id = #hosting_order_id AND 
          item_status IN ($status_query)
        ORDER BY user_account_login";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      $ret = array();
      while($row = red_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }
      if(count($ret) == 0) $ret[''] = red_t('[No user accounts have been entered!]');
      return $ret;
    }

    /**
     * Save disk usage.
     *
     * Takes the results of the get-disk-usage call.
     *
     */
    public static function save_disk_usage_for_service($service, $lines) {
      foreach ($lines as $line) {
        $line_parts = explode(':', $line);
        $name = $line_parts[0] ?? NULL;
        $size = $line_parts[1] ?? NULL;
        if (!$name) {
          throw new \RedException("Failed to extract name from disk usage line '{$line}'.");
        }
	// If $name has a space in it it's probably a PHP error.
	if (preg_match('/ /', $name)) {
	  throw new \RedException("Found space in name on line: $line");
	}
        if (!$size && $size !== "0") {
          throw new \RedException("Failed to extract size from disk usage line '{$line}'.");
        }
        // Nextcloud reports negative sizes sometimes. WTF?
        if ($size < 0) {
          $size = 0;
        }
        if ($service == 'psql') {
          $table = 'red_item_psql';
          $field = 'psql_name';
        }
        else if ($service == 'mysql_db') {
          $table = 'red_item_mysql_db';
          $field = 'mysql_db_name';
        }
        else if ($service == 'web_conf') {
          $table = 'red_item_web_conf';
          $field = 'red_item.item_id';
        }
        else if ($service == 'nextcloud') {
          $table = 'red_item_nextcloud';
          $field = 'nextcloud_login';
        }
        else if ($service == 'mailbox') {
          $table = 'red_item_mailbox';
          $field = 'mailbox_login';
        }
        else {
          throw new \RedException("Unknown service when calling red_item::save_disk_usage: '{$service}'");
        }
        $sql = "SELECT red_item.item_id, item_disk_usage FROM red_item JOIN !table USING (item_id)
          WHERE item_status != 'deleted' AND !field = @name";
        $params = [
          '@name' => $name,
          '!table' => $table,
          '!field' => $field,
        ];
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        $item_id = $row[0] ?? NULL;
        $disk_usage = $row[1] ?? NULL;
        if (!$item_id) {
          // Avoid a race condition. If a service is deleted from the control panel after we started
          // calculating disk usage we might end up here and it's not an error.
          $date = new DateTime('now', new DateTimeZone('UTC'));
          $date->modify('-10 minutes');
          $ten_minutes_ago = $date->format('Y-m-d H:i:s');
          $sql = "SELECT red_item.item_id, item_disk_usage FROM red_item JOIN !table USING (item_id)
            WHERE item_status = 'deleted' AND !field = @name AND item_modified > @ten_minutes_ago";
          $params = [
            '@name' => $name,
            '!table' => $table,
            '!field' => $field,
            '@ten_minutes_ago' => $ten_minutes_ago,
          ]; 
          $result = red_sql_query($sql, $params);
          $row = red_sql_fetch_row($result);
          $item_id = $row[0] ?? NULL;
          $disk_usage = $row[1] ?? NULL;
          if ($item_id) {
            // It's just been deleted, so we can simply continue
            continue;
          }
          else {
            // This is an actual error.
            throw new \RedException("Failed to find item id for {$name} in {$table} using {$field}.");
          }
        }
        if ($disk_usage == intval($size)) {
          continue;
        }
        $sql = "UPDATE red_item SET item_disk_usage = #size WHERE item_id = #item_id";
        red_sql_query($sql, ['#size' => $size, '#item_id' => $item_id]);
      }
    }

    public static function get_disk_usage_for_service($service, $server = NULL, $item_id = NULL) {
      $extra_args = [];

      if (!$server && !$item_id) {
        throw new \RedException("Please pass either a server or an item_id to red_item::get_disk_usage.");
      }
      if ($service == 'psql') {
        $table = 'red_item_psql';
        $field = 'psql_name';
        $extra_args = [
          "sudo",
          "-u",
          "postgres",
          '/usr/local/share/red/node/share/psql/disk-usage'
        ];
      }
      else if ($service == 'web_conf') {
        $table = 'red_item_web_conf';
        $field = 'red_item.item_id';
        $extra_args = [
          "sudo",
          '/usr/local/share/red/node/share/web-conf/disk-usage'
        ];
      }
      else if ($service == 'nextcloud') {
        $table = 'red_item_nextcloud';
        $field = 'nextcloud_login';
        $extra_args = [
          "sudo",
          '/usr/local/share/red/node/share/nextcloud/disk-usage'
        ];
      }
      else if ($service == 'mysql_db') {
        $table = 'red_item_mysql_db';
        $field = 'mysql_db_name';
        $extra_args = [
          "sudo",
          '/usr/local/share/red/node/share/mysql/disk-usage'
        ];
      }
      else if ($service == 'mailbox') {
        $table = 'red_item_mailbox';
        $field = 'mailbox_login';
        $extra_args = [
          "sudo",
          '/usr/local/share/red/node/share/mailbox/disk-usage'
        ];
      }
      else {
        throw new \RedException("Unknown service when calling red_item::save_disk_usage: '{$service}'");
      }

      $name = NULL;
      if ($item_id) {
        // We have to retrieve a name and server.
        $sql = "SELECT !field, item_host
          FROM red_item JOIN !table USING (item_id)
          WHERE item_id = #item_id";
        $params = [
          '#item_id' => $item_id,
          '!table' => $table,
          '!field' => $field,
        ];
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        if (!$row) {
          throw new RedException("Failed to find record in {$table} for item id {$item_id}");
        }

        // Initial arguments common to all actions.
        $name = $row[0];
        $server = $row[1];
      }

      $args = ["red-remote-tasker@{$server}"];
      $args = array_merge($args, $extra_args);

      if ($name) {
        $args[] = $name;
      }
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        $args_log = implode(" ", $args);
        $output_log = implode(" ", $output);
        throw new RedException("Failed to ssh to {$server} to run {$args_log}, output: $output_log.");
      }
      return $output;
    }

    /**
     *
     * Given any item id or hosting order id, return the item_id for the web
     * conf item that shares the same hosting order id.
     *
     */
    public static function get_web_conf_item_id($item_id, $hosting_order_id = NULL) {
      if ($item_id) {
        // Fetch the id of the web configuration for this hosting order.
        $sql = "SELECT item_id FROM red_item JOIN red_service USING (service_id)
          WHERE 
            service_name = 'web_conf' AND 
            item_status != 'deleted' AND
            hosting_order_id IN (
              SELECT hosting_order_id FROM red_item WHERE item_id = #item_id
            )";
        $result = red_sql_query($sql, ['#item_id' => $item_id]);
        $row = red_sql_fetch_row($result);
        return $row[0] ?? '';
      }
      if ($hosting_order_id) {
        $sql = "SELECT item_id FROM red_item JOIN red_service USING (service_id) WHERE item_status = 'active'
          AND service_name = 'web_conf' AND hosting_order_id = #hosting_order_id";
        $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
        $row = red_sql_fetch_row($result);
        return $row[0] ?? '';
      }
      return '';
    }
  }
}
?>
