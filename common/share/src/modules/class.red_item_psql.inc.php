<?php

if(!class_exists('red_item_psql')) {
  class red_item_psql extends red_item {
    static public $track_disk_usage = TRUE;
    var $_item_quota = '1gb';
    var $_psql_name;
    var $_psql_password;
    var $_psql_max_connections = 25;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      $this->_track_disk_usage = self::$track_disk_usage;
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields, [
        'psql_name' => [
           'req' => true,
           'pcre'   => RED_SQL_USER_MATCHER,
           'pcre_explanation'   => RED_SQL_USER_EXPLANATION,
           'type'  => 'varchar',
           'fname'  => red_t('Database and User Name'),
           'user_insert' => true,
           'user_update' => false,
           'user_visible' => true,
           'input_type' => 'text',
           'text_length' => 20,
           'text_max_length' => 64,
           'tblname'   => 'red_item_psql',
           'filter' => true
        ],
        'psql_password' => [
           'req' => true,
           'pcre'   => RED_TEXT_MATCHER,
           'pcre_explanation'   => RED_TEXT_EXPLANATION,
           'type'  => 'varchar',
           'fname'  => red_t('Database password'),
           'user_insert' => true,
           'user_update' => true,
           'user_visible' => false,
           'input_type' => 'text',
           'text_length' => 20,
           'text_max_length' => 64,
           'tblname'   => 'red_item_psql'
        ],
        'psql_max_connections' => [
           'req' => true,
           'pcre'   => RED_INT_MATCHER,
           'pcre_explanation'   => RED_INT_EXPLANATION,
           'type'  => 'int',
           'fname'  => red_t('Max Connections'),
           'user_insert' => TRUE,
           'user_update' => TRUE,
           'user_visible' => TRUE,
           'input_type' => 'text',
           'text_length' => 4,
           'text_max_length' => 4,
           'tblname'   => 'red_item_psql',
           'filter' => true
        ]
      ]);
      $this->_datafields['item_host']['user_visible'] = TRUE;
    }

    function set_psql_name($value) {
      $this->_psql_name = $value;
    }
    function get_psql_name() {
      return $this->_psql_name;
    }
    function set_psql_password($value) {
      $this->_psql_password = $value;
    }
    function get_psql_password() {
      return $this->_psql_password;
    }

    function set_psql_max_connections($value) {
      $this->_psql_max_connections = $value;
    }
    function get_psql_max_connections() {
      return $this->_psql_max_connections;
    }

    function additional_validation() {
      if ($this->_delete || $this->_disable) {
        return;
      }
      $reserved = red_get_sql_reserved_words();
      $reserved[] = 'postgres';
      if (in_array($this->get_psql_name(), $reserved)) {
        $this->set_error(red_t("Invalid name."), 'validation');
      }
      if (!$this->is_field_value_unique('psql_name','red_item_psql', $this->get_psql_name())) {
        $this->set_error(red_t('The database name you chose is already taken.'), 'validation');
      }
      if (strtolower($this->get_psql_name()) != $this->get_psql_name()) {
        $this->set_error(red_t('Please use only lower case for your database user.'), 'validation');
      }

    }

    /**
     * Encode a plain text password in the postgres way
     *
     * See: https://stackoverflow.com/a/71102510
     * which includes a python implementation I copied.
     * Also see Go implementation: https://github.com/supercaracal/scram-sha-256
     */
    static function hash_password($password) {
      $saltSize = 16;
      $digestLen = 32;
      $iterations = 4096;

      $salt = random_bytes($saltSize);
      $binary = TRUE;
      $digestKey = hash_pbkdf2('sha256', $password, $salt, $iterations, $digestLen, $binary);
      $clientKey = hash_hmac('sha256', 'Client Key', $digestKey, $binary);
      $storedKey = hash('sha256', $clientKey, true);
      $serverKey = hash_hmac('sha256', 'Server Key', $digestKey, true);
      return 'SCRAM-SHA-256$' . $iterations . ':' . base64_encode($salt) .
        '$' . base64_encode($storedKey) . ':' . base64_encode($serverKey);
    }

    /**
     * Enable, disable or delete a postgres account on the remote server.
     *
     * Set $action to enable, disable or delete.
     */
    public static function apply($item_id, $action) {
      $actions = ['enable', 'disable', 'delete'];

      if (!in_array($action, $actions)) {
        throw new RedException("Unknown action: $action");
      }
      $sql = "SELECT psql_name, psql_password, psql_max_connections, item_host
        FROM red_item JOIN red_item_psql USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      if (!$row) {
        throw new RedException("Failed to find psql record for item id {$item_id}");
      }

      // Initial arguments common to all actions.
      $name = $row[0];
      $server = $row[3];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        "-u",
        "postgres",
      ];

      if ($action == 'delete') {
        $args[] = '/usr/local/share/red/node/share/psql/ensure-gone';
        $args[] = $name;
      }
      else {
        $password = $row[1];
        $max_connections = $row[2];
        $args[] = '/usr/local/share/red/node/share/psql/ensure-exists';
        $args[] = $name;
        if ($action == 'disable') {
          // We "disable" the account by using a random password.
          $raw_password = red_generate_random_password(25);
          $password = red_item_psql::hash_password($raw_password);
        }
        // We have to base64 encode to prevent dollar signs from being expanded.
        $args[] = base64_encode($password);
        $args[] = $max_connections;
      }
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to create psql item {$item_id}");
      }
      return TRUE;
    }

    public static function save_disk_usage($lines) {
      self::save_disk_usage_for_service('psql', $lines);
    }
  }
}

?>
