<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_email_address')) {
  class red_item_email_address extends red_item {
    var $_email_address;
    var $_email_address_recipient;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
        array (
          'email_address' => array (
            'req' => true,
            'pcre'   => RED_VIRTUAL_EMAIL_MATCHER,
            'pcre_explanation'   => RED_VIRTUAL_EMAIL_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Email adddress'),
            'user_update' => TRUE,
            'user_insert' => TRUE,
            'user_visible' => TRUE,
            'css_class' => 'red_email',
            'input_type' => 'text',
            'text_length' => 40,
            'text_max_length' => 255,
            'tblname'   => 'red_item_email_address',
            'filter' => true
          ),
          'email_address_recipient' => array (
            'req' => true,
            'pcre'   => RED_MULTIPLE_EMAIL_MATCHER,
            'pcre_explanation'   => RED_MULTIPLE_EMAIL_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Recipient'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => TRUE,
            'input_type' => 'text',
            'text_length' => 20,
            'text_max_length' => 768,
            'tblname'   => 'red_item_email_address',
            'filter' => true
          ),
        )
      );
    }

    function set_email_address($value) {
      $this->_email_address = strtolower($value);
    }
    function get_email_address() {
      if (!$this->_email_address) {
        return '';
      }
      return trim($this->_email_address);
    }

    function set_email_address_recipient($value) {
      $this->_email_address_recipient = str_replace(' ', '', strtolower($value));
    }
    function get_email_address_recipient() {
      return $this->_email_address_recipient;
    }

    function get_domain_portion_of_address($email = NULL) {
      if (is_null($email)) {
        $email = $this->get_email_address();
      }
      $domain = strstr($email, '@');
      return substr($domain,1);
    }

    function get_username_portion_of_address() {
      $address = $this->get_email_address();
      $at_char_pos = strpos($this->get_email_address(),'@');
      return substr($address,0,$at_char_pos);
    }

    function additional_validation() {
      // if we're deleting don't do additional validation
      if($this->_delete) return true;

      // If we are disabling, don't do additional validation (the validation will kick in when they re-enable)
      if($this->_disable) return true;

      // Make sure email address is unique
      if(!$this->is_field_value_unique('email_address','red_item_email_address',$this->get_email_address())) {
        $this->set_error(red_t('The email address you chose is already taken.'),'validation');
      }

      $recipient = $this->get_email_address_recipient();
      $address = $this->get_email_address();

      // Now check the recipient
      $recipients_array = explode(',',$recipient);

      // Don't allow duplicates
      if (count($recipients_array) != count(array_unique($recipients_array))) {
        $this->set_error(red_t('You seem to have listed at least one duplciate email address. Please ensure your recipient list contains unique values.'),'validation');
      }

      // $domain_options is a list of domains managed by this hosting
      // order.
      $domain_options = $this->get_email_domains();
      // Every item in $recipients_array must be a email address, without dupes.
      foreach($recipients_array as $single_recipient) {
        $single_recipient = trim($single_recipient);

        // make sure recipient and address are not the same
        if($single_recipient == $address) {
          $this->set_error(red_t('You have set your address to be the same as the recipient - this will cause an endless loop. Please choose a different email address or login@mail.mayfirst.org.'),'validation');
        }
        // If the recipient is an email address using a domain handled by this hosting order
        // or mail.mayfirst.org, make sure that email address exists.
        $recipient_parts = explode('@', $single_recipient);
        $recipient_user = $recipient_parts[0];
        $recipient_domain = $recipient_parts[1] ?? NULL;
        if ($recipient_domain == 'mail.mayfirst.org') {
          $member_id = intval($this->get_member_id());
          // To avoid accidents, only allow @mail.mayfirst.org addresses that exist
          // and are owned by this member. If they want to forward to someone else,
          // then use an external email address.
          $sql = "SELECT COUNT(red_item.item_id) FROM red_item JOIN red_item_mailbox
            USING(item_id) JOIN red_hosting_order USING(hosting_order_id) 
            WHERE mailbox_login = @recipient_user AND item_status != 'deleted' 
            AND red_hosting_order.member_id = #member_id";
          $result = red_sql_query($sql, ['@recipient_user' => $recipient_user, '#member_id' => $member_id]);
          $row = red_sql_fetch_row($result);
          if ($row[0] == 0) {
            $this->set_error(red_t('That May First mailbox does not exist or is not under your control. If you want to deliver to another May First member consider using a different email address that delivers to their mailbox.'),'validation');
          };
        }
        elseif (in_array($recipient_domain, $domain_options)) {
          $sql = "SELECT COUNT(red_item.item_id) FROM red_item JOIN red_item_email_address
            USING(item_id) WHERE email_address = @single_recipient AND item_status != 'deleted'
            AND item_status != 'disabled'";
          $result = red_sql_query($sql, ['@single_recipient' => $single_recipient]);
          $row = red_sql_fetch_row($result);
          if ($row[0] == 0) {
            red_set_message(red_t('The @email recipient does not seem to exist. Your email record was just added or updated, but you may want to double check to ensure you did not make a typo.', [ '@email' => $single_recipient ]));
          };
        }
      }

      // make sure they are not sneaking in a domain they don't control
      $domain = $this->get_domain_portion_of_address();
      if(!in_array($domain,$domain_options)) {
        // if we're in limbo - loosen restrictions to allow email creation on
        // servers that are not yet configured to accept mail for us.
        if(!$this->in_limbo()) {
          $this->set_error(red_t('That domain is not under your control. It does not have a corresponding MX record in your DNS settings.'),'validation');
        }
      }
    }

    // return an array of available domain names that can be used
    // when creating email addresses
    function get_email_domains() {
      // find all mx records matching this host
      // in the default hosting order id 
      $host = $this->get_item_host();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT dns_fqdn FROM red_item_dns INNER JOIN red_item ON ".
        "red_item.item_id = red_item_dns.item_id WHERE ".
        "hosting_order_id = #hosting_order_id ".
        "AND dns_type = 'mx' AND dns_fqdn != @host AND ". 
        "item_status != 'deleted' AND item_status != 'pending-delete' ".
        "AND item_status != 'transfer-limbo'";
      $result = red_sql_query($sql, ['@host' => $host, '#hosting_order_id' => $hosting_order_id]);
      $options = array();
      while($row = red_sql_fetch_row($result)) {
        $fqdn = $row[0];
        $options[$fqdn] = $fqdn;
      }
      return $options;
        
    }

    function get_related_mailbox_logins() {
      $sql = "SELECT mailbox_login FROM red_item INNER JOIN 
        red_item_mailbox ON red_item.item_id = red_item_mailbox.item_id 
        WHERE hosting_order_id = #hosting_order_id AND 
        item_status != 'deleted' ORDER BY mailbox_login";
      //print_r(red_sql_resource);
      $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
      $ret = array();
      while($row = red_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }
      return $ret;
    }

    // Check if the passed domain name is configured with an mx 
    // record pointing to the primary host of this record
    //
    function mx_set_to_primary_host($domain) {
      // simply return true if we are offline (and can't reliably check)
      $tries = 1;
      //if(!red_check_online_status($tries)) return true;

      // by default, do not check to see if the $domain mx record
      // resolves to the computer this code is running on
      $check_local = false;

      // If we are running in server mode, however, we *do* want 
      // that additional check
      if($this->_mode == 'server') $check_local = true;

      $primary_host = $this->get_item_host();
      if(red_domain_mx_resolves_to_server($domain,$primary_host,$check_local))
        return true;

      return false;
    }
  }
}



?>
