<?php

if (!class_exists('red_item_xmpp')) {
  class red_item_xmpp extends red_item {
    var $_xmpp_login;
    var $_xmpp_domain;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      $this->set_default_host();  
      return TRUE;
    }

    /**
     * Set default host.
     *
     * Based on red_service table.
     */
    function set_default_host() {
      $sql = "SELECT service_name FROM red_service WHERE service_id = #service_id";
      $result = red_sql_query($sql, ['#service_id' => $this->get_service_id()]);
      $name = red_sql_fetch_row($result)[0] ?? NULL;
      if ($name == 'chat') {
        // Chat domains are simply user@mayfirst.org.
        $domain = 'mayfirst.' . red_get_top_level_domain();
      }
      elseif ($name == 'video_meeting') {
        $domain = 'meet.mayfirst.' . red_get_top_level_domain();
      }
      else {
        throw new \Exception("Unknown service_name for xmpp: {$name}");
      }
      $this->set_xmpp_domain($domain);
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields, [
          'xmpp_login' => [
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Login name'),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => $this->get_user_account_login_options(),
            'tblname'   => 'red_item_xmpp',
            'filter' => true,
          ],
          'xmpp_domain' => [
            'req' => false,
            'pcre'   => RED_DOMAIN_MATCHER,
            'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Domain'),
            'description' => red_t("The XMPP domain."),
            'user_insert' => FALSE,
            'user_update' => FALSE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 20,
            'text_max_length' => '255',
            'tblname'   => 'red_item_xmpp',
            'filter' => false,
          ],
        ]
      );
    }

    function set_xmpp_login($value) {
      $this->_xmpp_login = $value;
    }
    function get_xmpp_login() {
      return $this->_xmpp_login;
    }
    function set_xmpp_domain($value) {
      $this->_xmpp_domain = $value;
    }
    function get_xmpp_domain() {
      return $this->_xmpp_domain;
    }

    function additional_validation() {
      if ($this->_delete || $this->_disable) {
        return;
      }
      $related_user_accounts = $this->get_available_user_account_logins();
      if (!in_array($this->get_xmpp_login(), $related_user_accounts)) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }
    }

    function get_user_account_login_options() {
      static $all_logins = [];
      if ($all_logins) {
        return $all_logins;
      }
      $all_logins = $this->get_available_user_account_logins();
      $sql = "SELECT xmpp_login
        FROM red_item_xmpp JOIN red_item USING (item_id)
        WHERE
          item_host = @host AND
          hosting_order_id = #hosting_order_id AND
          item_status != 'deleted'";
      $params = [
        '#hosting_order_id' => $this->get_hosting_order_id(),
        '@host' => $this->get_item_host(),
      ];
      $result = red_sql_query($sql, $params);
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        unset($all_logins[$login]);
      }
      return $all_logins;
    }

    /**
     * Add/Enable/Update account.
     *
     */
    public static function apply($item_id, $action) {
      $cmd = NULL;
      if ($action == 'enable') {
        $cmd = '/usr/local/share/red/node/share/xmpp/enable-user';
      }
      elseif ($action == 'disable') {
        $cmd = '/usr/local/share/red/node/share/xmpp/disable-user';
      }
      elseif ($action == 'delete') {
        $cmd = '/usr/local/share/red/node/share/xmpp/delete-user';
      }
      else {
        throw new RedException("Unknown action {$action}.");
      }

      $sql = "SELECT xmpp_login, item_host
        FROM red_item JOIN red_item_xmpp USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      if (!$row) {
        throw new RedException("Failed to find xmpp record for item id {$item_id}");
      }

      $name = $row[0];
      $server = $row[1];

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        $cmd,
        $name,
      ];

      // We need the password to enable or create.
      if ($action == 'enable') {
        $sql = "SELECT user_account_password
          FROM red_item JOIN red_item_user_account USING (item_id)
          WHERE user_account_login = @name AND item_status != 'deleted'";
        $result = red_sql_query($sql, ['@name' => $name]);
        $row = red_sql_fetch_row($result);
        if (!$row) {
          throw new RedException("Failed to find password for {$name}");
        }
        $args[] = base64_encode($row[0]);
      }

      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to enable xmpp for item {$item_id}, {$name}");
      }
      return TRUE;
    }
  }
}

?>
