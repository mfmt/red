<?php

// email lists 
if(!class_exists('red_item_list')) {
  class red_item_list extends red_item {
    var $_list_name;
    var $_list_owner_email;
    var $_list_domain;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_default_hosting_order_id = 1; // hosting order id that provides domains for email liss.
    // available to everyone
    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('list_name' => array (
                               'req' => true,
                               'pcre'   => RED_LIST_NAME_MATCHER,
                               'pcre_explanation'   => RED_LIST_NAME_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('List name'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_list',
                               'filter' => true),
                             'list_domain' => array (
                               'req' => TRUE,  
                               'pcre'   => RED_DOMAIN_MATCHER,
                               'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Domain'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_list',
                               'filter' => true),
                             'list_owner_email' => array (
                               'req' => FALSE,
                               'pcre'   => RED_EMAIL_MATCHER,
                               'pcre_explanation'   => RED_EMAIL_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Owner Email'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 30,
                               'text_max_length' => 128,
                               'tblname'   => 'red_item_list',
                               'filter' => true)

                                 ));

    }

    function set_list_name($value) {
      // convert to lower case so as not to confuse mailman
      $this->_list_name = strtolower($value);
    }
    function get_list_name() {
      return $this->_list_name;
    }

    function set_list_owner_email($value) {
      $this->_list_owner_email = $value;
    }
    function get_list_owner_email() {
      return $this->_list_owner_email;
    }

    function set_list_domain($value) {
      $this->_list_domain = $value;
    }
    function get_list_domain() {
      return $this->_list_domain;
    }

    function set_default_hosting_order_id($value) {
      $this->_default_hosting_order_id = $value;
    }

    function get_default_hosting_order_id() {
      return $this->_default_hosting_order_id;
    }

    function get_list_domain_options() {
      // find all mx records matching this host
      // in either the default hosting order id or the
      // current one
      $host = $this->get_item_host();
      $default_hosting_order_id = $this->get_default_hosting_order_id();
      $current_hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT dns_fqdn FROM red_item_dns INNER JOIN red_item ON ".
        "red_item.item_id = red_item_dns.item_id WHERE ".
        "(hosting_order_id = #default_hosting_order_id OR ".
        "hosting_order_id = #current_hosting_order_id) AND ".
        "dns_type = 'mx' AND dns_server_name = @host AND ". 
        "dns_fqdn != @host AND ". 
        "item_status != 'deleted' AND item_status != 'pending-delete' ".
        "AND item_status != 'transfer-limbo'";
      $result = red_sql_query($sql, [
        '#default_hosting_order_id' => $default_hosting_order_id,
        '#current_hosting_order_id' => $current_hosting_order_id,
        '@host' => $host,
        ] 
      );
      $options = array();
      while($row = red_sql_fetch_row($result)) {
        $fqdn = $row[0];
        $options[$fqdn] = $fqdn;
      }
      return $options;
    }

    function additional_validation() {
      if(!$this->is_field_value_unique('list_name','red_item_list',$this->get_list_name())) {
        $this->set_error(red_t('The list name you chose is already taken.'),'validation');
      }
      if (!$this->exists_in_db()) {
        $owner = $this->get_list_owner_email();
        if (!$owner) {
          $this->set_error(red_t('Please enter an email owner address.'),'validation');
        }
      }
    }
  }  
}

?>
