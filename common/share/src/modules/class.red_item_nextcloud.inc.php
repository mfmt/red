<?php

if(!class_exists('red_item_nextcloud')) {
  class red_item_nextcloud extends red_item {
    static public $track_disk_usage = TRUE;
    var $_item_quota = '1gb';
    var $_nextcloud_login;
    var $_nextcloud_abandoned;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      $this->_track_disk_usage = self::$track_disk_usage;
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields, [
          'nextcloud_login' => [
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Login name'),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => $this->get_user_account_login_options(),
            'tblname'   => 'red_item_nextcloud',
            'filter' => true,
          ],
          'nextcloud_abandoned' => [
            'req' => false,  
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Abandoned'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 1,
            'text_max_length' => 1,
            'tblname'   => 'red_item_nextcloud',
            'filter' => false
          ],
        ]
      );
    }

    function is_abandoned() {
      if ($this->get_nextcloud_abandoned() == 0) {
        return FALSE;
      }
      elseif ($this->get_nextcloud_abandoned() == 1) {
        return TRUE;
      }
      elseif ($this->get_nextcloud_abandoned() == 2) {
        // This means they have never logged in.
        $last_modified = strtotime($this->get_item_modified());
        $six_months_ago = strtotime("6 months ago");
        if ($last_modified < $six_months_ago) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
    }

    function set_nextcloud_login($value) {
      $this->_nextcloud_login = $value;
    }
    function get_nextcloud_login() {
      return $this->_nextcloud_login;
    }
    function set_nextcloud_abandoned($value) {
      $this->_nextcloud_abandoned = $value;
    }
    function get_nextcloud_abandoned() {
      return $this->_nextcloud_abandoned;
    }

    function additional_validation() {
      if ($this->get_item_quota() == 0) {
        $this->set_error(red_t('The Nextcloud quota cannot be set to 0.'),'validation');
      }
      $related_user_accounts = $this->get_available_user_account_logins();
      if(!in_array($this->get_nextcloud_login(),$related_user_accounts) && !$this->_delete) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }
      if(!$this->is_field_value_unique('nextcloud_login','red_item_nextcloud',$this->get_nextcloud_login())) {
        $this->set_error(red_t("A nextcloud record with the same login exists."),'validation');
      }
    }

    function get_user_account_login_options() {
      static $all_logins = [];
      if ($all_logins) return $all_logins;
      $all_logins = $this->get_available_user_account_logins();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT nextcloud_login 
        FROM red_item_nextcloud JOIN red_item USING (item_id)
        WHERE
          hosting_order_id = #hosting_order_id AND
          item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        unset($all_logins[$login]);
      }
      return $all_logins;
    }

    /**
     * Add, enable, disable or delete a nextcloud account.
     *
     */
    public static function apply($item_id, $action) {
      $actions = ['enable', 'disable', 'delete', 'add'];

      if (!in_array($action, $actions)) {
        throw new RedException("Unknown action: $action");
      }
      $sql = "SELECT nextcloud_login, item_host, item_quota
        FROM red_item JOIN red_item_nextcloud USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      if (!$row) {
        throw new RedException("Failed to find nextcloud record for item id {$item_id}");
      }

      $name = $row[0];
      $server = $row[1];
      $quota = $row[2];

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/nextcloud/manipulate-user',
        $name,
        $action,
        $quota,
      ];

      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to handle nextcloud item {$item_id}");
      }
      return TRUE;
    }

    public static function save_disk_usage($lines) {
      self::save_disk_usage_for_service('nextcloud', $lines);
    }

    public static function process_abandoned() {
      // Select the nextcloud server address so we know what server to query
      // for abandoned accounts.
      $sql = "SELECT service_default_host FROM red_service
        WHERE service_table = 'red_item_nextcloud'";
      $result = red_sql_query($sql);
      $row = red_sql_fetch_row($result);
      $server = $row[0];

      // First get a report on abandoned records. This takes a long time.
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/nextcloud/process-abandoned',
      ];
      // Warning: this command takes about an hour to complete.
      if (!red_ssh_cmd($args, $output, $rc)) {
        $msg = "Failed to ssh to {$server} to get list of abandoned accounts.";
        throw new RedException($msg);
      }
      if ($rc !== 0) {
        $msg = "Failed to ssh to {$server} to get list of abandoned accounts. ".
          "Got rc {$rc} and output: " . implode(' ', $output);
        throw new RedException($msg);
      }
      // Build an array keyed to the user so we can also check for previously
      // abandoned users that should not be abandoned any more.
      $abandoned_users = [];
      foreach ($output as $line) {
        $parts = explode(':', $line);
        $user = $parts[0] ?? NULL;
        $status = $parts[1] ?? NULL;
        if (!$user) {
          $msg = "Failed to extract user from {$line} while processing ".
            "abandoned nextcloud accounts.";
          red_log($msg);
          continue;
        }
        $abandoned_users[] = $user;
        $sql = "SELECT red_item.item_id
          FROM red_item JOIN red_item_nextcloud USING (item_id)
          WHERE nextcloud_login = @user AND item_status != 'deleted'";
        $result = red_sql_query($sql, ['@user' => $user]);
        $row = red_sql_fetch_row($result);
        $item_id = $row[0] ?? NULL;
        if (!$item_id) {
          $msg = "Failed to find user {$user} while processing abandoned nextcloud accounts.";
          red_log($msg);
          continue;
        }
        $sql = "UPDATE red_item_nextcloud
          SET nextcloud_abandoned = #status WHERE item_id = #item_id";
        red_sql_query($sql, ['#status' => $status, '#item_id' => $item_id]);
      }
      // Go in opposite direction, see if anyone should be "un-abandoned".
      $sql = "SELECT nextcloud_login, red_item.item_id
        FROM red_item JOIN red_item_nextcloud USING(item_id)
        WHERE nextcloud_abandoned != 0 AND item_status != 'deleted'";
      $result = red_sql_query($sql);
      // Iterate over list of people we think are currently abandoned.
      while ($row = red_sql_fetch_row($result)) {
        $user = $row[0];
        $item_id = $row[1];
        // If this person is no longer in the official list of abandoned accounts...
        if (!in_array($user, $abandoned_users)) {
          // Return them to the living!
          $sql = "UPDATE red_item_nextcloud
            SET nextcloud_abandoned = 0 WHERE item_id = #item_id";
          red_sql_query($sql, ['#item_id' => $item_id]);
        }
      }
    }
  }
}

?>
