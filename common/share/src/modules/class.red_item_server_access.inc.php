<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_server_access')) {
  class red_item_server_access extends red_item {
    var $_server_access_login;
    var $_server_access_public_key;

    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('server_access_login' => array (
                               'req' => true,
                               'pcre'   => RED_LOGIN_MATCHER,
                               'pcre_explanation'   => RED_LOGIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Login name'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'input_type' => 'select',
                               'options' => $this->get_user_account_login_options(),
                               'tblname'   => 'red_item_server_access',
                               'filter' => true),
                             'server_access_public_key' => array (
                               'req' => false,  
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'text',
                               'fname'  => red_t('Public Key (optional)'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'textarea',
                               'textarea_cols' => 50,
                               'textarea_rows' => 5,
                               'tblname'   => 'red_item_server_access')
                                 ));

    }

    function set_server_access_login($value) {
      $this->_server_access_login = $value;
    }
    function get_server_access_login() {
      return $this->_server_access_login;
    }

    function set_server_access_public_key($value) {
      $this->_server_access_public_key = $value;
    }
    function get_server_access_public_key() {
      return $this->_server_access_public_key;
    }

    function additional_validation() {
      if ($this->_disable) {
        return;
      }
      if ($this->_delete) {
        // Note: on old infrastructure, we should stop you from deleting a server access record
        // if it's being used by the web conf. But, we can't. Because on new infrastructure it's
        // the opposite: you can't delete a web configuration item if you don't first delete the
        // server access record. When deleting a hosting order, we have to delete one of them
        // first and trying to have two rules of order seems too complicated. Eventually we won't
        // have annything on old infrastructure, so let's bend the rules here.
        return; 
      }
      $related_user_accounts = $this->get_available_user_account_logins();
      if(!in_array($this->get_server_access_login(),$related_user_accounts)) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }
      // No duplicate records.
      if(!$this->is_field_value_unique('server_access_login','red_item_server_access',$this->get_server_access_login())) {
        $this->set_error(red_t('The login has already been assigned a server access record. You can edit that one to add additional keys.'),'validation');
      }
      // Ensure each lines starts with ssh or sk-ssh
      $pubkey = $this->get_server_access_public_key();
      if ($pubkey) {
        $lines = explode("\n", $pubkey);

        foreach ($lines as $line) {
          $line = trim($line);
          if (empty($line)) {
            continue;
          }
          if (!preg_match('/^(#|ssh|sk)/', $line)) {
            $problem_line = substr($line, 0, 5);
            $this->set_error(red_t('An ssh key must start with either ssh or sk or a comment, your line started with: @problem_line', ['@problem_line' => $problem_line]),'validation');
          }
        }
      }

      // If on new infrastructure, you can only have a server access record if you have an active
      // web configuration.
      if ($this->on_new_infrastructure()) {
        $hosting_order_id = $this->get_hosting_order_id();
        $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_web_conf USING(item_id)
          WHERE hosting_order_id = #hosting_order_id AND item_status NOT IN 
          ('deleted', 'soft-error', 'hard-error')";
        $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
        $row = red_sql_fetch_row($result);
        if ($row[0] != 1) {
          $this->set_error(red_t("Please ensure your web configuration is active before creating any server access accounts."), 'validation');
        }
      }
    }

    function get_user_account_login_options() {
      static $all_logins = [];
      if ($all_logins) return $all_logins;
      $all_logins = $this->get_available_user_account_logins();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT server_access_login 
        FROM red_item_server_access JOIN red_item USING (item_id)
        WHERE
          hosting_order_id = #hosting_order_id AND
          item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        unset($all_logins[$login]);
      }
      return $all_logins;
    }

    static public function ensure_enabled($name, $uid, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/server-access/ensure-enabled',
        $name,
        $uid,
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to ensure enabled for {$name}");
      }
      return TRUE; 
    }

    static public function ensure_disabled($name, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/server-access/ensure-disabled',
        $name,
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to ensure disabled for {$name}");
      }
      return TRUE; 
    }

    static public function ensure_gone($name, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/server-access/ensure-gone',
        $name,
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to ensure gone for {$name}");
      }
      return TRUE; 
    }

    static public function set_password($name, $password, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/server-access/set-password',
        $name,
        base64_encode($password),
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to set password for {$name}");
      }
      return TRUE; 
    }

    static public function set_ssh_authorized_keys($name, $keys, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/server-access/set-ssh-authorized-keys',
        $name,
        $keys,
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to set authorized keys for {$name}");
      }
      return TRUE; 
    }

    static public function save_weborigin_login($name, $item_id, $weborigin, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/server-access/set-weborigin-login',
        $name,
        $item_id,
        $weborigin,
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to set weborigin login for {$name}");
      }
      return TRUE; 
    }
  }  
}

?>
