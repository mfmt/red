<?php

if(!class_exists('red_item_mysql_db')) {
  class red_item_mysql_db extends red_item {
    static public $track_disk_usage = TRUE;
    var $_mysql_db_name;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_illegal_database_name_patterns = [ '^information_schema$', '^performance_schema$', '^mysql$', '^lost\+found$', '^aria_log', '^ibdata', '^ib_logfile', '^ibtmp', '^phpmyadmin$', '^tc\.log$' ];
    var $_item_quota = '1gb';

    function __construct($construction_options) {
      $this->_track_disk_usage = self::$track_disk_usage;
      parent::__construct($construction_options);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
        array (
          'mysql_db_name' => array (
             'req' => true,
             'pcre'   => RED_SQL_DB_MATCHER,
             'pcre_explanation'   => RED_SQL_DB_EXPLANATION,
             'type'  => 'varchar',
             'fname'  => red_t('Database name'),
             'user_insert' => TRUE,
             'user_update' => FALSE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 20,
             'text_max_length' => 64,
             'tblname'   => 'red_item_mysql_db',
             'filter' => true
          ),
        )
      );
      $this->_datafields['item_host']['user_visible'] = TRUE;
    }

    function set_mysql_db_name($value) {
      $this->_mysql_db_name = $value;
    }
    function get_mysql_db_name() {
      return $this->_mysql_db_name;
    }

    public static function get_db_users($db) {
      // The database matcher matches things like:
      // mydb
      // mydb:myotherdb:myyetanotherdb
      $sql = "SELECT mysql_user_name FROM red_item JOIN red_item_mysql_user USING (item_id)
       WHERE item_status != 'deleted' and item_status != 'pending-delete' AND mysql_user_db REGEXP @user_regexp";
      $regexp = '(^|:)' . $db . '(:|$)';
      $result = red_sql_query($sql, ['@user_regexp' => $regexp]);
      $users = [];
      while ($row = red_sql_fetch_row($result)) {
        $users[] = $row[0];
      }
      return $users;

    }
    function additional_validation() {
      if ($this->_delete) {
        // Ensure no mysql users are using this database.
        $users = self::get_db_users($this->get_mysql_db_name());
        if (count($users) > 0) {
          $users_display = implode(',', $users);
          $this->set_error(red_t("This database is assigned permission to one or more MySQL users (@users_display). Please edit the user and remove permission before continuing.", [ '@users_display' => $users_display ]), 'validation');
        }
        // No more validation when deleting.
        return;
      }
      if ($this->get_item_quota() == 0) {
        $this->set_error(red_t('The database quota cannot be set to 0.'),'validation');
      }
      foreach($this->_illegal_database_name_patterns as $pattern) {
        if (preg_match("/$pattern/", $this->get_mysql_db_name())) {
          $this->set_error(red_t('The database name you chose is not allowed.'),'validation');
        }
      }

      if(!$this->_delete && !$this->is_field_value_unique('mysql_db_name','red_item_mysql_db',$this->get_mysql_db_name())) {
        $this->set_error(red_t('The database name you chose is already taken.'),'validation');
      }
    }

    /**
     * Create or delete MySQL database.
     *
     * Set $action to create or delete.
     */
    public static function apply($item_id, $action) {
      $actions = ['create', 'delete'];

      if (!in_array($action, $actions)) {
        throw new RedException("Unknown mysql db action: $action");
      }
      $sql = "SELECT mysql_db_name, item_host
        FROM red_item JOIN red_item_mysql_db USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      if (!$row) {
        throw new RedException("Failed to find mysql db for item id {$item_id}");
      }

      $name = $row[0];
      $server = $row[1];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
      ];

      if ($action == 'delete') {
        $args[] = '/usr/local/share/red/node/share/mysql/ensure-db-gone';
        $args[] = $name;
      }
      else {
        $args[] = '/usr/local/share/red/node/share/mysql/ensure-db-exists';
        $args[] = $name;
      }
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to {$action} mysql item {$item_id}");
      }
      return TRUE;
    }

    public static function save_disk_usage($lines) {
      self::save_disk_usage_for_service('mysql_db', $lines);

      // There is no enforcement of disk quotes in MySQL itself, so we have to do it ourselves
      // by changing all users with access to a database that is overquote to read-only.
      // FIXME: Drupal goes nuts when it can't write to the db. Should we outright disable?
      foreach ($lines as $line) {
        $line_parts = explode(':', $line);
        $name = $line_parts[0] ?? NULL;
        if (!$name) {
          continue;
        }
        $sql = "SELECT red_item.item_id
            FROM red_item JOIN red_item_mysql_db USING (item_id)
            WHERE 
              item_status != 'deleted' AND 
              mysql_db_name = @name AND
              item_disk_usage > item_quota";
        $params = [
          '@name' => $name,
        ];
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        $item_id = $row[0] ?? NULL;
        if (!$item_id) {
          continue;
        }
        // Find all related users.
        $users = self::get_db_users($name);
        foreach ($users as $user) {
          // Get all full priv users.
          $sql = "SELECT red_item.item_id
	    FROM red_item JOIN red_item_mysql_user USING (item_id)
            WHERE item_status != 'deleted' AND mysql_user_name = @user
            AND mysql_user_priv != 'read'";
          $result = red_sql_query($sql, ['@user' => $user]);
          while ($row = red_sql_fetch_row($result)) {
            red_log("{$user} is over quota");
            $item_id = $row[0];
            $options = ['mode' => 'ui', 'id' => $item_id];
            $user_obj = red_item::get_red_object($options);
            $user_obj->set_mysql_user_priv("read");
            if (!$user_obj->validate()) {
              red_log("Failed to validate mysql user when trying to set it to read-only.");
              red_log($user_obj->get_errors_as_string());
              throw new \RedException("There was an error with the quota. Please contact support.");
            }
            if (!$user_obj->commit_to_db()) {
              red_log("Failed to commit to db mysql user when trying to set it to read-only.");
              red_log($user_obj->get_errors_as_string());
              throw new \RedException("There was an error with the quota. Please contact support.");
            }
            // Now manually set it to soft error so we are alerted in the database.
            $sql = "UPDATE red_item SET item_status = 'soft-error' WHERE item_id = #item_id";
            red_sql_query($sql, ['#item_id' => $item_id]);
          }
        }
      }
    }
  }
}

?>
