<?php

if(!class_exists('red_item_mysql_user')) {
  class red_item_mysql_user extends red_item {
    var $_mysql_user_name;
    var $_mysql_user_password;
    var $_mysql_user_db;
    var $_mysql_user_priv;
    var $_mysql_user_max_connections = 25;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_illegal_user_names = [ 'root' ];

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('mysql_user_name' => array (
                               'req' => true,
                               'pcre'   => RED_SQL_USER_MATCHER,
                               'pcre_explanation'   => RED_SQL_USER_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('User Name'),
                               'user_insert' => true,
                               'user_update' => false,
                               'user_visible' => true,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),
                             'mysql_user_password' => array (
                               'req' => true,
                               'pcre'   => RED_TEXT_MATCHER,
                               'pcre_explanation'   => RED_TEXT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Password'),
                               'user_insert' => true,
                               'user_update' => true,
                               'user_visible' => false,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user'),
                             'mysql_user_db' => array (
                               'req' => false,
                               'pcre'   => RED_SQL_DBS_MATCHER,
                               'pcre_explanation'   => RED_SQL_DBS_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Databases'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),
                             'mysql_user_priv' => array (
                               'req' => true,
                               'pcre'   => RED_SQL_PRIV_MATCHER,
                               'pcre_explanation'   => RED_SQL_PRIV_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Type of access'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),
                             'mysql_user_max_connections' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Max Connections'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 4,
                               'text_max_length' => 4,
                               'tblname'   => 'red_item_mysql_user',
                               'filter' => true),

                                 ));


      $this->_datafields['item_host']['user_visible'] = TRUE;
    }

    function set_mysql_user_name($value) {
      $this->_mysql_user_name = $value;
    }
    function get_mysql_user_name() {
      return $this->_mysql_user_name;
    }
    function set_mysql_user_password($value) {
      $this->_mysql_user_password = $value;
    }
    function get_mysql_user_password() {
      return $this->_mysql_user_password;
    }

    function set_mysql_user_db($value) {
      $this->_mysql_user_db = $value;
    }
    function get_mysql_user_db() {
      return $this->_mysql_user_db;
    }

    function get_mysql_user_db_as_array() {
      if (!$this->_mysql_user_db) {
        return [];
      }
      return explode(':', $this->_mysql_user_db);
    }

    function set_mysql_user_priv($value) {
      $this->_mysql_user_priv = $value;
    }
    function get_mysql_user_priv() {
      return $this->_mysql_user_priv;
    }
    function set_mysql_user_max_connections($value) {
      $this->_mysql_user_max_connections = $value;
    }
    function get_mysql_user_max_connections() {
      return $this->_mysql_user_max_connections;
    }

    function additional_validation() {
      if ($this->_delete || $this->_disable) {
        return;
      }
      if (in_array($this->get_mysql_user_name(), $this->_illegal_user_names)) {
        $this->set_error(red_t("Invalid database username."),'validation');
      }

      $dbs = $this->get_mysql_user_db();
      if (empty($dbs)) {
        $this->set_error(red_t("You must select at least one database."), 'validation');
      }
      if (!$this->database_selection_is_valid()) {
        $this->set_error(red_t("Invalid database selected. You can only choose databases that are on the same host."),'validation');
      }
      if (!$this->is_field_value_unique('mysql_user_name','red_item_mysql_user',$this->get_mysql_user_name())) {
        $this->set_error(red_t('The database user name you chose is already taken.'),'validation');
      }
    }

    function get_over_quota_databases() {
      $ret = [];
      // Check if we are providing access to a database that is over quota.
      $user_dbs = $this->get_mysql_user_db_as_array();
      foreach ($user_dbs as $db) {
        $db = trim($db);
        // skip empties
        if(empty($db)) continue;
        $sql = "SELECT item_disk_usage, item_quota FROM red_item JOIN 
          red_item_mysql_db USING(item_id) WHERE item_status != 'deleted' AND 
          mysql_db_name = @db";

        $result = red_sql_query($sql, ['@db' => $db]);
        while($row = red_sql_fetch_row($result)) {
          $disk_usage = $row[0];
          $quota = $row[1];
          if (empty($quota)) {
            // 0 means no quota.
            continue;
          }
          if ($quota < $disk_usage) {
            $ret[] = $db;
          }
        }
      }
      if (count($ret) > 0) {
        return $ret;
      }
      return FALSE;
    }

    function database_selection_is_valid() {
      $user_dbs = $this->get_mysql_user_db_as_array();
      // If it's a new record, don't allow multiple databases on different
      // hosts. But otherwise, allow it in case we are moving from one host 
      // to another.
      $new_record = FALSE;
      if (!$this->exists_in_db()) {
        $new_record = TRUE;
      }
      $db_host = NULL;
      $allowed_dbs = $this->get_all_member_databases();
      $locked_in_db_host = NULL;
      foreach ($user_dbs as $db) {
        $db = trim($db);
        // skip empties (will be caught by previous validation)
        if(empty($db)) continue;

        // This database is not in the list of allowed databases. You can't pick
        // a database not owned by your membership.
        if (!array_key_exists($db, $allowed_dbs)) return FALSE;

        if ($new_record) {
          // New records cannot have multiple database on different hosts.
          if (!$db_host) {
            $db_host = $allowed_dbs[$db];
          }
          if ($db_host != $allowed_dbs[$db]) {
            $this->set_error(red_t("You cannot pick databases on different hosts."), 'validation');
          }
        }
      }
      return TRUE;
    }

    function get_priv_options() {
      return array('full' => 'full', 'read' => 'read');
    }

    function get_all_member_databases() {
      $ret = [];
      $params = ['#member_id' => $this->get_member_id()];
      $sql = "SELECT mysql_db_name, item_host
        FROM red_item JOIN red_item_mysql_db USING(item_id)
          JOIN red_hosting_order USING (hosting_order_id)
        WHERE
          red_hosting_order.member_id = #member_id AND 
          (
            item_status = 'active' OR
            item_status = 'pending-insert' OR
            item_status = 'pending-update' OR
            item_status = 'pending-disable' OR
            item_status = 'disabled'
          )";
      $result = red_sql_query($sql, $params);
      while($row = red_sql_fetch_row($result)) {
        $db = $row[0];
        $host = $row[1];
        $ret[$db] = $host;
      }
      return $ret;
    }

    function get_databases_matching_user_host() {
      $ret = [];
      $params = [
        '#member_id' => $this->get_member_id(),
        '@item_host' => $this->get_item_host(),
      ];
      $sql = "SELECT mysql_db_name, item_host FROM red_item JOIN red_item_mysql_db ".
        "USING(item_id) JOIN red_hosting_order USING(hosting_order_id) ".
        " WHERE red_hosting_order.member_id = #member_id " . 
        " AND item_host = @item_host AND  (item_status = 'active' OR ".
        "item_status = 'pending-insert' OR ".
        "item_status  = 'pending-update' OR item_status = 'disabled' )";
      $result = red_sql_query($sql, $params);
      while($row = red_sql_fetch_row($result)) {
        $db = $row[0];
        $host = $row[1];
        $ret[$db] = $host;
      }
      return $ret;
    }

    function get_available_databases() {
      // For a new user, show all databases owned by this member.
      if (!$this->exists_in_db()) {
        return $this->get_all_member_databases();
      }
      else {
        // For an existing user, only show databases on the same server as the user.
        return $this->get_databases_matching_user_host(); 
      }
    }

    // Override item_host. For mysql users, we have to pick the same
    // host as the database we are configured to use.
    function _pre_commit_to_db() {
      $user_dbs = $this->get_mysql_user_db_as_array();
      $dbs_with_hosts = $this->get_all_member_databases();
      foreach($user_dbs as $db) {
        if (empty($db)) continue;
        // Pick the first chosen database that is not empty and return
        // it's host.
        if (!array_key_exists($db, $dbs_with_hosts)) {
          continue;
        }
        $this->set_item_host($dbs_with_hosts[$db]);
        break;
      }
      return parent::_pre_commit_to_db();
    }

    /**
     * Create, disable or delete MySQL users.
     *
     * Set $action to create, disable or delete.
     */
    public static function apply($item_id, $action) {
      $actions = ['create', 'delete'];

      if (!in_array($action, $actions)) {
        throw new RedException("Unknown mysql user action: $action");
      }
      $sql = "SELECT mysql_user_name, mysql_user_password, mysql_user_priv,
          mysql_user_max_connections, mysql_user_db, item_host
        FROM red_item JOIN red_item_mysql_user USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      if (!$row) {
        throw new RedException("Failed to find mysql user for item id {$item_id}");
      }

      $name = $row[0];
      $password = $row[1];
      $priv = $row[2];
      $max_connections = $row[3];
      $db = $row[4];
      $server = $row[5];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
      ];

      if ($action == 'delete') {
        $args[] = '/usr/local/share/red/node/share/mysql/ensure-user-gone';
        $args[] = $name;
      }
      else {
        $args[] = '/usr/local/share/red/node/share/mysql/ensure-user-exists';
        $args[] = $name;
        $args[] = $password;
        $args[] = $max_connections;
        $args[] = $priv;
        $args[] = $db;
      }
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to {$action} mysql user item {$item_id}");
      }
      return TRUE;
    }
  }  
}

?>
