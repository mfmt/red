<?php

// emailbox - this class is for mainipulating email boxes 
if(!class_exists('red_item_mailbox')) {
  class red_item_mailbox extends red_item {
    static public $track_disk_usage = TRUE;
    # See: https://www.gnu.org/software/mailman/mailman-admin.pdf
    static public $mailman2_email_address_fragments = [
      'join',
      'leave',
      'owner',
      'request',
      'bounces',
      'confirm',
    ];
    var $_mailbox;
    var $_mailbox_login;
    var $_mailbox_auto_response;
    var $_mailbox_auto_response_reply_from;
    var $_mailbox_auto_response_action;
    var $_mailbox_auto_response_action_options;
    var $_mailbox_abandoned;
    var $_mailbox_mountpoint;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_item_quota = '1gb';

    function __construct($co) {
      $this->_track_disk_usage = self::$track_disk_usage;

      $this->_mailbox_auto_response_action_options = array(
        '' => red_t('Do not send auto response'),
        'respond_and_deliver' => red_t('Respond and deliver messages'),
        'respond_only' => red_t('Respond but do not deliver')
      );
      parent::__construct($co);
      $this->_datafields['item_quota']['user_visible'] = TRUE;
      $this->_datafields['item_disk_usage']['user_visible'] = TRUE;

      $this->_set_child_datafields();
      // If it's a new item, we have to autoset the mountopint if one is
      // specified in red_server for this item's host.
      if (!$this->exists_in_db()) {
        $this->set_mailbox_mountpoint($this->get_mountpoint_for_host());
      }
      
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields,
        array (
          'mailbox_login' => array (
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('User account login'),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => $this->get_user_account_login_options(),
            'text_length' => 20,
            'text_max_length' => 128,
            'tblname'   => 'red_item_mailbox',
            'filter' => true
          ),
          'mailbox_auto_response_action' => array (
            'req' => false,  
            'pcre'   => RED_AUTO_RESPONSE_ACTION_MATCHER,
            'pcre_explanation'   => RED_AUTO_RESPONSE_ACTION_EXPLANATION,
            'type'  => 'varchar',
            'input_type' => 'select',
            'options' => $this->_mailbox_auto_response_action_options,
            'fname'  => red_t('Auto Responder Action'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_mailbox'
          ),
          'mailbox_auto_response' => array (
            'req' => false,  
            'pcre'   => RED_ANYTHING_MATCHER,
            'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
            'type'  => 'text',
            'fname'  => red_t('Auto Responder Message'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'textarea',
            'textarea_cols' => 50,
            'textarea_rows' => 5,
            'tblname'   => 'red_item_mailbox'
          ),
          'mailbox_auto_response_reply_from' => array (
            'req' => false,  
            'pcre'   => RED_EMAIL_MATCHER,
            'pcre_explanation'   => RED_EMAIL_EXPLANATION,
            'type'  => 'text',
            'fname'  => red_t('Auto Responder Reply From Address'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'tblname'   => 'red_item_mailbox'
          ),
          'mailbox_mountpoint' => array (
            'req' => false,  
            'pcre'   => RED_ANYTHING_MATCHER,
            'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Mount Point'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 8,
            'text_max_length' => 8,
            'tblname'   => 'red_item_mailbox',
            'filter' => false
          ),
          'mailbox_abandoned' => array (
            'req' => false,  
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Abandoned mailbox'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 1,
            'text_max_length' => 1,
            'tblname'   => 'red_item_mailbox',
            'filter' => false
         ),
        )
      );
    }

    function is_abandoned() {
      if ($this->get_mailbox_abandoned() == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }

    // We have to Override. If we are not on a mailstore server, we don't set
    // the quota and we don't track usage.
    function set_item_host($value) {
      parent::set_item_host($value);
      if (!preg_match('/^mailstore/', $this->get_item_host())) {
        $this->set_item_quota(0);
        $this->_track_disk_usage = FALSE;
      }
    }
    function set_mailbox_login($value) {
      $this->_mailbox_login = strtolower($value);
    }
    function get_mailbox_login() {
      return $this->_mailbox_login;
    }

    function set_mailbox_mountpoint($value) {
      $this->_mailbox_mountpoint = $value;
    }
    function get_mailbox_mountpoint() {
      return $this->_mailbox_mountpoint;
    }
    function set_mailbox_auto_response($value) {
      $this->_mailbox_auto_response = $value;
    }
    function get_mailbox_auto_response() {
      return $this->_mailbox_auto_response;
    }
    function set_mailbox_auto_response_action($value) {
      $this->_mailbox_auto_response_action = $value;
    }
    function get_mailbox_auto_response_action() {
      return $this->_mailbox_auto_response_action;
    }
    function set_mailbox_auto_response_reply_from($value) {
      $this->_mailbox_auto_response_reply_from = $value;
    }
    function get_mailbox_auto_response_reply_from() {
      return $this->_mailbox_auto_response_reply_from;
    }
    function set_mailbox_abandoned($value) {
      $this->_mailbox_abandoned = $value;
    }
    function get_mailbox_abandoned() {
      return $this->_mailbox_abandoned;
    }

    function additional_validation() {
      if ($this->_delete) {
        // Ensure this mailbox is not being used in an email address. We allow the deletion of
        // the mailbox is owned by another member to ease the transition into this new validation
        // rule.
        $login = $this->get_mailbox_login();
        $member_id = $this->get_member_id();
        $sql = "SELECT COUNT(*) AS count FROM red_item JOIN red_item_email_address USING (item_id)
          JOIN red_hosting_order USING(hosting_order_id)
          WHERE item_status != 'deleted' AND item_status != 'pending-delete' AND email_address_recipient
          REGEXP @login_regexp AND red_hosting_order.member_id = #member_id";
        $result = red_sql_query($sql, [
            '@login_regexp' => "(^|,){$login}@mail.mayfirst.org", 
            '#member_id' => $member_id
          ]
        );
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t("This mailbox is in use in one of your email addresses. Please remove it from the email address before deleting."),'validation');
        }
        return true;
      } 
      if ($this->_disable) {
        $login = $this->get_mailbox_login();
        $member_id = $this->get_member_id();
        $sql = "SELECT COUNT(*) AS count FROM red_item JOIN red_item_email_address USING (item_id)
          JOIN red_hosting_order USING(hosting_order_id)
          WHERE item_status = 'active' AND email_address_recipient
          REGEXP @login_regexp AND red_hosting_order.member_id = #member_id";
        $result = red_sql_query($sql, [
            '@login_regexp' => "(^|,){$login}@mail.mayfirst.org", 
            '#member_id' => $member_id
          ]
        );
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t("This mailbox is in use in one of your email addresses. Please remove it from the email address before disabling."),'validation');
        }
        return true;
      }

      $related_user_accounts = $this->get_available_user_account_logins();
      if(!in_array($this->get_mailbox_login(),$related_user_accounts)) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }

      // Make sure login is unique
      if(!$this->is_field_value_unique('mailbox_login','red_item_mailbox',$this->get_mailbox_login())) {
        $this->set_error(red_t('The login has already been assigned a mailbox.'),'validation');
      }
      // If auto response is set, make sure auto_response_from_email
      // is also set
      $action = $this->get_mailbox_auto_response_action();
      $from = $this->get_mailbox_auto_response_reply_from();
      $message = $this->get_mailbox_auto_response();
      if(!empty($action)) {
        if(empty($from)) {
          $this->set_error(red_t('You must specify the email address you want your auto response to be sent from.'),'validation');
        }
        if(empty($message)) {
          $this->set_error(red_t('You must specify the auto response message that you want sent.'),'validation');
        }
      }
    }

    function get_user_account_login_options() {
      static $all_logins = [];
      if ($all_logins) return $all_logins;
      $all_logins = $this->get_available_user_account_logins();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT mailbox_login 
        FROM red_item_mailbox JOIN red_item USING (item_id)
        WHERE
          hosting_order_id = #hosting_order_id AND
          item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        unset($all_logins[$login]);
      }
      return $all_logins;
    }

    public static function webmail_purge_user($login, $webmail) {
      global $globals;

      $allowed_webmail = ['roundcube', 'horde'];
      if (!in_array($webmail, $allowed_webmail)) {
        throw new \RedException("Invalid webmail: {$webmail}");
      }
      $user_key = "{$webmail}_user";
      $server_key = "{$webmail}_server";

      $user = $globals['config'][$user_key] ?? NULL;
      $server = $globals['config'][$server_key] ?? NULL;

      if (!$user) {
        throw new \RedException("Missing user key: @user_key in configuration file.");
      }
      if (!$server) {
        throw new \RedException("Missing server key: @server_key in configuration file.");
      }

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        "-u",
        $user,
        "/usr/local/share/red/node/share/mailbox-webmail/{$webmail}-purge-user",
        $login,
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to purge webmail user: " . implode(' ', $output));
      }
      return TRUE;
    }
    public static function rebuild_email_databases() {
      global $globals;
      $config = $globals['config'];
      $domains = self::generate_domains_file();
      $relay_recipient_maps = self::generate_relay_recipient_maps_file();
      $virtual_alias_maps = self::generate_virtual_alias_maps_file();
      $transport_maps = self::generate_transport_maps_file();
      $over_quota_mailboxes = self::generate_over_quota_mailboxes_file();
      $sender_login_map = self::generate_sender_login_map_file();

      $error = NULL;
      if (!$domains || !$relay_recipient_maps || !$virtual_alias_maps || !$transport_maps) {
        $error = "Failed to generate one of the map files";
      }

      if (is_null($error)) {
        // MX servers.
        foreach($config['mx_servers'] as $server) {
          if (!red_item_mailbox::rsync_and_postmap($domains, 'relay-domains', $server)) {
            $error = "Failed to rsync and postmap relay-domains to {$server}";
            break;
          }
          if (!red_item_mailbox::rsync_and_postmap($relay_recipient_maps, 'relay-recipient-maps', $server)) {
            $error = "Failed to rsync and postmap relay-recipient-maps to {$server}";
            break;
          }
          if (!red_item_mailbox::rsync_and_postmap($over_quota_mailboxes, 'reject-over-quota-mailboxes', $server)) {
            $error = "Failed to rsync and postmap reject-over-quota-mailboxes to {$server}";
            break;
          }
        }
      }
      if (is_null($error)) {
        foreach($config['filter_servers'] as $server) {
          if (!red_item_mailbox::rsync_and_postmap($domains, 'virtual-alias-domains', $server)) {
            $error = "Failed to rsync and postmap virtual-alias-domains to {$server}";
            break;
          }
          if (!red_item_mailbox::rsync_and_postmap($virtual_alias_maps, 'virtual-alias-maps', $server)) {
            $error = "Failed to rsync and postmap virtual-alias-maps to {$server}";
            break;
          }
          if (!red_item_mailbox::rsync_and_postmap($transport_maps, 'transport-maps', $server)) {
            $error = "Failed to rsync and postmap transport-maps to {$server}";
            break;
          }
        }
      }

      if (is_null($error)) {
        // NOTE: we add/remote/edit users from the proxy database in a separate process - see
        // the remote_tasks function of the email address ui class.
        foreach($config['cf_servers'] as $server) {
          if (!red_item_mailbox::rsync_and_postmap($over_quota_mailboxes, 'reject-over-quota-mailboxes', $server)) {
            $error = "Failed to rsync and postmap reject-over-quota-mailboxes to {$server}";
            break;
          }
          if (!red_item_mailbox::rsync_and_postmap($sender_login_map, 'sender-login-map', $server)) {
            $error = "Failed to rsync and postmap sender-login-map to {$server}";
            break;
          }
        }
      }
      unlink($domains);
      unlink($relay_recipient_maps);
      unlink($virtual_alias_maps);
      unlink($transport_maps);
      unlink($over_quota_mailboxes);

      if ($error) {
        throw new RedException($error);
      }
      return TRUE;
    }

    // Used by both MX (relay_domains) and filter servers (virtual_alias_domains) -
    // a list of all email domains that we accept email on behalf of.
    public static function generate_domains_file() {
      $sql = "SELECT DISTINCT(SUBSTRING_INDEX(email_address, '@', -1))
        FROM red_item JOIN red_item_email_address USING (item_id)
        WHERE item_status != 'deleted' AND item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed the generate domains file query: $sql.");
      }
      $lines = [];
      while ($row = red_sql_fetch_row($result)) {
        $domain = trim($row[0]);
        if (!red_domain_is_verified($domain)) {
          continue;
        }
        $line = "{$domain} -";
        if (!in_array($line, $lines)) {
          $lines[] = $line;
        }
      }

      // Now get email list domain names.
      $sql = "SELECT DISTINCT(list_domain)
        FROM red_item JOIN red_item_list USING (item_id)
        WHERE item_status != 'deleted' AND item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed the generate list domains file query: $sql.");
      }
      while ($row = red_sql_fetch_row($result)) {
        $domain = trim($row[0]);
        if (red_domain_is_verified($domain)) {
          $line = "{$domain} -";
          if (!in_array($line, $lines)) {
            $lines[] = $line;
          }
        }
      }

      $tld = red_get_top_level_domain();
      if (!in_array("mail.mayfirst.{$tld} -", $lines)) {
        $lines[] = "mail.mayfirst.{$tld} -";
      }
      $file = tempnam('/tmp', 'relay-domains');
      file_put_contents($file, implode("\n", $lines) . "\n");
      return $file;
    }

    # Used on the filter servers. This is a transformation map - it
    # lists all email addresses that should be transformed or forwarded
    # to other email addresses. For email that we ultimately accept, the
    # email address is transformed into user_account@host.
    public static function generate_virtual_alias_maps_file() {
      // Handle email addresses.
      $sql = "SELECT email_address, email_address_recipient
        FROM red_item JOIN red_item_email_address USING (item_id) 
        WHERE 
          item_status != 'deleted' AND
          item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed to generate virtual_alias_maps query 1: $sql.");
      }
      $maps = [];
      $tld = red_get_top_level_domain();
      while ($row = red_sql_fetch_row($result)) {
        $email_address = trim($row[0]);
        $recipient = trim($row[1]);
        $verified = red_email_domain_is_verified($email_address);
        if ($verified) {
          $maps[] = "{$email_address} {$recipient}";
        }
      }

      // Handle mailboxes for email we accept for delivery.
      $sql = "
        SELECT mailbox_login, item_host
          FROM red_item JOIN red_item_mailbox USING (item_id)
          WHERE 
            item_status != 'deleted' AND 
            item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed to generate virtual_alias_maps query 2: $sql.");
      }
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        $host = $row[1];
        $maps[] = "{$login}@mail.mayfirst.{$tld} {$login}@{$host}";
      }

      // Handle email lists.
      $sql = "
        SELECT list_name, list_domain, item_host
          FROM red_item JOIN red_item_list USING (item_id)
          WHERE
            item_status != 'deleted' AND
            item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed to generate virtual_alias_maps query 3: $sql.");
      }
      while ($row = red_sql_fetch_row($result)) {
        $name = $row[0];
        $domain = $row[1];
        $host = $row[2];
        if (red_domain_is_verified($domain)) {
          $maps[] = "{$name}@{$domain} {$name}@{$host}";
          foreach(red_item_mailbox::$mailman2_email_address_fragments as $fragment) {
            $maps[] = "{$name}-{$fragment}@{$domain} {$name}-{$fragment}@{$host}";
          }
        }
      }

      $file = tempnam('/tmp', 'virtual-alias-maps');
      file_put_contents($file, implode("\n", $maps) . "\n");
      return $file;
    }

    // Used by MX servers - a complete list of all email addresses that we
    // accept email for.
    public static function generate_relay_recipient_maps_file() {
      $sql = "SELECT email_address 
        FROM red_item JOIN red_item_email_address USING (item_id)
        WHERE item_status != 'deleted' AND item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed the generate relay recipients_maps query: $sql.");
      }

      $lines = [];
      while ($row = red_sql_fetch_row($result)) {
        $email = trim($row[0]);
        if (red_email_domain_is_verified($email)) {
          $line = "{$email} -";
          if (!in_array($line, $lines)) {
            $lines[] = $line;
          }
        }
      }

      // Now add @mail.mayfirst.org addresses. 
      $sql = "SELECT mailbox_login FROM red_item JOIN red_item_mailbox
        USING (item_id) WHERE item_status != 'deleted' AND item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed the generate relay recipients_maps query: $sql.");
      }

      $tld = red_get_top_level_domain();
      while ($row = red_sql_fetch_row($result)) {
        $lines[] = trim($row[0]) . '@mail.mayfirst.' . $tld . ' -';
      }

      // Handle email lists.
      $sql = "
        SELECT list_name, list_domain
          FROM red_item JOIN red_item_list USING (item_id)
          WHERE 
            item_status != 'deleted' AND 
            item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed to generate virtual_alias_maps query 3: $sql.");
      }
      while ($row = red_sql_fetch_row($result)) {
        $name = $row[0];
        $domain = $row[1];
        if (red_domain_is_verified($domain)) {
          $lines[] = "{$name}@{$domain} -";
          foreach(red_item_mailbox::$mailman2_email_address_fragments as $fragment) {
            $lines[] = "{$name}-{$fragment}@{$domain} -";
          }
        }
      }
      $file = tempnam('/tmp', 'relay-recipient-maps');
      file_put_contents($file, implode("\n", $lines) . "\n");
      return $file;
    }

    // This is a list of hosts - so any email address that ends in one of our
    // @hosts addresses is delivered directly to the host. All others will be relayed
    // out of our network.
    // And also, we include email lists so that each lists is delivered to the correct
    // host.
    public static function generate_transport_maps_file() {
      $sql = "SELECT DISTINCT(item_host)
        FROM red_item JOIN red_item_mailbox USING (item_id)
        WHERE
          item_status != 'deleted' AND
          item_status != 'disabled'";

      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed the generate transport_maps query: $sql.");
      }
      $maps = [];
      while ($row = red_sql_fetch_row($result)) {
        $host = trim($row[0]);
        $maps[] = "{$host} smtp:{$host}";
      }

      // Handle lists.
      $sql = "SELECT DISTINCT item_host FROM red_item JOIN red_item_list
        USING (item_id) WHERE item_status != 'deleted' and item_status != 'disabled'";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed the generate transport_maps query for lists: $sql.");
      }
      while ($row = red_sql_fetch_row($result)) {
        $host = trim($row[0]);
        $maps[] = "{$host} smtp:{$host}";
      }
      $file = tempnam('/tmp', 'transport-maps');
      file_put_contents($file, implode("\n", $maps) . "\n");
      return $file;
    }

    public static function get_sender_login_map(): array {
      $map = [];
      $sql = "SELECT DISTINCT red_member.member_id, dns_fqdn
          FROM 
            red_member
              JOIN red_hosting_order USING (member_id)
              JOIN red_item USING (hosting_order_id) 
              JOIN red_item_dns USING (item_id) 
         WHERE
          member_status = 'active' AND
          hosting_order_status = 'active' AND
          item_status != 'deleted' AND 
          item_status != 'disabled' AND 
          dns_type = 'mx'";
      $result = red_sql_query($sql);
      while ($row = red_sql_fetch_row($result)) {
        $member_id = $row[0];
        $fqdn = $row[1];
        if (!array_key_exists($fqdn, $map)) {
          $map[$fqdn] = [];
        }
        $sql = "SELECT mailbox_login
          FROM
            red_hosting_order JOIN
            red_item USING(hosting_order_id) JOIN
            red_item_mailbox USING (item_id)
          WHERE 
            red_hosting_order.member_id = #member_id AND
            item_status = 'active'";
        $map_result = red_sql_query($sql, ['#member_id' => $member_id]);
        while ($map_row = red_sql_fetch_row($map_result)) {
          $map[$fqdn][] = $map_row[0];
        }
      }
      return $map;
    }

    // Used by CF servers - a complete list of user accounts and the domains they
    // are allowed to send email from.
    public static function generate_sender_login_map_file(): string {
      $map = self::get_sender_login_map();
      $content = '# This file is automatically generated by the control panel.';
      foreach ($map as $domain => $logins) {
        if (empty($logins)) {
          continue;
        }
        $content .= "{$domain} " . implode(",", $logins) . "\n";
      }
      $file = tempnam('/tmp', 'sender-login-map');
      file_put_contents($file, $content);
      return $file;
    }

    // Used by MX servers and CF servers - a complete list of all email addresses
    // that are over quota. Technically it's user accounts that are over quota but
    // we have to lookup the email addresses they are assigned to.
    public static function generate_over_quota_mailboxes_file() {
      $emails = self::get_recursive_list_of_email_addresses_matching_user_account_filter('over-quota');
      // Now we have a list of email addresses that are over quota.
      $action = " REJECT Delivery refused, this email address is over quota.\n";
      $content = '';
      if (count($emails) > 0) {
        $content = implode($action, $emails);
        $content .= $action;
      }
      else {
        // To avoid catastrophe, our system refuses to put an empty file in place.
        // But, this file *might* be empty, so add a comment to the top.
        $content = "# No over quota email addresses in control panel.\n";
      }

      $file = tempnam('/tmp', 'reject-over-quota-mailboxes');
      file_put_contents($file, $content);
      return $file;
    }

    public static function get_recursive_list_of_email_addresses_matching_user_account_filter($filter) {
      // This gets recursive. In other words, the mailbox login joe is assigned as
      // recipient for the email address joe@example.org. And, the email address
      // admin@example.org is assigned the recipient joe@example.org. And the email
      // address billing@example.org is assigned the recipient admin@example.org.
      // If joe is abandoned/over quota, we want a list of email addresses that
      // include all of those. To avoid an endless loop, we detect when we have a
      // duplicate and break and log.
      
      $allowed_filters = ['abandoned', 'over-quota'];
      if (!in_array($filter, $allowed_filters)) {
        throw new RedException("$filter is not allowed.");
      }

      // First generate a list of logins we want to block.
      $logins = [];
      $user_sqls = [];
      if ($filter == 'abandoned') {
        $user_sqls[] = "SELECT DISTINCT mailbox_login 
          FROM 
            red_item_mailbox JOIN 
            red_item USING(item_id) 
          WHERE
            mailbox_abandoned = 1 AND
            item_status != 'disabled' AND
            item_status != 'deleted'";
      }
      else {
        // Until we complete the email transition, we have to check both the
        // user account and mailbox table for potentially over quota mailboxes. 
        $user_sqls[] = "SELECT DISTINCT mailbox_login 
          FROM 
            red_item_mailbox JOIN 
            red_item USING(item_id) 
          WHERE
            item_quota != 0 AND
            item_disk_usage >= item_quota AND
            item_status != 'disabled' AND
            item_status != 'deleted'";

        // When checking user accounts, we only want user accounts linked to mailboxes
        // AND we only want them if the mailbox quota is zero indicating that they are
        // still on the old infrastructure. Lastly, we add 50K to avoid a situation where
        // you are technically not over quota but most email messages will bounce.
        $user_sqls[] = "SELECT DISTINCT user_account_login
          FROM 
            red_item riua JOIN red_item_user_account ua 
              ON riua.item_id = ua.item_id
            JOIN red_item_mailbox m
              ON  m.mailbox_login = ua.user_account_login
            JOIN red_item rim
              ON m.item_id = rim.item_id 
          WHERE  
            rim.item_quota = 0 AND
            rim.item_disk_usage = 0 AND
            riua.item_quota != 0 AND
            riua.item_disk_usage != 0 AND
            riua.item_status != 'disabled' AND
            riua.item_status != 'deleted' AND
            rim.item_status != 'disabled' AND
            rim.item_status != 'deleted' AND
            riua.item_disk_usage + 50000 >= riua.item_quota
        ";

      }

      foreach($user_sqls as $sql) {
        $result = red_sql_query($sql);
        if (!$result) {
          throw new RedException("Failed recursive email query 1: $sql.");
        }
        while ($row = red_sql_fetch_row($result)) {
          $login = $row[0];
          if (!in_array($login, $logins)) {
            $logins[] = $login;
          }
        }
      }

      // Now that we have a list of logins, we have to match every
      // possible email address that could delivery to this login.
      $emails = [];
      foreach ($logins as $login) {
        $email = "{$login}@mail.mayfirst.org";
        $emails[] = $email;
        $recursed_emails = self::fetch_email_addresses_for_recipient($email);
        while (count($recursed_emails) > 0) {
          $email = array_pop($recursed_emails);
          if (in_array($email, $emails)) {
            // Uh oh. We may have a loop.
            red_log("Loop detected with email @email.", [ '@email' => $email ]);
            break;
          }
          $emails[] = $email;
          $new_recursed_emails = self::fetch_email_addresses_for_recipient($email);
          if (count($new_recursed_emails) > 0) {
            $recursed_emails = array_merge($recursed_emails, $new_recursed_emails);
          }
        }
      }
      return $emails;
    }

    // A helper function - return all email addresses for a given recipient.
    public static function fetch_email_addresses_for_recipient($email) {
      $emails = [];
      $sql = "SELECT DISTINCT(email_address) FROM red_item JOIN red_item_email_address
        USING(item_id) WHERE item_status != 'deleted' AND item_status != 'disabled' 
        AND email_address_recipient = @email";
      $result = red_sql_query($sql, ['@email' => $email]);
      if (!$result) {
        throw new RedException("Failed to fetchemail address for recipient: $sql.");
      }
      while ($row = red_sql_fetch_row($result)) {
        $emails[] = $row[0];
      }
      return $emails;
    }

    public static function modify_dovecot_proxy_user($action, $login, $host) {
      global $globals;
      $config = $globals['config'];
      foreach($config['cf_servers'] as $server) {
        $cmd = "/usr/bin/ssh";
        $args = [
          "red-remote-tasker@{$server}",
          "sudo",
          "/usr/local/share/red/node/share/mailbox/proxy-user",
          $action,
          $login,
          $host,
        ];
        if (red_fork_exec_wait($cmd, $args) != 0) {
          throw new RedException("Failed to run proxy-user on '$server'");
          return FALSE;
        }
      }
      return TRUE;
    }

    public static function ensure($item_id, $status) {
      $statuses = ['gone', 'disabled', 'enabled'];
      if (!in_array($status, $statuses)) {
        throw new RedException("Unknown status: {$status}");
      }
      $sql = "SELECT mailbox_login, mailbox_mountpoint, item_host
        FROM red_item JOIN red_item_mailbox USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $login = $row[0];
      $mountpoint = $row[1];
      $server = $row[2];

      $args = [
        "red-remote-tasker@{$server}",
        "sudo"
      ];
      if ($status == 'enabled') {
        $args[] = '/usr/local/share/red/node/share/mailbox/ensure-enabled';
        $args[] = $login;
        $args[] = $mountpoint;
      }
      else if ($status == 'disabled') {
        $args[] = '/usr/local/share/red/node/share/mailbox/ensure-disabled';
        $args[] = $login;
      }
      else {
        $args[] = '/usr/local/share/red/node/share/mailbox/ensure-gone';
        $args[] = $login;
        $args[] = $mountpoint;
      }
      
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure mailbox {$status} for item {$item_id}");
      }
      return TRUE;
    }

    public static function set_password($item_id) {
      $sql = "SELECT mailbox_login, item_host
        FROM red_item JOIN red_item_mailbox USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $login = $row[0];
      $server = $row[1];

      // Get the password.
      $sql = "SELECT user_account_password 
        FROM red_item JOIN red_item_user_account USING (item_id) 
        WHERE item_status != 'deleted' AND user_account_login = @login";
      $result = red_sql_query($sql, ['@login' => $login]);
      $row = red_sql_fetch_row($result);
      $password = base64_encode($row[0]);

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/mailbox/set-password',
        $login,
        $password,
      ];

      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to set password for mailbox with item id {$item_id}");
      }
      return TRUE;
    }

    public static function set_quota($item_id) {
      $sql = "SELECT mailbox_login, mailbox_mountpoint, item_quota, item_host
        FROM red_item JOIN red_item_mailbox USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $login = $row[0];
      $mountpoint = $row[1];
      $quota = $row[2];
      $server = $row[3];

      // Quotas are set in 1024 byte blocks - so we have to divide bytes by 1024
      $quota_hard = ceil(intval($quota) / 1024);

      // By default ext4 assigns 1 inode for every 15 KB - so let's use that ratio
      $inodes_hard = ceil($quota / 15);

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/mailbox/set-quota',
        $login,
        $mountpoint,
        $quota_hard,
        $inodes_hard,
      ];

      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to set quota for mailbox with item id {$item_id}");
      }
      return TRUE;
    }

    public static function update_auto_responder($item_id) {
      $sql = "SELECT mailbox_login, mailbox_auto_response,
        mailbox_auto_response_reply_from, mailbox_auto_response_action, item_host
        FROM red_item JOIN red_item_mailbox USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $login = $row[0];
      $message = base64_encode($row[1]);
      $from = $row[2];
      $action = $row[3];
      $server = $row[4];

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/mailbox/update-auto-responder',
        $login,
        $action
      ];

      // Only add from and message if we have an action.
      if ($action) {
        $args[] = $from;
        $args[] = $message;
      }

      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to set auto responder for mailbox with item id {$item_id}");
      }
      return TRUE;
    }
    public static function save_disk_usage($lines) {
      self::save_disk_usage_for_service('mailbox', $lines);
    }

    public static function process_abandoned() {
      // Select the mailbox servers so we know what servers to query
      // for abandoned accounts.
      $sql = "SELECT server FROM red_server WHERE
        server_dedicated_mail = 1";
      $result = red_sql_query($sql);
      while ($row = red_sql_fetch_row($result)) {
        $server = $row[0];

        // First get a report on abandoned records.
        $args = [
          "red-remote-tasker@{$server}",
          "sudo",
          '/usr/local/share/red/node/share/mailbox/process-abandoned',
        ];
        if (!red_ssh_cmd($args, $output, $rc)) {
          $msg = "Failed to ssh to {$server} to get list of abandoned accounts.";
          throw new RedException($msg);
        }
        if ($rc !== 0) {
          $msg = "Failed to ssh to {$server} to get list of abandoned accounts. ".
            "Got rc {$rc} and output: " . implode(' ', $output);
          throw new RedException($msg);
        }
        // Build an array keyed to the user so we can also check for previously
        // abandoned users that should not be abandoned any more.
        $abandoned_users = [];
        foreach ($output as $line) {
          $parts = explode(':', $line);
          $user = $parts[0] ?? NULL;
          $status = $parts[1] ?? NULL;
          if (!$user) {
            $msg = "Failed to extract user from {$line} while processing ".
              "abandoned nextcloud accounts.";
            red_log($msg);
            continue;
          }
          $abandoned_users[] = $user;
          $sql = "SELECT red_item.item_id
            FROM red_item JOIN red_item_mailbox USING (item_id)
            WHERE mailbox_login = @user AND item_status != 'deleted'";
          $result = red_sql_query($sql, ['@user' => $user]);
          $row = red_sql_fetch_row($result);
          $item_id = $row[0] ?? NULL;
          if (!$item_id) {
            $msg = "Failed to find user {$user} while processing abandoned mailbox accounts.";
            red_log($msg);
            continue;
          }
          $sql = "UPDATE red_item_mailbox
            SET mailbox_abandoned = #status WHERE item_id = #item_id";
          red_sql_query($sql, ['#status' => $status, '#item_id' => $item_id]);
        }
        // Go in opposite direction, see if anyone should be "un-abandoned".
        $sql = "SELECT mailbox_login, red_item.item_id
          FROM red_item JOIN red_item_mailbox USING(item_id)
          WHERE mailbox_abandoned != 0 AND item_status != 'deleted'";
        $result = red_sql_query($sql);
        // Iterate over list of people we think are currently abandoned.
        while ($row = red_sql_fetch_row($result)) {
          $user = $row[0];
          $item_id = $row[1];
          // If this person is no longer in the official list of abandoned accounts...
          if (!in_array($user, $abandoned_users)) {
            // Return them to the living!
            $sql = "UPDATE red_item_mailbox
              SET mailbox_abandoned = 0 WHERE item_id = #item_id";
            red_sql_query($sql, ['#item_id' => $item_id]);
          }
        }
      }
    }
    public static function rsync_and_postmap($file, $target, $server) {
      $key = "postfix.{$target}.{$server}";
      if (red_file_in_sync($key, $file)) {
        return TRUE;
      }
      $dest = "red-remote-tasker@{$server}:{$file}";
      if (!red_rsync($file, $dest)) {
        return FALSE;
      }
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/mailbox/copy-and-postmap',
        $file,
        $target
      ];
      if (!red_ssh_cmd($args)) {
        return FALSE;
      }
      red_update_file_sync($key, $file);
      return TRUE;
    }
    static public function rebuild_unverified_domains_database() {
      global $globals;
      $config = $globals['config'];

      $domains = self::generate_unverified_domains_file();
      $defer_unverified = self::generate_defer_unverified_domains_file();

      $error = NULL;
      if (!$domains || !$defer_unverified) {
        throw new RedException("Failed to generate one of the map files'");
        return FALSE;
      }

      // MX servers.
      foreach($config['mx_servers'] as $server) {
        if (!red_item_mailbox::rsync_and_postmap($domains, 'virtual-alias-domains', $server)) {
          $error = "Failed to rsync and postmap virtual-alias-domains ({$domains}) to {$server}";
        }
        if (!red_item_mailbox::rsync_and_postmap($defer_unverified, 'defer-unverified-domains', $server)) {
          $error = "Failed to rsync and postmap defer-unverified-domains ({$defer_unverified}) to {$server}";
        }
      }
      unlink($domains);
      unlink($defer_unverified);
      if ($error) {
        throw new RedException($error);
      }
      return TRUE;
    }

    // Used by MX servers to find domains that need to be verified. 
    // This file will be used as the virtual-alias-domains file
    // to ensure we accept email from them (even though we will
    // defer it at the next step).
    public static function generate_unverified_domains_file() {
      $domains = self::get_unverified_domains();

      $lines = [];
      foreach($domains as $domain) {
        $lines[] = trim($domain) . " -";
      }
      $file = tempnam('/tmp', 'unverified-domains');
      file_put_contents($file, implode("\n", $lines) . "\n");
      return $file;
    }

    // Used by MX servers to set aliases so verification emails will work
    // even if the domain is not yet verified (and all other emails will
    // be deferred).
    public static function generate_defer_unverified_domains_file() {
      $domains = self::get_unverified_domains();
      $lines = [];
      foreach($domains as $domain) {
        $lines[] = "mayfirst-verify-mx-domain@{$domain}	OK";
        $lines[] = "{$domain}	DEFER This domain is no yet verified";
      }
      $file = tempnam('/tmp', 'defer-unverified-domains');
      file_put_contents($file, implode("\n", $lines) . "\n");
      return $file;
    }

    // List of MX server domains that are using our MX servers and are not
    // yet verified.
    public static function get_unverified_domains() {
      $sql = "SELECT DISTINCT(dns_fqdn) FROM red_item JOIN red_item_dns USING (item_id)
        WHERE 
          item_status != 'deleted' AND 
          item_status != 'disabled' AND 
          dns_mx_verified = 0 AND
          dns_type = 'mx' AND
          dns_server_name REGEXP '[abcd].mx.mayfirst.(org|dev)'
      ";
      $result = red_sql_query($sql);
      if (!$result) {
        throw new RedException("Failed to query for unverified domains: $sql.");
      }
      $domains = [];
      while ($row = red_sql_fetch_row($result)) {
        $domains[] = trim($row[0]);
      }
      return $domains;
    }
  }
}



?>
