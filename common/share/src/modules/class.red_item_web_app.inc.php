<?php

if(!class_exists('red_item_web_app')) {
  class red_item_web_app extends red_item {
    var $_web_app_name;
    var $_web_app_update = 'core';
    var $_web_app_install_ip = NULL;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_available_web_app_names = [ 
      'wordpress' => 'WordPress',
      'backdrop' => 'Backdrop',
    ];
    var $_available_web_app_updates;

    function __construct($construction_options) {
      parent::__construct($construction_options);
      $this->_available_web_app_updates = [
        'core' => red_t('Please apply updates to the core code and all plugins and themes automatically'),
        'none' => red_t('No updates, I take full responsibility for the security of this application'),
      ];
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array ('web_app_name' => array (
                               'req' => true,
                               'pcre'   => RED_WEB_APP_MATCHER,
                               'pcre_explanation'   => RED_WEB_APP_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Application to install'),
                               'user_insert' => TRUE,
                               'user_update' => FALSE,
                               'user_visible' => TRUE,
                               'input_type' => 'select',
                               'text_length' => 20,
                               'text_max_length' => 64,
                               'tblname'   => 'red_item_web_app',
                               'options' => $this->_available_web_app_names,
                               'filter' => true),
                             'web_app_update' => array (
                               'req' => TRUE,
                               'pcre'   => RED_WEB_APP_UPDATE_MATCHER,
                               'pcre_explanation'   => RED_WEB_APP_UPDATE_EXPLANATION,
                               'type'  => 'varchar',
                               'input_type' => 'select',
                               'fname'  => red_t('Security Updates'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'options' => $this->_available_web_app_updates,
                               'tblname'   => 'red_item_web_app',
                               'filter' => true),
                              'web_app_install_ip' => array (
                               'req' => FALSE,
                               'pcre'   => RED_IP_MATCHER,
                               'pcre_explanation'   => RED_IP_EXPLANATION,
                               'type'  => 'varchar',
                               'input_type' => 'text',
                               'fname'  => red_t('Restrict installation by IP address'),
                               'description' => red_t("To avoid having your web application taken over before you complete the installation, we will restrct the installation to the IP address you are currently using."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_web_app',
                               'filter' => true),
                           
      ));

    }

    function set_web_app_name($value) {
      $this->_web_app_name = $value;
    }
    function get_web_app_name() {
      return $this->_web_app_name;
    }
    function set_web_app_update($value) {
      $this->_web_app_update = $value;
    }
    function get_web_app_update() {
      return $this->_web_app_update;
    }
    
    function set_web_app_install_ip($value) {
      $this->_web_app_install_ip = $value;
    }
    function get_web_app_install_ip() {
      return $this->_web_app_install_ip;
    }
    function additional_validation() {
      if ($this->_delete) {
        return true;
      }

      if (!$this->get_web_conf_login()) {
        $this->set_error(red_t("You must first create a web configuration before you can install a web application"),'validation');
      }
    }

    function get_web_conf_login() {
      $acceptable_item_status = [];
      $acceptable_item_status[] = "'active'";
      $acceptable_item_status[] = "'pending-update'";
      $acceptable_item_status[] = "'pending-restore'";
      if ($this->_disable || $this->get_item_status() == 'disabled') {
        $acceptable_item_status[] = "'disabled'";
        $acceptable_item_status[] = "'pending-disable'";
      }
      $in_item_status = implode(',', $acceptable_item_status);

      $sql = "SELECT web_conf_login FROM red_item JOIN red_item_web_conf " .
        "USING (item_id) WHERE item_status in ({$in_item_status}) " .
        "AND hosting_order_id = #hosting_order_id";
      $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
      $row = red_sql_fetch_row($result);
      if (empty($row)) {
        return false;
      }
      return $row[0];
    }

    public static function ensure_exists($item_id) {
      $server = red_get_item_host_for_item_id($item_id);
      $web_conf_item_id = self::get_web_conf_item_id($item_id);
      $user = "site{$web_conf_item_id}writer";
      $sql = "SELECT web_app_name
        FROM red_item JOIN red_item_web_app USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $app = $row[0];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-app/ensure-exists',
        $user,
        $app,
      ];
      $output = [];
      $rc = NULL;
      if (!red_ssh_cmd($args, $output, $rc)) {
        if ($rc == 1) {
          // This means there are files in the web directory. We should notify the user about that.
          throw new RedException(red_t("You currently have files in your web directory. You can only install a web app in an empty directory. You will need to delete this record and delete your web configuration and start over again."));
        }
        throw new RedException("Failed to ssh to {$server} to ensure web app exists for item {$item_id}. Please contact support for help.");
      }
      return TRUE;
    }
    
    public static function secure_install_script($item_id) {
      $server = red_get_item_host_for_item_id($item_id);
      $web_conf_item_id = self::get_web_conf_item_id($item_id);
      $user = "site{$web_conf_item_id}writer";
      $sql = "SELECT web_app_name, web_app_install_ip
        FROM red_item JOIN red_item_web_app USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $app = $row[0];
      $ip = $row[1];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-app/secure-install-script',
        $user,
        $app,
        $ip,
      ];
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to secure web app install script for item {$item_id}");
      }
      return TRUE;
    }
  }  
}

?>
