<?php

 // cron job -for manipulating cron jobs 
if(!class_exists('red_item_cron')) {
  class red_item_cron extends red_item {
    var $_cron_login;
    var $_cron_schedule;
    var $_cron_cmd;
    var $_cron_working_directory;
    var $_cron_environment;
    var $_human_readable_description;
    var $_human_readable_name;
    // A schedule set to red_item_cron::FOREVER is not scheduled,
    // it should run as a regular service.
    public static $FOREVER = 'forever';

    function __construct($co = array()) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields,
        [ 
          'cron_login' => [ 
            'req' => FALSE,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Login'),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => $this->get_available_user_account_logins(),
            'tblname'   => 'red_item_cron',
            'filter' => true
           ],
           'cron_cmd' => [ 
             'req' => true,
             'pcre' => false,
             'pcre_explanation' => false,
             'type' => 'varchar',
             'fname' => red_t('Command'),
             'user_insert' => TRUE,
             'user_update' => TRUE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 100,
             'text_max_length' => 255,
             'tblname' => 'red_item_cron',
             'filter' => true,
             'description' => $this->get_home_dir() ? red_t("Your home directory path is: %path", [ '%path' => $this->get_home_dir() ]) : '',
           ],
           'cron_schedule' => [ 
             'req' => true,
             'pcre' => false,
             'pcre_explanation'   => false,
             'type' => 'varchar',
             'fname' => red_t('Schedule'),
             'user_insert' => TRUE,
             'user_update' => TRUE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 20,
             'text_max_length' => 128,
             'tblname' => 'red_item_cron'
           ],
            'cron_working_directory' => [ 
             'req' => FALSE,
             'pcre'   => RED_PATH_MATCHER,
             'pcre_explanation'   => RED_PATH_EXPLANATION,
             'type'  => 'varchar',
             'fname'  => red_t('Directory'),
             'description' => red_t("Optionally, provide the directory from which to execute the command. Defaults to your web directory (%path) if left empty.", [ '%path' => $this->get_home_dir() . '/web']),
             'user_insert' => TRUE,
             'user_update' => TRUE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 20,
             'text_max_length' => 128,
             'tblname'   => 'red_item_cron'
           ],
            'cron_environment' => [ 
             'req' => FALSE,
             'pcre'   => RED_SYSTEMD_ENVIRONMENT_MATCHER,
             'pcre_explanation'   => RED_SYSTEMD_ENVIRONMENT_EXPLANATION,
             'type'  => 'varchar',
             'fname'  => red_t('Environment'),
             'description' => red_t("Optionally, provide a space separator list of key value pairs of variables that will be set in the environment when the command is executed. By default your TMP directory is set to ~/.tmp and your PATH directory is configured to include ~/bin, so no need to include those."),
             'user_insert' => TRUE,
             'user_update' => TRUE,
             'user_visible' => FALSE,
             'input_type' => 'textarea',
             'textarea_rows' => 5,
             'tblname'   => 'red_item_cron'
           ],
        ]
      );
    }

    function set_cron_login($value) {
      $this->_cron_login = strtolower($value);
    }
    function get_cron_login() {
      return $this->_cron_login;
    }

    function set_cron_schedule($value) {
      $this->_cron_schedule = trim($value);
    }
    function get_cron_schedule() {
      return $this->_cron_schedule;
    }

    function set_cron_cmd($value) {
      $this->_cron_cmd = $value;
    }
    function get_cron_cmd() {
      return $this->_cron_cmd;
    }

    function set_cron_working_directory($value) {
      $this->_cron_working_directory = $value;
    }
    function get_cron_working_directory() {
      return $this->_cron_working_directory;
    }
    function set_cron_environment($value) {
      $this->_cron_environment = $value;
    }
    function get_cron_environment() {
      return $this->_cron_environment;
    }

    function get_user() {
      if ($this->on_new_infrastructure()) {
        $item_id = $this->get_item_id();
        $hosting_order_id = $this->get_hosting_order_id();
        $web_conf_item_id = self::get_web_conf_item_id($item_id, $hosting_order_id);
        if ($web_conf_item_id) {
          return "site{$web_conf_item_id}writer";
        }
        return '';
      }
      else {
        return $this->get_cron_login();
      }
    }

    function get_home_dir() {
      if ($this->on_new_infrastructure()) {
        $user = $this->get_user();
        if ($user) {
          preg_match('/^site([0-9]+)/', $user, $matches);
          return '/home/sites/' . $matches[1];
        }
        return '';
      }
      else {
        // The homedir on old infrastructure is more confusing than helpful.
        return '';
      }
    }
    
    function additional_validation() {
      if ($this->_delete) {
        return;
      }
      if ($this->on_new_infrastructure()) {
        // Check if we need to convert and do it if necessary.
        $this->convert_to_new_infrastructure();
        // We must have a web configuration for this to work (because
        // we depend on the user created by the web configuration).
        $sql = "SELECT item_id FROM red_item JOIN red_item_web_conf USING(item_id)
          WHERE
            hosting_order_id = #hosting_order_id AND service_id = 7 AND item_status != 'deleted'
            AND item_status != 'soft-error' AND item_status != 'hard-error'";
        $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
        $row = red_sql_fetch_row($result);
        $item_id = $row[0] ?? NULL;
        if (!$item_id) {
          $this->set_error(red_t("Please ensure you have a functioning web configuration first."), "validation");
          // No point in further validation.
          return;
        }
        $cmd = $this->get_cron_cmd();
        // Ensure no lines breaks
        if (preg_match("/(\n|\r)/", $cmd)) {
          $this->set_error(red_t("No lines breaks are allowed in a command."), 'validation');
        }
        // Ensure no "!" or "+" characters - these can be uesd to elevate permissions
        if (preg_match('/[!+]/', $cmd)) {
          $this->set_error(red_t("The ! and + characters are not allowed."), 'validation');
        }

        // Break it up into words like systemd will, respecting quotes.
        $regexp = '/(' . "'[^']*'" . ')|("[^"]*")|\h+/';
        $words = red_split_words_with_quotes($cmd);
        foreach ($words as $word) {
          $word = trim($word);
          // Match on any words that end with a semicolon but are not a semicolon by itself.
          if ($word != ';' && preg_match('/;$/', $word) ) {
            $this->set_error(red_t("You may have multiple commands split by semi-colons, but please ensure the semi-colon has a space on either side."), 'validation');
          }
          if ($word == ';' && $this->get_cron_schedule() == $this::$FOREVER) {
            $this->set_error(red_t("You cannot have multiple commands unless you are running a scheduled job, not a forever job."), 'validation');
          }
          if (preg_match('/^[><&|]/', $word )) {
            $this->set_error(red_t("Sorry, no redirection is allowed. You can create a bash script with redirection and call that script. Or use sh -c 'command > goes/here.txt' syntax."), 'validation');
          }
        }
      }
      $working_directory = $this->get_cron_working_directory();
      if ($working_directory) {
        if (substr($working_directory, 0, 1) != '/') {
          $this->set_error(red_t("Please specify an absolute directory that starts with a forward slash."), 'validation');
        }
        if (!preg_match("#^/home/sites/$item_id#", $working_directory)) {
          $this->set_error(red_t("Your working directory must start with /home/sites/$item_id"), "validation");
        }
      }
      $environment = $this->get_cron_environment();
      if ($environment) {
        if (preg_match('/="|=\'/', $environment)) {
          $this->set_error(red_t("Instead of starting your value with a quote, enclose the entire assignment in a quote. In other words, rather than var='foo and bar', write: 'var=foo and bar'"), 'validation');
        }
        else {
          // Split on spaces, but all quoted spaces, e.g.:
          // foo=bar "var1=foo bar foo" var2=foo
          // See: https://stackoverflow.com/a/2202489
          preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $environment, $matches);
          // Shift off the first match, which is all matches.
          foreach ($matches[0] as $match) {
            // People used to bash will want to enter things like:
            // foo="bar and baz". But this is not allowed. Instead it should be
            // "foo=bar and baz". Let's detect and warn about this problem first to avoid
            // sending confusing errors later.
            
            $parts = explode('=', $match, 2);
            if (count($parts) != 2) {
              $this->set_error(red_t("The environment should be a space separated list of assignments, but at least one assignment (@match) does not seem to have the right number of equal signs.", [ '@match' => str_replace([ '"', "'"], '', $match)]), 'validation');
            }
            $var = trim($parts[0], '"\'');
            if (!preg_match('/^[a-zA-Z]/', $var)) {
              $this->set_error(red_t("Variable names must start with an ASCII letter, you have @varname.", [ '@varname' => $var]), 'validation');
            }
            if (!preg_match('/^[a-zA-Z0-9_]+$/', $var)) {
              $this->set_error(red_t("Variable names can only contain ASCII letters, numbers and underscores."), 'validation');
            }
          }
        }
      }
      $login = $this->get_cron_login();
      // Check the login 
      // Login can be blank for the new infrastructure, but must be filled in for the old. 
      if (empty($login)) {
        if (!$this->on_new_infrastructure()) {
          $this->set_error(red_t("You must specify a user account to run the the job."),'validation');
        }
      }
      else {
        // must be a valid login (unless we're deleting)
        if(!in_array($login,$this->get_available_user_account_logins()) && !$this->_delete && !$this->_disable ) {
          $this->set_error(red_t("You must specify a user account that exists and has server access."),'validation');
        }
      }
      $schedule = $this->get_cron_schedule();
      // Check the schedule 
      // If schedule is left blank, it will be caught by the normal
      // validation, so skip it (we don't want to give two error
      // messages on the same field).
      if (!empty($schedule) && $schedule != $this::$FOREVER) {
        if ($this->on_new_infrastructure()) {
          $cmd = '/usr/bin/systemd-analyze';
          $args = [ 'calendar', $schedule ];
          if (red_fork_exec_wait($cmd, $args) != 0) {
            $this->set_error(red_t("Invalid scheduled time: @schedule. Please make sure you have entered a schedule that is compatible with the systemd calendar specification.", [ '@schedule' => $schedule]),'validation');
          }
        }
        else {
          // loads of possibilities
          // check for special values
          $specials = array('@reboot','@yearly','@annually','@monthly','@weekly','@daily','@midnight','@hourly');
          if(!in_array($schedule,$specials)) { 
            // If it's not a special value, it must be 5 white space 
            // separated values
            $allowed_times = array(
              '\*', // *
              '([0-9]{1,2}(\-[0-9]{1,2})?,?)+', // 4 or 11 or 5-12 or 5-12,2-3
              '([0-9]{1,2}\-[0-9]{1,2}|\*)\/[0-9]{1,2}', // 0-23/2 or */2
              'sun|mon|tue|wed|thu|fri|sat',
              'jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec',    
            );

            // Replace tabs and multiple spaces with single spaces
            $replace = array("\t",'    ','   ','  ');
            $schedule = str_replace($replace,' ',$schedule);
            $array = explode(' ',$schedule);
            if(count($array) != 5) {
              $count = count($array);
              $this->set_error(red_t("Invalid scheduled time. Please enter 5 space separated values."),'validation');
            } else {
              foreach($array as $line) {
                reset($allowed_times);
                $pass = false;
                foreach($allowed_times as $preg) {
                  if(preg_match("/^$preg$/",$line))
                    $pass = true;
                }
                if(!$pass) {
                  $this->set_error(red_t("Invalid scheduled time."),'validation');
                }
              }
            }
          }
        }
      }
    }

    // Override to limit to just logins with server access.
    function get_available_user_account_logins() {
      $acceptable_item_status[] = "'active'";
      $acceptable_item_status[] = "'pending-insert'";
      $acceptable_item_status[] = "'pending-update'";
      $acceptable_item_status[] = "'pending-restore'";
      if ($this->_disable || $this->get_item_status() == 'disabled') {
        $acceptable_item_status[] = "'disabled'";
        $acceptable_item_status[] = "'pending-disable'";
      }
      $in_item_status = implode(',', $acceptable_item_status);

      $sql = "SELECT server_access_login 
        FROM red_item INNER JOIN red_item_server_access USING (item_id) 
        WHERE 
          hosting_order_id = #hosting_order_id AND 
          item_status IN ({$in_item_status})  
        ORDER BY server_access_login";
      $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
      $ret = array();
      while($row = red_sql_fetch_row($result)) {
        $user_account = $row[0];
        $ret[$user_account] = $user_account;
      }
      if(count($ret) == 0) $ret[''] = red_t('[No user accounts have server access!]');
      return $ret;
    }

    function convert_to_new_infrastructure() { 
      // Make transition changes.
      if ($this->transitioning_to_new_infrastructure()) {
        // Unset the login - no longer used.
        $this->set_cron_login('');
        $cmd = $this->get_cron_cmd();
        $value = red_replace_old_paths($cmd, self::get_web_conf_item_id($this->get_item_id()));
        $value = red_strip_redirects($value);
        $pieces = red_extract_environment_variables($value);
        if ($pieces[0]) {
          $this->set_cron_environment($pieces[0]);
          $value = $pieces[1];
        }
        if ($cmd != $value) {
          $this->set_cron_cmd($value);
        }
        $sched = $this->get_cron_schedule();
        $value = red_convert_cron_to_timer($sched);
        if ($sched != $value) {
          $this->set_cron_schedule($value);
        }
      }
    }

    public static function ensure($item_id, $status) {
      $server = red_get_item_host_for_item_id($item_id);
      $web_conf_item_id = self::get_web_conf_item_id($item_id);
      $sql = "SELECT cron_schedule FROM red_item_cron WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $schedule = $row[0];

      
           
      $args = [
        "red-remote-tasker@{$server}",
        "sudo"
      ];
      if ($status == 'exists') {
        # We always need a service file.
        $service_file = tempnam(sys_get_temp_dir(), 'red-cron');
        if (!self::generate_service_file($item_id, $service_file)) {
          throw new \RedException("Failed to generate service file for {$item_id}");
        }
        // Ensure the file is readable by the user that will need it.
        chmod($service_file, 0644);

        # We sometimes need a timer file.
        $timer_file = '';
        if ($schedule != self::$FOREVER) {
          $timer_file = tempnam(sys_get_temp_dir(), 'red-cron');
          if (!self::generate_timer_file($item_id, $schedule, $timer_file)) {
            throw new \RedException("Failed to generate timer file for {$item_id}");
          }
          chmod($timer_file, 0644);
        }

        // Copy the files to the remote host and delete them from local host.
        if (!red_rsync($service_file, "red-remote-tasker@{$server}:{$service_file}")) {
          unlink($service_file);
          throw new \RedException("Failed to rsync service file to {$server}");
        }
        if (!unlink($service_file)) {
          throw new \RedException("Failed to delete {$service_file}.");
        }
        if ($timer_file) {
          if (!red_rsync($timer_file, "red-remote-tasker@{$server}:{$timer_file}")) {
            unlink($timer_file);
            throw new \RedException("Failed to rsync timer file to {$server}");
          }
          if (!unlink($timer_file)) {
            throw new \RedException("Failed to delete {$timer_file}.");
          }
        }
        $args[] = '/usr/local/share/red/node/share/cron/ensure-exists';
        $args[] = $item_id;
        $args[] = $web_conf_item_id;
        $args[] = $service_file;
        if ($timer_file) {
          $args[] = $timer_file;
        }
      }
      else {
        $args[] = '/usr/local/share/red/node/share/cron/ensure-gone';
        $args[] = $item_id;
        $args[] = $web_conf_item_id;
      }
      
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure cron {$status} for item {$item_id}");
      }
      return TRUE;
    }

    public static function generate_timer_file($item_id, $schedule, $file) {
      $timer_content = '';
      $timer_content .= "[Unit]\n";
      $timer_content .= "Description=Timer Job with item ID: {$item_id}\n\n";
      $timer_content .= "[Timer]\n";
      $timer_content .= "OnCalendar=$schedule\n";

      if ($schedule == 'minutely') {
        $timer_content .= "RandomizedDelaySec=10\n";
      }
      elseif ($schedule == 'hourly') {
        $timer_content .= "RandomizedDelaySec=600\n";
      }
      elseif ($schedule == 'daily') {
        $timer_content .= "RandomizedDelaySec=3600\n";
      }
      $timer_content .= "[Install]\n";
      $timer_content .= "WantedBy=default.target\n";
      if (!file_put_contents($file, $timer_content)) {
        throw new \RedException("Failed to put timer content into {$file}.");
      }
      return TRUE;
    }

    public static function generate_service_file($item_id, $file) {
      $web_item_id = self::get_web_conf_item_id($item_id);
      $home = "/home/sites/{$web_item_id}";
      $sql = "SELECT cron_cmd, cron_working_directory, cron_environment, cron_schedule
        FROM red_item_cron WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $cmd = $row[0]; 
      $working_directory = $row[1];
      if (!$working_directory) {
        $working_directory = "{$home}/web";
      }
      $environment = $row[2];
      $schedule = $row[3];
      $service_content = '';
      $service_content .= "[Unit]\n";
      if ($schedule == self::$FOREVER) {
        // Prevent a unit from restarting over and over.
        $service_content .= "StartLimitIntervalSec=1h\n";
        $service_content .= "StartLimitBurst=20\n";
      }
      $service_content .= "Description=Job with item ID: {$item_id}\n\n";
      $service_content .= "[Service]\n";
      $service_content .= "ExecStart={$cmd}\n"; 
      $service_content .= "WorkingDirectory=$working_directory\n";
      # Prevent users from filling /tmp directory.
      $tmp = "{$home}/.tmp";
      $default_environment="TEMP=$tmp TMP=$tmp TEMPDIR=$tmp TMPDIR=$tmp " .
        "PATH=/home/sites/{$web_item_id}/bin:/usr/local/bin:/usr/bin:/bin";
      $service_content .= "Environment={$default_environment} $environment\n";
      $service_content .= "MemoryAccounting=true\n";
      $service_content .= "MemoryMax=1G\n";
      $service_content .= "MemoryHigh=750M\n";

      if ($schedule == self::$FOREVER) {
        // This is the default, specify for clarity.
        $service_content .= "Type=simple\n";
        $service_content .= "RestartSec=5\n";
        $service_content .= "Restart=always\n\n";
        $service_content .= "[Install]\n";
        $service_content .= "WantedBy=multi-user.target\n";
      }
      else {
        $service_content .= "Type=oneshot\n";
      }
      
      if (!file_put_contents($file, $service_content)) {
        throw new \RedException("Failed to put service content into {$file}.");
      }
      return TRUE;
    }
  }  
}



?>
