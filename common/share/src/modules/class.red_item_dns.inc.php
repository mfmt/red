<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_dns')) {
  class red_item_dns extends red_item {
    var $_dns_zone;
    var $_dns_type;
    var $_dns_fqdn;
    var $_dns_ip;
    var $_dns_ttl = 3600;
    var $_dns_server_name;
    var $_sshfp_algorithm_options = array(
      1 => 'RSA',
      2 => 'DSA',
      3 => 'ECDSA',
      4 => 'Ed25519',
    );
    var $_sshfp_type_options = array(
      1 => 'SHA-1',
      2 => 'SHA-256',
    );
    var $_caa_tag_options = array(
      'issue' => 'issue (specify which CA can issue a certificate for this domain)',
      'issuewild' => 'issuewild (specify which CA can issue a wildcard certificate for this domain, if different)',
      'issuemail' => 'issuemail (specify which CA can issue a certificate for domains used for email)',
      'iodef' => 'iodef (specify how to report policy violations)',
    );
    var $_caa_flags_options = array(
      0 => '0 (default)',
      128 => '128 (restricted)',
    );
    var $_dns_text;
    var $_dns_dist = 0;
    var $_dns_weight = 0;
    var $_dns_port = 0;
    var $_dns_sshfp_fpr;
    var $_dns_sshfp_type;
    var $_dns_sshfp_algorithm;
    var $_dns_caa_flags = 0;
    var $_dns_caa_tag;
    var $_dns_caa_value;
    var $_dns_dkim_selector = 'mayfirst1';
    var $_dns_dkim_signing_domain;
    var $_dns_mx_verified = 0;
    var $_dns_type_options = array(
      'a' => 'a',
      'mx' => 'mx',
      'cname' => 'cname',
      'txt' => 'txt',
      'dkim' => 'dkim',
      'srv' => 'srv',
      'ptr' => 'ptr',
      'aaaa' => 'ipv6 (aaaa)',
      'sshfp' => 'sshfp',
      'alias' => 'alias',
      'caa' => 'caa',
    );
    // Domains that should be on the public suffix list but aren't.
    var $_additions_to_public_suffix_list = array('in-addr.arpa','indymedia.org', 'mayfirst.cx');
    // Domains that are allowed to have private ranges.
    var $_allowed_private_range_fqdns = array('mayfirst.cx', 'myorg.dev', 'mayfirst.dev');
    var $_allowed_top_level_domains = [
      // Tachanka
      'indymedia.org' => 174,
      'mayfirst.info'  => 1,
      'mayfirst.org' => 1,
      'mayfirst.cx' => 1,
    ];
    var $_human_readable_description;
    var $_human_readable_name;
    // When quick is set, use a faster validation routing. Useful when bulk validating
    var $quick = FALSE;

    function __construct($co) {
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
                           array (
                             'dns_zone' => array (
                               'req' => TRUE,  
                               'pcre'   => RED_DOMAIN_MATCHER,
                               'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Zone name'),
                               'user_insert' => FALSE,
                               'user_update' => FALSE,
                               'user_visible' => FALSE,
                               'tblname'   => 'red_item_dns',
                               'filter' => false),
                             'dns_type' => array (
                               'req' => true,
                               'pcre'   => RED_DNS_TYPE_MATCHER,
                               'pcre_explanation'   => RED_DNS_TYPE_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('DNS Type'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_fqdn' => array (
                               'req' => TRUE,  
                               'pcre'   => RED_DOMAIN_LOOSE_WILDCARD_MATCHER,
                               'pcre_explanation'   => RED_DOMAIN_LOOSE_WILDCARD_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Fully Qualified Domain Name'),
                               'description' => red_t("Please enter mygroup.org or www.mygroup.org. For most MX, TXT and DKIM records, just enter the plain domain name, e.g. mygroup.org."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_ip' => array (
                               'req' => false,
                               'pcre'   => RED_IP_MATCHER,
                               'pcre_explanation'   => RED_IP_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('IP Address'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 15,
                               'text_max_length' => 79,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_ttl' => array (
                               'req' => true,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Time to live'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 6,
                               'text_max_length' => 6,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_server_name' => array (
                               'req' => false,
                               'pcre'   => RED_SERVER_NAME_MATCHER,
                               'pcre_explanation'   => RED_SERVER_NAME_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Server Name'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => TRUE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => 255,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_text' => array (
                               'req' => false,
                               'pcre'   => RED_DNS_TEXT_MATCHER,
                               'pcre_explanation'   => RED_DNS_TEXT_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Text'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 40,
                               'text_max_length' => 1024,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                             'dns_dist' => array (
                               'req' => false,
                               'pcre'   => RED_TINYINT_MATCHER,
                               'pcre_explanation'   => RED_TINYINT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Distance'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 2,
                               'text_max_length' => 3,
                               'tblname'   => 'red_item_dns'),
                            'dns_weight' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Weight'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 6,
                               'text_max_length' => 6,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                            'dns_port' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('Port'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 6,
                               'text_max_length' => 6,
                               'tblname'   => 'red_item_dns',
                               'filter' => true),
                            'dns_sshfp_algorithm' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('SSF Fingerprint Algorithm'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'select',
                               'tblname'   => 'red_item_dns',
                                'options' => $this->get_sshfp_algorithm_options(),
                               'filter' => false),
                             'dns_sshfp_type' => array (
                               'req' => false,
                               'pcre'   => RED_INT_MATCHER,
                               'pcre_explanation'   => RED_INT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('SSH Fingerprint Type'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'select',
                               'tblname'   => 'red_item_dns',
                                'options' => $this->get_sshfp_type_options(),
                               'filter' => false),
                             'dns_sshfp_fpr' => array (
                               'req' => false,
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Data'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_dns',
                               'filter' => false),
                             'dns_caa_flags' => array (
                               'req' => true,
                               'pcre'   => RED_TINYINT_MATCHER,
                               'pcre_explanation'   => RED_TINYINT_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('CAA Flag'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'select',
                               'options' => $this->get_caa_flags_options(),
                               'tblname'   => 'red_item_dns',
                               'description' => red_t("Set to 0 in almost all cases."),
                               'filter' => true),
                             'dns_caa_tag' => array (
                               'req' => false,
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('CAA tag'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'select',
                               'tblname'   => 'red_item_dns',
                               'options' => $this->get_caa_tag_options(),
                               'filter' => false),
                             'dns_caa_value' => array (
                               'req' => false,
                               'pcre'   => RED_ANYTHING_MATCHER,
                               'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('CAA Value'),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 88,
                               'text_max_length' => '512',
                               'tblname'   => 'red_item_dns',
                               'description' => red_t("Please enter the domain name of the certificate authority, e.g. letsencrypt.org."),
                               'filter' => false),
                             'dns_dkim_selector' => array (
                               'req' => false,
                               'pcre'   => RED_DKIM_SELECTOR_MATCHER,
                               'pcre_explanation'   => RED_DKIM_SELECTOR_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('DKIM Selector'),
                               'description' => red_t("The selector should only be changed if you want to invalidate an old dkim record."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 5,
                               'text_max_length' => '32',
                               'tblname'   => 'red_item_dns',
                               'filter' => false),
                              'dns_dkim_signing_domain' => array (
                               'req' => false,
                               'pcre'   => RED_DOMAIN_MATCHER,
                               'pcre_explanation'   => RED_DOMAIN_EXPLANATION,
                               'type'  => 'varchar',
                               'fname'  => red_t('Signing Domain'),
                               'description' => red_t("Optional. By default, the domain name will be used. Enter a different domain name here if you plan to link this record via a CNAME."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 20,
                               'text_max_length' => '255',
                               'tblname'   => 'red_item_dns',
                               'filter' => false),
                              'dns_mx_verified' => array (
                               'req' => false,
                               'pcre'   => RED_ZERO_ONE_MATCHER,
                               'pcre_explanation'   => RED_ZERO_ONE_EXPLANATION,
                               'type'  => 'int',
                               'fname'  => red_t('MX Verified'),
                               'description' => red_t("Indicate whether the domain has been verified as delivering to May First or not."),
                               'user_insert' => TRUE,
                               'user_update' => TRUE,
                               'user_visible' => FALSE,
                               'input_type' => 'text',
                               'text_length' => 1,
                               'text_max_length' => '1',
                               'tblname'   => 'red_item_dns',
                               'filter' => false),

  ));
    }

    /**
     * Get SSF Type options.
     */
    function get_sshfp_type_options() {
      return $this->_sshfp_type_options;
    }

    /**
     * Get SSF Algorythm options.
     */
    function get_sshfp_algorithm_options() {
      return $this->_sshfp_algorithm_options;
    }

    /**
     * Get the CAA "tag" options: https://letsencrypt.org/docs/caa/#the-issue-and-issuewild-properties
     */
    function get_caa_tag_options() {
      return $this->_caa_tag_options;
    }

    function get_caa_flags_options() {
      return $this->_caa_flags_options;
    }

    /**
     * Check if passed in domain is local
     *
     * The mayfirst.cx domain is reserved in our cabinet for local use
     * only and only resolved to 10.9.67.0/24 IP address. It is inserted
     * into our local authoritative DNS servers.
     */
    public static function is_local($domain) {
      return preg_match('/mayfirst\.cx$/', $domain);
    }

    function set_dns_zone($value = NULL) {
      // Automatically set if emtpy
      if(empty($value)) {
        $value = strtolower($this->get_calculated_zone());
      }
      $this->_dns_zone = strtolower($value);
    }
    function get_dns_zone() {
      return $this->_dns_zone;
    }
    static function get_rfc4183_snippet($base, $last) {
      // Add ranges that are delegated by upstream using rfc4183
      // here. See http://faq.he.net/index.php/Reverse_DNS
      // for more information. 
      $rfc4183s = array(
        0 => array(
          'base' => '15.66.216',
          'min' => 2,
          'max' => 30,
          'return' => '0-27',
        ),
      );
      foreach($rfc4183s as $candidate) {
        if($candidate['base'] == $base) {
          if($last >= $candidate['min'] && $last <= $candidate['max']) {
            return $candidate['return'];
          }
        }
      }
      return NULL;
    }
    
    function get_calculated_zone() {
      $fqdn = $this->get_dns_fqdn();
      $ip = $this->get_dns_ip();
      $type = $this->get_dns_type();

      // To preserve backward compatibility, ensure that all
      // .mayfirst.org and .mayfirst.info stay in one zone file
      // even though they should be broken into separate ones
      // because they are now in the public suffix list.
      if ($fqdn && $type != 'ptr' && (preg_match('/\.mayfirst\.org$/', $fqdn))) {
        return 'mayfirst.org';
      }
      if ($fqdn && $type != 'ptr' && (preg_match('/\.mayfirst\.info$/', $fqdn))) {
        return 'mayfirst.info';
      }
      
      if(!empty($fqdn) && $type != 'ptr') {
        // Always break it down to last two pieces.
        // I.e. the following should return: mayfirst.org
        // mayfirst.org, joe.mayfirst.org, joe-bob.mayfirst.org, bob.mayfirst.org
        // www.joe.bob.mayfirst.org
        //
        // Unless the domain is one of the exceptions list in
        // /etc/red-dns-soa-exeptions.conf in which case use the
        // last three pieces.
        $allowed_pieces = 2;  
        if ($this->domain_has_two_part_tld($fqdn)) {
          $allowed_pieces = 3;
        }
        $pieces = explode('.', $fqdn);
        while(count($pieces) > $allowed_pieces) {
          array_shift($pieces);
        }
        return implode('.', $pieces);
      }
      elseif(!empty($ip) && $type == 'ptr') {
        return red_item_dns::get_calculated_reverse_zone($ip);
      }
      return NULL;
    }  

    function domain_is_top_level($domain) {
      $exceptions_file = '/etc/red-dns-soa-exceptions.conf';
      if (!file_exists($exceptions_file)) {
        red_log("Failed to locate soa exceptions file! $exceptions_file");
        // We assume they aren't any exceptions...
        return FALSE;
      }
      // Find this domain (terminated with white space) in the public suffix file.
      if(red_return_line($exceptions_file, $domain, '\s')) {
        return TRUE;
      }
      // Check for our exceptions.
      if (in_array($domain, $this->_additions_to_public_suffix_list)) {
        return TRUE;
      }
      return FALSE;
    }
    function domain_has_two_part_tld($domain) {
      // If the domain has at least one period, see if the
      // ending blah.tld part is listed in the tld exception list
      // which would indicate it's a two part top level domain (e.g.
      // org.mx or uk.com).
      if(preg_match('/\.?([^.]+\.[^.]+)$/', $domain, $matches)) {
        return $this->domain_is_top_level($matches[1]);
      }
      return FALSE;
    }

    static function get_calculated_reverse_zone($ip = NULL) {
      if(!empty($ip)) {
        if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          // ipv6 address. We only have one zone file.
          return "6.1.1.0.1.0.0.0.0.7.4.0.1.0.0.2.ip6.arpa";
        }
        else {
          // ipv4, Get that last three pieces of the given IP.
          $pieces = explode('.', $ip);
          if(count($pieces) > 2) {
            $first = array_pop($pieces);
            $second = array_pop($pieces);
            $third = array_pop($pieces);
            $fourth = array_pop($pieces);

            // Now check... a normal zone for 1.2.3.4 would be
            // 3.2.1.in-addr.arpa
            // However, some of our blocks use rfc4183, which might 
            // need a zone file in the form of:
            // 1-10.3.2.1.in-addr.arpa instad.
            $base = "{$second}.{$third}.{$fourth}"; 
            $snippet = red_item_dns::get_rfc4183_snippet($base, $first);
            if($snippet) {
              return "{$snippet}.{$base}.in-addr.arpa";
            }
            return "{$base}.in-addr.arpa"; 
          }
        }
      }
      return NULL;
    }
    function set_dns_type($value) {
      $this->_dns_type = $value;
    }
    function get_dns_type() {
      return $this->_dns_type;
    }

    function set_dns_fqdn($value) {
      $this->_dns_fqdn = $value;
    }
    function get_dns_fqdn() {
      return $this->_dns_fqdn;
    }

    function set_dns_ip($value) {
      $this->_dns_ip = $value;
    }
    function get_dns_ip() {
      return $this->_dns_ip;
    }

    function set_dns_ttl($value) {
      $this->_dns_ttl = $value;
    }
    function get_dns_ttl() {
      return $this->_dns_ttl;
    }

    function set_dns_server_name($value) {
      $this->_dns_server_name = $value;
    }
    function get_dns_server_name() {
      return $this->_dns_server_name;
    }

    function set_dns_text($value) {
      $this->_dns_text = $value;
    }
    function get_dns_text() {
      return $this->_dns_text;
    }

    function set_dns_dist($value) {
      $this->_dns_dist = $value;
    }
    function get_dns_dist() {
      return $this->_dns_dist;
    }

    function set_dns_weight($value) {
      $this->_dns_weight = $value;
    }
    function get_dns_weight() {
      return $this->_dns_weight;
    }

    function set_dns_port($value) {
      $this->_dns_port = $value;
    }
    function get_dns_port() {
      return $this->_dns_port;
    }

    function set_dns_sshfp_type($value) {
      $this->_dns_sshfp_type = $value;
    }
    function get_dns_sshfp_type() {
      if (!$this->_dns_sshfp_type) {
        return 0;
      }
      return $this->_dns_sshfp_type;
    }

    function set_dns_sshfp_algorithm($value) {
      $this->_dns_sshfp_algorithm = $value;
    }
    function get_dns_sshfp_algorithm() {
      if (!$this->_dns_sshfp_algorithm) {
        return 0;
      }
      return $this->_dns_sshfp_algorithm;
    }
    function set_dns_sshfp_fpr($value) {
      $this->_dns_sshfp_fpr = $value;
    }
    function get_dns_sshfp_fpr() {
      return $this->_dns_sshfp_fpr;
    }
    function set_dns_caa_flags($value) {
      $this->_dns_caa_flags = $value;
    }
    function get_dns_caa_flags() {
      return $this->_dns_caa_flags;
    }
    function set_dns_caa_tag($value) {
      $this->_dns_caa_tag = $value;
    }
    function get_dns_caa_tag() {
      return $this->_dns_caa_tag;
    }
    function set_dns_caa_value($value) {
      $this->_dns_caa_value = $value;
    }
    function get_dns_caa_value() {
      return $this->_dns_caa_value;
    }
    function set_dns_dkim_selector($value) {
      $this->_dns_dkim_selector = $value;
    }
    function get_dns_dkim_selector() {
      return $this->_dns_dkim_selector;
    }
    function set_dns_dkim_signing_domain($value) {
      $this->_dns_dkim_signing_domain = $value;
    }
    function get_dns_dkim_signing_domain() {
      return $this->_dns_dkim_signing_domain;
    }
    function get_dns_type_options() {
      return $this->_dns_type_options;
    }
    function get_dns_mx_verified() {
      return $this->_dns_mx_verified;
    }
    function set_dns_mx_verified($value) {
      return $this->_dns_mx_verified = $value;
    }

    function validate() {
      // FIXME - we probably should not be modifying data in the validate function
      // But we need to auto-set the zone field before we validate or we will get 
      // a validation error.
      $this->set_dns_zone();
      return parent::validate();
    }

    function additional_validation() {
      $type = $this->get_dns_type();
      $fqdn = $this->get_dns_fqdn();
      // If we're deleting - ensure we are not deleting the last MX record that is used
      // by an email address - you have to delete the email address first.
      if ($this->_delete || $this->_disable) {
        if ($type == 'mx') {
          if ($this->_delete) {
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_email_address USING (item_id) WHERE
            item_status != 'deleted' AND item_status != 'pending-delete' AND email_address LIKE @fqdn";
          }
          else {
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_email_address USING (item_id) 
              WHERE
                item_status != 'deleted' AND 
                item_status != 'pending-delete' AND 
                item_status != 'disabled' AND 
                item_status != 'pending-disable' AND
                email_address LIKE @fqdn";
          }
          
          $result = red_sql_query($sql, ['@fqdn' => "%@{$fqdn}"]);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            $item_id = $this->get_item_id();
            // Only disallow if this is the last active mx record.
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) WHERE
              dns_type = 'mx' AND dns_fqdn = @fqdn AND item_status = 'active' AND
              item_id != #item_id";
            $result = red_sql_query($sql, ['@fqdn' => $fqdn, '#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            if ($row[0] == 0) {
              $this->set_error(red_t("You have email addresses using this domain name. Please delete them before deleting your last MX record."), 'validation');
            }
          }
        }
        if ($type == 'a') {
          // And don't delete the last A record used by a web configuration item.
          // I couldn't find any documentation on this, but it seems that mariadb
          // needs to have literal periods escaped with two slashes (which requires
          // three slashes to avoid escaping the slash).
          $fqdn_mariadb_escaped = str_replace('.', '\\\.', addslashes($fqdn));
          // Also for * which might be in a domain name.
          $fqdn_mariadb_escaped = str_replace('*', '\\\*', $fqdn_mariadb_escaped);
          if ($this->_delete) {
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_web_conf USING (item_id) WHERE
              item_status != 'deleted' AND item_status != 'pending-delete' AND web_conf_domain_names REGEXP '(^| )$fqdn_mariadb_escaped($| )'";
          }
          else {
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_web_conf USING (item_id) 
              WHERE
                item_status != 'deleted' AND 
                item_status != 'pending-delete' AND 
                item_status != 'disabled' AND 
                item_status != 'pending-disable' AND 
                web_conf_domain_names REGEXP '(^| )$fqdn_mariadb_escaped($| )'";
          }
          $result = red_sql_query($sql);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            $item_id = intval($this->get_item_id());
            // Only disallow if this is the last active a record.
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) WHERE
              dns_type = 'a' AND dns_fqdn = @fqdn AND item_status = 'active' AND
              item_id != #item_id";
            $result = red_sql_query($sql, ['@fqdn' => $fqdn, '#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            if ($row[0] == 0) {
              $this->set_error(red_t("You have a web configuration using this domain name. Please remove it before deleting your last A record."), 'validation');
            }
          }
        }
        return; 
      }

      if (preg_match('/_/', $this->get_dns_zone())) {
        $this->set_error("An underscore is invalid as a zone, please ensure your fully qualified domain name includes your full domain.", 'validation');
      }
      if (preg_match('/_domainkey$/', $fqdn)) {
        // This seems to be a common mistake.
        $this->set_error("Please enter your fully qualified domain name - e.g. mayfirst1._domainkey.yourdomain.org.", 'validation');
      }
      $server_name = $this->get_dns_server_name();
      $ip = $this->get_dns_ip();
      $text = $this->get_dns_text();
      $ttl = intval($this->get_dns_ttl());
      $dns_type = $this->get_dns_type();

      if ($this->exists_in_db()) {
        // You can't change the dns type unless it's A => ALIAS or vice versa.
        // You can't change the fqdn unless it's a PTR record (in which case you can't change the ip).
        $item_id = $this->get_item_id();
        $sql = "SELECT dns_type, dns_fqdn, dns_ip FROM red_item_dns WHERE item_id = #item_id";
        $result = red_sql_query($sql, ['#item_id' => $item_id]);
        $row = red_sql_fetch_row($result);
        $existing_type = $row[0];
        $existing_fqdn = $row[1];
        $existing_ip = $row[2];
        if ($existing_type != $type) {
          $allowed = [ 'a', 'alias' ];
          if (!in_array($existing_type, $allowed) || !in_array($type, $allowed)) {
            $this->set_error(red_t("You may not change the type of an existing DNS record."), 'validation');
          }
        }
        if ($type == 'ptr') {
          if ($existing_ip != $this->get_dns_ip()) {
            $this->set_error(red_t("You may not change the IP address of an existing PTR record. Please delete it and create a new one"), 'validation');
          }
        }
        else {
          if ($existing_fqdn != $fqdn) {
            $this->set_error(red_t("You may not change the fully qualified domain name of an existing record. Please delete it and create a new one"), 'validation');
          }
        }
      } 
      if (self::is_local($fqdn)) {
        if (!empty($ip)) {
          if (!preg_match('/^10\.9\.67\./', $ip)) {
            $this->set_error(red_t("The domain mayfirst.cx is reserved for IPs in the 10.9.67.0/24 range"), 'validation');
          }
        }
      }
      else {
        if ($ttl < 300) {
          $this->set_error(red_t("Please use a minimum time to live value of 300."), 'validation');
        }

        // No private ranges allowed
        if (!empty($ip) && !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE) && !in_array($fqdn, $this->_allowed_private_range_fqdns)) {
          // Private IP addresses are only allowed if we are running tests.
          // So, test for any of the testing constants.
          if (!defined('RED_TEST') && preg_match('/mayfirst\.dev$/', $fqdn)) {
            $this->set_error(red_t("Private IP ranges are not allowed. You entered: $ip for $fqdn"),'validation');
          }
        }
      }

      if (!empty($ip)) {
        // Must be valid IP address.
        if (!filter_var($ip, FILTER_VALIDATE_IP)) {
          $this->set_error(red_t('That IP address does not appear to a valid ipv4 or ipv6 address.'),'validation');
        }
      }

      if ($type == 'mx' ||  $type == 'cname' || $type == 'srv' || $type == 'alias') {
        if (empty($server_name)) {
          $this->set_error(red_t('MX, SRV, ALIAS and CNAME records require a server name'),'validation');
        }
      }

      if ($type == 'alias') {
        // For the time being, we only allow ALIAS files to be used with May First proxy servers.
        if (!preg_match('/^[a-z]\.webproxy\.mayfirst\.(org|dev)$/', $server_name)) {
          $this->set_error(red_t("Only May First web proxy servers are allowed to use the ALIAS DNS type. If you would like us to expand this feature, please contact support."), 'validation');
        }
        // Don't set two ALIASes on the same fqdn 
        $params = [];
        $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) 
          WHERE 
            dns_type = 'alias' AND 
            dns_fqdn = @fqdn AND 
            item_status != 'deleted'
        ";
        $params['@fqdn'] = $fqdn;
        $item_id = $this->get_item_id();
        if ($item_id) {
          $sql .= " AND red_item.item_id != #item_id";
          $params['#item_id'] = $item_id;
        }
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t('A ALIAS record for this domain already exists.'),'validation');
        } 
        // Don't allow an alias if an A record is already set.
        $params['@fqdn'] = $fqdn;
        $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) 
          WHERE 
            dns_type = 'a' AND 
            dns_fqdn = @fqdn AND 
            item_status != 'deleted'
        ";
        $item_id = intval($this->get_item_id());
        if ($item_id) {
          $sql .= " AND red_item.item_id != #item_id";
          $params['#item_id'] = $item_id;
        }
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t("An A record is already set for this domain, please delete it before creating an alias record."), 'validation');
        }
      }
      if ($type == 'mx') {
        $dist = $this->get_dns_dist();
        if ($dist === '') {
          $this->set_error(red_t('MX records require a numeric distance. If you are not sure, you can enter 10.'),'validation');
        }
        // We don't allow wildcard MX records because it makes it hard to verify if the MX record
        // is pointing to us or not.
        if (preg_match('/\*/', $fqdn)) {
          $this->set_error(red_t('Wildcard MX records are not allowed.'),'validation');
        }
      }

      if ($type == 'txt') {
        if (empty($text)) {
          $this->set_error(red_t('Text records requires a value in the text field.'),'validation');
        }
        if (preg_match('/"/', $text)) {
          $this->set_error(red_t('Text records cannot include a double quote.'),'validation');
        }  
        // Protect against duplicate dmarc, dkim and spf records. You are only allowed to have
        // one per domain.
        $exclude_active_item_sql = '';
        $params = [
          '@fqdn' => $fqdn,
        ];
        $item_id = $this->get_item_id();
        if ($item_id) {
          // When searching, exclude the item id we are working on.
          $params['#item_id'] = $item_id;
          $exclude_active_item_sql .= " AND red_item.item_id != #item_id";
        }
        if (preg_match('/^_dmarc\./', $fqdn)) {
          $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id)
            WHERE item_status = 'active' AND dns_fqdn = @fqdn AND dns_type = 'txt'";
          $sql .= $exclude_active_item_sql;
          $result = red_sql_query($sql, $params);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            $this->set_error(red_t("You already have a _dmarc txt record for this domain. You are only allowed one."), 'validation');
          }
        }
        if (preg_match('/^v=spf/', $text)) {
          $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id)
            WHERE item_status = 'active' AND dns_fqdn = @fqdn AND dns_type = 'txt'
            AND dns_text LIKE 'v=spf%'";
          $sql .= $exclude_active_item_sql;
          $result = red_sql_query($sql, $params);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            $this->set_error(red_t("You already have an spf txt record for this domain. You are only allowed one."), 'validation');
          }
        }
      }

      if($type == 'a' ||  $type == 'ptr') {
        if(empty($ip)) {
          $this->set_error(red_t("You must enter an IP address for A and PTR records."), 'validation');
        }
      }
      if($type == 'a') {
        // Don't allow an a if an alias record is already set.
        $params = [];
        $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) 
          WHERE 
            dns_type = 'alias' AND 
            dns_fqdn = @fqdn AND 
            item_status != 'deleted'
        ";
        $params['@fqdn'] = $fqdn;
        $item_id = $this->get_item_id();
        if ($item_id) {
          $sql .= " AND red_item.item_id != #item_id";
          $params['#item_id'] = $item_id;
        }
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t("An alias record is already set for this domain, please delete it before creating an a record."), 'validation');
        }
      }
      if($type == 'aaaa') {
        // Ensure we have a valid ipv6 address. 
        if(!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          $this->set_error(red_t("That did not seem to be a valid ipv6 address."), 'validation');
        }
      }
      if($type == 'sshfp') {
        // Ensure we have required fields. 
        if ($this->get_dns_sshfp_type() == 0) {
          $this->set_error(red_t("Please choose a valid sshfp fingerprint type."), 'validation');
        }
        if ($this->get_dns_sshfp_algorithm() == 0) {
          $this->set_error(red_t("Please choose a valid sshfp algorithm."), 'validation');
        }
        if ($this->get_dns_sshfp_fpr() == '') {
          $this->set_error(red_t("Please enter a fingerprint."), 'validation');
        }
      }
      if($type == 'caa') {
        if ($this->get_dns_caa_flags() != '0' && $this->get_dns_caa_flags() != '128') {
          $this->set_error(red_t('Currently 0 and 128 are the only valid values for CAA flags.'), 'validation');
        }
        $error = $this->validate_caa_value($this->get_dns_caa_tag(), $this->get_dns_caa_value());
        if ($error != NULL) {
          $msg_args = array('@error' => $error);
          $this->set_error(red_t('Invalid caa value: @error. Make sure not to use quotes.', $msg_args), 'validation');
        }
      }
      if($type == 'ptr') {
        if(!filter_var($ip, FILTER_VALIDATE_IP)) {
          $this->set_error(red_t("Sorry, that IP did not look like a valid ipv4 or ipv6 address."), 'validation');
        }
        if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
          // If it is ipv6, ensure it is in our range. FIXME: this should be configured somewhere.
          if (!preg_match('/^2001:470:1:116:/', $ip)) {
            $this->set_error(red_t("We can only offer ipv6 pointer records for ipv6 addresses in the range specified to us (2001:470:1:116::/64)."), 'validation');
          }
        }
        if(FALSE !== $existing_ttl = $this->mismatched_ip_ttls()) {
          $msg_args = array('@ttl' => $existing_ttl);
          $msg = red_t("There is already a ptr record with the same IP address but a different time to live.Try using @ttl as the time to live instead.", $msg_args);
          $this->set_error($msg, 'validation');
        }
        // Only one record per IP.
        $params = [];
        $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) 
          WHERE 
            dns_type = 'ptr' AND 
            dns_ip = @ip AND 
            item_status != 'deleted'
        ";
        $params['@ip'] = $ip;
        $item_id = $this->get_item_id();
        if ($item_id) {
          $sql .= " AND red_item.item_id != #item_id";
          $params['#item_id'] = $item_id;
        }
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t('A PTR record for this IP already exists. Please delete the existing one before creating a second.'),'validation');
        }
      }
      else if(FALSE !== $existing_ttl = $this->mismatched_fqdn_ttls()) {
        $msg_args = array('@ttl' => $existing_ttl);
        $msg = red_t("All records with the same domain name and type must have the same time to live value. Try using @ttl as the time to live instead.", $msg_args);
        $this->set_error($msg, 'validation');
      }
      if($type == 'srv') {
        // Make sure the service/protocol fields look reasonable.
        $parts = explode('.', $this->get_dns_fqdn());
        // Should be in format: _http._tcp.domain.name.org
        // First, ensure the first two pieces start with an underscore.
        if(!preg_match('/^_/', $parts[0]) || !preg_match('/^_/', $parts[1])) {
          $this->set_error(red_t("The domain name must start with _service._protocol (e.g. _xmpp._tcp.mayfirst.org)."), 'validation');
        }
        // Only supported protocols are tcp and udp
        if($parts[1] != '_udp' && $parts[1] != '_tcp' && $parts[1] != '_tls') {
          $this->set_error(red_t("The protocol (second part after the service) must be either _tcp or _udp or _tls."), 'validation');
        }
        // Ensure port is set
        if(empty($this->get_dns_port())) {
          $this->set_error(red_t("Please set a valid port when creating a srv record."), 'validation');
        }
      }
      $matches = array();
      if(preg_match_all('/\*/', $fqdn, $matches)) {
        // Make sure we only have one asterisk.
        if(count($matches[0]) > 1) {
          $msg = red_t("Only one asterisk is allowed in a domain name.");
          $this->set_error($msg, 'validation');
        }
        // Only allow asterisk as the first character.
        if (!preg_match('/^\*/', $fqdn)) {
          $msg = red_t("An asterisk can only be the first character in a domain name.");
          $this->set_error($msg, 'validation');
        }
      }

      if($type != 'txt' && $type != 'cname' && $type != 'srv') {
        # enforce stricter domain name checking
        if(!preg_match(RED_DOMAIN_WILDCARD_MATCHER, $this->get_dns_fqdn())) {
          $this->set_error(red_t('That does not look like a valid domain. Ensure it has a period and no unusual characters, like an underscore in them.'),'validation');
        }
      }
      if ($this->domain_is_top_level($this->get_dns_fqdn())) {
        // We have a bare top level domain. This is normally not allowed, but we have
        // exceptions.
        if (array_key_exists($fqdn, $this->_allowed_top_level_domains)) {
          if ($this->get_member_id() == $this->_allowed_top_level_domains[$fqdn]) {
            return TRUE;
          }
        }
        $this->set_error(red_t("@fqdn is a top level domain.", [ '@fqdn' => $fqdn]), 'validation');
        return FALSE;
      }
      if (!$this->domain_is_allowed($this->get_dns_fqdn())) {
        $this->set_error(red_t('That domain name is already in use by another member or hosting order or is otherwise not allowed.'),'validation');
      }
      if(!empty($ip) && !empty($server_name)) {
        $this->set_error(red_t("You may not enter both a server name and an IP address. If you are entering an MX record, only enter a server name ('$ip' and '$server_name')."),'validation');
      }
      // CNAMES should not have any matching fqdn's
      if($type == 'cname') {
        // make sure this fqdn is not in use
        $exact_match = true;
        if($this->domain_in_use($fqdn,$exact_match)) {
          $this->set_error(red_t('That domain name is already entered in the system. When entering a CNAME, the only instance of that domain name should be the Cname record. Please remove the other instance of the domain name and try again. If you just want an alias, then create an A record instead.'),'validation');
        }
      }
      if($type == 'dkim') {
        // We must have a selector.
        $selector = $this->get_dns_dkim_selector();
        if (empty($selector)) {
          $this->set_error(red_t('You must enter a dkim selector.'),'validation');
        }
        $params = [];
        $fqdn = $this->get_dns_fqdn();
        // Only one dkim record per domain.
        $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) 
          WHERE dns_type = 'dkim' AND (dns_fqdn = @fqdn OR dns_dkim_signing_domain = @fqdn) AND item_status != 'deleted'";
        $params['@fqdn'] = $fqdn;
        $item_id = $this->get_item_id();
        if ($item_id) {
          $sql .= " AND red_item.item_id != #item_id";
          $params['#item_id'] = $item_id;
        }
        $result = red_sql_query($sql, $params);
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t('A DKIM record for this domain name already exists. Please delete the existing one before creating a second.'),'validation');
        } 
        $signing_domain = $this->get_dns_dkim_signing_domain();
        if ($signing_domain) {
          if (!$this->domain_is_allowed($signing_domain)) {
            $this->set_error(red_t('That signing domain is in use by another member.'),'validation');
          }
          else {
            // Only one dkim signing domain per domain.
            $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING (item_id) 
              WHERE dns_type = 'dkim' AND 
                (dns_dkim_signing_domain = @signing_domain OR dns_fqdn = @signing_domain) AND 
                item_status != 'deleted'";
            $params = ['@signing_domain' => $signing_domain];
            if ($item_id) {
              $sql .= " AND red_item.item_id != #item_id";
              $params['#item_id'] = $item_id;
            }
            $result = red_sql_query($sql, $params);
            $row = red_sql_fetch_row($result);
            if ($row[0] > 0) {
              $this->set_error(red_t('A DKIM record for this signing domain already exists. Please delete the existing one before creating a second.'),'validation');
            }
          }
        }
      }
      // Make sure an existing record doesn't exist.
      $fields[] = 'dns_fqdn';
      if ($type == 'mx' ||  $type == 'cname' || $type == 'srv' || $type == 'alias') {
        $fields[] = 'dns_server_name';
      }
      elseif ($type == 'a' || $type == 'aaaa' || $type == 'ptr') {
        $fields[] = 'dns_ip';
      }
      elseif ($type == 'txt') {
        $fields[] = 'dns_text';
      }
      elseif ($type == 'dkim') {
        // pass, dns_fqdn is enough.
      }
      elseif ($type == 'sshfp') {
        $fields[] = 'dns_sshfp_algorithm';
        $fields[] = 'dns_sshfp_type';
      }
      elseif ($type == 'caa') {
        $fields[] = 'dns_caa_tag';
      }
      $item_id = $this->get_item_id();
      $params = [];
      $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING(item_id) WHERE
        dns_fqdn = @fqdn AND dns_type = @dns_type AND item_status != 'deleted'";
      $params['@fqdn'] = $fqdn; 
      $params['@dns_type'] = $dns_type; 
      $sqls = [];
      if ($item_id) {
        $sqls[] = "item_id != #item_id";
        $params['#item_id'] = $item_id;
      }
      foreach($fields as $field) {
        $func = "get_$field";
        $sqls[] = "$field = @{$field}";
        $params["@{$field}"] = $this->$func();
      }
      if (count($sqls) > 0) {
        $sql .= ' AND ' . implode(' AND ', $sqls);
      }
      $result = red_sql_query($sql, $params);
      $row = red_sql_fetch_row($result);
      if ($row[0] > 0) {
        $this->set_error("A DNS record with the same value already exists for $fqdn / $dns_type.", 'validation');
      }
    }

    /*
     * Check if another record exists with the same ip and type
     * but with a different ttl. This check avoids having a 
     * reverse lookup zone in which two records have mis-matched
     * ttls. Return FALSE if there are no mis-matched ttls
     * and the ttl of one existing row if it exists.
     */
    function mismatched_ip_ttls() {
      $params = [
          '@ttl' => $this->get_dns_ttl(), 
          '@ip' => $this->get_dns_ip(), 
      ];
      $sql = "SELECT red_item.item_id, dns_ttl FROM red_item JOIN red_item_dns USING(item_id) " .
        "WHERE item_status != 'deleted' AND item_status != 'transfer-limbo' AND ".
        "item_status != 'pending-delete' AND item_status != 'disabled' AND dns_type = 'ptr' " .
        "AND dns_ttl != @ttl AND dns_ip = @ip";
      $item_id = $this->get_item_id();
      if ($item_id) {
       $sql .= " AND red_item.item_id != #item_id";
       $params['#item_id'] = $item_id;
      }
      $result = red_sql_query($sql, $params);
      if(red_sql_num_rows($result) == 0) return false;
      $row = red_sql_fetch_row($result);
      return $row[1];
    }

    /*
     * Check if another record exists with the same fqdn and type
     * but with a different ttl. 
     */
    function mismatched_fqdn_ttls() {
      $params = [];
      $type = $this->get_dns_type();
      if ($type == 'a' || $type == 'alias') {
        $types = "('a', 'alias')";
      }
      else {
        $types = "(@type)";
        $params['@type'] = $type;
      }
          
      $sql = "SELECT red_item.item_id, dns_ttl FROM red_item JOIN red_item_dns USING(item_id) " .
        "WHERE item_status != 'deleted' AND item_status != 'transfer-limbo' AND ".
        "item_status != 'pending-delete' AND dns_fqdn = @fqdn AND dns_type IN $types " .
        "AND dns_ttl != @ttl AND dns_type != 'ptr'";
      $item_id = $this->get_item_id();
      if ($item_id) {
        // If this is not a new item, we should exclude the existing record.
        $sql .= " AND red_item.item_id != #item_id";
        $params['#item_id'] = $item_id;
      }

      $params['@ttl'] = $this->get_dns_ttl();
      $params['@fqdn'] = $this->get_dns_fqdn();
      $result = red_sql_query($sql, $params);
      if (red_sql_num_rows($result) == 0) {
        return false;
      }
      $row = red_sql_fetch_row($result);
      return $row[1];
    }

    function domain_is_allowed($fqdn) {
      $parts = explode('.',$fqdn);
      $tld = array_pop($parts);
      $name = array_pop($parts);
      $short_domain = "$name.$tld";
      // regular domain - allowed provided nobody else
      // has a variation of it.
      // But first check if it is using a special top level domain
      // (e.g. org.mx, or com.uk)
      if ($this->domain_has_two_part_tld($fqdn)) {
        // Take the next part of the domain.
        $next_name = array_pop($parts);
        $short_domain = "{$next_name}.{$short_domain}";
      }
      // Now a weird exception. laneta.apc.org is a *three* part tld.
      // May First is allowed to have laneta.apc.org itself.
      if ($fqdn == 'laneta.apc.org') {
        if ($this->get_member_id() == 1) {
          return TRUE;
        }
        return FALSE;
      }
      // apc.org is owned by APC
      elseif ($fqdn == 'apc.org') {
        if ($this->get_member_id() == 1078) {
          return TRUE;
        }
        return FALSE;
      }
      // Here is where laneta.apc.org is like a public suffix.
      elseif (preg_match('/[0-9a-zA-Z\-.]+\.laneta.apc.org$/', $fqdn)) {
        $laneta_part = array_pop($parts);
        $next_name = array_pop($parts);
        $short_domain = "{$next_name}.laneta.apc.org";
      }
      // And, of course, APC will have other domain names ending in apc.org
      // that should not be treated as public suffixes.
      elseif (preg_match('/\.apc.org$/', $fqdn)) {
        if ($this->get_member_id() == 1078) {
          return TRUE;
        }
        return FALSE;
      }

      $exact_match = false;
      $hosting_order_id = $this->domain_in_use($short_domain, $exact_match);

      // not in use by anyone
      if(false === $hosting_order_id) return true;

      // in use by the current hosting order - so allowed
      if($hosting_order_id == $this->get_hosting_order_id()) return true;

      // in use by another hosting order tied to the same member - so allowed
      $member_id = $this->get_member_id();
      if($this->does_hosting_order_belong_to_member($hosting_order_id, $member_id)) return true;

      // otherwise - already in use
      return false;
    }

    function does_hosting_order_belong_to_member($hosting_order_id,$member_id) {
      // We need to account for member_parent_id.
      $sql = "SELECT hosting_order_id 
        FROM red_hosting_order
          LEFT JOIN red_member ON 
            red_member.member_id = red_hosting_order.member_id OR
            red_member.member_parent_id = red_hosting_order.member_id
        WHERE hosting_order_id = #hosting_order_id AND 
          (
            red_hosting_order.member_id = #member_id OR 
            red_member.member_id = #member_id OR
            red_member.member_parent_id = #member_id
          )
      ";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id, '#member_id' => $member_id]);
      if(red_sql_num_rows($result) == 0) return false;
      return true;
    }

    // returns false if not in use or the hosting_order_id
    // of the hosting order it is in use by
    function domain_in_use($domain, $exact_match = false) {
      $params = [];
      $params['@domain'] = $domain;
      if ($exact_match) {
        $condition = "dns_fqdn = @domain";
      }
      else {
        if ($this->quick) {
          // Experimental - might be less accurate, but is tons faster.
          $condition = "(dns_fqdn = @domain OR dns_zone = @domain)";
        }
        else {
          $condition = "(dns_fqdn = @domain OR dns_fqdn LIKE @like_domain)";
          $params['@like_domain'] = "%.{$domain}";
        }
      }
      $sql = "SELECT red_item.item_id,hosting_order_id FROM red_item_dns ".
        "INNER JOIN red_item ON red_item_dns.item_id = red_item.item_id ".
        "WHERE $condition AND item_status != 'deleted' AND item_status != 'disabled'";
      $result = red_sql_query($sql, $params);

      // no records, not in use
      if(red_sql_num_rows($result) == 0) return false;

      $row = red_sql_fetch_row($result);
      // one record
      if(red_sql_num_rows($result) == 1)  {
        $item_id = $row[0];
        // we're updating the record we found - so ignore it
        if($item_id == $this->get_item_id()) return false;
      }
      // return the hosting order id of the first record
      return $row[1];
    }

    /**
     * Return list of authoritative name servers
     */
    public static function get_nameservers($domain) {
      if (defined('RED_TEST') || defined('RED_DEV')) {
        return ['a.ns.mayfirst.dev'];
      }
      if (self::is_local($domain)) {
        return ['a.ns.mayfirst.cx', 'b.ns.mayfirst.cx'];
      }
      return ['a.ns.mayfirst.org', 'b.ns.mayfirst.org', 'c.ns.mayfirst.org'];
    }  

    function validate_caa_value($tag, $value) {
      if ($tag == 'issue' || $tag == 'issuewild' || $tag == 'issuemail') {
        if ($value == ';') {
          return NULL;
        }
        $parts = explode(';', $value);
        if (count($parts) < 1) {
          return red_t('value should not be empty');
        }
        if (!preg_match(RED_DOMAIN_MATCHER, $parts[0])) {
          return red_t('value does not start with domain');
        }
        for ($i = 1; $i < count($parts); $i++) {
          $param = explode('=', $parts[$i], 2);
          if (count($param) != 2) {
            $msg_args = array('@param' => $parts[$i]);
            return red_t('you entered @param, should be key=value format', $msg_args);
          }
          [$tag, $value] = $param;
          if (!preg_match('/^[a-zA-Z0-9][-a-zA-Z0-9]*$/', $tag)) {
            $msg_args = array('@tag' => $tag);
            return red_t("you entered @tag: must be numbers, letters or - (but cannot start with -)", $msg_args);
          }
          if (!preg_match('/^[!-~]*$/', $value)) {
            $msg_args = array('@value' => $value);
            return red_t('you entered @value: must be a plain text character.', $msg_args);
          }
        }
      }
      elseif ($tag == 'iodef') {
        if (str_starts_with($value, 'mailto:') || str_starts_with($value, 'http://') || str_starts_with($value, 'https://')) {
          return NULL;
        }
        $msg_args = array('@value' => $value);
        return red_t('value @value must be a mailto:, http:// or https:// URL', $msg_args);
      }
      return NULL;
    }

    /**
     * Create all mail records.
     *
     * Given a domain name and hosting order, create all the required
     * mail related domain names.
     */
    public static function create_mail_records($hosting_order_id, $domain) {
      $co = [
        'mode' => 'ui', 
        'hosting_order_id' => $hosting_order_id,
        'service_id' => 9,
      ];

      $records = [];

      // Create MX records.
      $mx_servers = [ 'a', 'b', 'c' ];
      $base_domain = "mayfirst.org";
      shuffle($mx_servers);
      $assigned_servers = [
        10 => array_pop($mx_servers),
        20 => array_pop($mx_servers),
        30 => array_pop($mx_servers),
      ];
      foreach ($assigned_servers as $dist => $server) {
        $records[] = [
          'type' => 'mx',
          'fqdn' => $domain,
          'server' => "{$server}.mx.{$base_domain}",
          'dist' => $dist,
        ];
      }
      $records[] = [
        'type' => 'dkim',
        'fqdn' => $domain,
      ];
      $records[] = [
        'type' => 'txt',
        'fqdn' => $domain,
        'text' => 'v=spf1 a:spf.mayfirst.org ~all',
      ];
      $records[] = [
        'type' => 'txt',
        'fqdn' => "_dmarc.{$domain}",
        'text' => 'v=DMARC1; p=none;',
      ];
      $ttl = '3600';
      foreach ($records as $record) {
        $type = $record['type'] ?? NULL;
        $fqdn = $record['fqdn'] ?? NULL;
        $server = $record['server'] ?? NULL;
        $dist = $record['dist'] ?? NULL;
        $text = $record['text'] ?? NULL;

        $dns_item = red_item::get_red_object($co);
        $dns_item->set_dns_sshfp_algorithm(0);
        $dns_item->set_dns_sshfp_type(0);
        if (!$dns_item) {
          red_log("Failed to create dns object.");
          return false;
        }
        $dns_item->set_dns_type($type);
        $dns_item->set_dns_fqdn($fqdn);
        if ($dist) {
          $dns_item->set_dns_dist($dist);
        }
        if ($server) $dns_item->set_dns_server_name($server);
        if ($text) $dns_item->set_dns_text($text);
        $dns_item->set_dns_ttl($ttl);
        if (!$dns_item->validate()) {
          red_log("Failed to validate: " . $dns_item->get_errors_as_string());
          return FALSE;
        }
        if (!$dns_item->commit_to_db()) {
          red_log("Failed to commit to db: " . $dns_item->get_errors_as_string());
          return false;
        }
      }
      return TRUE;
    }
    /*
     * For a given zone, determine if all records have been deleted.
     */
    protected function zone_has_records() {
      $sql = "SELECT COUNT(*) 
        FROM red_item_dns JOIN red_item USING(item_id) 
        WHERE 
          item_status != 'pending-delete' AND
          item_status != 'deleted' AND 
          item_status != 'transfer-limbo'
          AND item_status != 'disabled' AND 
          item_status != 'pending-disable' AND 
          dns_zone = @zone";
      $result = red_sql_query($sql, ['@zone' => $this->get_dns_zone()]);
      $row = red_sql_fetch_row($result);
      if ($row[0] > 0) {
        return TRUE;
      }
      return FALSE;
    }

    /*
     * Get Public DKIM Key
     *
     * Either generate a DKIM public/private key pair, sync to mail relay
     * servers and return the public key, or just return the public key if
     * it has already been generated.
     */
    public static function get_dkim_public_key($item_id) {
      $sql = "SELECT dns_fqdn, dns_dkim_signing_domain, dns_dkim_selector, item_host
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $fqdn = $row[0];
      $signing_domain = $row[1] ?? NULL;
      $selector = $row[2];
      $server = $row[3];
      $domain = $fqdn;
      if ($signing_domain) {
        $domain = $signing_domain;
      }
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        "/usr/local/bin/dkim-generate",
        $domain,
        $selector,
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to generate or retrieve dkim key: " . implode(' ', $output));
      } 
      // Now we have to parse the output. We get something like (note multi line):
      //
      // pb1._domainkey  IN  TXT  ( "v=DKIM1; h=rsa-sha256; k=rsa; s=email; "
      // "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2jMXn/y2bIAe2xZOeDCts2UNz7YJvh5xy6+0aZMKn/Rt1swIB1UCWt60//VR6Ra4UUW7FcBpW3BX+qdQ0+kdZ6w9ekHYfhAk6pUZlBGs6+GPEFtBXcVLMiafHU2A0sK/Gvwzt9myPg+yF2mq055B/4hsaEdOD7pZ2dwUHy7OTjKCamaB2lwQA+HNDY1frgPQUBzGJZfc3JVy4i"
     // "Y2cpF6Hw5TNG0ohCfY3Paf2YzBtGE+RcpsPwfgGFiRDO1NmTowxkCPZMY3Gi8OlFlp2lPdrg4UgD+6jc9o71DnBPbnV7u/U56WRicMHMNCZqVDkQbcSe8Yi9khJy3W4Xsade3SzQIDAQAB" )  ; ----- DKIM key pb1 for myorg.mayfirst.dev
      // We want the part in quotes, and rsa-sha256 must be converted to just sha256.
      $dkim_sig = '';
      foreach($output as $line) {
        if (preg_match('/"(.*)"/', $line, $matches)) {
          if (substr($dkim_sig, -1, 1) == ';') {
            // If this is the line ending with s=email; then we want a space. Not required, but makes
            // it look better.
            $dkim_sig .= ' ';
          }
          $dkim_sig .= trim($matches[1]);
        }
        $dkim_sig = str_replace('h=rsa-sha256', 'h=sha256', $dkim_sig);
      }
      return ['item_id' => $item_id, 'dkim_sig' => $dkim_sig];

    }
    public static function append_dot($value) {
      // Ensure server_name end in a dot so we don't append $ORIGIN.
      if(!preg_match('/\.$/', $value)) {
        $value .= '.';
      }
      return $value;
    }

    /*
     * Set DKIM txt record
     *
     * Given the output from the commmand the generates the DKIM key,
     * parse it and then create a DNS txt record with the output.
     */
    public static function set_dkim_txt_record($output) {
      $item_id = $output['item_id'];
      $dkim_sig = $output['dkim_sig'];

      $sql = "SELECT dns_zone, dns_fqdn, dns_dkim_selector, dns_ttl, item_host
        FROM red_item JOIN red_item_dns USING (item_id) 
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $zone = $row[0];
      $fqdn = $row[1];
      $selector = $row[2];
      $ttl = $row[3];
      $item_host = $row[4];

      $domain = self::append_dot("{$selector}._domainkey.{$fqdn}");
      $args = [
        "red-remote-tasker@{$item_host}",
        "sudo",
        "/usr/local/share/red/node/share/dns/update-record",
        $zone,
        $domain,
        $ttl,
        "TXT",
        "'$dkim_sig'",
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to set dkim txt record: " . implode(' ', $output));
      }
      return TRUE;
    }

    /*
     * Update DNS record. 
     *
     * Given the item_id, update the record on the ns staging server. 
     * NOTE: For DNS record types that can have multiple valies (e.g. A
     * records) we have to send all the values for the given domain name,
     * not just the value being edited.
     *
     */
    public static function update_record($item_id) {
      $sql = "SELECT dns_zone, dns_type, dns_fqdn, dns_ip, dns_ttl, dns_server_name, dns_text,
        dns_dist, dns_port, dns_weight, dns_sshfp_algorithm, dns_sshfp_type, dns_sshfp_fpr,
        dns_caa_flags, dns_caa_tag, dns_caa_value, item_host
        FROM red_item JOIN red_item_dns USING (item_id) WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $zone = $row[0];
      $type = $row[1];
      $fqdn = $row[2];
      $ip = $row[3];
      $ttl = $row[4];
      $server_name = $row[5];
      $text = $row[6];
      $dist = $row[7];
      $port = $row[8];
      $weight = $row[9];
      $sshfp_algorithm = $row[10];
      $sshfp_type = $row[11];
      $sshfp_fpr = $row[12];
      $caa_flags = $row[13];
      $caa_tag = $row[14];
      $caa_value = $row[15];
      $item_host = $row[16];
      
      // Base arguments for all types.
      $common_args = [
        "red-remote-tasker@{$item_host}",
        "sudo",
        "/usr/local/share/red/node/share/dns/update-record",
        $zone,
      ];
      $standard_types = ['a', 'aaaa', 'cname', 'alias'];
      if (in_array($type, $standard_types)) {
        $extra_args = self::get_standard_multi_value_update_args($type, $fqdn, $ttl);
      }
      elseif ($type == 'txt') {
        $extra_args = self::get_txt_update_args($fqdn, $ttl);
      }
      elseif ($type == 'ptr') {
        $extra_args = self::get_ptr_update_args($ip, $fqdn, $ttl);
      }
      elseif ($type == 'mx') {
        $extra_args = self::get_mx_update_args($fqdn, $ttl);
      }
      elseif ($type == 'srv') {
        $extra_args = self::get_srv_update_args($fqdn, $ttl);
      }
      elseif ($type == 'sshfp') {
        $extra_args = self::get_sshfp_update_args($fqdn, $ttl);
      }
      elseif ($type == 'caa') {
        $extra_args = self::get_caa_update_args($fqdn, $ttl);
      }
      $args = array_merge($common_args, $extra_args);
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to update record: " . implode(' ', $output));
      }
      return TRUE;
    }

    public static function get_standard_multi_value_update_args($type, $fqdn, $ttl) {
      if ($type == 'a' || $type == 'aaaa') {
        $value_field = 'dns_ip';
      }
      elseif ($type == 'cname' || $type == 'alias') {
        $value_field = 'dns_server_name';
      }
      else {
        throw new \RedException("Unknown standard dns type: {$type}");
      }
      // Get all matching records with this fqdn and type.
      $sql = "SELECT !value_field FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_status IN ('active','pending-insert','pending-update','pending-restore') AND
        dns_fqdn = @fqdn AND dns_type = @type";
      $params = [
        '!value_field' => $value_field,
        '@fqdn' => $fqdn,
        '@type' => $type,
      ];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $value = $row[0];
        if ($type == 'cname') {
          $value = self::append_dot($value);
        }
        $values[] = $value;
      }
      $type = strtoupper($type);
      return [
        self::append_dot($fqdn),
        $ttl,
        $type,
        implode(chr(31), $values)
      ];
    } 

    public static function get_txt_update_args($fqdn, $ttl) {
      // Get all matching records with this fqdn
      $sql = "SELECT dns_text FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_status IN ('active','pending-insert','pending-update','pending-restore') AND
        dns_fqdn = @fqdn AND dns_type = 'txt'";
      $params = [
        '@fqdn' => $fqdn,
      ];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $value = $row[0];
        $values[] = $value;
      }
      $values = implode(chr(31), $values);
      return [
        self::append_dot($fqdn),
        $ttl,
        'TXT',
        "'$values'",
      ];
    }
    
    public static function get_srv_update_args($fqdn, $ttl) {
      // Get all matching SRV records with this fqdn.
      $sql = "SELECT dns_server_name, dns_dist, dns_port, dns_weight 
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_status IN ('active','pending-insert','pending-update','pending-restore') AND
        dns_fqdn = @fqdn AND dns_type = 'srv'";
      $params = ['@fqdn' => $fqdn];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $server_name = self::append_dot($row[0]);
        $dist = $row[1];
        $port = $row[2];
        $weight = $row[3];
        $values[] = "{$dist} {$weight} {$port} {$server_name}";
      }
      $values = implode(chr(31), $values);
      $args = [self::append_dot($fqdn), $ttl, 'SRV', "'$values'" ];
      return $args;
    }

    public static function get_sshfp_update_args($fqdn, $ttl) {
      // Get all matching SSHFP records with this fqdn.
      $sql = "SELECT dns_sshfp_algorithm, dns_sshfp_type, dns_sshfp_fpr 
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_status IN ('active','pending-insert','pending-update','pending-restore') AND
        dns_fqdn = @fqdn AND dns_type = 'sshfp'";
      $params = ['@fqdn' => $fqdn];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $algorithm = $row[0];
        $type = $row[1];
        $fpr = $row[2];
        $values[] = "{$algorithm} {$type} {$fpr}";
      }
      $values = implode(chr(31), $values);
      $args = [self::append_dot($fqdn), $ttl, 'SSHFP', "'$values'"];
      return $args;
    }

    public static function get_caa_update_args($fqdn, $ttl) {
      // Get all matching CAA records with this fqdn.
      $sql = "SELECT dns_caa_flags, dns_caa_tag, dns_caa_value 
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_status IN ('active','pending-insert','pending-update','pending-restore') AND
        dns_fqdn = @fqdn AND dns_type = 'caa'";
      $params = ['@fqdn' => $fqdn];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $flags = $row[0];
        $tag = $row[1];
        $caa = $row[2];
        $values[] = "{$flags} {$tag} {$caa}";
      }
      $values = implode(chr(31), $values);
      $args = [self::append_dot($fqdn), $ttl, 'CAA', "'$values'"];
      return $args;
    }

    public static function get_mx_update_args($fqdn, $ttl) {
      // Get all matching MX records with this fqdn.
      $sql = "SELECT dns_server_name, dns_dist
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE 
          item_status IN ('active','pending-insert','pending-update','pending-restore') AND
          dns_fqdn = @fqdn AND
          dns_type = 'mx'
      ";
      $params = ['@fqdn' => $fqdn];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $server_name = self::append_dot($row[0]);
        $dist = $row[1];
        $values[] = "{$dist} {$server_name}";
      }
      $values = implode(chr(31), $values);
      $args = [self::append_dot($fqdn), $ttl, 'MX', "'$values'"];
      return $args;
    }

    public static function get_ptr_update_args($ip, $fqdn, $ttl) {
      // Get all matching PTR records with this ip. Should be either
      // 1 record or 0 records if we are deleting.
      $sql = "SELECT dns_fqdn
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE 
          item_status IN ('active','pending-insert','pending-update','pending-restore') AND
          dns_ip = @ip AND
          dns_type = 'ptr'
      ";
      $params = ['@ip' => $ip];
      $result = red_sql_query($sql, $params);
      $values = [];
      while ($row = red_sql_fetch_row($result)) {
        $values[] = self::append_dot($row[0]);
      }
      $values = implode(chr(31), $values);

      if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
        $unpacked = unpack("H*hex", inet_pton($ip));
        // Chop off the first 64 bits - that's the zone.
        $significant_part = substr($unpacked['hex'], 16);
        // Reverse it (zone files want it backwards, what can I say?).
        $reversed = strrev($significant_part);
        // Now separate with periods.
        $ip_last_part = substr(preg_replace("/([A-f0-9]{1})/", "$1.", $reversed), 0, -1); 
      }
      else {
        // IPv4
        $ip_parts = explode('.', $ip);
        $ip_last_part = array_pop($ip_parts);
      }
      $args = [$ip_last_part, $ttl, 'PTR', "'$values'"];
      return $args;
    }

    /**
     * Purge zone conf files. 
     *
     */
    public static function purge_zone($item_id) {
      $sql = "SELECT item_host, dns_zone
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $server = $row[0];
      $zone = $row[1];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        "/usr/local/share/red/node/share/dns/purge-zone",
        $zone,
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to purge zone: " . implode(' ', $output));
      }
      return TRUE;
    }

    /**
     * Initialize the zone conf files. 
     *
     */
    public static function initialize_zone($item_id) {
      $sql = "SELECT item_host, dns_zone 
        FROM red_item JOIN red_item_dns USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $server = $row[0];
      $zone = $row[1];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        "/usr/local/share/red/node/share/dns/initialize-zone",
        $zone,
        implode(':', self::get_nameservers($zone)),
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to initialize zone: " . implode(' ', $output));
      }
      return TRUE;
    }

    /**
     * Delete alias / target link
     *
     * When deleting an ALIAS record, ensure it is purged from the sqlite database.
     */
    public static function unlink_alias_from_target($item_id) {
      $sql = "SELECT dns_zone, dns_fqdn, dns_server_name, item_host
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $zone = $row[0];
      $fqdn = $row[1];
      $target = $row[2];
      $item_host = $row[3];
      $args = [
        "red-remote-tasker@{$item_host}",
        "sudo",
        "/usr/local/share/red/node/share/dns/unlink-alias-from-target",
        $zone,
        $fqdn,
        $target
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to unlink alias from target: " . implode(' ', $output));
      }
      return TRUE;
    }
    /**
     * Link alias to target
     *
     * When creating an ALIAS record, add the domain to our sqlite database
     * so we can update it if the IP changes.
     */
    public static function link_alias_to_target($item_id) {
      $sql = "SELECT dns_zone, dns_fqdn, dns_ttl, dns_server_name, item_host
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $zone = $row[0];
      $fqdn = $row[1];
      $ttl = $row[2];
      $target = $row[3];
      $item_host = $row[4];
      $args = [
        "red-remote-tasker@{$item_host}",
        "sudo",
        "/usr/local/share/red/node/share/dns/link-alias-to-target",
        $zone,
        $fqdn,
        $ttl,
        $target
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to link alias to target: " . implode(' ', $output));
      }
      return TRUE;
    }

    /**
     * Delete DKIM keys
     *
     * When deleting a DKIM record we have to clean up.
     */
    public static function purge_dkim($item_id) {
      $sql = "SELECT dns_fqdn, dns_dkim_signing_domain, dns_dkim_selector, item_host
        FROM red_item JOIN red_item_dns USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $fqdn = $row[0];
      $signing_domain = $row[1] ?? NULL;
      $selector = $row[2];
      $server = $row[3];
      $domain = $fqdn;
      if ($signing_domain) {
        $domain = $signing_domain;
      }
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        "/usr/local/share/red/node/share/dns/purge-dkim",
        $domain,
        $selector,
      ];
      $output = [];
      if (!red_ssh_cmd($args, $output)) {
        throw new \RedException("Failed to purge dkim: " . implode(' ', $output));
      }
      return TRUE;
    }
  }
}

?>
