<?php

// email-account - this class is for mainipulating email accounts
if(!class_exists('red_item_web_conf')) {
  class red_item_web_conf extends red_item {
    static public $track_disk_usage = TRUE;
    var $_web_conf_login;
    var $_web_conf_execute_as_user;
    var $_web_conf_settings;
    var $_web_conf_cache_type = 'none';
    var $_web_conf_cache_settings;
    var $_web_conf_domain_names;
    var $_web_conf_port;
    var $_web_conf_cgi = 0;
    var $_web_conf_max_processes = '12';
    var $_web_conf_max_memory = '256';
    // 1 means add https, 0 means http only.
    var $_web_conf_tls;
    // a value for key or cert means use that key or cert, leave blank
    // means use lets encrypt.
    var $_web_conf_tls_key;
    var $_web_conf_tls_cert;
    // 0 means don't redirect http to https automaticalluy, 1 means redirect
    // (but leave exception for .well-known/acme-challenge).
    var $_web_conf_tls_redirect = 1;
    var $_web_conf_document_root;
    // 0 means no logging at all, 1 means only error logging, 2 means full logging. 
    var $_web_conf_logging = 2;
    var $_web_conf_php_version = NULL;
    var $_web_conf_mountpoint;
    var $_web_conf_writer_uid;
    var $_web_conf_reader_uid;
    var $_web_conf_php_version_options = array(
      '8.3' => '8.3',
      '8.1' => '8.1',
      '7.4' => '7.4',
      '7.3' => '7.3',
      '7.2' => '7.2',
      '7.1' => '7.1',
      '7.0' => '7.0',
      '5.6' => '5.6 - DEPRECATED, will not be available after 2018-12-31',
    );

    var $_human_readable_description;
    var $_human_readable_name;
    var $_delete_confirmation_message;
    var $_item_quota = '5gb';

    // Specify which keys should be validated against which tests
    var $_email_values = array('ServerAdmin');
    var $_path_values = array(
      'AuthUserFile',
      'AuthOpenIDDBLocation',
      'SSLCACertificateFile',
      'Alias',
      'ScriptAlias',
    );

    // Double values are two values separated by a space
    // For these, test both parts
    var $_double_values = array('Alias','ScriptAlias');

    function __construct($co) {
      parent::__construct($co);
      if (!$this->on_new_infrastructure()) {
        $this->_track_disk_usage = FALSE;
      }
      else {
        $this->_track_disk_usage = TRUE;
      }
      // Now re-adjust disk usage visibility in case it changed.
      $this->adjust_disk_usage_visibility();

      $this->_set_child_datafields();
      // If it's a new item, we have to autoset the mountopint if one is
      // specified in red_server for this item's host.
      if (!$this->exists_in_db()) {
        $this->set_web_conf_mountpoint($this->get_mountpoint_for_host());
        // And auto set the user account UID
        $this->set_web_conf_reader_uid($this->get_next_uid());
        $this->set_web_conf_writer_uid($this->get_next_uid());
      }
      return true;
    }

    function get_default_php_version_for_host($host) {
      $default_php_version = NULL;  
      $server = $host;
      $sql = "SELECT server_default_php_version FROM red_server WHERE server = @server";
      $result = red_sql_query($sql, ['@server' => $server]);
      $row = red_sql_fetch_row($result);
      if ($row) {
        $default_php_version = $row[0];
      }
      return $default_php_version;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
        array(
          'web_conf_login' => array (
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Owner'),
            'description' => red_t("The sftp or ssh user owning the web file and responsible for the disk quota."),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'options' => $this->get_available_user_account_logins(),
            'input_type' => 'select',
            'filter' => true),
          'web_conf_execute_as_user' => array (
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Execute as'),
            'description' => red_t("The user that site scripts will execuate as."),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'options' => $this->get_available_user_account_logins(),
            'input_type' => 'select',
            'filter' => true),
          'web_conf_domain_names' => array (
            'req' => true,
            'pcre'   => RED_MULTIPLE_DOMAIN_WILDCARD_MATCHER,
            'pcre_explanation'   => RED_MULTIPLE_DOMAIN_WILDCARD_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Domain Names'),
            'description' => red_t("List all domain names this site should respond to. Domains must be entered in the DNS tab even if managed by external DNS service."),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => TRUE,
            'text_length' => 88,
            'text_max_length' => '512',
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_tls' => array (
            'req' => true,
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Encryption'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => array(0 => 'http only', 1 => 'https enabled'),
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_document_root' => array (
            'req' => false,
            'pcre'   => RED_PATH_MATCHER,
            'pcre_explanation'   => RED_PATH_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Document Root'),
            'description' => red_t("If your web files are in a subdirectory of your web directory, enter the subdirectory here (without a leading slash). Your absolute web root path is: %path.", [ '%path' => $this->get_web_root_path()]),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'text_length' => 88,
            'text_max_length' => '512',
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_settings' => array (
            'req' => false,
            'pcre'   => false,
            'pcre_explanation'   => false,
            'type'  => 'varchar',
            'fname'  => red_t('Settings'),
            'description' => red_t("Add any additional apache configurations you want."),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'textarea',
            'textarea_cols' => 50,
            'textarea_rows' => 5,
            'filter' => true),
          'web_conf_logging' => array (
            'req' => true,
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Logging'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'select',
            'options' => array(0 => red_t("Disable Logging"), 1 => red_t("Error logging only"), 2 => red_t("Access and Error logging")),
            'filter' => true),
          'web_conf_max_processes' => array (
            'req' => true,
            'pcre'   => RED_INT_MATCHER,
            'pcre_explanation'   => RED_INT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Max PHP Processes'),
            'description' => red_t("Maximum number of allowed PHP processes, 0 to disable PHP"),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_max_memory' => array (
            'req' => true,
            'pcre'   => RED_INT_MATCHER,
            'pcre_explanation'   => RED_INT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Max PHP Memory'),
            'description' => red_t("PHP memory limit in Mb, contact support if you need more than 512"),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_cgi' => array (
            'req' => true,
            'pcre'   => RED_INT_MATCHER,
            'pcre_explanation'   => RED_INT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Enable CGI'),
            'description' => red_t("CGI is not needed for PHP"),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'select',
            'options' => array(0 => red_t('disabled'), 1 => red_t('enabled')),
            'filter' => true),
          'web_conf_tls_redirect' => array (
            'req' => true,
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Redirect http'),
            'description' => red_t("This setting is ignored on http only sites."),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'select',
            'options' => array(0 => red_t("Do not redirect traffic"), 1 => red_t("Redirect http traffic to https")),
            'filter' => true),
          'web_conf_tls_key' => array (
            'req' => false,
            'pcre'   => RED_PATH_MATCHER,
            'pcre_explanation'   => RED_PATH_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('TLS Key path'),
            'description' => red_t("Absolute path to https key (leave blank to use Lets Encrypt)"),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'text_length' => 88,
            'text_max_length' => '512',
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_tls_cert' => array (
            'req' => false,
            'pcre'   => RED_PATH_MATCHER,
            'pcre_explanation'   => RED_PATH_EXPLANATION,
            'type'  => 'varchar',
            'fname' => red_t("TLS Cert path"),
            'description'  => red_t('Absolute path to https cert (leave blank to use Lets Encrypt)'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'text_length' => 88,
            'text_max_length' => '512',
            'tblname'   => 'red_item_web_conf',
            'filter' => true),
          'web_conf_cache_type' => [
            'req' => true,
            'pcre'   => RED_WEB_CONF_CACHE_TYPE_MATCHER,
            'pcre_explanation'   => RED_WEB_CONF_CACHE_TYPE_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Cache Type'),
            'description' => red_t("Type of reverse cache"),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'select',
            'options' => $this->get_web_conf_cache_type_options(),
            'filter' => true
          ],
          'web_conf_cache_settings' => array (
            'req' => false,
            'pcre'   => false,
            'pcre_explanation'   => false,
            'type'  => 'varchar',
            'fname'  => red_t('Cache Settings'),
            'description' => red_t("Settings used by nginx caching server"),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'textarea',
            'textarea_cols' => 50,
            'textarea_rows' => 5,
            'filter' => true),
          'web_conf_php_version' => array (
            'req' => true,
            'pcre'   => RED_DECIMAL_MATCHER,
            'pcre_explanation'   => RED_DECIMAL_EXPLANATION,
            'type'  => 'decimal',
            'fname'  => red_t('PHP Version'),
            'description' => red_t("Choosing the non-default version may result in being auto-upgraded when that version stops being supported. Choose the default version to minimize transitions."),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf',
            'input_type' => 'select',
            'options' => $this->_web_conf_php_version_options,
            'filter' => true),
          'web_conf_mountpoint' => array (
            'req' => false,  
            'pcre'   => RED_ANYTHING_MATCHER,
            'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Mount Point'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 8,
            'text_max_length' => 8,
            'tblname'   => 'red_item_web_conf',
            'filter' => false),
          'web_conf_writer_uid' => array (
            'req' => false,
            'pcre'   => RED_ID_MATCHER,
            'pcre_explanation'   => RED_ID_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Writer User ID'),
            'user_insert' => FALSE,
            'user_update' => FALSE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf'
          ),
          'web_conf_reader_uid' => array (
            'req' => false,
            'pcre'   => RED_ID_MATCHER,
            'pcre_explanation'   => RED_ID_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Reader User ID'),
            'user_insert' => FALSE,
            'user_update' => FALSE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_web_conf'
          ),
        )
      );
    }

    function set_web_conf_login($value) {
      $this->_web_conf_login = $value;
    }
    function get_web_conf_login() {
      // Override if on new infrastructure.
      if ($this->on_new_infrastructure()) {
        return "site" . $this->get_item_id() . 'writer';
      }
      return $this->_web_conf_login;
    }

    function set_web_conf_execute_as_user($value) {
      $this->_web_conf_execute_as_user = $value;
    }
    function get_web_conf_execute_as_user() {
      // Override if on new infrastructure.
      // FIXME - adjust to reader if user wants to be more secure.
      if ($this->on_new_infrastructure()) {
        return "site" . $this->get_item_id() . 'writer';
      }
      return $this->_web_conf_execute_as_user;
    }

    function set_web_conf_domain_names($value) {
      $this->_web_conf_domain_names = $value;
    }
    function get_web_conf_domain_names() {
      return $this->_web_conf_domain_names;
    }

    function set_web_conf_settings($value) {
      $this->_web_conf_settings = $value;
    }
    function get_web_conf_settings() {
      return $this->_web_conf_settings;
    }

    function set_web_conf_max_processes($value) {
      $value = $value ?? '';
      $this->_web_conf_max_processes = trim($value);
    }
    function get_web_conf_max_processes() {
      return $this->_web_conf_max_processes;
    }

    function set_web_conf_cgi($value) {
      $value = $value ?? '';
      $this->_web_conf_cgi = trim($value);
    }
    function get_web_conf_cgi() {
      return $this->_web_conf_cgi;
    }
    function set_web_conf_logging($value) {
      $value = $value ?? '';
      $this->_web_conf_logging = trim($value);
    }
    function get_web_conf_logging() {
      return $this->_web_conf_logging;
    }

    function set_web_conf_document_root($value) {
      $value = $value ?? '';
      $this->_web_conf_document_root = trim($value);
    }
    function get_web_conf_document_root() {
      return $this->_web_conf_document_root;
    }
    function set_web_conf_tls($value) {
      $value = $value ?? '';
      $this->_web_conf_tls = trim($value);
    }
    function get_web_conf_tls() {
      return $this->_web_conf_tls;
    }
    function set_web_conf_tls_key($value) {
      $value = $value ?? '';
      $this->_web_conf_tls_key = trim($value);
    }
    function get_web_conf_tls_key() {
      return $this->_web_conf_tls_key;
    }
    function set_web_conf_tls_cert($value) {
      $value = $value ?? '';
      $this->_web_conf_tls_cert = trim($value);
    }
    function get_web_conf_tls_cert() {
      return $this->_web_conf_tls_cert;
    }
    function set_web_conf_tls_redirect($value) {
      $value = $value ?? '';
      $this->_web_conf_tls_redirect = trim($value);
    }
    function get_web_conf_tls_redirect() {
      return $this->_web_conf_tls_redirect;
    }

    function set_web_conf_max_memory($value) {
      $value = $value ?? '';
      $this->_web_conf_max_memory = trim($value);
    }
    function get_web_conf_max_memory() {
      return $this->_web_conf_max_memory;
    }
    function set_web_conf_cache_type($value) {
      $value = $value ?? '';
      $this->_web_conf_cache_type = trim($value);
    }
    function get_web_conf_cache_type() {
      return $this->_web_conf_cache_type;
    }

    function set_web_conf_cache_settings($value) {
      $value = $value ?? '';
      $this->_web_conf_cache_settings = trim($value);
    }
    function get_web_conf_cache_settings() {
      return $this->_web_conf_cache_settings;
    }
    function set_web_conf_php_version($value) {
      $value = $value ?? '';
      $this->_web_conf_php_version = trim($value);
    }
    function get_web_conf_php_version() {
      if (empty($this->_web_conf_php_version)) {
        // Set default if we can.
        $host = $this->get_item_host();
        if ($host) {
          $this->_web_conf_php_version = $this->get_default_php_version_for_host($host);
        } 
      }
      return $this->_web_conf_php_version;
    }
    function set_web_conf_mountpoint($value) {
      $this->_web_conf_mountpoint = $value;
    }
    function get_web_conf_mountpoint() {
      return $this->_web_conf_mountpoint;
    }
    function set_web_conf_writer_uid($value) {
      $this->_web_conf_writer_uid = $value;
    }
    function get_web_conf_writer_uid() {
      return $this->_web_conf_writer_uid;
    }
    function set_web_conf_reader_uid($value) {
      $this->_web_conf_reader_uid = $value;
    }
    function get_web_conf_reader_uid() {
      return $this->_web_conf_reader_uid;
    }

    function get_web_root_path() {
      // For new records, we don't have an item id so we can't provide a path.
      if (!$this->get_item_id()) {
        return red_t("[create web configuration first, then edit to see path]");
      }
      return $this->get_site_dir() . '/web/' . $this->get_web_conf_document_root();
    }

    function additional_validation() {
      // If we're deleting - don't need to do most validation
      // (and we don't want to choke on something like, user does
      // not exist)
      if($this->_delete) {
        if ($this->on_new_infrastructure()) {
          // If we delete the web site before deleting the scheduled jobs the jobs
          // get abandoned. 
          $sql = "SELECT COUNT(*) FROM red_item 
            WHERE 
              hosting_order_id = #hosting_order_id AND 
              service_id = 24 AND
              item_status != 'deleted' AND item_status != 'pending-delete'";
          $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            $this->set_error(red_t("Please delete your scheduled jobs before deleting your web configuration."), 'validation');
          }

          // If we delete the web site without deleting the server access, then when a user
          // sftp's into the shell server, they will get an empty folder on the shell server
          // to copy files to rather than their actual web site. Those files will probably end
          // up deleted.
          $sql = "SELECT COUNT(*) FROM red_item 
            WHERE 
              hosting_order_id = #hosting_order_id AND 
              service_id = 3 AND
              item_status != 'deleted' AND item_status != 'pending-delete'";
          $result = red_sql_query($sql, ['#hosting_order_id' => $this->get_hosting_order_id()]);
          $row = red_sql_fetch_row($result);
          if ($row[0] > 0) {
            $this->set_error(red_t("Please delete your server access accounts before deleting your web configuration."), 'validation');
          }
        }
        return;
      }
      if ($this->on_new_infrastructure()) {
        
        if (!defined('RED_TEST') && !defined('RED_DEV')) {
          if (!$this->_disable && !$this->_delete) {
            if ($this->get_web_conf_tls() == 1) {
              // Ensure all of their domains are properly pointing to us.
              $domain_names = explode(' ', $this->get_web_conf_domain_names());
              foreach($domain_names as $domain) {
                $domain = trim($domain);
                if (!$domain) {
                  continue;
                }
                $url = "http://{$domain}/.well-known/acme-challenge/test";
                $expected = "This domain should pass a Let's Encrypt challenge.";
                // Make the time out shorter, just 10 seconds instead of default 60.
                $ctx = stream_context_create(array('http'=>
                  array(
                      'timeout' => 10,
                      'user_agent' => 'May First Red Control Panel',
                  )
                ));
                // The default is no include path.
                $use_include_path = FALSE;
                $out = file_get_contents($url, $use_include_path, $ctx);
                if (trim($out) != $expected) {
                  $err = red_t("The domain @domain does not seem to be resolving to our servers. If you recently configured the domain name, please wait a few minutes and try again. Or set encryption to http only. Otherwise, contact support for more help.", ['@domain' => $domain]);
                  $this->set_error($err, 'validation');
                }
              }
            }
          }
        }

        // Check if we need to convert and do it if necessary.
        $this->convert_to_new_infrastructure();
      }

      $login = $this->get_web_conf_login();

      $web_conf_count = $this->web_conf_count();
      if($web_conf_count > 0) {
        $msg = red_t("You can only have one web configuration per hosting order.");
        $this->set_error($msg,'validation');
      }

      // make sure login as execute_as_user are valid users
      $related_logins = $this->get_available_user_account_logins();
      if (!$this->on_new_infrastructure()) {
        if($login != 'none' && !in_array($login, $related_logins)) {
          $this->set_error(red_t("You must specify a user account that exists."),'validation');
        }
        $execute_as_user = $this->get_web_conf_execute_as_user();
        if($execute_as_user != 'none' && !in_array($execute_as_user, $related_logins)) {
          $this->set_error(red_t("You must specify a user account that exists."),'validation');
        }
      }

      // Ensure domains are owned by this user.
      $domain_names = explode(' ', $this->get_web_conf_domain_names());
      $member_id = intval($this->get_member_id());
      foreach ($domain_names as $domain_name) {
        $domain_name = trim($domain_name);
        if (!$domain_name) {
          continue;
        }
        if (substr($domain_name, -6) === ".onion") {
          continue;
        }
        $sql = "SELECT COUNT(*) 
          FROM 
            red_item JOIN 
            red_item_dns USING (item_id) JOIN
            red_hosting_order USING (hosting_order_id)
          WHERE
            dns_type IN('a','alias','cname') AND
            dns_fqdn = @domain_name AND
            item_status != 'deleted' AND
            red_hosting_order.member_id = #member_id
        ";
        $result = red_sql_query($sql, ['@domain_name' => $domain_name, '#member_id' => $member_id]);
        $row = red_sql_fetch_row($result);
        if ($row[0] == 0) {
          $this->set_error(red_t("The domain (@domain_name) is not entered as a DNS record under your membership.", [ '@domain_name' => $domain_name]), 'validation');
        }
      }

      // Ensure no other web_conf on the same server is using any of these domain names.
      $host = $this->get_item_host();
      $escaped_domain_names = [];
      foreach ($domain_names as $domain_name) {
        $domain_name = trim(red_sql_real_escape_string($domain_name));
        if (!$domain_name) {
          continue;
        }
        // Remove wildcards.
        $domain_name = str_replace('*.', '', $domain_name);
        $domain_name = str_replace('*', '', $domain_name);
        $escaped_domain_names[] = preg_quote($domain_name);
      }
      // Match space separated lists of domain names, e.g. "www.mayfirst.org mayfirst.org foo.mayfirst.org"
      // We match either the begining of the string or a space, followed by the domain name followed by
      // either the end of the string or a space.
      $regexp = '(^| )(' . implode('|', $escaped_domain_names) . ')($| )';
      $sql = "SELECT COUNT(*)
        FROM red_item JOIN red_item_web_conf USING (item_id)
        WHERE
          item_status != 'deleted' AND
          item_status != 'disabled' AND
          item_host = @host AND
          web_conf_domain_names REGEXP '$regexp'
      ";
      $params = [];
      $params['@host'] = $host;
      if ($this->exists_in_db()) {
        $sql .= " AND red_item.item_id != #item_id";
        $params['#item_id'] = $this->get_item_id();
      }
      $result = red_sql_query($sql, $params);
      $row = red_sql_fetch_row($result);
      if ($row[0] > 0) {
        $msg = red_t("One of your domains is configured for use in another web configuration on the same host. Please remove it from the other web configuration before continuing.");
        $this->set_error($msg, 'validation');
      }
      $settings = $this->get_web_conf_settings();
      $settings_array = explode("\n", $settings);

      // ensure no domains are listed twice.
      $non_unique = array_diff_assoc($domain_names, array_unique($domain_names));
      if ($non_unique) {
        $this->set_error(red_t("You have listed the following domain names more than once: @non_unique_str.", [ '@non_unique_str' => implode(',', $non_unique) ]), 'validation');
      }

      foreach($settings_array as $line) {
        $line = trim($line);
        // Allow blank lines and lines that start with a comment
        // per dkg's request
        if ($line == '') {
          continue;
        }
        if (preg_match('/^#/',$line)) {
          continue;
        }
        // All lines must have at least one space to match the
        // regexp below (FIXME: need a better regexp). If the
        // line has no spaces, like: </Directory>, add one to the
        // end so it matches
        if (false === strpos($line,' ')) {
          $line .= ' ';
        }
        if (preg_match('#^(.*?) (.*)?#', $line, $matches) > 0) {
          $key = trim($matches[1]);
          $given_value = '';
          if (array_key_exists(2, $matches)) {
            $given_value = trim($matches[2]);
          }
          if (!preg_match(RED_WEB_CONF_KEY_MATCHER, $key)) {
            $line = $this->get_htmlentities($line);
            $key = $this->get_htmlentities($key);
            $this->set_error(RED_WEB_CONF_KEY_EXPLANATION, 'validation');
            $this->set_error(red_t("You entered: @key on line: @line.",array('@key' => $key, '@line' => $line)),'validation');
            continue 1;
          }
          if ($key == '<Directory') {
            $stripped_value = trim(rtrim($given_value, '>'));
            $this->validate_path($stripped_value);
          }
          // red_set_message("Sanity checkeroo, key: $key, given value: $given_value");
          if ($key == 'LogLevel') {
            $allowed = ['debug', 'info', 'notice', 'warn', 'error', 'crit', 'alert', 'emerg'];
            if (!in_array($given_value,$allowed)) {
              $this->set_error(red_t("LogLevel can only be set to one of: @loglevels." ,array('@loglevels' => implode(' ',$allowed))), 'validation');
            }
          }
          if ($key == 'Satisfy') {
            $allowed = ['All','Any'];
            if (!in_array($given_value,$allowed)) {
              $this->set_error(red_t("Satisfy should be set to either All or Any."),'validation');
            }
          }
          if ($key == 'SuexecUserGroup' ) {
            $group_name = $this->get_hosting_order_unix_group_name();
            $login = $this->get_web_conf_login();
            $user_group = explode(' ',$given_value);
            if(($user_group[0] != $login && $user_group != '#user#') || $user_group[1] != $group_name) {
              $this->set_error("SuexecUserGroup must be set to $login $group_name", 'validation');
            }
          }
          $check = '';
          if (in_array($key,$this->_email_values)) {
            $check = 'email';
          }
          if (in_array($key,$this->_path_values)) {
            $check = 'path';
          }

          // If this value actually has two values in it (such as Alias or
          // ScriptAlias). The first value is the URL alias and the second is
          // the directory.
          if (in_array($key, $this->_double_values)) {
            $url_path = explode(' ',$given_value)[0];
            if (!preg_match(RED_PATH_MATCHER, $url_path)) {
              $this->set_error(red_t("Illegal characters found in URL path: @url_path", ['@url_path' => $url_path]), 'validation');
            }
            // Validate the second value below.
            $given_value = explode(' ',$given_value)[1];
          }
          switch($check) {
            case 'email':
              if (!preg_match(RED_EMAIL_MATCHER, $given_value)) {
                $this->set_error(RED_EMAIL_EXPLANATION,'validation');
              }
              break;
            case 'path':
              $this->validate_path($given_value);
              break;
          }
        }
        else {
          // might have html code that will hide it
          $line = $this->get_htmlentities($line);
          $this->set_error(red_t("There was an error parsing your configuration. Please review your changes and try again."),'validation');
        }
      }
      if ($this->get_web_conf_cache_type() == 'custom') {
        // There's not a lot we can do but at least we can catch obvious problems.
        $required_lines = [
         'listen' => red_t("There does not seem to be a listen line."),
          trim('server_name ' . $this->get_web_conf_domain_names()) => red_t("Please include a server_name line followed by the domains listed for this web configuration."),
          'proxy_pass' => red_t("There does not seem to be a single proxy_pass line."),
        ];
        $cache_settings = explode("\n", $this->get_web_conf_cache_settings());
        foreach ($required_lines as $required_line => $error) {
          $found = FALSE;
          foreach ($cache_settings as $cache_line) {
            if (preg_match("/$required_line/", $cache_line)) {
              $found = TRUE;
            }
          }
          if (!$found) {
            $this->set_error($error, 'validation');
          }
        }
      }
      if (preg_match('/[.][.]/', $this->get_web_conf_document_root())) {
        $this->set_error(red_t("Your document root cannot have two periods in them."));
      }
    }

    /**
     * Ensure path is in site directory.
     */
    function validate_path($path) {
      $path = trim($path, '"');
      $path = trim($path, "'");
      if (strpos($path, $this->get_site_dir()) !== 0) {
        $params = ['@site_dir' => $this->get_site_dir(), '@path' => $path];
        $msg = red_t("Paths must be in site directory, which is @site_dir. You entered @path.", $params);
        $this->set_error($msg, 'validation');
      }
      if (!preg_match(RED_PATH_MATCHER,$path)) {
        $this->set_error(RED_PATH_EXPLANATION . red_t("You entered @path.", ['@path' => $path]),'validation');
      }
      if (preg_match('/[.][.]/', $path)) {
        $this->set_error(red_t("Paths cannot have two periods in them."), 'validation');
      }
    }

    /**
     * Count existing web confs other than this one
     *
     * We need to be sure there are no
     * other web confs in place.
     **/
    function web_conf_count() {
      $hosting_order_id = intval($this->get_hosting_order_id());
      $item_id = intval($this->get_item_id());
      $sql = "SELECT red_item.item_id FROM red_item INNER JOIN ".
        "red_item_web_conf ON red_item.item_id = ".
        "red_item_web_conf.item_id WHERE ".
        "hosting_order_id = #hosting_order_id  AND red_item.item_id != #item_id ".
        "AND red_item.item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id, '#item_id' => $item_id]);
      return intval(red_sql_num_rows($result));
    }

    function get_web_conf_cache_type_options() {
      return [
        'none' => red_t("No caching"),
        'performance' => red_t("Cache static items for better performance"),
        'custom' => red_t("Custom cache settings"),
      ];
    }
    function convert_to_new_infrastructure() {
      // Make transition changes.
      if ($this->transitioning_to_new_infrastructure()) {
        $old_settings = $this->get_web_conf_settings();
        $new_settings = [];
        foreach (explode("\n", $old_settings) as $line) {
          $new_settings[] = red_replace_old_paths($line, $this->get_item_id());
        }
        if ($new_settings != $old_settings) {
          $this->set_web_conf_settings(implode("\n", $new_settings));
        }
      }
    }
    public static function delete_proxy_configuration($item_id) {
      global $globals;
      $config = $globals['config'];
      foreach($config['web_proxy_servers'] as $server) {
        if (!self::delete_and_nginx_reload($item_id, $server)) {
          throw new RedException("Failed to delete and nginx reload ($item_id) to $server");
          return FALSE;
        }
      }
      return TRUE;
    }

    public static function update_certs_file() {
      global $globals;
      $config = $globals['config'];

      $error = NULL;
      $certs_file = self::generate_certs_file();
      foreach($config['key_servers'] as $server) {
        if (!self::rsync_and_certify($certs_file, $server)) {
          $error = "Failed to generate a https certificate. Please ensure 
            all domains are properly configured with an ALIAS DNS record 
            pointing to one of our web proxy servers.";
          break;
        }
      } 
      unlink($certs_file);
      if ($error) {
        throw new RedException($error);
      }
      return TRUE;
    }

    public static function update_default_web_proxies_file() {
      global $globals;
      $config = $globals['config'];

      $error = NULL;
      $web_proxies_file = self::generate_default_web_proxies_file();
      foreach ($config['key_servers'] as $server) {
        $key = "default.web.proxies.{$server}";
        if (!red_file_in_sync($key, $web_proxies_file)) {
          if (!red_rsync($web_proxies_file, "red-remote-tasker@{$server}:/var/lib/red/default-web-proxies.yml")) {
            $error = "Failed to rsync web_proxies_yaml to {$server}";
            break;
          }
          red_update_file_sync($key, $web_proxies_file);
        }
      }
      unlink($web_proxies_file);
      if ($error) {
        throw new RedException($error);
      }
    }

    /**
     * Copy over nginx proxy for a given web configuration.
     *
     * $values are the values of the web conf.
     * $reload - whether or not to reload nginx after copying.
     * $server - send to specified server rather than all servers.
     */
    public static function set_proxy_configuration($values) {
      global $globals;
      $error = NULL;

      $config_file = self::get_nginx_cache_file($values);
      $item_id = $values['item_id'];
      foreach($globals['config']['web_proxy_servers'] as $server) {
        if (!self::rsync_and_nginx_reload($config_file, $item_id, $server)) {
          $error = "Failed to rsync and nginx reload ({$item_id}) to {$server}";
          break;
        }
      }
      unlink($config_file);
      if ($error) {
        throw new RedException($error);
      }
      return TRUE;
    }
    static public function build_nginx_cache_file($item_id, $item_host, $domains, $tls, $tls_redirect, $logging, $cache_type, $cache_settings) {
      // Early return if we are using custom settings.
      if ($cache_type == 'custom') {
        return $cache_settings;
      }
      // Convert weborigin001.mayfirst.org to weborigin001.
      $host_pieces = explode('.', $item_host);
      $upstream = array_shift($host_pieces);
      $lines = [];
      // Begin the http server section.
      $lines[] = 'server {';
      $lines[] = '  listen 80;';
      $lines[] = "  server_name $domains;";
      $lines[] = '  include snippets/bad-bots.conf;';
      $lines[] = '  include snippets/passthrough-headers.conf;';
      $lines[] = '  include snippets/common.conf;';
      // Even http only sites get acme-challenge so lets encrypt requests are
      // properly proxied to our key server.
      $lines[] = '  include snippets/acme-challenge.conf;';
      if ($logging == 0) {
        $lines[] = "  error_log /dev/null emerg;";
        $lines[] = "  access_log off;";
      }
      else if ($logging == 1) {
        $lines[] = "  error_log /var/log/nginx/site{$item_id}.error.log;";
        $lines[] = "  access_log off;";
      }
      elseif ($logging == 2) {
        $lines[] = "  error_log /var/log/nginx/site{$item_id}.error.log;";
        $lines[] = "  access_log /var/log/nginx/site{$item_id}.access.log;";
      }
      if ($tls && $tls_redirect) {
        $lines[] = '  location / {';
        $lines[] = '    return 301 https://$host$request_uri;';
        $lines[] = '  }';
      }
      else {
        $lines[] = '  location / {';
        $lines[] = "    proxy_pass http://{$upstream};";
        $lines[] = '  }';
        if ($cache_type == 'performance') {
          $lines[] = '  location ~* \.(js|css|png|jpg|jpeg|gif|ico|webp|html|htm|pdf|txt|xml|svg|mp3|mp4|woff|ttf|otf)$ {';
          $lines[] = "    proxy_pass http://{$upstream};";
          $lines[] = '    proxy_cache my_cache;';
          $lines[] = '    proxy_cache_valid 15m;';
          $lines[] = '    expires 15m;';
          $lines[] = '    include snippets/performance-cache.conf;';
          $lines[] = '  }';
        }
      }
      $lines[] = '}';

      // Now the https server block.
      if ($tls) {
        $lines[] = 'server {';
        $lines[] = '  listen 443 ssl;';
        $lines[] = "  server_name $domains;";
        $lines[] = '  include snippets/bad-bots.conf;';
        $lines[] = '  include snippets/passthrough-headers.conf;';
        $lines[] = '  include snippets/common.conf;';
        $lines[] = '  include snippets/acme-challenge.conf;';
        if ($logging == 0) {
          $lines[] = "  error_log /dev/null emerg;";
          $lines[] = "  access_log off;";
        }
        else if ($logging == 1) {
          $lines[] = "  error_log /var/log/nginx/site{$item_id}.error.log;";
          $lines[] = "  access_log off;";
        }
        elseif ($logging == 2) {
          $lines[] = "  error_log /var/log/nginx/site{$item_id}.error.log;";
          $lines[] = "  access_log /var/log/nginx/site{$item_id}.access.log;";
        }
        $lines[] = "  ssl_certificate /etc/ssl/mayfirst/site{$item_id}/fullchain.pem;";
        $lines[] = "  ssl_certificate_key /etc/ssl/mayfirst/site{$item_id}/privkey.pem;";
        $lines[] = '  include snippets/tls-settings.conf;';
        $lines[] = '  location / {';
        $lines[] = "    proxy_pass https://{$upstream}_tls;";
        $lines[] = '  }';
        if ($cache_type == 'performance') {
          $lines[] = '  location ~* \.(js|css|png|jpg|jpeg|gif|ico|webp|html|htm|pdf|txt|xml|svg|mp3|mp4)$ {';
          $lines[] = "    proxy_pass https://{$upstream}_tls;";
          $lines[] = '    proxy_cache my_cache;';
          $lines[] = '    proxy_cache_valid 15m;';
          $lines[] = '    expires 15m;';
          $lines[] = '    include snippets/performance-cache.conf;';
          $lines[] = '  }';
        }
        $lines[] = '}';
      }
      return implode("\n", $lines) . "\n";
    }

    public static function get_nginx_cache_file($values) {
      $contents = self::build_nginx_cache_file(
        $values['item_id'], 
        $values['item_host'], 
        $values['domains'], 
        $values['tls'], 
        $values['tls_redirect'], 
        $values['logging'],
        $values['cache_type'],
        $values['cache_settings']
      );
      $item_id = $values['item_id'];
      $config_file = tempnam('/tmp', "site{$item_id}");
      file_put_contents($config_file, $contents);
      if (!file_exists($config_file)) {
        throw new RedException("Failed to generate caching config file'");
        return FALSE;
      }
      return $config_file;
    }

    static public function delete_and_nginx_reload($item_id, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-conf-proxy/delete-and-nginx-reload',
        $item_id,
      ];
      if (!red_ssh_cmd($args)) {
        return FALSE;
      }
      
      $key = "nginx.{$item_id}.{$server}.conf";
      red_purge_file_sync($key);

      return TRUE;
    }
    public static function rsync_and_nginx_reload($file, $item_id, $server, $reload = TRUE) {
      $key = "nginx.{$item_id}.{$server}.conf";
      if (red_file_in_sync($key, $file)) {
        return TRUE;
      }

      // First copy the file.
      $dest = "red-remote-tasker@{$server}:{$file}";
      if (!red_rsync($file, $dest)) {
        return FALSE;
      }

      if ($reload) {
        $reload_nginx = 'reload';
      }
      else {
        $reload_nginx = '';
      }
      // Now reload nginx.
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-conf-proxy/copy-and-nginx-reload',
        $file,
        $item_id,
        $reload_nginx,
      ];
      if (!red_ssh_cmd($args)) {
        return FALSE;
      }
      red_update_file_sync($key, $file);
      return TRUE;
    }
    public static function generate_certs_file() {
      global $globals;
      $config = $globals['config'];

      // Get all web configs with tls enabled.
      $sql = "SELECT red_item.item_id, web_conf_domain_names, item_host
        FROM red_item JOIN red_item_web_conf USING (item_id)
        WHERE 
          item_status NOT IN ('deleted', 'pending-delete', 'pending-disable', 'disabled', 'soft-error') AND
          web_conf_tls = 1 AND
          item_host REGEXP 'weborigin[0-9]+'";
      $results = red_sql_query($sql);
      $certs = [];
      if (defined('RED_TEST')) {
        $ca = 'mfca';
        $challenge = NULL; 
      }
      else {
        $ca = 'letsencrypt';
        $challenge = 'http';
      }
      while ($row = red_sql_fetch_row($results)) {
        $item_id = $row[0];
        $domain_names = $row[1];
        $item_host = $row[2];

        $cert = []; 
        $cert['name'] = "site{$item_id}";
        $cert['ca'] = $ca;
        if ($challenge) {
          $cert['challenge'] = $challenge;
        }
        $cert['common_names'] = explode(' ', $domain_names);

        $distribute_hosts = [];
        $distribute_hosts[$item_host] = [
          '/usr/local/sbin/mf-cert-renew-base',
          '/usr/local/sbin/mf-cert-renew-apache2'
        ];
        $cert['distribute_hosts'] = $distribute_hosts;
        $cert['distribute_hosts_file'] = '/var/lib/red/default-web-proxies.yml';
        $certs[] = $cert;
      }
      $yaml = red_yaml_emit($certs);
      $certs_file = tempnam('/tmp', "red-yaml");
      file_put_contents($certs_file, $yaml);
      return $certs_file;
    }

    public static function generate_default_web_proxies_file() {
      global $globals;
      // First generate and copy over the web proxies file.
      $web_proxies = [];
      foreach($globals['config']['web_proxy_servers'] as $server) {
        $web_proxies[$server] = [
          '/usr/local/sbin/mf-cert-renew-base',
          '/usr/local/sbin/mf-cert-renew-nginx'
        ];
      }
      $yaml = red_yaml_emit($web_proxies);
      $web_proxies_file = tempnam('/tmp', "red-web-proxies");
      file_put_contents($web_proxies_file, $yaml);
      return $web_proxies_file;
    }

    public static function ensure_user($item_id, $status) {
      $server = red_get_item_host_for_item_id($item_id);
      $sql = "SELECT web_conf_mountpoint, web_conf_writer_uid, item_quota 
        FROM red_item JOIN red_item_web_conf USING (item_id) 
        WHERE red_item.item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $mountpoint = $row[0];
      $uid = $row[1];
      $quota = $row[2];
      $args = [
        "red-remote-tasker@{$server}",
        "sudo"
      ];
      if ($status == 'exists') {
        // Quotas are set in 1024 byte blocks - so we have to divide bytes by 1024
        $quota_hard = ceil(intval($quota) / 1024);

        // By default ext4 assigns 1 inode for every 15 KB - so let's use that ratio
        $inodes_hard = ceil($quota / 15);

        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-user-exists';
        $args[] = $item_id;
        $args[] = $mountpoint;
        $args[] = $uid;
        $args[] = $quota_hard;
        $args[] = $inodes_hard;
      }
      else {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-user-gone';
        $args[] = $item_id;
      }
      
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure user {$status} for item {$item_id}");
      }
      return TRUE;
    }

    public static function ensure_directories($item_id, $status) {
      $server = red_get_item_host_for_item_id($item_id);
      $sql = "SELECT web_conf_mountpoint, web_conf_document_root FROM
        red_item_web_conf WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $mountpoint = $row[0] ?? '';
      $doc_root = $row[1] ?? '';
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
      ];
      if ($status == 'exists') {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-directories-exists';
        $args[] = $item_id;
        $args[] = $mountpoint;
        $args[] = $doc_root;
      }
      else {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-directories-gone';
        $args[] = $item_id;
        $args[] = $mountpoint;
      }

      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure directories {$status} for item {$item_id}");
      }
      return TRUE;
    }

    public static function ensure_php($item_id, $status) {
      $server = red_get_item_host_for_item_id($item_id);
      $sql = "SELECT web_conf_max_processes, web_conf_php_version, web_conf_max_memory
        FROM red_item_web_conf
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $row = red_sql_fetch_row($result);
      $processes = $row[0] ?? '';
      $version = $row[1] ?? '';
      $memory = $row[2] ?? '';

      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
      ];
      if ($status == 'exists') {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-php-exists';
        $args[] = $item_id;
        $args[] = $processes;
        $args[] = $version;
        $args[] = $memory;
      }
      else {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-php-gone';
        $args[] = $item_id;
      }
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure php {$status} for item {$item_id}");
      }
      return TRUE;
    }

    // Copy apache conf files for the given item id to the target weborigin.
    public static function ensure_apache_conf($item_id, $status) {
      $server = red_get_item_host_for_item_id($item_id);
      $sql = "SELECT * FROM red_item JOIN red_item_web_conf USING (item_id)
        WHERE item_id = #item_id";
      $result = red_sql_query($sql, ['#item_id' => $item_id]);
      $rs = red_sql_fetch_assoc($result);
      $co = ['mode' => 'ui', 'rs' => $rs];
      $item = red_item::get_red_object($co);
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
      ];

      if ($status == 'exists') {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-apache-conf-exists';
        $args[] = $item_id;
        $http_settings = tempnam(sys_get_temp_dir(), 'red-apache-conf');
        $port = 'http';
        if (!$item->create_apache_conf_file($port, $http_settings)) {
          throw new \RedException("Failed to ssh to {$server} to ensure apache conf {$status} for item {$item_id}");
        }
        if (!red_rsync($http_settings, "red-remote-tasker@{$server}:{$http_settings}")) {
          throw new \RedException("Failed to rsync http_settings to {$server}");
        }
        if (!unlink($http_settings)) {
          throw new \RedException("Failed to delete {$http_settings}.");
        }

        $args[] = $http_settings;
        $https_settings = '';
        if ($item->get_web_conf_tls()) {
          $https_settings = tempnam(sys_get_temp_dir(), 'red-apache-conf');
          $port = 'https';
          if (!$item->create_apache_conf_file($port, $https_settings)) {
            throw new RedException("Failed to create https apache file for {$item_id}");
          }
          if (!red_rsync($https_settings, "red-remote-tasker@{$server}:{$https_settings}")) {
            throw new \RedException("Failed to rsync https_settings to {$server}");
          }
          if (!unlink($https_settings)) {
            throw new \RedException("Failed to delete {$https_settings}.");
          }
          $args[] = $https_settings;
        }
      }
      else {
        $args[] = '/usr/local/share/red/node/share/web-conf/ensure-apache-conf-gone';
        $args[] = $item_id;
      }
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure apache web conf {$status} for item {$item_id}");
      }
      return TRUE;
    }

    public static function ensure_disabled($item_id) {
      $server = red_get_item_host_for_item_id($item_id);
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-conf/ensure-disabled',
        $item_id,
      ];
      if (!red_ssh_cmd($args)) {
        throw new RedException("Failed to ssh to {$server} to ensure disabled for item {$item_id}");
      }
      return TRUE;
    }

    public static function rsync_and_certify($file, $server) {
      $key = "red.certs.yaml.{$server}";
      if (red_file_in_sync($key, $file)) {
        return TRUE;
      }
      // Copy the file from the tmp directory on this server to the
      // tmp directory on the target server.
      if (!red_rsync($file, "red-remote-tasker@{$server}:{$file}")) {
        return FALSE;
      }

      # Run copy and certify.
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-conf-keystore/copy-and-certify',
        $file,
      ];
      if (!red_ssh_cmd($args)) {
        return FALSE;
      }
      red_update_file_sync($key, $file);
      return TRUE;
    }


    function create_apache_conf_file($port, $target) {
      $tls = $this->get_web_conf_tls();
      $tls_redirect = $this->get_web_conf_tls_redirect();
      if (!$this->on_new_infrastructure() && $port == 'http' && $tls == 1 && $tls_redirect == 1) {
        $content = $this->get_http_redirect_content();
      }
      else {
        $content = $this->get_apache_conf_file_content($port);
      }
      if(!red_write_to_file($target, $content)) {
        $message = "Failed to create apache conf file. Trying: ".
          $target;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }
    function get_http_redirect_content() {
      $content = "<VirtualHost *:80>\n";

      $content .= $this->get_apache_conf_file_shared_content();
      $content .= "\tRewriteEngine On\n";
      // Allow letsencrypt acme challenge to access via http or else
      // adding new domains to existing auto sites will fail because
      // letsencrypt authorization will get redirected to https and
      // the https certificate will not be valid.
      $content .= "\tRewriteCond %{REQUEST_URI} !/.well-known/acme-challenge/.*\n";
      $content .= "\tRewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}\n";
      $content .= "</VirtualHost>\n";
      return $content;
    }

    function get_apache_conf_file_content($port) {
      $port_number = 80;
      if ($port == 'https') $port_number = 443;
      $content = "<VirtualHost *:$port_number>\n";
      $content .= $this->get_apache_conf_file_shared_content();
      if($this->get_web_conf_cgi()) {
        $content .= "\t<Directory " . $this->get_site_dir() . '/cgi-bin' . '>' . "\n";
        $content .= "\t\tOptions FollowSymLinks ExecCGI\n";
        $content .= "\t\tAllowOverride  All\n";
        $content .= "\t\tRequire all granted\n";
        $content .= "\t</Directory>\n";
        $content .= "\tScriptAlias /cgi-bin " . $this->get_site_dir() . "/cgi-bin\n";
        $group_name = $this->get_hosting_order_unix_group_name();
        $user = $this->get_web_conf_execute_as_user();
        $content .= "\tSuexecUserGroup $user $group_name\n";
      }
      $settings = explode("\r\n",$this->get_web_conf_settings());
      foreach($settings as $line) {
        $content .= "\t$line\n";
      }
      $content .= $this->get_tls_settings();
      $content .= $this->get_apache_conf_file_children_additions();
      $content .= '</VirtualHost>'."\n";
      return $content;
    }

    function get_apache_conf_file_shared_content() {
      $domain_names = explode(' ', $this->get_web_conf_domain_names());
      $server_name = array_shift($domain_names);
      $server_aliases = trim(implode(' ', $domain_names));
      $base = $this->get_site_dir();

      $content = "\tServerName $server_name\n";
      if (!empty($server_aliases)) {
        $content .= "\tServerAlias $server_aliases\n";
      }
      $root = ltrim($this->get_web_conf_document_root(), '/');
      $content .= "\tDocumentRoot {$base}/web/{$root}\n";
      $logging = $this->get_web_conf_logging();
      $error_log = '/dev/null';
      $web_log = '/dev/null';
      if ($logging > 0) {
        $error_log = "{$base}/logs/error.log";
      }
      if ($logging > 1) {
        $web_log = "{$base}/logs/web.log";
      }
      $content .= "\tErrorLog {$error_log}\n";
      $content .= "\tCustomLog {$web_log} combined\n";
      $content .= "\t<Directory " . $this->get_site_dir() . "/web/{$root}" . '>' . "\n";
      $content .= "\t\tOptions FollowSymLinks\n";
      $content .= "\t\tAllowOverride  All\n";
      $content .= "\t\tRequire all granted\n";
      $content .= "\t\tRewriteEngine On\n";
      $content .= "\t\tRewriteCond %{REQUEST_FILENAME} !-f\n";
      $content .= "\t\tRewriteRule ^robots\.txt$ /default-robots.txt [PT]\n";
      $content .= "\t\t<Files .user.ini>\n";
      $content .= "\t\t\t\tRequire all denied\n";
      $content .= "\t\t</Files>\n";
      $content .= "\t</Directory>\n";
      $content .= "\t<Location />\n";
      $content .= "\t\tOptions +IncludesNoExec -ExecCGI\n";
      $content .= "\t</Location>\n";
      return $content;
    }

    function get_tls_settings() {
      if ($this->get_web_conf_tls() != 1) {
        // Return empty string if tls is not configured for this site.
        return '';
      }

      if ($this->on_new_infrastructure()) {
        $base = '/etc/ssl/mayfirst/';

        $cert_name = 'site' . $this->get_item_id();
        $cert_dir = $base . $cert_name;
        $tls_key = "{$cert_dir}/privkey.pem";
        $tls_cert = "{$cert_dir}/fullchain.pem";
      }
      else {
        // Check if the user wants to supply their own certs.
        $tls_key = $this->get_web_conf_tls_key();
        $tls_cert = $this->get_web_conf_tls_cert();

        if (empty($tls_key)) {
          // No user supplied certs, it's lets encrypt.
          $le_base = '/etc/letsencrypt/live/';

          $le_cert_name = 'site' . $this->get_item_id();
          $le_cert_dir = $le_base . $le_cert_name;

          if ($le_cert_dir) {
            $tls_key = "{$le_cert_dir}/privkey.pem";
            $tls_cert = "{$le_cert_dir}/fullchain.pem";
          }
        }
        // Ensure both cert and key exist.
        if(!file_exists($tls_key) || !file_exists($tls_cert)) {
          return '';
        }
      }
    
      $ret = "\tSSLEngine On\n";
      $ret .= "\tSSLCertificateKeyFile {$tls_key}\n";
      $ret .= "\tSSLCertificateFile {$tls_cert}\n";
      return $ret;
    }

    function get_apache_conf_file_children_additions() {
      // If PHP is enabled, add directive that matches PHP files.
      $ret = '';
      if($this->get_web_conf_max_processes() != '0') {
        $version = $this->get_web_conf_php_version();
        $socket_path = '/var/run/php/' . $version . '-site' . $this->get_item_id() . '.sock';
        $ret .= "\t" . '<FilesMatch \.php$>' . "\n";
        $ret .= "\t\t" . 'SetHandler "proxy:unix:' . $socket_path . '|fcgi://localhost"' . "\n";
        $ret .= "\t". '</FilesMatch>'. "\n";
      }
      return $ret;
    }
    public static function save_disk_usage($lines) {
      self::save_disk_usage_for_service('web_conf', $lines);
    }
    public static function revoke_site_access($site_id, $user, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-conf/revoke-ssh-site-access',
        $site_id,
        $user
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to revoke ssh site access to {$user}");
      }
      return TRUE;

    }
    public static function grant_site_access($site_id, $user, $server) {
      $args = [
        "red-remote-tasker@{$server}",
        "sudo",
        '/usr/local/share/red/node/share/web-conf/grant-ssh-site-access',
        $site_id,
        $user
      ];
      if (!red_ssh_cmd($args)) {
        throw new \RedException("Failed to grant ssh site access to {$user}");
      }
      return TRUE;
    }

    function get_hosting_order_unix_group_name() {
      if ($this->on_new_infrastructure()) {
        return $this->get_web_conf_execute_as_user();
      }
      return parent::get_hosting_order_unix_group_name();
    }
  }

}



?>
