<?php

/**
 * Custom red exception used to catch our own errors. 
 */
class RedException extends Exception
{
  public function __construct($msg, $code = 0, Exception $previous = null) {
    red_log("RedExecption thrown.");
    red_log($msg);
    parent::__construct($msg, $code, $previous);
  } 
}


// Matchers
// logins may have an @ sign

// FIXME - this pattern should be 1,255 - lowering because of pcre3 bug
// that reports "Compilation failed: repeated subpattern is too long
define ('RED_LOGIN_REGEXP', '[a-z0-9]([a-z0-9\-_.@]){0,32}');
// the user portion of an email address may not have the @ sign
// FIXME - this pattern should be 1,251 - lowering because of pcre3 bug
// that reports "Compilation failed: repeated subpattern is too long
define ('RED_EMAIL_USER_REGEXP', '([a-zA-Z0-9\-_.+])+');
// we should allow emails to be forwarded to addresses with a plus in them
// see: https://support.mayfirst.org/ticket/4794
define ('RED_IP4_REGEXP', '(([0-9]){1,3}\.){3}([0-9]){1,3}');
define ('RED_IP6_GROUP_REGEXP', '([0-9abcdefABCDEF]){1,4}');
define ('RED_IP6_REGEXP', '(::1|::FFFF:'.RED_IP4_REGEXP.'|('.RED_IP6_GROUP_REGEXP.':+){1,15}'.RED_IP6_GROUP_REGEXP.')');
define ('RED_MD5_REGEXP', '^\$1\$(.*){31}');
define ('RED_SHA512_REGEXP', '\$6\$rounds=[0-9]{4,9}(.*){93,}');
define ('RED_DOMAIN_LABEL_REGEXP', '([\-0-9a-zA-Z_]){1,63}');
// Underscore is not allowed in an email domain.
define ('RED_EMAIL_DOMAIN_LABEL_REGEXP', '([\-0-9a-zA-Z]){1,63}');
define ('RED_DOMAIN_WILDCARD_LABEL_REGEXP', '([\-0-9a-zA-Z*]){1,63}');
# Loose also allows _ which is not stricly allowed in domain names
# but is allowed (and common) in txt records
define ('RED_DOMAIN_LOOSE_WILDCARD_LABEL_REGEXP', '([_\-0-9a-zA-Z*]){1,63}');
define ('RED_DOMAIN_LOOSE_LABEL_REGEXP', '([_\-0-9a-zA-Z]){1,63}');
define ('RED_DOMAIN_REGEXP', '(localhost|(' . RED_DOMAIN_LABEL_REGEXP . '\.){1,18}' . RED_DOMAIN_LABEL_REGEXP . ')');   // arbitrarily only allowing 18 groups of labels - e.g. sub.sub.sub.sub.domain.org (since this is used to match the zone file for ipv6, which can have 18 parts to it).
define ('RED_EMAIL_DOMAIN_REGEXP', '(localhost|(' . RED_EMAIL_DOMAIN_LABEL_REGEXP . '\.){1,18}' . RED_EMAIL_DOMAIN_LABEL_REGEXP . ')');   // arbitrarily only allowing 18 groups of labels - e.g. sub.sub.sub.sub.domain.org (since this is used to match the zone file for ipv6, which can have 18 parts to it).
define ('RED_DOMAIN_WILDCARD_REGEXP', '(localhost|(' . RED_DOMAIN_WILDCARD_LABEL_REGEXP . '\.){1,6}' . RED_DOMAIN_LABEL_REGEXP . ')');   // arbitrarily only allowing 6 groups of labels - e.g. sub.sub.sub.sub.domain.org
define ('RED_DOMAIN_LOOSE_WILDCARD_REGEXP', '(localhost|(' . RED_DOMAIN_LOOSE_WILDCARD_LABEL_REGEXP . '\.){1,6}' . RED_DOMAIN_LOOSE_LABEL_REGEXP . ')');   // arbitrarily only allowing 6 groups of labels - e.g. sub.sub.sub.sub.domain.org
define ('RED_DOMAIN_LOOSE_REGEXP', '(localhost|(' . RED_DOMAIN_LOOSE_LABEL_REGEXP . '\.){1,6}' . RED_DOMAIN_LOOSE_LABEL_REGEXP . ')');   // arbitrarily only allowing 6 groups of labels - e.g. sub.sub.sub.sub.domain.org
define ('RED_EMAIL_REGEXP', RED_EMAIL_USER_REGEXP.'@'.RED_EMAIL_DOMAIN_REGEXP);
define ('RED_PATH_REGEXP', '([a-zA-Z0-9\-_./]){1,255}');
define ('RED_MULTIPLE_EMAIL_REGEXP','('.RED_EMAIL_REGEXP.'(,|, |)){1,}');
define ('RED_MULTIPLE_EMAIL_AND_LOGIN_REGEXP','(('.RED_EMAIL_REGEXP.'|'.RED_LOGIN_REGEXP.')(,|, |)){1,}');
define ('RED_MULTIPLE_DOMAIN_REGEXP','('.RED_DOMAIN_REGEXP.'( |)){1,}');
define ('RED_MULTIPLE_DOMAIN_WILDCARD_REGEXP','('.RED_DOMAIN_WILDCARD_REGEXP.'( |)){1,}');
define ('RED_SQL_DB_REGEXP', '([a-zA-Z]+([a-zA-Z_0-9]+)?){1,64}');
// Environment line can contain anything except non-printable characters.
define ('RED_SYSTEMD_ENVIRONMENT_REGEXP', '[^\x0-\x1f]+');
define ('RED_SYSTEMD_ENVIRONMENT_MATCHER', '/^' . RED_SYSTEMD_ENVIRONMENT_REGEXP . '$/');
define ('RED_DOMAIN_MATCHER', '/^' . RED_DOMAIN_REGEXP . '$/');
# unix groups are used to create default .mayfirst.org domains, so must
# match the lowest common denominator
define ('RED_UNIX_GROUP_MATCHER', '/^' . RED_DOMAIN_LABEL_REGEXP . '$/');
define ('RED_SERVER_NAME_MATCHER', '/^('.RED_DOMAIN_REGEXP.'|'.RED_DOMAIN_LABEL_REGEXP.')$/');
define('RED_IP_MATCHER','/^[0-9.:a-fA-F]+$/');
define('RED_IP_WILDCARD_MATCHER','/^('.RED_IP4_REGEXP.'|'.RED_IP6_REGEXP.'|\*)$/');
define ('RED_DOMAIN_WILDCARD_MATCHER', '/^' . RED_DOMAIN_WILDCARD_REGEXP . '$/');
define ('RED_DOMAIN_LOOSE_WILDCARD_MATCHER', '/^' . RED_DOMAIN_LOOSE_WILDCARD_REGEXP . '$/');
define ('RED_DOMAIN_LOOSE_MATCHER', '/^' . RED_DOMAIN_LOOSE_WILDCARD_REGEXP . '$/');
define ('RED_MULTIPLE_DOMAIN_MATCHER', '/^' . RED_MULTIPLE_DOMAIN_REGEXP . '$/');
define ('RED_MULTIPLE_DOMAIN_WILDCARD_MATCHER', '/^' . RED_MULTIPLE_DOMAIN_WILDCARD_REGEXP . '$/');
define ('RED_MULTIPLE_EMAIL_MATCHER', '/^' . RED_MULTIPLE_EMAIL_REGEXP . '$/');
// either an email address or just @domain.org for a catchall
define ('RED_VIRTUAL_EMAIL_MATCHER', '/^(' . RED_EMAIL_USER_REGEXP . '|)\@'.RED_EMAIL_DOMAIN_REGEXP .'$/');
define ('RED_EMAIL_MATCHER', '/^' . RED_EMAIL_REGEXP . '$/');
define ('RED_LIST_NAME_MATCHER', '/^' . RED_EMAIL_USER_REGEXP . '$/');
define ('RED_SQL_DB_MATCHER', '/^'.RED_SQL_DB_REGEXP . '$/');
define ('RED_SQL_DBS_MATCHER', '/^(:?' . RED_SQL_DB_REGEXP . '?:?)*$/');
define ('RED_SQL_PRIV_MATCHER', '/^(full|read)$/');
define ('RED_SQL_USER_MATCHER', '/^[a-zA-Z][a-zA-Z_0-9]{1,15}$/');
define ('RED_LOGIN_MATCHER', '/^' . RED_LOGIN_REGEXP . '$/');
define ('RED_TINYINT_MATCHER', '/^(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)$/');
define ('RED_EMAIL_RECIPIENT_MATCHER', '/^' . RED_MULTIPLE_EMAIL_AND_LOGIN_REGEXP . '$/');
define ('RED_ID_MATCHER', '/^[0-9]+$/');
define ('RED_INT_MATCHER', '/^[0-9]+$/');
define ('RED_BYTES_MATCHER', '/^[0-9]+\.?[0-9]?([bkmg]b?)?$/i');
define ('RED_DECIMAL_MATCHER', '/^[0-9]+\.[0-9]+$/');
define ('RED_MONEY_MATCHER', '/^[0-9]+(\.[0-9]{2})?$/');
define ('RED_STATUS_MATCHER',  '/^(active|pending-update|pending-delete|pending-insert|deleted|hard-error|soft-error|pending-restore|transfer-limbo|disabled|pending-disable)$/');
define ('RED_MEMBER_BENEFITS_LEVEL_MATCHER',  '/^(basic|standard|extra)$/');
define ('RED_MEMBER_STATUS_MATCHER',  '/^(active|inactive|suspended)$/');
define ('RED_WEB_APP_UPDATE_MATCHER', '/(none|core)/' );
define ('RED_BOOL_MATCHER', '/^[yn]$/');
define ('RED_DATE_MATCHER', '/^(\d){4}-(\d){2}-(\d){2}$/');
define ('RED_CURRENCY_MATCHER', '/^USD|MXN$/');
define ('RED_DATETIME_MATCHER', '/^(\d){4}-(\d){2}-(\d){2}\s(\d){2}:(\d){2}(:(\d){2})?$/');
define ('RED_TEXT_MATCHER', '/^.*$/');
define ('RED_DNS_TEXT_MATCHER', '/^.{1,1024}$/');
define ('RED_ANYTHING_MATCHER', '/.*/');
define ('RED_PASSWORD_HASH_MATCHER', '/^' . RED_MD5_REGEXP . '|' . RED_SHA512_REGEXP . '$/');
define ('RED_HOST_MATCHER', '/^(' . RED_DOMAIN_REGEXP . '(|:([0-9]){1,5})(|,)){1,}$/'); // like domain, but includes optional :port and can have multiple defined, separated by commas 
define ('RED_WEB_CONF_KEY_MATCHER', '#^(ServerAdmin|Alias|ScriptAlias|ScriptAliasMatch|DirectoryIndex|RedirectCond|RedirectMatch|RedirectEngine|RewriteEngine|RewriteCond|RewriteRule|RewriteMap|Options|AllowOverride|Order|Allow|Deny|<Directory|</Directory>|<Files|</Files>|<FilesMatch|</FilesMatch>|RewriteBase|Redirect|AuthUserFile|AuthName|AuthType|require|Require|<Location|</Location>|<LocationMatch|</LocationMatch>|SetEnv|AddType|SuexecUserGroup|LogLevel|Action|AddHandler|Satisfy|AuthOpenIDEnabled|AuthOpenIDDBLocation|AuthOpenIDTrusted|AuthOpenIDDistrusted|AuthOpenIDUseCookie|AuthOpenIDTrustRoot|AuthOpenIDCookieName|AuthOpenIDLoginPage|ErrorDocument|PerlAccessHandler|SetHandler|PerlHandler|DAV|FcgidMaxRequestLen|FcgidMaxProcessesPerClass|ProxyErrorOverride|ProxyPass|SSLVerifyClient|SSLVerifyDepth|SSLCACertificateFile|SSLProxyVerify|SSLProxyCheckPeerCN|SSLProxyCheckPeerName|LimitRequestBody|ProxyTimeout|ProxyBadHeader|Header|RequestHeader|<If|</If>|ProxyPreserveHost|ProxyPassReverse|ProxyPassReverseCookieDomain|SSLProxyEngine)$#');
define ('RED_WEB_CONF_STATUS_MATCHER', '/^(enabled|disabled)$/');
define ('RED_WEB_CONF_CACHE_TYPE_MATCHER', '/^(none|custom|performance)$/');
define ('RED_PATH_MATCHER', '#^' . RED_PATH_REGEXP . '$#');
define ('RED_DNS_TYPE_MATCHER', '/^(host|a|mx|txt|cname|srv|aaaa|ptr|sshfp|dkim|alias|caa)$/');
define ('RED_YES_NO_MATCHER', '/^(y|n)$/');
define ('RED_ACTIVE_DELETED_MATCHER', '/^(active|deleted)$/');
define ('RED_HOSTING_ORDER_STATUS_MATCHER', '/^(active|deleted|disabled)$/');
define ('RED_AUTO_RESPONSE_ACTION_MATCHER', '/^(respond_and_deliver|respond_only)$/');
define('RED_MEMBER_TYPE_MATCHER','/^(individual|organization)$/');
define('RED_WEB_APP_MATCHER','/^(drupal9|drupal5|drupal6|drupal7|wordpress|backdrop)$/');
define('RED_INTERFACE_LANG_MATCHER','/^(en_US|es_MX)$/');
define('RED_COUNTRY_CODE_MATCHER','/^(ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|ss|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$/');
define('RED_STREET_NUMBER_MATCHER','/^[a-zA-Z0-9\-]+$/');
define('RED_PROVINCE_MATCHER',"/^[a-zA-Z.'\- ]+$/");
define('RED_ZERO_ONE_MATCHER',"/^(0|1)$/");
define('RED_DKIM_SELECTOR_MATCHER', '/^[a-z0-9]+$/');

// Explanations
define ('RED_SYSTEMD_ENVIRONMENT_EXPLANATION', red_t("The environment line cannot contain non-printable characters like tabs and line breaks."));
define ('RED_SQL_DB_EXPLANATION', red_t('Database names must be between 1 and 64 characters, start with a letter, and only contain letters, numbers and underscores.'));
define ('RED_SQL_DBS_EXPLANATION', red_t('Multiple database names must be between 1 and 64 characters, start with a letter, and only contain letters, numbers and underscores. Different names must be separated by a colon.'));
define ('RED_SQL_USER_EXPLANATION', red_t('Database user names must be between 1 and 16 characters, start with a letter, and only contain letters, numbers and underscores.'));
define ('RED_SQL_PRIV_EXPLANATION', red_t('Privileges can only be full or read.'));
define ('RED_EMAIL_EXPLANATION', red_t('Valid emails must be in the form user@domain.org with only letters, numbers, underscores, periods and dashes.'));
define('RED_IP_EXPLANATION', red_t('Valid IP addresses only have numbers, periods, colons or the letters a-f.'));
define('RED_IP_WILDCARD_EXPLANATION', red_t('Valid IP addresses must have 4 groups 1 - 3 numbers separated by periods, for example: 1.23.456.789, or a wildcard (*).'));
define ('RED_LIST_NAME_EXPLANATION', red_t('List name can only contain letters, numbers, dashes and underscores.'));
define ('RED_VIRTUAL_EMAIL_EXPLANATION', red_t('Valid emails must be in the form user@domain.org with only letters, numbers, underscores, periods and dashes. Or, you may use @domain.org for a catchall email address that will receive mail for any message sent to a user that does not exist.'));
define ('RED_MULTIPLE_EMAIL_EXPLANATION', red_t('Valid emails must be in the form user@domain.org with only letters, numbers, underscores, periods and dashes. You can enter multiple emails separated by a comma.'));
define ('RED_MULTIPLE_EMAIL_AND_LOGIN_EXPLANATION', red_t('You may enter one or more comma separated lists of email addresses or user logins. Valid emails must be in the form user@domain.org. Emails and login can only have letters, numbers, underscores, periods and dashes.'));
define ('RED_ID_EXPLANATION', red_t('ID values must be all numeric.'));
define ('RED_INT_EXPLANATION', red_t('Integer values must be all numeric, such as 1 or 234, but not 34.5.'));
define ('RED_BYTES_EXPLANATION', red_t('Please enter a number (with no more then one decimal place), optionally followed by a b for bytes, k for kilobytes, m for megabytes, or g for gigabytes.'));
define ('RED_MONEY_EXPLANATION', red_t('Money values must be all numeric with an optional period followed by two additional numbers.'));
define ('RED_DECIMAL_EXPLANATION', red_t('Decimal values must be all numeric, with a period in the middle.'));
define ('RED_STATUS_EXPLANATION', red_t( 'The following status values are allowed: active, pending-update, pending-delete, pending-insert, pending-restore, deleted, soft-error, and hard-error.'));
define ('RED_HOSTING_ORDER_STATUS_EXPLANATION', red_t( 'The following status values are allowed: active, disabled and deleted.'));
define ('RED_WEB_APP_UPDATE_EXPLANATION', red_t('Web app updates can only be set to none or core.'));
define ('RED_MEMBER_BENEFITS_LEVEL_EXPLANATION', red_t( 'The following levels are allowed: basic, standard, and extra.'));
define ('RED_MEMBER_STATUS_EXPLANATION', red_t( 'The following status values are allowed: active, inactive and suspended.'));
define ('RED_ACTIVE_DELETED_EXPLANATION', red_t( 'The following status values are allowed: active or deleted.'));
define ('RED_DATE_EXPLANATION', red_t('Datetime must be entered as yyyy-mm-dd.'));
define ('RED_CURRENCY_EXPLANATION', red_t('Currency must be either USD (US Dollars) or MXN (Mexican Pesos).'));
define ('RED_DATETIME_EXPLANATION', red_t('Datetime must be entered as yyyy-mm-dd hh:mm:ss.'));
define ('RED_BOOL_EXPLANATION', red_t('Only y and n are allowed.'));
define ('RED_TEXT_EXPLANATION', red_t('That value failed the text test. Did you accidentally add a line break or something like that?'));
define ('RED_DNS_TEXT_EXPLANATION', red_t('A DNS txt field is limited to 1024 characters.'));
define ('RED_ANYTHING_EXPLANATION', red_t('There some kind of strange validation error. You should be able to enter anything you want, but what ever it is you entered did not pass.'));
define ('RED_TINYINT_EXPLANATION', red_t('Please enter a value between 0 and 255.'));
define ('RED_WEB_CONF_STATUS_EXPLANATION', red_t('Status must either be enabled or disabled.'));
define('RED_WEB_CONF_CACHE_TYPE_EXPLANATION', red_t("Web conf cache type must be none, performance or custom."));
define ('RED_PASSWORD_HASH_EXPLANATION', red_t('There was an error encrypting your password. I was expecting either an md5 hash (begining with $1$) or a SHA512 hash (beginning with $6$).'));
define ('RED_DOMAIN_EXPLANATION', red_t('A valid domain has only numbers, letters, and dashes and at least one period.'));
define ('RED_UNIX_GROUP_EXPLANATION', red_t('A valid group has only letters, numbers and dashes (underscores are not allowed).'));
define ('RED_SERVER_NAME_EXPLANATION', red_t('A valid server name is either a domain or one segment of a domain.'));
define ('RED_DOMAIN_WILDCARD_EXPLANATION', red_t('A valid domain has at least one period and has only letters, numbers and dashes. You may use a single * to denote a wildcard.'));
define ('RED_DOMAIN_LOOSE_WILDCARD_EXPLANATION', red_t('A valid "loose" domain has at least one period and has only letters, numbers, dashes and underscores. You may use a single * to denote a wildcard.'));
define ('RED_DOMAIN_LOOSE_EXPLANATION', red_t('A valid domain has at least one period and has only letters, numbers, dashes and underscores. You may not use a an * in the zone name.'));
define ('RED_MULTIPLE_DOMAIN_EXPLANATION', red_t('A valid domain has at least one period. You can add multiple domains separated by spaces.'));
define ('RED_MULTIPLE_DOMAIN_WILDCARD_EXPLANATION', red_t('A valid domain has at least one period. You can add multiple domains separated by spaces. You can use * as a wild card.'));
define ('RED_LOGIN_EXPLANATION', red_t('Login names can only use lower case letters, number, dashes, periods and underscores.  A login name must not exceed 32 characters.'));
define ('RED_EMAIL_RECIPIENT_EXPLANATION', red_t('Email recipients should either be a single user name or one or more valid email addresses separated by a comma.'));
define ('RED_HOST_EXPLANATION', red_t('A valid host is in the form: host.tld[:port].'));
define ('RED_WEB_CONF_KEY_EXPLANATION', red_t('Only the following values are allowed in web configuration settings: ServerAdmin, Alias, ScriptAlias, ScriptAliasMatch, RedirectCond, RedirectMatch, RedirectEngine, RewriteCond, RewriteRule, RewriteEngine, RewriteMap, AllowOverride, Order, Deny, Options, Allow, Directory (both opening and closing), Files and FilesMatch (both opening and closing), RewriteBase, DirectoryIndex, Redirect, AuthUserFile, AuthName, AuthType, require, Location (both opening and closing), SetEnv, AddType, SuexecUserGroup, ErrorLog, LogLevel, Action, Satisfy, AddHandler, SetHandler, PerlHandler, PerlAccessHandler, ErrorDocument, DAV, FcgidMaxRequestLen,FcgidMaxProcessesPerClass, SSLVerifyClient, SSLVerifyDepth, SSLCACertificateFile, all AuthOpenID related keys, RequestHeader, Header, SSLProxyEngine, and If.'));
define ('RED_PATH_EXPLANATION', red_t('A path can only contain numbers, letters, periods, dashes, underscores and forward slashes.'));
define('RED_DNS_TYPE_EXPLANATION', red_t('Only the following dns types are supported: host, a, mx, txt, cname, srv, sshfp, caa and aaaa.'));
define('RED_YES_NO_EXPLANATION', red_t('Please enter either \'y\' or \'n\'.'));
define('RED_AUTO_RESPONSE_ACTION_EXPLANATION', red_t('Auto response action should be either respond_and_deliver or respond_only.'));
define('RED_MEMBER_TYPE_EXPLANATION', red_t('Type can only be organization or individual.'));
define('RED_WEB_APP_EXPLANATION', red_t('Only Drupal, WordPress, and Backdrop and  are supported web apps.'));
define('RED_INTERFACE_LANG_EXPLANATION',red_t("Only es_MX and en_US are supported languages."));
define('RED_COUNTRY_CODE_EXPLANATION',red_t("Please use two character top level domain abbreviations for country."));
define('RED_STREET_NUMBER_EXPLANATION',red_t("Please only use numbers and letters. Apartment numbers, etc. should go in the postal address extra field."));
define('RED_PROVINCE_EXPLANATION',red_t("Province can only have letters, periods, dashes, spaces and single quotes."));
define('RED_ZERO_ONE_EXPLANATION', red_t("Please enter 0 or 1."));
define('RED_DKIM_SELECTOR_EXPLANATION', red_t("Please enter just lower case letters and numbers."));

function red_generate_random_password($length = 15) {
  // Round $length to be evenly divisible by 4
  $round = ceil($length/4) * 4;
  // Get contents of /dev/urandom
  $urand = file_get_contents('/dev/urandom', 0, null, 0, ($round/4)*3);
  // Check for problems with /dev/urandom stream
  if (FALSE === $urand) {
    return FALSE;
  }
  // Make sure enough bytes were generated by /dev/urandom
  if (strlen($urand) < ($round/4)*3) {
    return FALSE;
  }
  // Generate a raw sha256 hash of our /dev/urandom output
  $hash = substr(hash('sha256',$urand,$raw_output = true), 0, ($round/4)*3);
  // Get a base64_encoded substring of the hash with specified password length
  $password = substr(base64_encode($hash),0,$length);
  return $password;
}

function red_create_directory_recursively($full_path,$group_writable = FALSE) {
  $dirs = explode('/', $full_path);
  $built_path = '';
  foreach($dirs as $dir) {
    if($dir <> '') {
      $dir = "/$dir";
      if(!file_exists($built_path . $dir)) {
        if(!mkdir($built_path . $dir)) return false;
        if($group_writable) {
          if(!chmod($built_path . $dir,0775)) return false;
        }
      }
      $built_path .= $dir;
    }
  }
  return true;
}

// If $exists is set, it will continue going up the
// file hieararchy until it finds a parent directory
// that exists
function red_get_parent_directory($directory, $exists = FALSE)
{
  // make sure first character is a slash
  if(substr($directory,0,1) != '/') return false;
  if(substr($directory,-1) == '/') $directory = substr($directory,0,-1);
  $last_slash = strrpos($directory,'/');
  $parent = substr($directory,0,$last_slash);
  if(is_dir($parent) || !$exists) return $parent;
  return red_get_parent_directory($parent,$exists);
}

function red_get_service_id_for_service_name($service_name) {
  if (empty($service_name)) {
    return FALSE;
  }
  $sql = "SELECT service_id FROM red_service WHERE ".
    "service_name = @service_name";
  $result = red_sql_query($sql, ['@service_name' => $service_name]);
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_get_service_table_for_service_name($service_name) {
  if (empty($service_name)) {
    return FALSE;
  }
  $sql = "SELECT service_table FROM red_service WHERE ".
    "service_name = @service_name";
  $result = red_sql_query($sql, ['@service_name' => $service_name]);
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_get_service_name_for_service_id(int $service_id): string {
  if (empty($service_id)) {
    return FALSE;
  }
  $sql = "SELECT service_name FROM red_service WHERE ".
    "service_id = #service_id";
  $result = red_sql_query($sql, ['#service_id' => $service_id]);
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_get_service_table_for_service_id($service_id)
{
  if(empty($service_id)) return false;
  $sql = "SELECT service_table FROM red_service WHERE ".
    "service_id = #service_id";
  $result = red_sql_query($sql, ['#service_id' => $service_id]);
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_get_service_id_for_service_table($service_table) {
  if (empty($service_table)) {
    return false;
  }
  $sql = "SELECT service_id FROM red_service WHERE ".
    "service_table = @service_table";
  $result = red_sql_query($sql, ['@service_table' => $service_table]);
  $row = red_sql_fetch_row($result);
  return $row[0];
}

function red_get_item_host_for_item_id($item_id) {
  $sql = "SELECT item_host FROM red_item WHERE item_id = #item_id";
  $result = red_sql_query($sql, ['#item_id' => $item_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? '';
}

function red_get_service_id_for_item_id($item_id) {
  if (empty($item_id)) {
    return false;
  }
  $sql = "SELECT service_id FROM red_item 
    WHERE item_id = #item_id";
  $result = red_sql_query($sql, ['#item_id' => $item_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? NULL;
}

function red_get_service_table_for_item_id($item_id) {
  if(empty($item_id)) {
    return false;
  }
  $sql = "SELECT service_table FROM red_service INNER JOIN red_item ".
    "ON red_service.service_id = red_item.service_id WHERE ".
    "red_item.item_id = #item_id";
  $result = red_sql_query($sql, ['#item_id' => $item_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? NULL;
}
function red_preg_match_array($regexp,$array)
{
  reset($array);
  foreach($array as $line) {
    if(preg_match($regexp,$line)) return true;
  }
  return false;
}

function red_append_to_file($file,$content)
{
  if(!$handle = fopen($file,'a')) return false;
  if(false === fwrite($handle,$content)) return false;
  fclose($handle);
  return true;
}

// Insert a line at line number specified
function red_insert_into_file($file,$content,$insert_before_line,$direction = 'bottom',$comment = '#')
{
  $file_contents = red_file($file);

  // add a line for the one we're about to insert
  $total_lines_final = count($file_contents) + 1;

  if($direction == 'bottom') {
    $insert_before_line = $total_lines_final - $insert_before_line;
  }

  // Return error if you want to insert the line at a line number greater
  // than the total lines in the file. In other words, there are 10 lines
  // and I want to insert before line 15 (regardless if it's from the top
  // or the bottom). Note - if there are no lines in the file and you want
  // to insert before line 1 from the top (meaning make it the first line),
  // that's ok. If there are no lines and you want to insert 1 line from
  // the bottom, that's an error - because that is not possible. The
  // resulting file will have the given line as the last line rather than
  // being one line before the last line.
  if($total_lines_final < $insert_before_line || $insert_before_line < 1)
    return false;

  $current_line = 1;
  $new_file_contents = array();
  if(count($file_contents) == 0 && $insert_before_line == 1) {
    // File is empty, insert the line
    $new_file_contents[] = $content;
  }
  else {
    reset($file_contents);
    foreach($file_contents as $file_line) {
      if($current_line == $insert_before_line) {
        $new_file_contents[$current_line] = $content;
        $current_line++;
      }
      $new_file_contents[$current_line] = $file_line;
      $current_line++;
    }
  }
  return red_write_to_file($file,implode('',$new_file_contents),$comment);
}

function red_file($file_path,$comment = '#')
{
  $content = file($file_path);
  // remove the comments
  if(is_array($content)) {
    foreach($content as $k => $v) {
      // remove blank lines and comments
      $v = trim($v);
      if(substr($v,0,1) == $comment || empty($v)) unset($content[$k]);
    }
  }
  return $content;

}

function red_delete_from_file($file,$key,$delimiter,$comment = '#')
{
  $content = red_file($file,$comment);
  $regexp = "/^$key$delimiter/";
  reset($content);
  foreach($content as $key => $line) {
    // delete empty lines and matching lines
    if(preg_match($regexp,$line) || $line == '') unset($content[$key]);
  }
  return red_write_to_file($file,implode('',$content),$comment);
}

function red_update_file($file,$key,$delimiter,$updated_line,$comment = '#')
{
  $content = red_file($file,$comment);
  $regexp = "/^$key$delimiter/";
  reset($content);
  foreach($content as $key => $line) {
    // replace matching lines
    if(preg_match($regexp,$line)) $content[$key] = $updated_line;
  }
  return red_write_to_file($file,implode('',$content),$comment);
}

function red_return_line($file,$key,$delimiter)
{
  $content = red_file($file);
  $regexp = "/^" . preg_quote($key) . $delimiter . "/";
  reset($content);
  foreach($content as $key => $line) {
    // replace matching lines
    if(preg_match($regexp,$line)) return $line;
  }
  return false;
}

function red_delete_directory_recursively($dir_name)
{
  // thanks  czambran at gmail dot com
  if(empty($dir_name))  {
    return true;
  }
  if(is_dir($dir_name))  {
    $dir = new DirectoryIterator($dir_name);
    foreach ($dir as $fileInfo) {
      if (!$fileInfo->isDot()) {
  $file = $fileInfo->getFilename();
        // don't follow symlinks!
        if(is_dir($dir_name.'/'.$file) && !is_link($dir_name.'/'.$file)) {
          red_delete_directory_recursively($dir_name.'/'.$file);
        }
        else  {
          if(!unlink($dir_name.'/'.$file)) return false;
        }
      }
    }
    if(!rmdir($dir_name)) return false;
  }
  return true;
}

function red_write_to_file($file,$content,$comment = '#')
{
  if(!$handle = fopen($file,'w')) return false;
  $pre_pend = '';
  if(!is_null($comment)) {
    $pre_pend = "$comment This file was created by Red. Best not to modify\n".
      "$comment by hand unless you know what you're doing.\n";
  }
  $content = $pre_pend . $content;
  if(!fwrite($handle,$content)) return false;
  fclose($handle);
  return true;
}

function red_key_exists_in_file($key,$delimiter,$file_path)
{
  $file = file($file_path);
  if(is_array($file)) {
    $regexp = '/^' . preg_quote($key) . $delimiter . '/';
    return red_preg_match_array($regexp,$file);
  }
  return false;
}

function red_get_all_keys_in_file($delimiter,$file)
{
  $content = file($file);
  $keys = array();
  $regexp = "/^(.*?)$delimiter/";
  foreach($content as $line) {
    if(preg_match($regexp,$line,$matches)) $keys[] = $matches[1];
  }
  return $keys;
}

// Take a string and return the number of unique characters
// in the string.
function red_number_of_unique_characters($str) {
  $len = strlen($str);
  $found = [];
  $i = 0;
  while ($i < $len) {
    $letter = substr($str, $i, 1);
    if (!in_array($letter, $found)) {
      $found[] = $letter;
    }
    $i++;
  }
  return count($found);
}

function red_is_good_password($password)
{
  $unique = red_number_of_unique_characters($password);
  // Less than 6 unique characters is not allowed.
  if($unique < 6) {
    return FALSE;
  }

  // Any password that has more than 15 unique character is a good password. 
  if($unique > 15) {
    return TRUE;
  }
  // Otherwise, it's ok provided you have more than just letters.
  $regexps = array('/[a-zA-Z]/','/[^a-zA-Z]/');
  foreach ($regexps as $regexp) {
    if (!preg_match($regexp, $password)) {
      return FALSE;
    }
  }
  return TRUE;
}

// This function is repeated in the red.utils.inc.php file
// with the name red_load_file.
function red_check_file($file,$restriction = 'write-restrict')
{
  if(!file_exists($file)) return false;

  // When running on test docker images, files will not be owned by root
  // since they are mounted from the host. Skip checks if running in docker.
  if (file_exists('/.dockerenv')) {
    return true;
  }
  if(posix_getuid() == 0) {
    // If we're be run with root privs, make sure the config
    // file is only writable by root
    if(fileowner($file) != 0) {
      return false;
    }
    $perms = fileperms($file);
    $group_octal_perms = intval(substr(sprintf('%o',$perms),-2,1));
    $user_octal_perms = intval(substr(sprintf('%o',$perms),-1,1));

    if($group_octal_perms > 5 || $user_octal_perms > 5) return false;
    if($restriction == 'read-restrict') {
      if($group_octal_perms > 3 || $user_octal_perms > 3) return false;
    }

  }
  return true;
}

// Database functions
function red_sql_real_escape_string($string, $resource = NULL) {
  if (is_null($resource)) {
    global $globals;
    $resource = $globals['sql_resource'];
  }
  return mysqli_real_escape_string($resource, $string);
}

function red_sql_query($sql, $params = [], $resource = NULL) {
  if (is_null($resource)) {
    global $globals;
    $resource = $globals['sql_resource'];
  }
  if (!is_object($resource)) {
    throw new RedException("The query '{$sql}' was requested without a proper sql resource variable.");
  }
  if ($params) {
    // Replace parameters.
    foreach ($params as $k => $v) {
      // Ensure key is in sql.
      if (mb_strpos($sql, $k) === false) {
        throw new RedException("Failed to find {$k} in sql: {$sql}");
      }

      // Determine type of param by key prefix.
      $type = NULL;
      $prefix = substr($k, 0, 1);
      if ($prefix == '@') {
        // String.
        if (is_null($v)) {
          $v = '';
        }
        $v = "'" . red_sql_real_escape_string($v) . "'";
      }
      elseif ($prefix == '#') {
        // Integer.
        if (is_numeric($v)) {
          $v = intval($v);
        }
        else {
          $err = "Parameter {$k} is not numeric, value is: '{$v}' in query: {$sql}";
          throw new RedException($err);
        }
      }
      elseif ($prefix == '!') {
        $v = red_escape_field_or_table_name($v, $k);
      }
      else {
        throw new RedException("Unknown prefix: {$prefix} in key: {$key}."); 
      }
      $sql = str_replace($k, $v, $sql); 
    }
  }
  try {
    $result = mysqli_query($resource, $sql);
  }
  catch (mysqli_sql_exception $e) {
    // Catch so we can log it.
    red_log("Caught mysqli_sql_exception: '$sql' with error: ". $e->getMessage());
    throw new RedException($e);
  }
  if ($result === FALSE) {
    red_log("mysqli_query returned FALSE: '$sql' with error: ". mysqli_error($resource));
  }
  return $result;
}

// Ensure the string is a valid mysql field or table name.
function red_escape_field_or_table_name($str, $token) {
  // Table name or field name
  if (!preg_match('/^[a-zA-Z0-9._]+$/', $str)) {
    $err = "{$str} is not a valid table or field name. Passed token is: {$token}.";
    throw new RedException($err);
  }
  return $str;
}

function red_sql_fetch_row($result)
{
  return @mysqli_fetch_row($result);
}

function red_sql_fetch_assoc($result)
{
  return @mysqli_fetch_assoc($result);
}

function red_sql_insert_id($resource = NULL) {
  if (is_null($resource)) {
    global $globals;
    $resource = $globals['sql_resource'];
  }
  return mysqli_insert_id($resource);
}

function red_sql_num_rows($result)
{
  return @mysqli_num_rows($result);
}

function red_sql_error($resource = NULL)
{
  if (is_null($resource)) {
    global $globals;
    $resource = $globals['sql_resource'];
  }
  return @mysqli_error($resource);
}

function red_chown_symlink($dir_name,$user)
{
  if(posix_getuid() == 0) {
    // PHP can't chown symlinks! grr. It chowns the target
    // Is chown available?
    if($chown_cmd = red_search_path('chown')) {
      if (red_fork_exec_wait($chown_cmd,
          array('--no-dereference',
            '--', // avoids passing weird options if $dir_name or $user start with a -
            $user,
            $dir_name)))
        return false;
    }
    // return true if we can't find chown - it simply means
    // that a symlink will be left owned by root
  }
  return true;
}

function red_chgrp_symlink($dir_name,$group)
{
  if(posix_getuid() == 0) {
    // PHP can't chgrp symlinks! grr. It chowns the target
    // Is chown available?
    if($chgrp_cmd = red_search_path('chgrp')) {
      if (red_fork_exec_wait($chgrp_cmd,
          array('--no-dereference',
            '--', // avoids passing weird options if $dir_name or $group start with a -
            $group,
            $dir_name)))
        return false;
    }
    // return true if we can't find chgrp - it simply means
    // that a symlink will be left with a group of root
  }
  return true;
}

function red_search_path($arg)
{
  // looking for anything executable on the path
  $paths = explode(":", getenv('PATH'));
  foreach($paths as $p) {
    $f = $p."/".$arg;
    if (file_exists($f) && ($s = stat($f)) && ($s['mode'] & 0111))
      return $f;
  }
}

function red_make_world_writable_recursively($dir_name)
{
  if(empty($dir_name))  {
    return true;
  }
  if(file_exists($dir_name))  {
    $dir = dir($dir_name);
    while($file = $dir->read())  {
      if($file != '.' && $file != '..')  {
        // don't follow symlinks!
        if(is_dir($dir_name.'/'.$file) && !is_link($dir_name.'/'.$file)) {
          if(!chmod($dir_name.'/'.$file,0777)) return false;
          red_make_world_writable_recursively($dir_name.'/'.$file);
        }
        elseif(is_file($dir_name.'/'.$file)) {
          if(!chmod($dir_name.'/'.$file,0666)) return false;
        }
      }
    }
    $dir->close();
    if(is_dir($dir_name))  {
      if(!chmod($dir_name.'/'.$file,0777)) return false;
    }
    else {
      if(!chmod($dir_name.'/'.$file,0666)) return false;
    }
  }
  return true;
}

function red_mail($to,$subject,$body,$headers = null)
{
  return mail($to,$subject,$body, $headers);
}

/*
 * Return true if a single ping packet is successful
 *
 */
function red_single_ping_result($ip)
{
  static $ping_results = [];
  if (array_key_exists($ip, $ping_results)) {
    return $ping_results[$ip];
  }
  $cmd = "/bin/ping";
  $args = '-q -c 1 -w 1 ' . escapeshellarg($ip);

  $return_line = exec("$cmd $args",$output,$return_value);
  if($return_value !== 0) {
    $ping_results[$ip] = FALSE;
    return FALSE;
  }
  $ping_results[$ip] = TRUE;
  return true;
}

/*
 * Check an IP to see if we have connectivity
 */
function red_check_online_status($tries = 5,$ip = '1.1.1.1')
{
  $try = 0;
  while($try < $tries) {
    if(red_single_ping_result($ip)) return true;
    $try++;
  }
  return false;
}

/*
 *
 * Check domain to see if it has an mx record
 * that resolves to the given server.
 *
 * This function does not need to be called
 * from the $server being checked. It can be
 * called from anywhere.
 *
 * This function will fail in the event that $check_local
 * is false and an mx record resolves to a domain that
 * resolves to an IP address that is different from the IP
 * address of the $server, but still is hosted by
 * the $server. This corner case can only be determined
 * by running on the server in question (see function
 * red_ip_assigned_to_local_server() function below).
 *
 * Check local is false by default. When we are running
 * in client mode, we are not running on the server in
 * question.
 *
 * If $chck_local is true, then the additional local
 * server check is made.
 *
 */
function red_domain_mx_resolves_to_server($domain,$server,$check_local = false)
{
  $mx_hosts = array();
  if(getmxrr($domain,$mx_hosts)) {
    // return true if we are listed as at least one of the
    // mx records
    if(in_array($server,$mx_hosts))
      return true;

    // the mx record may resolve to a different domain name
    // that has an IP address that resolves to $server
    $server_ip = gethostbyname($server);
    foreach($mx_hosts as $mx_host) {
      // $mx_ip is an array
      $mx_ips = gethostbynamel($mx_host);
      foreach($mx_ips as $mx_ip) {
        if($mx_ip == $server_ip) return true;
        if($check_local) {
          if(red_ip_assigned_to_local_server($mx_ip))
            return true;
        }
      }
    }
  }
  return false;
}

/*
 * Check all local IP addresses and see if any of them
 * match the given $ip. This function should only
 * be called when you want to compare the $ip with
 * the IP addresses assigned to the local server.
 *
 * It requires the iproute package.
 *
 */
function red_ip_assigned_to_local_server($ip)
{
  $cmd = "ip -o addr show";
  exec($cmd,$output);
  foreach($output as $line) {
    // check for both ipv4 and ipv6
    if(preg_match('# inet6? ([0-9:.a-zA-Z]+)/#',$line,$matches)) {
      // We only need one match to return true
      if($matches[1] == $ip) return true;
    }
  }
  return false;
}

/* Safely chown a file by first
 * checking to make sure there
 * is no more than 1 link
 */
function red_chown($file,$user)
{
  // if we're not root (meaning we're in a testing environment)
  // simply return true
  if(posix_getuid() != 0) return true;
  if(!is_dir($file)) {
    $stat = stat($file);
    if($stat[3] > 1) return false;
  }
  return chown($file,$user);
}

function red_chgrp($file,$user)
{
  // if we're not root (meaning we're in a testing environment)
  // simply return true
  if(posix_getuid() != 0) return true;
  if(!is_dir($file)) {
    $stat = stat($file);
    if($stat[3] > 1) return false;
  }
  return chgrp($file,$user);
}

function red_convert_to_friendly_date($iso_date,$date_format = 'mmddyyyy',$date_delimiter = '/')
{
  if($date_format == 'mmddyyyy') {
    $date = substr($iso_date,5,2) . $date_delimiter;
    $date .= substr($iso_date,8,2) . $date_delimiter;
    $date .= substr($iso_date,0,4);
  }
  elseif($date_format == 'ddmmyyyy') {
    $date = substr($iso_date,8,2) . $date_delimiter;
    $date .= substr($iso_date,5,2) . $date_delimiter;
    $date .= substr($iso_date,0,4);
  }
  else {
    $date = $iso_date;
  }

  return $date;
}

function red_db_convert_to_db_acceptable_date($date,&$validated_date,$date_format = 'mmddyyyy',$date_delimiter = '/') {
  // If the date is blank return true
  if($date == '') {
    $validated_date = '';
    return TRUE;
  }

  // First check for and replace the given delimiter (as well
  // as common delimter) with a dash
  $delimiters = array('-','/','.',$date_delimiter);
  $delimiters = array_unique($delimiters);
  $date = str_replace($delimiters,'-',$date);

  // Now if there are no dashes then we're hosed
  if(!preg_match('/-/',$date)) {
    return FALSE;
  }

  // Allow all 0 dates (00-00-0000, 0-0-0000, etc.)
  // also allow time at the end (don't include $)
  if(preg_match('/^(0){1,4}-(0){1,4}-(0){1,4}/',$date)) {
    $validated_date = '0000-00-00';
    return TRUE;
  }

  // Create an array with the month, date, and year parts
  // First remove the time
  $parts = explode('-',$date);
  // See if it is already in ISO format
  if(
    count($parts) > 2 &&
    strlen($parts[0]) == 4 &&
    checkdate(intval($parts[1]),intval($parts[2]),intval($parts[0]))) {
    $validated_date = $date;

    return TRUE;
  }

  if($date_format == 'yyyymmdd') {
    $year = $parts[0];
    $month = $parts[1];
    $date = $parts[2];
  } elseif($date_format == 'mmddyyyy') {
    $month = $parts[0];
    $date = $parts[1];
    $year = $parts[2];
  } elseif($date_format == 'ddmmyyyy') {
    $date = $parts[0];
    $month = $parts[1];
    $year = $parts[2];
  }

  // Sanity checks
  if  (  (strlen($month) <> 1 && strlen($month) <> 2) ||
    (strlen($date) <> 1 && strlen($date) <> 2) ||
    (strlen($year) <> 2 && strlen($year) <> 4)
      ) {
    return FALSE;
  }

  if(strlen($month) == 1) {
    $month = '0' . $month;
  }

  if(strlen($date) == 1) {
    $date = '0' . $date;
  }

  if(strlen($year) == 2) {
    if($year < '25') {
      $year = '20' . $year;
    }
    else {
      $year = '19' . $year;
    }
  }

  if(!checkdate($month,$date,$year)) {
    return FALSE;
  }
  $validated_date = $year . '-' . $month . '-' . $date;
  return TRUE;
}

function red_convert_to_db_acceptable_date($date,&$validated_date,$date_format = 'mmddyyyy',$date_delimiter = '/') {
  // If the date is blank return true
  if($date == '') {
    $validated_date = '';
    return TRUE;
  }

  //
  // First check for and replace the given delimiter (as well
  // as common delimter) with a dash
  $delimiters = array('-','/','.',$date_delimiter);
  $delimiters = array_unique($delimiters);
  $date = str_replace($delimiters,'-',$date);

  // Now if there are no dashes then we're hosed
  if(!preg_match('/-/',$date)) {
    return FALSE;
  }

  // Allow all 0 dates (00-00-0000, 0-0-0000, etc.)
  // also allow time at the end (don't include $)
  if(preg_match('/^(0){1,4}-(0){1,4}-(0){1,4}/',$date)) {
    $validated_date = '0000-00-00';
    return TRUE;
  }

  // Create an array with the month, date, and year parts
  // First remove the time
  $parts = explode('-',$date);
  // See if it is already in ISO format
  if(
    count($parts) > 2 &&
    strlen($parts[0]) == 4 &&
    checkdate(intval($parts[1]),intval($parts[2]),intval($parts[0]))) {
    $validated_date = $date;

    return TRUE;
  }

  if($date_format == 'yyyymmdd') {
    $year = $parts[0];
    $month = $parts[1];
    $date = $parts[2];
  }
  elseif($date_format == 'mmddyyyy') {
    $month = $parts[0];
    $date = $parts[1];
    $year = $parts[2];
  }
  elseif($date_format == 'ddmmyyyy') {
    $date = $parts[0];
    $month = $parts[1];
    $year = $parts[2];
  }

  // Sanity checks
  if  (  (strlen($month) <> 1 && strlen($month) <> 2) ||
    (strlen($date) <> 1 && strlen($date) <> 2) ||
    (strlen($year) <> 2 && strlen($year) <> 4)
      ) {
    return FALSE;
  }

  if(strlen($month) == 1) {
    $month = '0' . $month;
  }

  if(strlen($date) == 1) {
    $date = '0' . $date;
  }

  if(strlen($year) == 2) {
    if($year < '25') {
      $year = '20' . $year;
    }
    else {
      $year = '19' . $year;
    }
  }

  if(!checkdate($month,$date,$year)) {
    return FALSE;
  }
  $validated_date = $year . '-' . $month . '-' . $date;
  return TRUE;
}

function red_convert_to_db_acceptable_date_time($date_time,&$converted_date_time,$date_format = 'mmddyyyy',$date_delimiter = '/') {
  if(preg_match('/^(\d){1,4}.(\d){1,2}.(\d){1,4}/',$date_time,$date_matches)) {
    $date = $date_matches[0];
    // Now the time
    if(preg_match('/(\d){1,2}:(\d){2}(|:)(|\d){2}(| am| pm)$/i',$date_time,$time_matches)) {
      $time = $time_matches[0];
      $hour = intval($time_matches[1]);
      $minute = intval($time_matches[2]);
      $second_colon = $time_matches[3];
      $seconds = intval($time_matches[4]);
      $dorn = trim($time_matches[5]);
      if(red_convert_to_db_acceptable_date($date,$converted_date)) {
        // sanity checking
        if($hour > 24 || $minute > 60 || ($seconds <> '' && $seconds > 60)) {
          return FALSE;
        }
        $dorn = strtolower($dorn);
        if($dorn == '') {
          // do nothing
        }
        else {
          // Chop the am/pm from the time
          $time = substr($time,0,-3);
          if($dorn == 'pm' && $hour < 12) {
            // replace the hour and chop of the space am or space pm
            $time = $hour = $hour + 12 . substr($time,2);
          }
          $converted_date_time = $converted_date . ' ' . $time;
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

function red_get_ip_for_host($host)
{
  // useful for testing on my dev machine
  if($host == 'localhost') return '127.0.0.1';
  $sql = "SELECT dns_ip FROM red_item_dns INNER JOIN red_item USING ".
    "(item_id) WHERE item_status = 'active' AND dns_fqdn = @host ".
    "AND dns_type = 'a' LIMIT 1";
  $result = red_sql_query($sql, ['@host' => $host]);
  $row = red_sql_fetch_row($result);
  if(red_sql_num_rows($result) == 0) return 'Unknown IP';
  return $row[0];
}

function red_truncate_from_middle($string, $max) {
  $len = strlen($string);
  
  if($len < $max) return $string;
  $half = ceil($max / 2);
  return mb_strcut($string, 0, $half) . '...' . mb_strcut($string, -$half, $half);
}

// Return true if the passed in unix_id is in use
// Optionally pass a member_id or
// hosting_order_id to not include in the results.
// This function is useful for determining if a unix
// group is being used by a member or hosting order
// not includig the one being deleting
function red_unix_group_id_in_use($unix_group_id, $member_id = NULL, $hosting_order_id = NULL) {
  $keys = array('member', 'hosting_order');
  foreach ($keys as $key) {
    $table = "red_{$key}";
    $select_field = "{$key}_id";
    $status_field = "{$key}_status";
    $sql = "SELECT !select_field FROM !table ".
      "JOIN red_unique_unix_group USING(unique_unix_group_id) WHERE  ".
      "red_unique_unix_group.unique_unix_group_id = #unix_group_id AND ".
      "unique_unix_group_status = 'active' AND !table.!status_field = ".
      "'active'";
    $result = red_sql_query($sql, [
      '!select_field' => $select_field,
      '!status_field' => $status_field,
      '!table' => $table,
      '#unix_group_id' => $unix_group_id,
    ]);
    while($row = red_sql_fetch_row($result)) {
      $id = $row[0];
      if ($key == 'hosting_order' && $id == $hosting_order_id) {
        continue;
      }
      if ($key == 'member' && $id == $member_id) {
        continue;
      }
      return TRUE;
    }
  }
  return FALSE;
}

function red_htmlentities($value) {
  if (is_null($value)) {
    return '';
  }
  return htmlentities($value, ENT_COMPAT, 'UTF-8');
}

function red_set_message($messages,$type = 'info', $key = NULL) {
  if(is_array($messages)) {
    // check for special format provided by red library
    if(array_key_exists('error',$messages)) {
      // we're only interested in displaying the error message
      // itself
      red_set_message($messages['error'],$type);
    } else {
      // in case we get an array of errors
      foreach($messages as $k => $message) {
        red_set_message($message,$type, $k);
      }
    }
  } else {
    $prefix = is_null($key) ? $key : $key . ': ';
    if (php_sapi_name() == "cli") {
      // If we are running via cli, echo the output.
      // echo $prefix . $messages . "\n";
    }
    else {
      $_SESSION['red']['messages'][$type][] = $prefix . $messages;
    }
  }
}

// Execute shell commands as safely and flexibly as possible.
function red_fork_exec_wait($cmd, $args = [], $env = [], $as_user = NULL, &$output = []) {
  $output = [];
  if(!file_exists($cmd)) {
    red_log("$cmd does not exist.");
    return FALSE;
  }
  $status = 0;
  if (!is_null($as_user)) {
    $runas_bin = NULL;
    // runas changed location between buster and bullseye.
    $runas_bin_candidates = [ '/usr/sbin/runuser', '/sbin/runuser' ];
    foreach ($runas_bin_candidates as $runas_bin_candidate) {
      if(file_exists($runas_bin_candidate)) {
        $runas_bin = $runas_bin_candidate;
        break;
      }
    }
    if (!$runas_bin) {
      red_log("error: runuser not found");
      return 1;
    }

    // In order to execute runuser in the user's environment
    // (with the --login option instead of the --user option)
    // we have to pass the full command and arguments via the
    // --command argument, which means ugly quoting.
    $runas_cmd = escapeshellcmd($cmd);
    foreach ($args as $arg) {
      $runas_cmd .= " " . escapeshellarg($arg);
    }

    $safe_as_user = escapeshellarg($as_user);
    $cmd = "{$runas_bin} --login {$safe_as_user} --command \"$runas_cmd\"";
    
  }
  else {
    $cmd = escapeshellcmd($cmd);
    foreach($args as $arg) {
      $cmd .= ' ' . escapeshellarg($arg);
    }
  }
  if (count($env) > 0) {
    foreach ($env as $env_key => $env_value) {
      putenv("$env_key=$env_value");
    }
  }
  // Lastly, ensure standard error goes to stdout.
  $cmd .= ' 2>&1';
  $result_code = '';
  exec($cmd, $output, $result_code);
  $ret = intval($result_code);
  if ($ret != 0) {
    red_log("Failed to execute $cmd.");
    if ($output) {
      red_log("Command output: " . print_r($output, TRUE));
    }
    else {
      red_log("The command returned no output. Sorry!");
    }
  }
  if (count($env) > 0) {
    foreach ($env as $env_key => $env_value) {
      // Unset the environment variables we set.
      putenv("$env_key");
    }
  }
  return $ret;
}

function red_subscribe_email_to_list($list, $email, $login) {
  return red_list_action('subscribe', $list, $email, $login);
}

function red_unsubscribe_email_from_list($list, $email, $login) {
  return red_list_action('unsubscribe', $list, $email, $login);
}

function red_list_action($action, $list, $email, $login) {
  // pcntl_exec is not an option when calling via the web,
  // the funciton is not available
  $cmd = '/usr/bin/ssh';
  $login = escapeshellarg($login);
  $additional_args = escapeshellarg("$action $list $email");
  $ret = exec("HOME=/var/www $cmd $login $additional_args > /dev/null 2>&1",$output,$response);
  if($response != 0)   return false;
  return true;
}

function red_add_password_to_cache($item_id, $password) {
  $password = base64_encode($password);
  if(empty($item_id) || empty($password)) return FALSE;
  $sql = "INSERT INTO red_password_cache SET password = @password, item_id = #item_id";
  return red_sql_query($sql, [ '@password' => $password, '#item_id' => $item_id]);
}

function red_get_password_from_cache($item_id) {
  $sql = "SELECT password FROM red_password_cache WHERE item_id = @item_id";
  $result = red_sql_query($sql, ['#item_id' => $item_id]);
  if(red_sql_num_rows($result) == 0) return NULL;
  $row = red_sql_fetch_row($result);
  return base64_decode($row[0]);
}

function red_delete_password_from_cache($item_id) {
  $sql = "DELETE FROM red_password_cache WHERE item_id = #item_id";
  return red_sql_query($sql, ['#item_id' => $item_id]);
}

/**
 * Convert from bytes to a number with a label.
 *
 **/
function red_human_readable_bytes($size) {
  if (is_null($size)) {
    $size = 0;
  }

  // Don't double convert. If we are not an integer, then just return
  // what we were given.
  if (!preg_match('/^[0-9]+$/', $size)) {
    return $size;
  }

  // If we are 0, return 0 because 0b looks funny.
  if ($size === 0) {
    return $size;
  }
  if ($size >= 1073741824) {
    return round($size / 1024 / 1024 / 1024, 1) . 'gb';
  } 
  elseif ($size >= 1048576) {
    return round($size / 1024 / 1024, 1) . 'mb';
  } 
  elseif($size >= 1024) {
    return round($size / 1024, 1) . 'kb';
  } 
  else {
    return $size . 'b';
  }
}

/**
 * Convert from human readable to bytes
 *
 * Return FALSE if we can't parse it.
 *
 **/
function red_machine_readable_bytes($size) {
  $size = trim($size);

  // If empty, return 0, the default.
  if (empty($size)) {
    return 0;
  }

  // If there is more then one period in the size we bail early.
  if (substr_count($size, '.') > 1) {
    return FALSE;
  }

  // If no label, then we assume bytes.
  if (preg_match('/^[0-9.]+$/', $size)) {
    return ceil($size);
  } 

  // If there is no number, we bail.
  if (!preg_match('/[0-9]/', $size)) {
    return FALSE;
  }

  // Convert to lower case.
  $size = strtolower($size);

  // Bust it out. We expect any of the following:
  //  * 12g
  //  * 12.34m
  //  * 12.34mb
  //  * etc.
  // You can add units: b, k, kb, m, mg, g, gb after
  // your number.
  if (preg_match('/^([0-9.]+)([bkmg]{1})b?$/', $size, $matches)) {
    $number = $matches[1];
    $unit = $matches[2];
    $multiplier = NULL;
    
    if ($unit == 'b') { 
      $multiplier = 1;
    }
    elseif ($unit == 'k') {
      $multiplier = 1024;
    }
    elseif ($unit == 'm') {
      $multiplier = 1024 * 1024;
    }
    elseif ($unit == 'g') {
      $multiplier = 1024 * 1024 * 1024;
    }
    // Ensure we return an integer and always round up to avoid quota
    // suprises. When in doubt, allow a little more rather then a little
    // less.
    return ceil($multiplier * $number);
  }
  else {
    // We got something we cannot parse.
    return FALSE;
  }

}

/**
 * Get assigned member quota
 *
 * Note: this is the single number assigned to the membership. See below for
 * functions to calculate the allocated total of all items and vps for a given
 * member.
 */
function red_get_member_quota($member_id) {
  $sql = "SELECT member_quota FROM red_member WHERE member_id = #member_id";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  // When running on a node, we may not have permission to query this table, so just
  // return 0, which means no quota, to prevent any validation errors.
  if (!$row) {
    return 0;
  }
  return $row[0];
}

/**
 * Get total disk usage for membership, including both items and vps. 
 *
 * Optionally pass hosting_order_id to restrict to the given hosting order.
 *
 * When calculating VPS disk usage we use the total allocated for VPS's that
 * are not private moshes. With private moshes, the disk usage is already
 * calculated per item id.
 *
 * When passing in a hosting order id, the VPS usage is not included.
 *
 */
function red_get_disk_usage_for_membership($member_id, $hosting_order_id = NULL) {
  // Calculate total usage from the red_item table. 
  $sql = "SELECT COALESCE(SUM(item_disk_usage), 0) FROM red_item JOIN red_hosting_order 
    USING(hosting_order_id) WHERE 
    hosting_order_status != 'deleted' AND red_hosting_order.member_id = #member_id 
    AND item_status != 'deleted' AND item_status != 'pending-delete'";

  $params = ['#member_id' => $member_id];
  if ($hosting_order_id) {
    $sql .= " AND red_item.hosting_order_id = #hosting_order_id";
    $params['#hosting_order_id'] = $hosting_order_id;
  }
  $result = red_sql_query($sql, $params);
  $row = red_sql_fetch_row($result);
  $red_item_usage = $row[0];
    
  if ($hosting_order_id) {
    return $red_item_usage;
  }

  // Calculate total usage for vps. NOTE: we exclude any VPS that is in our
  // red_server table because those servers are private moshes, and those
  // totals are calculated like the rest of our moshes. We only include VPS's
  // that are not moshes, since we have no other way to calculate them. Also,
  // it's weird to say the entire disk we have allocated is "in use" since we
  // don't really know how much of it is in use. But, what else to do? 
  $sql = "SELECT COALESCE(SUM(vps_hd), 0) FROM red_vps 
    LEFT JOIN red_server ON red_vps.vps_server = red_server.server
    WHERE red_server.server_id IS NULL AND vps_status = 'active' AND 
    vps_type = 'virtual' AND 
    red_vps.member_id = #member_id";

  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  $vps_usage  = $row[0];

  return $red_item_usage + $vps_usage;
}

/**
 * Get related member ids
 *
 * Some members have two or more memberships because we can't
 * merge two members into on. They are indicated by the member_parent_id
 * column in the red_member table.
 *
 * This function takes a single member_id and returns an array of all the member
 * ids that are related to the single one.
 */
function red_get_related_member_ids($member_id) {
  if ($member_id == 0) {
    return [];
  }
  $member_ids = [$member_id];
  // Does it have a parent?
  $sql = "SELECT member_parent_id FROM red_member WHERE member_id = #member_id AND member_status = 'active'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  $member_parent_id = intval($row[0] ?? 0);

  if ($member_parent_id > 0) {
    // This member has a parent, now we have to include both the parent
    // and all the parents children, so all children show the same totals.
    $member_ids[] = $member_parent_id;
    $sql = "SELECT member_id FROM red_member WHERE (member_parent_id = #member_parent_id OR member_id = #member_parent_id) AND member_id != #member_id AND member_status = 'active'"; 
    $result = red_sql_query($sql, ['#member_parent_id' => $member_parent_id, '#member_id' => $member_id]);
    while ($row = red_sql_fetch_row($result)) {
      $member_ids[] = intval($row[0]);
    }
  }

  // Now see if this member id is a parent.
  $sql = "SELECT member_id FROM red_member WHERE member_parent_id = #member_id AND member_status = 'active'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  while ($row = red_sql_fetch_row($result)) {
      $member_ids[] = intval($row[0]);
  }

  return $member_ids;

}

/**
 * Get total virtual private servers allocated to a given membership.
 */
function red_get_virtual_private_servers_for_membership($member_id) {
  $sql = "SELECT COUNT(vps_id) FROM red_vps WHERE member_id = #member_id AND vps_status = 'active'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? 0;
}

/**
 * Get total VPS Cpus allocated to a given membership.
 */
function red_get_cpu_for_membership($member_id) {
  $sql = "SELECT SUM(vps_cpu) FROM red_vps WHERE member_id = #member_id AND vps_status = 'active'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? 0;
}

/**
 * Get total VPS RAM allocated to a given membership.
 */
function red_get_ram_for_membership($member_id) {
  $sql = "SELECT SUM(vps_ram) FROM red_vps WHERE member_id = #member_id AND vps_status = 'active'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? 0;
}

/**
 * Get total VPS SSD allocated to a given membership.
 */
function red_get_ssd_for_membership($member_id) {
  $sql = "SELECT SUM(vps_ssd) FROM red_vps WHERE member_id = #member_id AND vps_status = 'active'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $row = red_sql_fetch_row($result);
  return $row[0] ?? 0;
}

/**
 * Get total allocated quota for all items (including VPS) 
 *
 * Optionally pass in hosting_order_id to restrict to a given hosting order
 * and ignore VPS.
 *
 */
function red_get_allocated_quota_for_membership($member_id, $hosting_order_id = NULL) {
  // If the member_id is a parent for another member OR has a parent, then
  // we have to calculate the total for the parent and all children.

  // And... if an item is disabled, we count the disk usage, not the disk
  // allocation UNLESS it was disabled by an admin.

  // First run... get allocation for all non-disabled items.
  if (!$hosting_order_id) {
    if (empty($member_id)) {
      // I'm not sure why this happens?
      return 0;
    }
    $member_ids = red_get_related_member_ids($member_id); 

    $member_ids_clause = implode(',', $member_ids);
    // Note: We exclude any user account on a mosh that is a VPS from the
    // allocated total to avoid having it counted twice.
    // And we don't count disabled items that are not admin disabled 
    // (those will be counted below).
    $sql = "
      SELECT COALESCE(SUM(item_quota), 0) 
      FROM 
        red_item JOIN 
        red_hosting_order USING(hosting_order_id) LEFT JOIN
        red_vps ON vps_server = item_host AND service_id = 1 AND vps_status = 'active'
      WHERE 
        hosting_order_status != 'deleted' AND 
        red_hosting_order.member_id IN ($member_ids_clause) AND 
        (
          item_status IN ('active', 'soft-error', 'hard-error', 'pending-update', 'pending-insert', 'pending-restore') OR 
          (item_status = 'disabled' AND hosting_order_disabled_by_admin = 1 AND item_disabled_by_hosting_order = 1)
        ) AND
        vps_id IS NULL
    ";
    $params = [];
  }
  else {
    // Note: when calculating for just a hosting order, we use the
    // hosting order allocation totals, not the vps totals like we do
    // when calculating for the entire membership.
    $sql = "
      SELECT COALESCE(SUM(item_quota), 0)
      FROM
        red_item JOIN
        red_hosting_order USING(hosting_order_id)
      WHERE
        hosting_order_status != 'deleted' AND
        (
          item_status IN ('active', 'soft-error', 'hard-error', 'pending-update', 'pending-insert', 'pending-restore') OR 
          (item_status = 'disabled' AND hosting_order_disabled_by_admin = 1 AND item_disabled_by_hosting_order = 1)
        ) AND
        red_item.hosting_order_id = #hosting_order_id
    ";
    $params = ['#hosting_order_id' => $hosting_order_id];
  }
  $result = red_sql_query($sql, $params);
  $row = red_sql_fetch_row($result);
  $item_quota_active = intval($row[0] ?? 0);

  // Second run... get disk usage for all disabled items.
  if (!$hosting_order_id) {
    $member_ids = red_get_related_member_ids($member_id);

    $member_ids_clause = implode(',', $member_ids);
    // Note: We exclude any user account on a mosh that is a VPS from the
    // allocated total to avoid having it counted twice.
    $sql = "
      SELECT COALESCE(SUM(item_disk_usage), 0)
      FROM
        red_item JOIN
        red_hosting_order USING(hosting_order_id) LEFT JOIN
        red_vps ON vps_server = item_host AND service_id = 1 AND vps_status = 'active'
      WHERE
        hosting_order_status != 'deleted' AND
        red_hosting_order.member_id IN ($member_ids_clause) AND
        item_status = 'disabled' AND
        (hosting_order_disabled_by_admin = 0 OR item_disabled_by_hosting_order = 0) AND
        vps_id IS NULL
    ";
    $params = [];
  }
  else {
    // Note: when calculating for just a hosting order, we use the
    // hosting order allocation totals, not the vps totals like we do
    // when calculating for the entire membership.
    $sql = "
      SELECT COALESCE(SUM(item_disk_usage), 0)
      FROM
        red_item JOIN
        red_hosting_order USING(hosting_order_id)
      WHERE
        hosting_order_status != 'deleted' AND
        item_status = 'disabled' AND
        (hosting_order_disabled_by_admin = 0 OR item_disabled_by_hosting_order = 0) AND
        red_item.hosting_order_id = #hosting_order_id
    ";
    $params = ['#hosting_order_id' => $hosting_order_id];
  }
  $result = red_sql_query($sql, $params);
  $row = red_sql_fetch_row($result);
  $item_quota_disabled = intval($row[0] ?? 0);

  $item_quota = $item_quota_active + $item_quota_disabled;
  if ($hosting_order_id) {
    return $item_quota;
  }

  // Note: allocated totals always include the VPS's, even if the VPS
  // is a mosh.
  $sql = "SELECT SUM(vps_hd) FROM red_vps WHERE member_id IN ($member_ids_clause) AND 
    vps_type = 'virtual' AND vps_status = 'active'";
  $result = red_sql_query($sql);
  $row = red_sql_fetch_row($result);
  if (!$row) {
    $vps_quota = 0;
  }
  else {
    $vps_quota = $row[0];
  }
  return $item_quota + $vps_quota;
}

/**
 * Does this hosting order have user accounts on a vps?
 */
function red_hosting_order_on_vps($hosting_order_id) {
  $sql = "SELECT vps_id FROM red_vps JOIN red_item ON red_vps.vps_server = red_item.item_host AND vps_status = 'active'
    WHERE hosting_order_id = #hosting_order_id AND item_status != 'deleted' AND item_status != 'pending-delete' LIMIT 1";
  $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
  if (red_sql_num_rows($result) > 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Does this member have user accounts on a vps?
 */
function red_member_has_vps($member_id) {
  $member_id = intval($member_id);
  if ($member_id == 0) {
    return FALSE;
  }
  $member_ids = red_get_related_member_ids($member_id);
  $member_ids_clause = implode(',', $member_ids);
  $sql = "
    SELECT COUNT(*) AS count
    FROM
      red_vps
    WHERE
      member_id IN ($member_ids_clause) AND
      vps_status = 'active'
  ";
  $result = red_sql_query($sql);
  $row = red_sql_fetch_row($result);
  if ($row[0] > 0) {
    return TRUE;
  }
  return FALSE;
}

function red_email_domain_is_verified($email) {
  static $verified_domains = [];
  $pieces = explode('@', $email);
  $domain = trim($pieces[1]);
  if (in_array($domain, $verified_domains)) {
    return TRUE;
  }
  $result = red_domain_is_verified($domain);
  if ($result) {
    $verified_domains[] = $domain;
  }
  return $result;
}

// NOTE: should we be checking the actual MX record? By relying on our own
// DNS database we have more control over how we want this to go.
function red_domain_uses_mx_servers($domain) {
  $sql = "SELECT COUNT(*) FROM red_item JOIN red_item_dns USING(item_id)
    WHERE 
      item_status != 'deleted' AND 
      item_status != 'disabled' AND
      dns_fqdn = @domain AND 
      dns_server_name REGEXP '[a-z]\.mx\.mayfirst\.(org|dev)'
  ";
  $result = red_sql_query($sql, ['@domain' => $domain]);
  $row = red_sql_fetch_row($result);
  if ($row[0] == 0) {
    return FALSE;
  }
  return TRUE;
}

function red_domain_is_verified($domain) {
  global $globals;
  $verified_domains = $globals['cache']['verified_domains'] ?? [];
  if (empty($verified_domains)) {
    // Fill the cache.
    $sql = "SELECT dns_fqdn FROM red_item JOIN red_item_dns USING(item_id)
      WHERE item_status != 'deleted' AND item_status != 'disabled' AND
      dns_type = 'MX' AND dns_mx_verified = 1";
    $result = red_sql_query($sql);
    if (!array_key_exists('cache', $globals)) {
      $globals['cache'] = [];
    }
    if (!array_key_exists('verified_domains', $globals['cache'])) {
      $globals['cache']['verified_domains'] = [];
    }
    while ($row = red_sql_fetch_row($result)) {
      $globals['cache']['verified_domains'][] = $row[0];
    }
    $verified_domains = $globals['cache']['verified_domains'];
  }
  return in_array($domain, $verified_domains);
}

function red_get_top_level_domain() {
  // We don't know whether we're in live mode or dev mode. So, we get the top
  // level domain from the server address.
  $domain_parts = explode('.', $_SERVER['SERVER_NAME'] ?? '');
  $tld = array_pop($domain_parts);
  if (empty($tld)) {
    $tld = 'org';
  }
  return $tld;
}

function red_rsync($file, $dest) {
  $cmd = "/usr/bin/rsync";
  $args = [
    $file,
    $dest,
  ];
  // Try three times since ssh connections are inconsitent.
  $count = 1;
  while (1) {
    if (red_fork_exec_wait($cmd, $args) == 0) {
      // Success!
      return TRUE;
    } 
    elseif ($count < 3) {
      // Try again.
      red_log("Failed to rsync $file to $dest, attempt: $count.");
      $count++;
      sleep($count);
    }
    else {
      // Give up.
      red_log("Failed to rsync $file to $server. Gave up.");
      return FALSE;
    }
    $count++;
  }
}

function red_ssh_cmd($args, &$output = [], &$rc = NULL) {
  $cmd = "/usr/bin/ssh";
  $env = [];
  $as_user = NULL;
  $count = 1;
  // In case we need to log an error.
  $report_args = implode(' ', $args);
  while (1) {
    $rc = red_fork_exec_wait($cmd, $args, $env, $as_user, $output);
    if ($rc == 0) {
      // If first line is the generic ssh warning about adding the host
      // key (since we use TOFU), shift it off the stack.
      $first_line = $output[0] ?? NULL;
      if ($first_line && preg_match('/Warning: Permanently added/', $output[0])) {
        array_shift($output);
      }
      // Success!
      return TRUE;
    }

    // If we fail to connect, try again, otherwise give up.
    $first_line_err = $output[0] ?? NULL;
    $try_again_errors = [
      'ssh_exchange_identification: Connection closed by remote host',
      'kex_exchange_identification: Connection closed by remote host',
    ];
    // If something unknown goes wrong, fail immediately.
    if (!in_array($first_line_err, $try_again_errors)) {
      red_log("Failed to: {$cmd} {$report_args}, rc: $rc.");
      return FALSE;
    }
    // Otherwise, try 3 times before we give up.
    elseif ($count < 3) {
      // Try again.
      red_log("Failed to: {$cmd} {$report_args}, rc: $rc, attempt: $count.");
      $count++;
      sleep($count);
    }
    else {
      // Give up.
      red_log("Failed to: {$cmd} {$report_args}, rc: $rc. Gave up.");
      return FALSE;
    }
    $count++;
  }
}

function red_update_network_mysql_users($server) {
  $cmd = "/usr/bin/ssh";
  $args = [
    "red-remote-tasker@{$server}",
    "sudo",
    '/usr/local/sbin/proxysql-load',
  ];
  $count = 1;
  while (1) {
    if (red_fork_exec_wait($cmd, $args) == 0) {
      // Success!
      break;
    } 
    elseif ($count < 3) {
      // Try again.
      red_log("Failed to run proxysql-load to $server, attempt: $count.");
      $count++;
      sleep($count);
    }
    else {
      // Give up.
      throw new RedException("Failed to reload database users. Please edit and re-submit or contact support for help.");
    }
    $count++;
  }
}

function red_yaml_emit($certs) {
  return yaml_emit($certs);
}

/**
 * Deprecated.
 *
 * This is used on mosh email servers. 
 */
function red_write_virtual_alias_domains_red_file($hostname, $destination = '/etc/postfix/virtual_alias_domains.red') {
  global $globals;

  $sql = "SELECT email_address
    FROM red_item JOIN red_item_email_address USING(item_id)
    WHERE
      item_status != 'deleted' AND
      item_status != 'disabled' AND
      item_status != 'pending-delete' AND
      item_status != 'pending-disable' AND
      item_host = @hostname";
  $result = red_sql_query($sql, ['@hostname' => $hostname]);
  if (!$result) {
    red_log("Failed to qeuery for virtual alias domains file: @sql", [ "@sql" => $sql]);
    return FALSE;
  }
  $domains = [];
  while ($row = red_sql_fetch_row($result)) {
    $email_address = $row[0];
    $pieces = explode('@', $email_address);
    $domain = trim($pieces[1]);
    if (!in_array("{$domain}\t-", $domains)) {
      $domains[] = "{$domain}\t-";
    }
  }
  // Always accept email to mail.mayfirst.org
  $domains[] = 'mail.mayfirst.org';
  if (!file_put_contents($destination, implode("\n", $domains) . "\n")) {
    red_log("Failed to write to virtual alias domains file");
    return FALSE;
  }

  return TRUE;
}

/**
 * Deprecated.
 *
 * This is used on mosh email servers. 
 */
function red_write_virtual_alias_maps_red_file($hostname, $destination = '/etc/postfix/virtual_alias_maps.red') {
  $sql = "SELECT email_address, email_address_recipient
      FROM red_item JOIN red_item_email_address USING(item_id)
      WHERE
        item_status != 'deleted' AND
        item_status != 'disabled' AND
        item_status != 'pending-delete' AND
        item_status != 'pending-disable' AND
        item_host = @hostname";
    $result = red_sql_query($sql, ['@hostname' => $hostname]);
    if (!$result) {
      red_log("Failed to query #1 for virtual alias maps red file: @sql.", [ '@sql' => $sql]);
      return FALSE;
    }
    $lines = [];
    while ($row = red_sql_fetch_row($result)) {
      $email_address = $row[0];
      $email_address_recipient = $row[1];
      $lines[] = "{$email_address}\t{$email_address_recipient}";
    }

    // Now build out the map for all the @mail.mayfirst.org addresses, regardless of
    // which server they belong to, so we can accurately forward those emails.
    $sql = "SELECT mailbox_login, item_host FROM red_item_mailbox JOIN red_item USING (item_id)
      WHERE item_status NOT IN ('deleted', 'disabled')";
    $result = red_sql_query($sql);
    if (!$result) {
      red_log("Failed to query #2 for virtual alias maps red file: @sql.", [ '@sql' => $sql]);
      return FALSE;
    }
    while ($row = red_sql_fetch_row($result)) {
      $login = $row[0];
      $host = $row[1];
      $lines[] = "{$login}@mail.mayfirst.org\t{$login}@{$host}";
    }
    if (!file_put_contents($destination, implode("\n", $lines) . "\n")) {
      red_log("Failed to write virtual alias maps red file.");
      return FALSE;
    }
    return TRUE;

}
function red_log($msg, $vars = [], $priority = 'info') {
  $dir = '/var/log/red';
  $path = "{$dir}/red.log";
  if (!is_dir($dir)) {
    mkdir($dir);
  }
  if (is_array($msg)) {
    $msg = implode(',', $msg);
  }
  if (count($vars) > 0) {
    $keys = array_keys($vars);
    $values = array_values($vars);
    $msg = str_replace($keys, $values, $msg);
  }
  $msg = date('Y-m-d H:i:s') . ' ' . $msg . "\n";
  file_put_contents($path, $msg, FILE_APPEND);
}

function red_replace_old_paths($str, $item_id) {
  // We expect paths like:
  // /home/members/mcclelland/sites/workingdirectory.net/users/jamie/bin
  // /home/members/mcclelland/sites/workingdirectory.net/users/jamie
  // /home/members/mcclelland/sites/workingdirectory.net/web
  // /home/members/mcclelland/sites/workingdirectory.net/users/jamie/web
  // /home/members/mcclelland/sites/workingdirectory.net/users/jamie/workingdirectory.net/web
  //
  // Also replace: $HOME 
  //
  // We want:
  // /home/sites/1234/{web,include,cgi-bin,bin}
  $matcher = '[a-zA-Z\-_.0-9@]+';
  // Start with longer/more exact ones first.
  $new = preg_replace("#/home/members/$matcher/sites/$matcher/users/$matcher/bin#", "/home/sites/$item_id/bin", $str);
  $new = preg_replace("#/home/members/$matcher/sites/$matcher/users/$matcher/$matcher/web#", "/home/sites/$item_id/web", $new);
  $new = preg_replace("#/home/members/$matcher/sites/$matcher/users/$matcher/web#", "/home/sites/$item_id/web", $new);
  $new = preg_replace("#/home/members/$matcher/sites/$matcher/web#", "/home/sites/$item_id/web", $new);
  $new = preg_replace("#/home/members/$matcher/sites/$matcher/\.red#", "/home/sites/$item_id/.red", $new);
  $new = preg_replace("#/home/members/$matcher/sites/$matcher/#", "/home/sites/$item_id/", $new);
  $new = preg_replace('/$HOME/', "/home/sites/$item_id", $new);
  return $new;
}

function red_convert_cron_to_timer($value) {
  // Check for * in the schedule to determine if we need to be
  // converted.
  if (strpos($value, '*') !== FALSE) {
    $value = trim($value);
    // We are expecting something like:
    // 6 * * * *
    // */5 * * * *
    // 51 */4 * * *
    $pieces = explode(' ', $value);
    if (count($pieces) != 5) {
      red_log("Did not find 5 pieces: $value");
      return $value;
    }

    $first = array_shift($pieces);
    $second = array_shift($pieces);
    $third = array_shift($pieces);
    $fourth = array_shift($pieces);
    $fifth = array_shift($pieces);
    // Check for per minute or per hour jobs.
    if ($second == '*' && $third == '*' && $fourth == '*' && $fifth == '*') {
      if ($first == '*' || $first == '*/1') {
        // This is easy.
        return 'minutely';
      }
      elseif (is_numeric($first)) {
        return 'hourly';
      }
      elseif (strpos($first, '/')) {
        $per_minute = intval(str_replace('*/', '', $first));
        if ($per_minute) {
          return "*:0/{$per_minute}"; 
        }
      }
      elseif (strpos($first, ',')) {
        // This is probably a list of minutes to run on. Sheesh. You get every 5 minutes.
        return '*:0/5';
      }
      // We failed to figure this one out.
      red_log("Failed to convert $value schedule to new infrastructure value.");
      // Return every 15 minutes.
      return '*:0/15'; 
    }
    // Check for daily jobs
    elseif ($third == '*' && $fourth == '*' && $fifth == '*') {
      if (is_numeric($second)) {
        return 'daily';
      }
      // You might want every 3 hours at 49 minutes after the hour, but
      // you are just going to get hourly. Sorry.
      return 'hourly';
    }
    elseif ($fourth == '*' && $fifth == '*') {
      if (is_numeric($third)) {
        return 'monthly';
      }
      // You might want every 3 days but you are getting daily.
      return 'daily';
    }
    elseif ($fourth != '*')  {
      // Really? Once a year?
      return 'annually';
    }
    // Now we check for day of the week schedules.
    $weekdays = [ 'sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    $day = NULL;
    if (strpos($fifth, ',')) {
      // Twice a week maybe? We're not doing that.
      $week_day_parts = explode(',', $fifth);
      // We'll take the first day of the week.
      $fifth = $week_day_parts[0];
    }
    if (in_array($fifth , $weekdays)) {
      $day = $fifth;
    }
    elseif (is_numeric($fifth) && array_key_exists(intval($fifth), $weekdays)) {
      $day = $weekdays[intval($fifth)];
    }
    else {
      red_log("Failed to convert $value schedule to new infrastructure value.");
      return 'daily';
    }
    return ucfirst($day) . " *-*-* $second:$first:00";
  }
  else if ($value == '@reboot') {
    return 'forever';
  }
  else {
    // If there are not asterisks we assume this is already converted?
    red_log("$value seems already converted.");
    return $value;
  }
}

function red_extract_environment_variables($value) {
  // We are looking for space separate var=value pairs in the front of the command.
  $pieces = explode(' ', $value);
  $env = []; 
  $cmd = [];
  $start = TRUE;
  foreach($pieces as $piece) {
    if ($piece == 'env') {
      continue;
    }
    if ($start && strpos($piece, '=')) {
      $env[] = $piece;
    }
    else {
      $cmd[] = $piece;
      // Once we get a command, we no longer check for env vars
      $start = FALSE;
    }
  }
  return [
    0 => implode(' ', $env),
    1 => implode(' ', $cmd)
  ];
}


// This function is used to transition to the new systemd from cron.
function red_strip_redirects($cmd) {
  // Return early if no matching symbols are present.
  if (!preg_match('/[&><|]/', $cmd)) {
    return $cmd;
  }
  // Special case for job with special character in password
  if (preg_match('#http://www.leftforum.org/sites/all/modules/civicrm/#', $cmd)) {
    return $cmd;
  }
  // Replace ' && ' with a semi-colon.
  $cmd = preg_replace('/ && /', ' ; ', $cmd);
  // We just remove anything that will not validate. Note, an ampersand
  // might legitamtely exist in an url, e.g. https://site.org/foo?bar&boo.
  return trim(preg_replace('/( &|[><|]+)[^;]+/', ' ', $cmd));
}

function red_split_words_with_quotes($str) {
  $regexp = '/(' . "'[^']*'" . ')|("[^"]*")|\h+/';
  return preg_split($regexp, $str, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
}

/**
 * Debian bookworm and higher is running openssl version 3,
 * which changed the defaults. So we now need a custom
 * openssl.cnf file to ensure we can continue connecting
 * to mysql via tls.
 */
function red_openssl_environment_is_properly_set() {
  if (red_needs_openssl_custom_path()) {
    if (!getenv('OPENSSL_CONF')) {
      return FALSE;
    }
  }
  return TRUE;
}

function red_needs_openssl_custom_path() {
  if (red_get_debian_major_version() > 11) {
    return TRUE;
  }
  return FALSE;
}

function red_get_debian_major_version() {
  // Versions:
  // 12: bookworm
  // 11: bullseye
  // 10: buster
  $version = file_get_contents('/etc/debian_version');
  $version_parts = explode(".", $version);
  return intval($version_parts[0]);
}

/*
 * For a given member_id, return a list of server names that this membership
 * is hosted on.
 */
function red_get_servers_for_membership($member_id) {

  // Limit to service ids:
  // 1 - user account (should be phased out when we move to new infrastructure)
  // 7 - web sites
  // 38 - mailboxes
  $sql = "SELECT DISTINCT(item_host) FROM red_item JOIN red_hosting_order USING (hosting_order_id) 
    WHERE red_hosting_order.member_id = #member_id AND item_status IN ('active', 'disabled')
    AND item_host != '' AND item_host IS NOT NULL AND service_id IN (1, 7, 38)";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $hosts = [];
  while ($row = red_sql_fetch_row($result)) {
    $hosts[] = $row[0];
  }
  return $hosts;
}

/**
 * Notify a remote host of pending tasks
 */
function red_notify_host($user, $host, $cmd) {
  $cmd = escapeshellcmd($cmd);
  $login = escapeshellarg("$user@$host");
  $ret = exec("HOME=/var/www $cmd -T $login 2>&1", $output, $ret_code);
  if ($ret_code != 0)  {  
    $error_message = implode(' ', $output);
    if (!$error_message) {
      $error_message = $ret;
    }
    throw new RedException($error_message);
  }
  return TRUE;
}

function red_send_mx_verification_after_delay($item_id) {
  // It takes up to 1 minute for an MX record to be put in
  // place. If we send the MX verification immediately, it will
  // not only get bounced for lack of an MX record, but it will
  // also poison the cache with a negative lookup. This function
  // execs a process that has a built in sleep to ensure we delay
  // sending the verification while still letting the rest of the
  // queue processing continue.
  global $globals;
  // Shave off two directories to get to the "ui" directory, then add sbin.
  $cmd =  dirname(dirname($globals['config']['src_path'])) . "/sbin/red-send-delayed-mx-verification";
  $cmd = escapeshellcmd($cmd);
  $arg = escapeshellarg($item_id);
  exec("{$cmd} {$arg} 2>&1 &");
  return TRUE;
}

function red_send_mx_verification($item_id) {
  global $globals;
  $sql_resource = $globals['sql_resource'];
  $mx_verify = new red_mx_verify($sql_resource);
  $mx_verify->set_item_id($item_id);
  if (!$mx_verify->send()) {
    $err = "Failed to send verification email." .
      $mx_verify->get_errors();
    throw new RedException($err);
    return FALSE;
  }
  return TRUE;

}

function red_get_error_items() {
    $ret = [];
    $sql = "
      SELECT
        red_item.item_id, item_status, service_id, hosting_order_id, red_hosting_order.member_id, item_host, hosting_order_status, member_status
      FROM
        red_item JOIN red_hosting_order USING (hosting_order_id)
        JOIN red_member ON red_member.member_id = red_hosting_order.member_id 
      WHERE
        item_status IN ('hard-error', 'soft-error')
        OR
        (item_status LIKE 'pending-%' AND item_modified < DATE_SUB(NOW(), INTERVAL 15 MINUTE))
      GROUP BY
        red_item.item_id";

    $result = red_sql_query($sql);
    $item = [];
    while ($row = red_sql_fetch_row($result)) {
      $item['item_id'] = $row[0];
      $item['item_status'] = $row[1];
      $item['service_id'] = $row[2];
      $item['hosting_order_id'] = $hosting_order_id = $row[3];
      $item['member_id'] = $member_id = $row[4];
      $item['item_host'] = $row[5];
      $hosting_order_status = $row[6];
      $member_status = $row[7];
      if ($hosting_order_status == 'deleted') {
        // This means a record went into an error status while deleting the hosting order. We have to
        // put the hosting order back so we can fix it.
        $sql = "UPDATE red_hosting_order SET hosting_order_status = 'disabled' WHERE hosting_order_id = #hosting_order_id";
        red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      }
      if ($member_status != 'active') {
        // This means a record went into an error status while deleting the member. We have to
        // put the member back so we can fix it.
        $sql = "UPDATE red_member SET member_status = 'active' WHERE member_id = #member_id";
        red_sql_query($sql, ['#member_id' => $member_id]);
      }
      $ret[] = $item;
    }
    return $ret;
  }

function red_get_member_dns_status($member_id) {
  // Get an array of all IP addresses assigned to hosts.
  $sql = "SELECT dns_ip FROM red_item JOIN red_item_dns USING(item_id) WHERE item_status = 'active' AND dns_type = 'ptr'";
  $result = red_sql_query($sql);
  $our_ips = [];
  while ($row = red_sql_fetch_row($result)) {
    $our_ips[] = $row[0]; 
  }
  $member_id = $member_id;
  // Collect all dns zones for this membership
  $sql = "SELECT DISTINCT dns_zone FROM red_item JOIN red_item_dns USING(item_id) WHERE item_status = 'active' AND member_id = #member_id AND dns_zone NOT REGEXP 'mayfirst\.(org|info|cx)' UNION DISTINCT SELECT DISTINCT dns_zone FROM red_hosting_order JOIN red_item USING(hosting_order_id) JOIN red_item_dns USING(item_id) WHERE item_status = 'active' AND red_hosting_order.member_id = #member_id AND dns_zone NOT REGEXP 'mayfirst\.(org|info|cx)'";
  $result = red_sql_query($sql, ['#member_id' => $member_id]);
  $records = 0;
  while($row = red_sql_fetch_row($result)) {
    $records++;
    $domain = $row[0];

    // First try MX record.
    $mxhosts = [];
    if (getmxrr($domain, $mxhosts)) {
      foreach($mxhosts as $host) {
        if (preg_match('/\.mayfirst\.org$/', $host)) {
          return 'still with us';
        }
      }
    }
    // Now try A records.
    if ($records = @dns_get_record($domain, DNS_A)) {
      foreach($records as $host_values) {
        $ip = $host_values['ip'];
        if (in_array($ip, $our_ips)) {
          return 'still with us';
        }
      }
    }
    // Lastly try NS.
    if ($records = @dns_get_record($domain, DNS_NS)) {
      foreach($records as $host_values) {
        $host = $host_values['host'];
        if (preg_match('/\.mayfirst\.org$/', $host)) {
          return 'still with us';
        }
        if (preg_match('/\.cloudflare\.com$/', $host)) {
          return "can't tell (proxy)";
        }
        if (preg_match('/^adns(1|2).easydns\.com$/', $host)) {
          return "can't tell (proxy)";
        }
      }
    }
  }
  if ($records == 0) {
    return "can't tell (no domains)";
  }
  return 'probably gone';
}

function red_populate_disk_usage_snapshot() {
  // Limit to items with a quota set to avoid tracking items in which we don't
  // keep track of disk usage.
  $sql = "
    SELECT i.item_id, i.item_disk_usage
    FROM red_item i LEFT JOIN red_item_disk_usage_snapshot s USING(item_id)
    WHERE
      i.item_status != 'deleted' AND
      i.item_quota != 0 AND
      (s.item_disk_usage IS NULL OR i.item_disk_usage != s.item_disk_usage)
    ";
  $result = red_sql_query($sql);
  while ($row = red_sql_fetch_row($result)) {
    $item_id = $row[0];
    $item_disk_usage = $row[1];
    $sql = "REPLACE INTO red_item_disk_usage_snapshot (item_id, item_disk_usage) VALUES
      (#item_id, #item_disk_usage)";
    red_sql_query($sql, ['#item_id' => $item_id, '#item_disk_usage' => $item_disk_usage]);
  }
}

function red_initialize_lock_file() {
  global $globals;
  $lock_file = $globals['config']['lock_file'];
  if (file_exists($lock_file))   {
    $msg = "Lock file exists!";
    red_log($msg);
    echo "{$msg}\n";
    exit(2);
  }

  // Use a lock directory because mkdir fails if the directory exists. 
  if(!mkdir($lock_file)) {
    // This may happen during a race condition if the directory was created
    // between the check above and the creation here.
    echo "Failed to create the lock file: $lock_file\n";
    exit(1);
  }
}

function red_remove_lock_file() {
  global $globals;
  $lock_file = $globals['config']['lock_file'];
  if (file_exists($lock_file))   {
    rmdir($lock_file);
  }
}

function red_node_update() {
  // Check for any requests
  global $globals;

  // This query should only return one record at a time.
  $sql = "
    SELECT 
      red_item.item_id
    FROM 
      red_item 
        JOIN red_queue USING (item_id) 
        JOIN red_queue_task USING (queue_id) 
    WHERE 
      item_host = @node
      AND item_status IN 
        ( 
          'pending-delete', 
          'pending-disable', 
          'pending-update', 
          'pending-insert',
          'pending-restore'
        ) 
      AND queue_task_function = 'red_notify_host'
      AND queue_task_status = 'running'
    ";

  if (!$result = red_sql_query($sql, ['@node' => $globals['config']['node']])) {
    $msg = "Failed query to pull in task to execute.";
    red_log($msg);
    return FALSE;
  }

  while($row = red_sql_fetch_row($result)) {
    if (is_array($row)) {
      $item_id = $row[0];
      $co = ['id' => $item_id, 'mode' => 'node'];
      $item = red_item::get_red_object($co);

      if (!isset($item) || empty($item))  {
        $msg = 'Failed to create red_item object!';
        red_log($msg);
        return FALSE;
      }

      // Janky hack to force faster dns/knot updates
      $quick = getenv('RED_QUICK');
      if ($quick == 1 && $row['service_id'] == 9) {
        $item->quick = TRUE;
      }
      $item->execute();
    }
  }
  return TRUE;
}

function red_file_in_sync($key, $path) {
  if (!$path || !file_exists($path)) {
    throw new RedException("Can't check if file is in sync without a path that exists: {$path}.");
  }
  $new_hash = md5_file($path);
  $sql = "SELECT file_sync_hash FROM red_file_sync WHERE file_sync_key = @key";
  $result = red_sql_query($sql, ['@key' => $key]);
  $row = red_sql_fetch_row($result);
  $saved_hash = $row[0] ?? '';
  if ($new_hash == $saved_hash) {
    return TRUE;
  }
  return FALSE;
}

function red_update_file_sync($key, $path) {
  if (!$path || !file_exists($path)) {
    throw new RedException("Can't update file sync without a path that exists: {$path}.");
  }
  $sql = "REPLACE INTO red_file_sync SET file_sync_key = @key, file_sync_hash = @hash";
  red_sql_query($sql, ['@key' => $key, '@hash' => md5_file($path)]);
}

function red_purge_file_sync($key) {
  $sql = "DELETE FROM red_file_sync WHERE file_sync_key = @key";
  red_sql_query($sql, ['@key' => $key]);
}

// Derived from: https://www.postgresql.org/docs/current/sql-keywords-appendix.html
function red_get_sql_reserved_words() {
  $stock = ["a","abort","abs","absent", "absolute", "access", "according",
    "acos", "action", "ada", "add", "admin", "after", "aggregate", "all",
    "allocate", "also", "alter", "always", "analyse", "analyze", "and", "any",
    "any_value", "are", "array", "array_agg", "array_max_cardinality", "as",
    "asc", "asensitive", "asin", "assertion", "assignment", "asymmetric", "at",
    "atan", "atomic", "attach", "attribute", "attributes", "authorization",
    "avg", "backward", "base64", "before", "begin", "begin_frame",
    "begin_partition", "bernoulli", "between", "bigint", "binary", "bit",
    "bit_length", "blob", "blocked", "bom", "boolean", "both", "breadth",
    "btrim", "by", "c", "cache", "call", "called", "cardinality", "cascade",
    "cascaded", "case", "cast", "catalog", "catalog_name", "ceil", "ceiling",
    "chain", "chaining", "char", "character", "characteristics", "characters",
    "character_length", "character_set_catalog", "character_set_name",
    "character_set_schema", "char_length", "check", "checkpoint", "class",
    "classifier", "class_origin", "clob", "close", "cluster", "coalesce",
    "cobol", "collate", "collation", "collation_catalog", "collation_name",
    "collation_schema", "collect", "column", "columns", "column_name",
    "command_function", "command_function_code", "comment", "comments",
    "commit", "committed", "compression", "concurrently", "condition",
    "conditional", "condition_number", "configuration", "conflict", "connect",
    "connection", "connection_name", "constraint", "constraints",
    "constraint_catalog", "constraint_name", "constraint_schema",
    "constructor", "contains", "content", "continue", "control", "conversion",
    "convert", "copartition", "copy", "corr", "corresponding", "cos", "cosh",
    "cost", "count", "covar_pop", "covar_samp", "create", "cross", "csv",
    "cube", "cume_dist", "current", "current_catalog", "current_date",
    "current_default_transform_group", "current_path", "current_role",
    "current_row", "current_schema", "current_time", "current_timestamp",
    "current_transform_group_for_type", "current_user", "cursor",
    "cursor_name", "cycle", "data", "database", "datalink", "date",
    "datetime_interval_code", "datetime_interval_precision", "day", "db",
    "deallocate", "dec", "decfloat", "decimal", "declare", "default",
    "defaults", "deferrable", "deferred", "define", "defined", "definer",
    "degree", "delete", "delimiter", "delimiters", "dense_rank", "depends",
    "depth", "deref", "derived", "desc", "describe", "descriptor", "detach",
    "deterministic", "diagnostics", "dictionary", "disable", "discard",
    "disconnect", "dispatch", "distinct", "dlnewcopy", "dlpreviouscopy",
    "dlurlcomplete", "dlurlcompleteonly", "dlurlcompletewrite", "dlurlpath"
    ,"dlurlpathonly", "dlurlpathwrite", "dlurlscheme", "dlurlserver",
    "dlvalue", "do" ,"document", "domain", "double", "drop", "dynamic"
    ,"dynamic_function", "dynamic_function_code", "each", "element", "else"
    ,"empty", "enable", "encoding", "encrypted", "end" ,"end-exec",
    "end_frame", "end_partition", "enforced", "enum" ,"equals", "error",
    "escape", "event", "every" ,"except", "exception", "exclude", "excluding",
    "exclusive", "exec" ,"execute", "exists", "exp", "explain", "expression"
    ,"extension", "external", "extract", "false", "family" ,"fetch", "file",
    "filter", "final", "finalize" ,"finish", "first", "first_value", "flag",
    "float" ,"floor", "following", "for", "force", "foreign" ,"format",
    "fortran", "forward", "found", "frame_row" ,"free", "freeze", "from", "fs",
    "fulfill" ,"full", "function", "functions", "fusion", "g", "general",
    "generated", "get" ,"global", "go", "goto", "grant", "granted", "greatest",
    "group", "grouping", "groups" ,"handler", "having", "header", "hex",
    "hierarchy", "hold", "hour", "id", "identity" ,"if", "ignore", "ilike",
    "immediate", "immediately", "immutable", "implementation", "implicit",
    "import", "in", "include" ,"including", "increment", "indent", "index",
    "indexes", "indicator", "inherit", "inherits", "initial" ,"initially",
    "inline", "inner", "inout", "input", "insensitive", "insert", "instance"
    ,"instantiable", "instead", "int", "integer", "integrity", "intersect"
    ,"intersection", "interval", "into", "invoker", "is", "isnull"
    ,"isolation", "join", "json", "json_array", "json_arrayagg", "json_exists",
    "json_object", "json_objectagg", "json_query", "json_scalar",
    "json_serialize", "json_table", "json_table_primitive", "json_value", "k",
    "keep", "key", "keys", "key_member", "key_type", "label", "lag",
    "language", "large", "last", "last_value", "lateral", "lead", "leading"
    ,"leakproof", "least", "left", "length", "level", "library", "like",
    "like_regex", "limit", "link" ,"listagg", "listen", "ln", "load", "local",
    "localtime", "localtimestamp", "location", "locator", "lock", "locked",
    "log", "log10", "logged", "lower", "lpad" ,"ltrim", "m", "map", "mapping",
    "match" ,"matched", "matches", "match_number", "match_recognize",
    "materialized", "max", "maxvalue", "measures" ,"member", "merge",
    "message_length", "message_octet_length", "message_text", "method", "min",
    "minute", "minvalue", "mod", "mode", "modifies", "module", "month", "more",
    "move", "multiset", "mumps", "name", "names", "namespace", "national",
    "natural", "nchar", "nclob", "nested", "nesting", "new", "next", "nfc",
    "nfd", "nfkc", "nfkd", "nil", "no", "none", "normalize", "normalized",
    "not", "nothing", "notify", "notnull", "nowait", "nth_value", "ntile",
    "null", "nullable", "nullif", "nulls" ,"null_ordering", "number",
    "numeric", "object", "occurrence", "occurrences_regex", "octets",
    "octet_length", "of", "off", "offset", "oids" ,"old", "omit", "on", "one",
    "only", "open", "operator", "option", "options", "or", "order", "ordering",
    "ordinality", "others", "out", "outer", "output", "over", "overflow",
    "overlaps", "overlay", "overriding", "owned", "owner", "p", "pad",
    "parallel", "parameter", "parameter_mode", "parameter_name",
    "parameter_ordinal_position", "parameter_specific_catalog",
    "parameter_specific_name", "parameter_specific_schema", "parser",
    "partial", "partition", "pascal", "pass", "passing", "passthrough",
    "password", "past", "path", "pattern", "per", "percent", "percentile_cont",
    "percentile_disc", "percent_rank", "period", "permission", "permute",
    "pipe", "placing", "plan", "plans", "pli", "policy", "portion", "position",
    "position_regex", "power", "precedes", "preceding", "precision", "prepare",
    "prepared" ,"preserve", "prev", "primary", "prior", "private",
    "privileges", "procedural", "procedure", "procedures", "program", "prune",
    "ptf", "public", "publication", "quote", "quotes", "range", "rank", "read",
    "reads", "real", "reassign", "recheck", "recovery", "recursive", "ref",
    "references", "referencing", "refresh", "regr_avgx", "regr_avgy",
    "regr_count", "regr_intercept", "regr_r2", "regr_slope", "regr_sxx",
    "regr_sxy", "regr_syy", "reindex", "relative", "release", "rename",
    "repeatable", "replace", "replica", "requiring", "reset", "respect",
    "restart", "restore", "restrict", "result", "return",
    "returned_cardinality", "returned_length", "returned_octet_length",
    "returned_sqlstate", "returning", "returns", "revoke", "right", "role",
    "rollback", "rollup", "routine", "routines", "routine_catalog",
    "routine_name", "routine_schema", "row", "rows", "row_count", "row_number",
    "rpad", "rtrim", "rule", "running", "savepoint", "scalar", "scale",
    "schema", "schemas", "schema_name", "scope", "scope_catalog", "scope_name",
    "scope_schema", "scroll", "search" ,"second", "section", "security",
    "seek", "select", "selective", "self", "semantics", "sensitive",
    "sequence", "sequences", "serializable", "server", "server_name",
    "session", "session_user", "set", "setof", "sets", "share", "show",
    "similar", "simple", "sin", "sinh", "size", "skip", "smallint", "snapshot",
    "some", "sort_direction", "source", "space", "specific", "specifictype",
    "specific_name", "sql", "sqlcode", "sqlerror", "sqlexception", "sqlstate",
    "sqlwarning", "sqrt", "stable", "standalone", "start", "state",
    "statement", "static", "statistics", "stddev_pop", "stddev_samp", "stdin",
    "stdout", "storage", "stored", "strict", "string", "strip", "structure",
    "style", "subclass_origin", "submultiset", "subscription", "subset",
    "substring", "substring_regex", "succeeds", "sum", "support", "symmetric",
    "sysid", "system", "system_time", "system_user", "t", "table", "tables",
    "tablesample", "tablespace", "table_name", "tan", "tanh", "temp",
    "template", "temporary", "text", "then", "through", "ties", "time",
    "timestamp", "timezone_hour", "timezone_minute", "to", "token",
    "top_level_count", "trailing", "transaction", "transactions_committed",
    "transactions_rolled_back", "transaction_active", "transform",
    "transforms", "translate", "translate_regex", "translation", "treat",
    "trigger", "trigger_catalog", "trigger_name", "trigger_schema", "trim",
    "trim_array", "true", "truncate", "trusted", "type", "types", "uescape",
    "unbounded", "uncommitted", "unconditional", "under", "unencrypted",
    "union", "unique", "unknown", "unlink", "unlisten", "unlogged",
    "unmatched", "unnamed", "unnest", "until", "untyped", "update", "upper",
    "uri", "usage", "user", "user_defined_type_catalog",
    "user_defined_type_code", "user_defined_type_name",
    "user_defined_type_schema", "using", "utf16", "utf32", "utf8", "vacuum",
    "valid", "validate", "validator", "value", "values", "value_of",
    "varbinary", "varchar", "variadic", "varying", "var_pop", "var_samp",
    "verbose", "version", "versioning", "view", "views", "volatile", "when",
    "whenever", "where", "whitespace", "width_bucket", "window", "with",
    "within", "without", "work", "wrapper", "write", "xml", "xmlagg",
    "xmlattributes", "xmlbinary", "xmlcast", "xmlcomment", "xmlconcat",
    "xmldeclaration", "xmldocument", "xmlelement", "xmlexists", "xmlforest",
    "xmliterate", "xmlnamespaces", "xmlparse", "xmlpi", "xmlquery", "xmlroot",
    "xmlschema", "xmlserialize", "xmltable", "xmltext", "xmlvalidate", "year",
    "yes", "zone"]; 
  
  // And let's add some more:
  $stock[] = "root";
  $stock[] = "postgres";
  return $stock;
}


?>
